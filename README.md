# ga2w20 - Qual-e-tree

This is Qual-e-tree: an online electronic book store. Users can register, 
login, and shop for a book in various genres. The website will recommend a genre 
based on previously viewed books.

With a manager account, you can access the management side of the site which 
offers a user interface that allows you to interact with the database and 
manager customer orders, book reviews, book inventory, client accounts, manage 
surveys and ads displayed on the front page, and view various sales reports.

This site is hosted on the Dawson College Waldo server at:

[http://waldo.dawsoncollege.qc.ca:8080/ga2w20/](http://waldo.dawsoncollege.qc.ca:8080/ga2w20/)

The frontend of the website is designed using the PrimeFaces Java Server Faces 
framework. The backend of the website is a web application written in Java 
using the Java Server Faces framework. Data is stored on a MySQL database that is
hosted on the Dawson College waldo2 server and accessed using the Java 
Persistence API. The application runs on a Payara application server.

This is our final project for the Computer Science Java Server Side course (420-625-DW).

# Developers:

* Liam Harbec
* Cadin Londono
* Svitlana Myronova
* Thomas Ouellette

# Known bugs/defects:


*  Functionality related to adding to cart, checking out, and RSS news feed missing due to missing team member.
*  JSESSIONID cookie: on Google Chrome, sometimes it configures the site-specific settings to only send the cookie on a secure connection, which prevents the application from authenticating the user and causes strange behavior such as the cookie's value being appended to the url (solution: enter site settings, delete all cookies).
*  Ad blocking browser extensions will sometimes prevent entire pages on the management side from being rendered and can cause strange behavior on the client side (removing all images, for instance).


*Updated April 15, 2020*