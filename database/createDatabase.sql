-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';
SET character_set_client = UTF8MB4;

-- -----------------------------------------------------
-- Schema ga2w20
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ga2w20
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ga2w20` ;
USE `ga2w20` ;

-- -----------------------------------------------------
-- Table `ga2w20`.`advertisement`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`advertisement` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`advertisement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `img_url` VARCHAR(500) NOT NULL,
  `site_url` VARCHAR(500) NOT NULL,
  `startDate` DATETIME NOT NULL,
  `endDate` DATETIME NOT NULL,
  `adsActive` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`news`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`news` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`news` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `img_url` VARCHAR(500) NOT NULL,
  `site_url` VARCHAR(500) NOT NULL,
  `startDate` DATETIME NOT NULL,
  `endDate` DATETIME NOT NULL,
  `newsActive` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`author`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`author` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`author` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `authorName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`format` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`format` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `formatTitle` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`genre`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`genre` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`genre` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `genreTitle` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`publisher`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`publisher` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`publisher` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `publisherName` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`book` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`book` (
  `ISBN` BIGINT NOT NULL,
  `title` VARCHAR(300) NOT NULL,
  `publisherId` INT NOT NULL,
  `publicationDate` DATE NOT NULL,
  `numberOfPages` INT NOT NULL,
  `genreId` INT NOT NULL,
  `description` TEXT NOT NULL,
  `imageFile` VARCHAR(100) NOT NULL,
  `wholeSalePrice` DECIMAL(10,2) NOT NULL,
  `listPrice` DECIMAL(10,2) NOT NULL,
  `enteredDate` DATE NOT NULL,
  `removalStatus` TINYINT(1) NOT NULL,
  PRIMARY KEY (`ISBN`),
  CONSTRAINT `genreId_fk_b`
    FOREIGN KEY (`genreId`)
    REFERENCES `ga2w20`.`genre` (`id`),
  CONSTRAINT `publisherId_fk_b`
    FOREIGN KEY (`publisherId`)
    REFERENCES `ga2w20`.`publisher` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `publisherId_fk_b_idx` ON `ga2w20`.`book` (`publisherId` ASC) VISIBLE;

CREATE INDEX `genreId_fk_b_idx` ON `ga2w20`.`book` (`genreId` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`book_author`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`book_author` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`book_author` (
  `ISBN` BIGINT NOT NULL,
  `authorId` INT NOT NULL,
  PRIMARY KEY (`ISBN`, `authorId`),
  CONSTRAINT `authorId_fk_ba`
    FOREIGN KEY (`authorId`)
    REFERENCES `ga2w20`.`author` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ISBN_fk_ba`
    FOREIGN KEY (`ISBN`)
    REFERENCES `ga2w20`.`book` (`ISBN`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `ISBN_fk_ba_idx` ON `ga2w20`.`book_author` (`ISBN` ASC) VISIBLE;

CREATE INDEX `authorId_fk_ba_idx` ON `ga2w20`.`book_author` (`authorId` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`book_format`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`book_format` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`book_format` (
  `ISBN` BIGINT NOT NULL,
  `formatId` INT NOT NULL,
  PRIMARY KEY (`ISBN`, `formatId`),
  CONSTRAINT `formatId_fk_bf`
    FOREIGN KEY (`formatId`)
    REFERENCES `ga2w20`.`format` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ISBN_fk_bf`
    FOREIGN KEY (`ISBN`)
    REFERENCES `ga2w20`.`book` (`ISBN`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `ISBN_fk_bf_idx` ON `ga2w20`.`book_format` (`ISBN` ASC) VISIBLE;

CREATE INDEX `formatId_fk_bf_idx` ON `ga2w20`.`book_format` (`formatId` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`province`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`province` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`province` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `provinceName` VARCHAR(45) NOT NULL,
  `GSTpercentage` DECIMAL(6,3) NOT NULL,
  `PSTpercentage` DECIMAL(6,3) NOT NULL,
  `HSTpercentage` DECIMAL(6,3) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`client` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NULL DEFAULT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `address1` VARCHAR(500) NOT NULL,
  `address2` VARCHAR(500) NULL DEFAULT NULL,
  `city` VARCHAR(100) NOT NULL,
  `provinceId` INT NOT NULL,
  `country` VARCHAR(100) NOT NULL,
  `postalCode` VARCHAR(6) NOT NULL,
  `homeTelephone` VARCHAR(45) NULL DEFAULT NULL,
  `companyName` VARCHAR(100) NULL DEFAULT NULL,
  `cellTelephone` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(320) NOT NULL,
  `password` VARCHAR(320) NOT NULL,
  `salt` VARCHAR(320) NOT NULL,
  `manager` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `provinceId_fk_c`
    FOREIGN KEY (`provinceId`)
    REFERENCES `ga2w20`.`province` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE UNIQUE INDEX `email_UNIQUE` ON `ga2w20`.`client` (`email` ASC) VISIBLE;

CREATE INDEX `provinceId_fk_c_idx` ON `ga2w20`.`client` (`provinceId` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`order`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`order` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`order` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clientId` INT NOT NULL,
  `orderDate` DATETIME NOT NULL,
  `totalPrice` DECIMAL(10,2) NOT NULL,
  `totalPST` DECIMAL(10,2) NOT NULL,
  `totalGST` DECIMAL(10,2) NOT NULL,
  `totalHST` DECIMAL(10,2) NOT NULL,
  `removalStatus` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `clientId_fk_o`
    FOREIGN KEY (`clientId`)
    REFERENCES `ga2w20`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `clientId_fk_o_idx` ON `ga2w20`.`order` (`clientId` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`order_book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`order_book` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`order_book` (
  `orderId` INT NOT NULL,
  `ISBN` BIGINT NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `PSTpercentage` DECIMAL(6,3) NOT NULL,
  `GSTpercentage` DECIMAL(6,3) NOT NULL,
  `HSTpercentage` DECIMAL(6,3) NOT NULL,
  `removalStatus` TINYINT(1) NOT NULL,
  PRIMARY KEY (`orderId`, `ISBN`),
  CONSTRAINT `ISBN_fk_ob`
    FOREIGN KEY (`ISBN`)
    REFERENCES `ga2w20`.`book` (`ISBN`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `orderId_fk_ob`
    FOREIGN KEY (`orderId`)
    REFERENCES `ga2w20`.`order` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `orderId_fk_ob_idx` ON `ga2w20`.`order_book` (`orderId` ASC) VISIBLE;

CREATE INDEX `ISBN_fk_ob_idx` ON `ga2w20`.`order_book` (`ISBN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`review`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`review` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`review` (
  `clientId` INT NOT NULL,
  `ISBN` BIGINT NOT NULL,
  `rating` INT NOT NULL,
  `approved` TINYINT(1) NOT NULL,
  `reviewDate` DATETIME NOT NULL,
  `reviewText` TEXT NOT NULL,
  PRIMARY KEY (`clientId`, `ISBN`),
  CONSTRAINT `clientId_fk_r`
    FOREIGN KEY (`clientId`)
    REFERENCES `ga2w20`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ISBN_fk_r`
    FOREIGN KEY (`ISBN`)
    REFERENCES `ga2w20`.`book` (`ISBN`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `clientId_fk_r_idx` ON `ga2w20`.`review` (`clientId` ASC) VISIBLE;

CREATE INDEX `ISBN_fk_r_idx` ON `ga2w20`.`review` (`ISBN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`special_book`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`special_book` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`special_book` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ISBN` BIGINT NOT NULL,
  `startDate` DATE NOT NULL,
  `endDate` DATE NOT NULL,
  `percentage` DECIMAL(6,3) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `ISBN_fk_sb`
    FOREIGN KEY (`ISBN`)
    REFERENCES `ga2w20`.`book` (`ISBN`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `ISBN_fk_sb_idx` ON `ga2w20`.`special_book` (`ISBN` ASC) VISIBLE;


-- -----------------------------------------------------
-- Table `ga2w20`.`survey`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`survey` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`survey` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `surveyText` TEXT NOT NULL,
  `answer1` VARCHAR(100) NOT NULL,
  `answer2` VARCHAR(100) NOT NULL,
  `answer3` VARCHAR(100) NOT NULL,
  `answer4` VARCHAR(100) NOT NULL,
  `surveyActive` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `ga2w20`.`survey_client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ga2w20`.`survey_client` ;

CREATE TABLE IF NOT EXISTS `ga2w20`.`survey_client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `surveyId` INT NOT NULL,
  `clientId` INT NULL,
  `answer` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `clientId_fk_sc`
    FOREIGN KEY (`clientId`)
    REFERENCES `ga2w20`.`client` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `surveyId_fk_sc`
    FOREIGN KEY (`surveyId`)
    REFERENCES `ga2w20`.`survey` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE INDEX `surveyId_fk_sc_idx` ON `ga2w20`.`survey_client` (`surveyId` ASC) VISIBLE;

CREATE INDEX `clientId_fk_sc_idx` ON `ga2w20`.`survey_client` (`clientId` ASC) INVISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `ga2w20`.`advertisement`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (1, 'https://i.ytimg.com/vi/QlNQryi20n4/mqdefault.jpg', 'https://www.honda.ca/?ds_rl=1006182&ds_rl=1235681&gclid=CjwKCAiA4Y7yBRB8EiwADV1haWbxp9coBzGZL2FIqDc9E143-pTAWWVESYnhxJSmi1TjqooKSCrKmBoCVKsQAvD_BwE&gclsrc=aw.ds', '2020-02-01', '2022-02-28', 0);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (2, 'https://cdn.devaltitude.com/altitude/public/AltiBlocCollectionRaquette1200x792-1581452221760.jpg', 'https://www.altitude-sports.com/collections/snowshoeing-apparel-gear', '2020-02-01', '2022-02-28', 0);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (3, 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRKn4bl68soDQB594LKL8FtlwToo6Qd817JrEgcnL0U7pJObfvX', 'https://www.apple.com/ca/iphone-11/?afid=p238%7CskngTQQJq-dc_mtid_20925xpb40345_pcrid_410531662934_pgrid_77457582625_&cid=wwa-ca-kwgo-iphone-slid-', '2020-03-01', '2022-03-31', 1);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (4, 'https://www.lenovo.com/medias/lenovo-laptop-thinkpad-x1-carbon-7th-gen-hero.png?context=bWFzdGVyfHJvb3R8NjczMDB8aW1hZ2UvcG5nfGg4My9oMjIvMTA1MDA1OTY2Mjk1MzQucG5nfDE2ZGViMmU4ZTk5ZDg5NTBkMGQxZjBmZmQwZWYwNDIyOTUxNTFhMDk4NzJiZDhiNmY1NGY2ZDllYTgyYzA5YTI', 'https://www.lenovo.com/us/en/laptops/thinkpad/thinkpad-x/X1-Carbon-Gen-7/p/22TP2TXX17G', '2020-03-01', '2022-03-31', 1);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (5, 'https://www.audi.ca/dam/nemo/ca/Models/s7/compressed/1920x1080/1920x1080_black_2_A193848_large-min.jpg?output-format=webp&downsize=1920px', 'https://www.audi.ca/ca/web/en/models/a7/s7-sportback.html', '2020-04-01', '2022-04-30', 0);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (6, 'https://carsclues.com/wp-content/uploads/2018/06/2019-Ford-Edge.jpg', 'https://www.ford.ca/suvs-crossovers/edge/', '2020-04-01', '2022-04-30', 0);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (7, 'https://lh3.googleusercontent.com/proxy/fPeNrafsLsnWaEuCFiHdyUI5o_Ei5Qt473dGWV74ogZCwu15p4omkCUQVEiLhJRCk-NISA2agv_i6ZpuJtQL47xmY2axcJT_yRUOuXp5z3TSRjU3wa4', 'https://www.atmosphere.ca/categories/gear-by-activity/camp-hike/tents.html?lastVisibleProductNumber=20', '2020-05-01', '2022-05-31', 0);
INSERT INTO `ga2w20`.`advertisement` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `adsActive`) VALUES (8, 'https://columbia.scene7.com/is/image/ColumbiaSportswear2/1890021_556_f?wid=767&hei=767&fmt=jpeg&qlt=80,1&op_sharpen=0&resMode=sharp2&op_usm=1,1,6,0&iccEmbed=0', 'https://www.columbiasportswear.ca/en/zigzag-22l-backpack-1890021.html?dwvar_1890021_variationColor=556&cgid=activity-equipment#icpa=activitylp&icid=catbanner&icsa=s18&prid=equipment&crid=shop_equipment&start=2', '2020-05-01', '2022-05-31', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`news`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`news` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `newsActive`) VALUES (1, 'https://i.cbc.ca/1.5461885.1581546657!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/canadiens-flyers-hockey.jpg', 'https://www.cbc.ca/sports/hockey/nhl/montreal-canadiens-shea-weber-injured-reserve-1.5461867', '2020-02-01', '2022-02-28', 1);
INSERT INTO `ga2w20`.`news` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `newsActive`) VALUES (2, 'https://i.cbc.ca/1.5458718.1581368996!/fileImage/httpImage/image.jpg_gen/derivatives/16x9_780/gillian-flies-and-brent-preston.jpg', 'https://www.cbc.ca/news/canada/toronto/farmers-for-climate-solutions-launch-1.5458676', '2020-03-01', '2022-03-31', 0);
INSERT INTO `ga2w20`.`news` (`id`, `img_url`, `site_url`, `startDate`, `endDate`, `newsActive`) VALUES (3, 'https://i.cbc.ca/1.5460923.1581544446!/cpImage/httpImage/image.jpg_gen/derivatives/16x9_780/trudeau-africa-2020212.jpg', 'https://www.cbc.ca/news/politics/trudeau-anti-pipeline-protests-1.5460793', '2020-04-01', '2022-04-30', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`author`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (1, 'Charles Petzold');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (2, 'Gerald Jay Sussman');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (3, 'Harold Abelson');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (4, 'Wladston Ferreira Filho');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (5, 'Robert C. Martin');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (6, 'Subrata Dasgupta');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (7, 'Steven S Skiena');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (8, 'Grant Smith');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (9, 'Workman Publishing');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (10, 'Kevin Wayne');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (11, 'Robert Sedgewick');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (12, 'Prof Hamzeh Roumani PhD');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (13, 'Roselyn Teukolsky M.S.');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (14, 'J. Glenn Brookshear');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (15, 'John Lewis');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (16, 'Nell Dale');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (17, 'Jason Montojo');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (18, 'Jennifer Campbell');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (19, 'Paul Gries');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (20, 'Mr. Kevin P Hare');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (21, 'Rachel H. Grunspan');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (22, 'Simson L Garfinkel');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (23, 'Computer Science Academy');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (24, 'N. David Mermin');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (25, 'Judith Gersting');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (26, 'G.Michael Schneider');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (27, 'DK');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (28, 'Bill Bryson');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (29, 'Richard Ingersoll');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (30, 'Holly Mayer');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (31, 'David Emory Shi');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (32, 'Robert Dean');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (33, 'Captivating History');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (34, 'Colin Woodard');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (35, 'Eric Chaline');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (36, 'H. W. Brands');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (37, 'Sun Tzu');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (38, 'Robert Greene');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (39, 'Ganesh Sitaraman');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (40, 'Harvey C. Mansfield');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (41, 'Alexis de Tocqueville');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (42, 'David A. Moss');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (43, 'Nancy MacLean');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (44, 'Archie Brown');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (45, 'Friedrich Engels');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (46, 'Karl Marx');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (47, 'Jane Austen');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (48, 'Emily Brontë');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (49, 'Bella Jean');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (50, 'Penny Boumelha');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (51, 'Thomas Hardy');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (52, 'George Eliot');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (53, 'Charlotte Brontë');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (54, 'Glen Craney');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (55, 'Susan Fraser King');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (56, 'Allison Pataki');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (57, 'Kelly Rimmer');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (58, 'Anna Mansell');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (59, 'Holly Martin');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (60, 'Lucy Coleman');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (61, 'Sofia Stril-Rever');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (62, 'Dalai Lama');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (63, 'Anne Frank');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (64, 'Phil Knight');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (65, 'Robert Iger');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (66, 'Walter Isaacson');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (67, 'Nelson Mandela');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (68, 'Brosmind');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (69, 'Maria Isabel Sanchez Vegara');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (70, 'Kobe Bryant');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (71, 'Robert G. Hagstrom');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (72, 'Stephen A. Schwarzman');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (73, 'Melinda Gates');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (74, 'Taylor Jenkins Reid');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (75, 'Alfred Lansing');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (76, 'Ron Chernow');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (77, 'Saint Augustine');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (78, 'Andrew Roberts');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (79, 'Jesse Thistle');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (80, 'Anonymous');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (81, 'Prince');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (82, 'Alafair Burke');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (83, 'Mary Higgins Clark');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (84, 'Agatha Christie');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (85, 'Carolyn Keene');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (86, 'Ben Straub');
INSERT INTO `ga2w20`.`author` (`id`, `authorName`) VALUES (87, 'Scott Chacon');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`format`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (1, 'txt');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (2, 'docx');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (3, 'pdf');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (4, 'fb2');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (5, 'djvu');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (6, 'lit');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (7, 'rft');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (8, 'epub');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (9, 'mobi');
INSERT INTO `ga2w20`.`format` (`id`, `formatTitle`) VALUES (10, 'azw');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`genre`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`genre` (`id`, `genreTitle`) VALUES (1, 'Computer Science');
INSERT INTO `ga2w20`.`genre` (`id`, `genreTitle`) VALUES (2, 'History');
INSERT INTO `ga2w20`.`genre` (`id`, `genreTitle`) VALUES (3, 'Romance');
INSERT INTO `ga2w20`.`genre` (`id`, `genreTitle`) VALUES (4, 'Biography');
INSERT INTO `ga2w20`.`genre` (`id`, `genreTitle`) VALUES (5, 'Mystery');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`publisher`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (1, 'Microsoft Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (2, 'The MIT Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (3, 'Code Energy LLC');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (4, 'Prentice Hall');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (5, 'Oxford University Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (6, 'Springer');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (7, 'Workman Publishing Company');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (8, 'Addison-Wesley Professional');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (9, 'CompuScope Consulting');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (10, 'Barron\'s Educational Series');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (11, 'Pearson');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (12, 'Jones & Bartlett Learning');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (13, 'Pragmatic Bookshelf');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (14, 'Kevin P. Hare');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (15, 'Sterling ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (16, 'Independently published ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (17, 'Cambridge University Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (18, 'Course Technology');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (19, 'DK');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (20, 'Broadway Books');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (21, 'W. W. Norton & Company');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (22, 'CreateSpace Independent Publishing Platform');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (23, 'Captivating History');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (24, 'Penguin Books');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (25, 'Firefly Books');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (26, 'Basic Books');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (27, 'Filiquarian');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (28, 'Viking');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (29, 'David & Charles Publishers');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (30, 'History Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (31, 'University of Chicago Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (32, 'Belknap Press: An Imprint of Harvard University Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (33, 'Ecco');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (34, 'International Publishers Co');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (35, 'AmazonClassics');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (36, 'AmazonClassics ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (37, 'Wisehouse Classics');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (38, 'EuroBooks');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (39, 'Oxford World\'s Classics');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (40, 'Vintage Classics');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (41, 'e-artnow');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (42, 'Brigid\'s Fire Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (43, 'Random House');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (44, 'Graydon House');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (45, 'Bookouture');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (46, 'HQ Digital');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (47, 'Aria');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (48, 'HarperOne');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (49, 'Lifebooks');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (50, 'Scribner');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (51, 'Simon & Schuster');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (52, 'Little, Brown and Company');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (53, 'Frances Lincoln Children\'s Books');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (54, 'MCD');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (55, 'Wiley');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (56, 'Avid Reader Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (57, 'Flatiron Books ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (58, 'Washington Square Press');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (59, 'Penguin Books ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (60, 'OUP Oxford');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (61, 'Penguin');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (62, 'Simon & Schuster ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (63, 'Twelve ');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (64, 'Spiegel & Grau');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (65, 'William Morrow Paperbacks');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (66, 'Real epublisher');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (67, 'Grosset & Dunlap');
INSERT INTO `ga2w20`.`publisher` (`id`, `publisherName`) VALUES (68, 'Apress');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`book`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780735611313    , 'Code: The Hidden Language of Computer Hardware and Software', 1, '2000-10-11', 400, 1, 'What do flashlights, the British invasion, black cats, and seesaws have to do with computers? In CODE, they show us the ingenious ways we manipulate language and invent new means of communicating with each other. And through CODE, we see how this ingenuity and our very human compulsion to communicate have driven the technological innovations of the past two centuries.\nUsing everyday objects and familiar language systems such as Braille and Morse code, author Charles Petzold weaves an illuminating narrative for anyone who’s ever wondered about the secret inner life of computers and other smart machines.', 'images/978-0735611313.jpg', 1.44, 14.39, '2020-01-07', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780262510875    , 'Structure and Interpretation of Computer Programs', 2, '1996-07-25', 657, 1, 'Structure and Interpretation of Computer Programs has had a dramatic impact on computer science curricula over the past decade. This long-awaited revision contains changes throughout the text. There are new implementations of most of the major programming systems in the book, including the interpreters and compilers, and the authors have incorporated many small changes that reflect their experience teaching the course at MIT since the first edition was published. A new theme has been introduced that emphasizes the central role played by different approaches to dealing with time in computational models: objects with state, concurrent programming, functional programming and lazy evaluation, and nondeterministic programming. There are new example sections on higher-order procedures in graphics and on applications of stream processing in numerical programming, and many new exercises. In addition, all the programs have been reworked to run in any Scheme implementation that adheres to the IEEE standard.', 'images/978-0262510875.jpg', 4, 40, '2020-01-22', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780997316025    , 'Computer Science Distilled: Learn the Art of Solving Computational Problems', 3, '2017-01-17', 180, 1, 'A walkthrough of computer science concepts you must know. Designed for readers who don\'t care for academic formalities, it\'s a fast and easy computer science guide. It teaches the foundations you need to program computers effectively. After a simple introduction to discrete math, it presents common algorithms and data structures. It also outlines the principles that make computers and programming languages work.', 'images/978-0997316025.jpg', 2.59, 25.85, '2020-01-18', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780132350884    , 'Clean Code: A Handbook of Agile Software Craftsmanship', 4, '2008-08-01', 464, 1, 'Even bad code can function. But if code isn’t clean, it can bring a development organization to its knees. Every year, countless hours and significant resources are lost because of poorly written code. But it doesn’t have to be that way.\n\nNoted software expert Robert C. Martin presents a revolutionary paradigm with Clean Code: A Handbook of Agile Software Craftsmanship . Martin has teamed up with his colleagues from Object Mentor to distill their best agile practice of cleaning code “on the fly” into a book that will instill within you the values of a software craftsman and make you a better programmer—but only if you work at it.\n\nWhat kind of work will you be doing? You’ll be reading code—lots of code. And you will be challenged to think about what’s right about that code, and what’s wrong with it. More importantly, you will be challenged to reassess your professional values and your commitment to your craft.', 'images/978-0132350884.jpg', 3.12, 31.19, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780198733461    , 'Computer Science: A Very Short Introduction', 5, '2016-03-07', 144, 1, 'Over the past sixty years, the spectacular growth of the technologies associated with the computer is visible for all to see and experience. Yet, the science underpinning this technology is less visible and little understood outside the professional computer science community. As a scientific\ndiscipline, computer science stands alongside the likes of molecular biology and cognitive science as one of the most significant new sciences of the post Second World War era.\n\nIn this Very Short Introduction, Subrata Dasgupta sheds light on these lesser known areas and considers the conceptual basis of computer science. Discussing algorithms, programming, and sequential and parallel processing, he considers emerging modern ideas such as biological computing and cognitive\nmodelling, challenging the idea of computer science as a science of the artificial.\n\nABOUT THE SERIES: The Very Short Introductions series from Oxford University Press contains hundreds of titles in almost every subject area. These pocket-sized books are the perfect way to get ahead in a new subject quickly. Our expert authors combine facts, analysis, perspective, new ideas, and\nenthusiasm to make interesting and challenging topics highly readable.', 'images/978-0198733461.jpg', 0.69, 6.93, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781848000698    , 'The Algorithm Design Manual', 6, '2011-04-27', 730, 1, 'This newly expanded and updated second edition of the best-selling classic continues to take the \"mystery\" out of designing algorithms, and analyzing their efficacy and efficiency. Expanding on the first edition, the book now serves as the primary textbook of choice for algorithm design courses while maintaining its status as the premier practical reference guide to algorithms for programmers, researchers, and students.\n\nThe reader-friendly Algorithm Design Manual provides straightforward access to combinatorial algorithms technology, stressing design over analysis. The first part, Techniques, provides accessible instruction on methods for designing and analyzing computer algorithms. The second part, Resources, is intended for browsing and reference, and comprises the catalog of algorithmic resources, implementations and an extensive bibliography.', 'images/978-1848000698.jpg', 7.62, 76.18, '2020-01-15', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781523502776    , 'Everything You Need to Ace Computer Science and Coding in One Big Fat Notebook: The Complete Middle School Study Guide', 7, '2019-04-28', 576, 1, 'Released just three years ago, The Big Fat Notebooks revolutionized the study guide for middle schoolers, and students, parents, and teachers responded—the series has nearly 4 million copies in print with sales escalating every year. Now introducing Everything You Need to Ace Computer Science and Coding, an essential new title with the potential to run hand-in-hand with—or even outrun—Math (over 1.3 million copies in print) and Science (925,000 copies in print) as the next critical STEM companion.\n          \nInstruction is presented in the simple but powerful format of the previous Big Fat Notebooks. The key concepts of coding and computer science easily digested and summarized, with critical ideas clearly explained, doodles that illuminate tricky concepts, and quizzes to recap it all. Kids will explore the concepts of computer science, learn how websites are designed and created, and understand the fundamentals of coding with Scratch, Python, HTML, and CSS. \n\nWritten by Grant Smith, a computer science education expert—and vetted by an award-winning computer-science teacher—this Big Fat Notebook is for every student who is either taking computer science in school or is a passionate code warrior.', 'images/978-1523502776.jpg', 2.27, 22.72, '2020-01-06', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780134076423    , 'Computer Science: An Interdisciplinary Approach', 8, '2016-06-15', 1168, 1, 'Named a Notable Book in the 21st Annual Best of Computing list by the ACM!\n\nRobert Sedgewick and Kevin Wayne’s  Computer Science: An Interdisciplinary Approach  is the ideal modern introduction to computer science with Java programming for both students and professionals. Taking a broad, applications-based approach, Sedgewick and Wayne teach through important examples from science, mathematics, engineering, finance, and commercial computing.\n\n \n\nThe book demystifies computation, explains its intellectual underpinnings, and covers the essential elements of programming and computational problem solving in today’s environments. The authors begin by introducing basic programming elements such as variables, conditionals, loops, arrays, and I/O. Next, they turn to functions, introducing key modular programming concepts, including components and reuse. They present a modern introduction to object-oriented programming, covering current programming paradigms and approaches to data abstraction.\n\n \n\nBuilding on this foundation, Sedgewick and Wayne widen their focus to the broader discipline of computer science. They introduce classical sorting and searching algorithms, fundamental data structures and their application, and scientific techniques for assessing an implementation’s performance. Using abstract models, readers learn to answer basic questions about computation, gaining insight for practical application. Finally, the authors show how machine architecture links the theory of computing to real computers, and to the field’s history and evolution.\n\n \n\nFor each concept, the authors present all the information readers need to build confidence, together with examples that solve intriguing problems. Each chapter contains question-and-answer sections, self-study drills, and challenging problems that demand creative solutions.', 'images/978-0134076423.jpg', 6.4, 63.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781775125402    , 'Introduction to Computer Science with Android: The Learning-By-Doing Series', 9, '2018-05-19', 330, 1, 'In addition to self-learning, this book is suitable for a first-year computing course themed around object-oriented programming, Java, and Android. No prior exposure to any of these topics is assumed. What makes this book different is that it uses Android to teach foundational computing concepts and it does so using experiential learning.\n\nThrough a sequence of projects (such as Calculators, Encrypted Notes, the Country Game, and Stock Trader) you will learn how to build mobile apps using best-practice methodologies and established design patterns. At a deeper level, this will enable you to construct concepts (such as confronting complexity through abstraction, the separation of concerns principle, and OOP vs Functional) that will endure long after the platform and the language fall out of fashion.', 'images/978-1775125402.jpg', 4.12, 41.17, '2020-01-22', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781438009193    , 'Barron\'s AP Computer Science A, 8th Edition', 10, '2018-01-01', 528, 1, 'This best-selling guide from Barron\'s offers practical, proven test-taking strategies and preparation for the Advanced Placement test. This updated manual presents computer science test takers with:\nThree AP practice tests for the AP Computer Science A test, including a diagnostic test\nCharts detailing the scoring suggestions for each free-response question\nAnswers and explanations for every test question\nA subject review includes static variables, the List interface, enhanced for loops, the import statement, many questions on 2-dimensional arrays, and a detailed analysis of the binary search algorithm. The book reflects the fact that the ClassCastException and downcasting have been removed from the AP Java subset. The practice exams reflect the new free-response style used on recent AP exams.\nBONUS ONLINE PRACTICE TESTS: Students who purchase this book will also get FREE access to three additional full-length online AP Computer Science A tests with all questions answered and explained. These online exams can be easily accessed by smartphone, tablet, or computer.', 'images/978-1438009193.jpg', 2.27, 22.72, '2020-01-17', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780132569033    , 'Computer Science: An Overview (11th Edition)', 11, '2011-01-21', 624, 1, 'Computer Science: An Overview uses broad coverage and clear exposition to present a complete picture of the dynamic computer science field. Accessible to students from all backgrounds, Glenn Brookshear uses a language-independent context to encourage the development of a practical, realistic understanding of the field. An overview of each of the important areas of Computer Science (e.g. Networking, OS, Computer Architecture, Algorithms) provides students with a general level of proficiency for future courses.\n\nThe Eleventh Edition features two new contributing authors (David Smith — Indiana University of PA; Dennis Brylow — Marquette University), new, modern examples, and updated coverage based on current technology.', 'images/978-0132569033.jpg', 1.55, 15.46, '2020-01-17', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781284155617    , 'Computer Science Illuminated', 12, '2019-01-31', 745, 1, 'Designed for the introductory computing and computer science course, the student-friendly Computer Science Illuminated, Seventh Edition provides students with a solid foundation for further study, and offers non-majors a complete introduction to computing. Fully revised and updated, the Seventh Edition of this best-selling text retains the accessibility and in-depth coverage of previous editions, while incorporating all-new material on cutting-edge issues in computer science. Authored by the award-winning team Nell Dale and John Lewis, the text provides a unique and innovative layered approach, moving through the levels of computing from an organized, language-neutral perspective. Each new print copy includes Navigate 2 Advantage Access that unlocks a comprehensive and interactive eBook, student practice activities and assessments, a full suite of instructor resources, and learning analytics reporting tools.', 'images/978-1284155617.jpg', 13.07, 130.71, '2020-01-18', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781680502688    , 'Practical Programming: An Introduction to Computer Science Using Python 3.6', 13, '2017-12-16', 412, 1, 'Classroom-tested by tens of thousands of students, this new edition of the bestselling intro to programming book is for anyone who wants to understand computer science. Learn about design, algorithms, testing, and debugging. Discover the fundamentals of programming with Python 3.6--a language that\'s used in millions of devices. Write programs to solve real-world problems, and come away with everything you need to produce quality code. This edition has been updated to use the new language features in Python 3.6.', 'images/978-1680502688.jpg', 4.56, 45.59, '2020-01-19', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9790692106715    , 'Computer Science Principles: The Foundational Concepts of Computer Science - For AP® Computer Science Principles', 14, '2018-04-14', 199, 1, 'Computer science is the world\'s fastest growing field of study, and this growth is showing no signs of slowing down. As a new field, computer science can seem intimidating, but it should not be scary to learn or difficult to understand. If you have ever turned on a phone or surfed the Internet then you have used a computer and should have a basic understanding of what happens when you click the mouse or touch the screen—and how fast it happens! Computer Science Principles introduces the creative side of computing. Once you\'ve made your way through this book, you\'ll be editing photos, designing websites, coding JavaScript, and getting organized with spreadsheets—and along the way you\'ll learn the foundational concepts of computer science. How do computers convert information into ones and zeros and send it thousands of miles in a blink of the eye? What is an IP address? What do TCP/IP, DNS, HTML, and CSS stand for? How can a hard drive store large movies and thousands of songs? How can secrets be sent in plain sight? These questions—and more—are answered in Computer Science Principles.', 'images/979-0692106715.jpg', 1.57, 15.7, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781454926214    , 'The Computer Book: From the Abacus to Artificial Intelligence, 250 Milestones in the History of Computer Science', 15, '2018-11-06', 528, 1, 'Part of Sterling’s extremely popular Milestones series, this illustrated exploration of computer science ranges from the ancient abacus to superintelligence and social media.\n \nWith 250 illustrated landmark inventions, publications, and events—encompassing everything from ancient record-keeping devices to the latest computing technologies—this highly topical addition to the Sterling Milestones series takes a chronological journey through the history and future of computer science. Two expert authors, with decades’ of experience working in computer research and innovation, explore topics including the Sumerian abacus, the first spam message, Morse code, cryptography, early computers, Isaac Asimov’s laws of robotics, UNIX and early programming languages, movies, video games, mainframes, minis and micros, hacking, virtual reality, and more.', 'images/978-1454926214.jpg', 1.58, 15.79, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781676140252    , 'Python for Data Science: A Crash Course for Data Science and Analysis, Python Machine Learning and Big Data', 16, '2019-12-16', 177, 1, 'If you are looking to master the fundamental concepts of Data Science driven by the Python programming language to develop a solid understanding of all the latest cutting edge technologies, then this is just that one comprehensive book you have been waiting for. This book is carefully written to help you master the core concepts of Python programming and utilize your coding skills to analyze a large volume of data and uncover valuable information that can otherwise be easily lost in such volume even if you have never learned any programming languages before. Python has been designed primarily to emphasize readability of the programming code and its syntax will enable you to convey ideas using fewer lines of code.\n\nIf you are looking to learn how to write effective and efficient codes in Python and master this extremely intuitive and flexible programming language that can be used for a variety of coding projects including machine learning algorithms, web applications, data mining and visualization, game development. Then this is just the book that you need. Some of the highlights of this book include:\nThe five major stages of the TDSP lifecycle that outline the interactive steps required for project execution along with the deliverables created at each stage.\nInstallation instructions for Python so you can download and install Python on your operating system and get hands-on coding experience.\nPython coding concepts such as data types, classes, and objects variables, numbers, constructor functions, Booleans and much more.\nLearn the functioning of various data science libraries like Scikit-Learn, which has evolved as the gold standard for machine learning and data analysis.\nDeep dive into the Matplotlib library, which offers visualization tools and science computing modules supported by SciPy and learn how to create various graphs using Matplotlib and Pandas library.  \nLearn how machine learning allows analysis of large volumes of data and delivers faster and more accurate results.\nOverview of four different machine learning algorithms that can be used to cater to the available data set and create a desired machine learning model.\nLearn how companies are able to employ a predictive analytics model to gain an understanding of customer interactions with their products or services based on customer’s feelings or emotions shared on the social media platforms.  \nEvery concept in this book is explained with examples and exercises so you can learn and test your learning at the same time. There are a variety of real life examples of the application of machine learning technology that has been provided to help you understand the importance of all the cutting edge technologies in shaping our world today. Remember, knowledge is power, and with the great power you will gather from this book, you will be armed to make sound personal and professional technological choices. Your Python programming skillset will improve drastically, and you will be poised to develop your very own machine learning model in no time.\n\nSo don’t wait and click on that BUY NOW button! Then be a good Samaritan, and spread the word to your tech-savvy friends and family, help them get access to this power!', 'images/978-1676140252.jpg', 2.24, 22.37, '2020-01-22', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780521876582    , 'Quantum Computer Science: An Introduction', 17, '2017-07-19', 233, 1, 'In the 1990\'s it was realized that quantum physics has some spectacular applications in computer science. This book is a concise introduction to quantum computation, developing the basic elements of this new branch of computational theory without assuming any background in physics. It begins with an introduction to the quantum theory from a computer-science perspective. It illustrates the quantum-computational approach with several elementary examples of quantum speed-up, before moving to the major applications: Shor\'s factoring algorithm, Grover\'s search algorithm, and quantum error correction. The book is intended primarily for computer scientists who know nothing about quantum theory, but will also be of interest to physicists who want to learn the theory of quantum computation, and philosophers of science interested in quantum foundational issues. It evolved during six years of teaching the subject to undergraduates and graduate students in computer science, mathematics, engineering, and physics, at Cornell University.', 'images/978-0521876582.jpg', 7.42, 74.23, '2020-01-17', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781337685931    , 'Invitation to Computer Science', 18, '2018-01-01', 898, 1, 'Gain a contemporary overview of today\'s computer science with the best-selling INVITATION TO COMPUTER SCIENCE, 8E. This flexible, non-language-specific book uses an algorithm-centered approach that\'s ideal for your first introduction to computer science. Measurable learning objectives and a clear hierarchy help introduce algorithms, hardware, virtual machines, software development, applications, and social issues. Exercises, practice problems, and feature boxes emphasize real-life context as well as the latest material on privacy, drones, cloud computing, and net neutrality. Optional online language modules for C++, Java, Python, C#, and Ada let you learn a programming language. MindTap is available with online study tools, a digital Lab Manual and lab software with 20 laboratory projects. Hands-on activities enable you to truly experience the fundamentals of today\'s computer science.', 'images/978-1337685931.jpg', 14.54, 145.35, '2020-01-20', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781465473608    , 'Help Your Kids with Computer Science', 19, '2018-07-03', 256, 1, 'A clear, visual guide to the technical, societal, and cultural aspects of computers and social media, using step-by-step diagrams and graphics to explore how kids can get the most from computers while staying safe.\n\nCovering everything from data to digital life, from computer coding to cyber attacks, this unique guide gives parents and kids the most up-to-date and comprehensive facts and information in a visually appealing way. It examines the technical aspects of computers, such as how they function, the latest digital devices and software, and how the Internet works. It also builds the confidence of parents and kids when facing challenges such as staying safe online, digital etiquette, and how to navigate the potential pitfalls of social media. Jargon-free language helps to explain difficult and potentially dread-inducing concepts such as hacking, Bitcoin, and malware, while colorful graphics help make learning about the world of computer science exciting. For those who want to make the most out of the digital world, Help Your Kids with Computer Science is the perfect platform to discover more.\n\nSeries Overview: DK\'s bestselling Help Your Kids With series contains crystal-clear visual breakdowns of important subjects. Simple graphics and jargon-free text are key to making this series a user-friendly resource for frustrated parents who want to help their children get the most out of school.', 'images/978-1465473608.jpg', 2.57, 25.73, '2020-01-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781465475855    , 'History of the World Map by Map', 19, '2018-10-23', 440, 2, 'Custom regional and global maps present the history of the world in action, charting how events traced patterns on land and ocean--patterns of exploration, discovery, or conquest that created empires, colonies, or theaters of war. Thoughtful organization of information will help you follow the story of civilizations through ancient, medieval, and modern times.', 'images/978-1465475855.jpg', 2.8, 27.99, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780767908184    , 'A Short History of Nearly Everything', 20, '2004-09-14', 544, 2, 'In A Walk in the Woods, Bill Bryson trekked the Appalachian Trail—well, most of it. In A Sunburned Country, he confronted some of the most lethal wildlife Australia has to offer. Now, in his biggest book, he confronts his greatest challenge: to understand—and, if possible, answer—the oldest, biggest questions we have posed about the universe and ourselves. Taking as territory everything from the Big Bang to the rise of civilization, Bryson seeks to understand how we got from there being nothing at all to there being us. To that end, he has attached himself to a host of the world’s most advanced (and often obsessed) archaeologists, anthropologists, and mathematicians, travelling to their offices, laboratories, and field camps. He has read (or tried to read) their books, pestered them with questions, apprenticed himself to their powerful minds. A Short History of Nearly Everything is the record of this quest, and it is a sometimes profound, sometimes funny, and always supremely clear and entertaining adventure in the realms of human knowledge, as only Bill Bryson can render it. Science has never been more involving or entertaining.', 'images/978-0767908184.jpg', 1, 9.99, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780195139570    , 'World Architecture: A Cross-Cultural History', 5, '2012-12-13', 992, 2, 'Spiro Kostof\'s groundbreaking work, A History of Architecture: Settings and Rituals, helped to reshape the study of architectural history. His book extended beyond the discussion of great monuments to find connections with ordinary dwellings, urbanism, and different cultures from around the world. World Architecture: A Cross-Cultural History is an entirely new, student-friendly text by Richard Ingersoll. Building on Kostof\'s global vision and social context, Ingersoll integrates extensive coverage of world and contemporary architecture in order to provide the most comprehensive survey in the field. Presented chronologically, each chapter now focuses on three unique architectural cultures, which gives instructors the flexibility to choose which traditions are the most relevant to their courses. The text also provides students with numerous pedagogical tools, including timelines, comparative maps, a glossary, and text boxes devoted to social factors and specific issues in technology and philosophy. The result is a comprehensive method for understanding and appreciating the history, cultural significance, and beauty of architecture from around the world.', 'images/978-0195139570.jpg', 1.3, 13, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780393283037    , 'For the Record: A Documentary History of America (Sixth Edition) (Vol. 1)', 21, '2016-03-01', 480, 2, 'For the Record: A Documentary History features nearly 250 primary source selections, both textual and visual, drawn from a broad range of government documents, newspapers, speeches, letters, novels, and images. A revised table of contents reflects the structure, organization, and emphasis on the culture of daily life found within America: A Narrative History, Tenth Edition, for which editor David Shi also serves as the author. The Sixth Edition’s selections are heavily informed by instructor feedback, resulting in a text rich with the pieces that historians prefer to assign, and, at just $10 net additional per volume when packaged with any edition of America, For the Record is available at an unbeatable value.', 'images/978-0393283037.jpg', 1.33, 13.25, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542382595    , 'Ancient History: History of the Ancient World: Ancient Civilizations, and Ancient Empires. History that Defined our World', 22, '2017-01-10', 54, 2, 'History of the Ancient World: Ancient Civilizations, and Ancient Empires. History that Defined our World offers a tantalizing glimpse into our vast and complex ancient history that is sure to inspire further study. There is a lot to be learned from ancient history.', 'images/978-1542382595.jpg', 1.8, 17.97, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781647483753    , 'History of Korea: A Captivating Guide to Korean History, Including Events Such as the Mongol Invasions, the Split into North and South, and the Korean War', 23, '2020-01-14', 120, 2, 'The Korean Peninsula today is divided into two, but there was a time when this peninsula was divided into many states. Over the course of time, and besieged by expansive transient dynasties outside of this modest piece of land, many clans and tribes overran their lands. Of all those malicious and greedy potential overlords, none managed to prevail.', 'images/978-1647483753.jpg', 1.33, 13.34, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780143122029    , 'American Nations: A History of the Eleven Rival Regional Cultures of North America', 24, '2012-09-25', 384, 2, 'According to award-winning journalist and historian Colin Woodard, North America is made up of eleven distinct nations, each with its own unique historical roots. In American Nations he takes readers on a journey through the history of our fractured continent, offering a revolutionary and revelatory take on American identity, and how the conflicts between them have shaped our past and continue to mold our future. From the Deep South to the Far West, to Yankeedom to El Norte, Woodard (author of American Character: A History of the Epic Struggle Between Individual Liberty and the Common Good) reveals how each region continues to uphold its distinguishing ideals and identities today, with results that can be seen in the composition of the U.S. Congress or on the county-by-county election maps of this year\'s Trump versus Clinton presidential election.', 'images/978-0143122029.jpg', 1.58, 15.79, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781770850903    , 'Fifty Machines that Changed the Course of History (Fifty Things That Changed the Course of History)', 25, '2012-10-11', 224, 2, 'Fifty Machines that Changed the Course of History is a fascinating survey of the mechanical devices that propelled 18th-century society into the 19th and 20th centuries. The book celebrates more than 200 years of technological development at the height of the Industrial Revolution. These are not generic inventions but rather specific, branded machines whose names in many cases have become synonymous with the machine or its purpose.', 'images/978-1770850903.jpg', 2, 19.99, '2020-01-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781541672529    , 'Dreams of El Dorado: A History of the American West', 26, '2019-10-22', 544, 2, 'In Dreams of El Dorado, H. W. Brands tells the thrilling, panoramic story of the settling of the American West. He takes us from John Jacob Astor\'s fur trading outpost in Oregon to the Texas Revolution, from the California gold rush to the Oklahoma land rush. He shows how the migrants\' dreams drove them to feats of courage and perseverance that put their stay-at-home cousins to shame-and how those same dreams also drove them to outrageous acts of violence against indigenous peoples and one another. The West was where riches would reward the miner\'s persistence, the cattleman\'s courage, the railroad man\'s enterprise; but El Dorado was at least as elusive in the West as it ever was in the East.', 'images/978-1541672529.jpg', 1.6, 15.99, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781599869773    , 'The Art Of War', 27, '2007-11-07', 496, 2, '', 'images/978-1599869773.jpg', 0.4, 3.99, '2020-01-05', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780670034574    , 'The 33 Strategies of War', 28, '2006-01-19', 496, 2, 'Spanning world civilizations, synthesizing dozens of political, philosophical, and religious texts and thousands of years of violent conflict, The 33 Strategies of War is a comprehensive guide to the subtle social game of everyday life informed by the most ingenious and effective military principles in war. Structured in Greene’s trademark style, The 33 Strategies of War is the I-Ching of conflict, the contemporary companion to Sun Tzu’s The Art of War.', 'images/978-0670034574.jpg', 2.53, 25.25, '2020-01-01', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781446301432    , 'Fifty Animals That Changed the Course of History', 29, '2011-11-01', 212, 2, 'Charting the relationship between humans and animals and how certain species have been instrumental in the development of our understanding of the evolution of the natural world, this title explores the role animals have played in our culture and history.', 'images/978-1446301432.jpg', 2.08, 20.76, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781770855878    , 'Fifty Minerals that Changed the Course of History (Fifty Things That Changed the Course of History)', 25, '2017-09-05', 224, 2, 'Fifty Minerals that Changed the Course of History is a beautifully presented guide to the minerals that have had the greatest impact on human civilization. These are the materials used from the Stone Age to the First and Second Industrial Revolutions to the Nuclear Age and include metals, ores, alloys, salts, rocks, sodium, mercury, steel and uranium. The book includes minerals used as currency, as jewelry and as lay and religious ornamentation when combined with gem minerals like diamonds, amber, coral, and jade.', 'images/978-1770855878.jpg', 1.48, 14.84, '2020-01-03', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780752457710    , 'History\'s Greatest Deceptions, and the People Who Planned Them', 30, '2010-06-08', 374, 2, 'Human history is positively shrouded in the dark arts of deceit and subterfuge. According to the Bible, the story of mankind began with the Serpent\'s lie to Eve, and we have happily profited from deceiving our brothers and sisters ever since. No matter the period or place, the religion or ideology, individuals and institutions have schemed and scammed their way into positions of power and wealth, taking full advantage of man\'s unique capacity for believing almost anything. With no little skill and imaginative flair, History\'s Greatest Deceptions chronicles the 50 most remarkable tales of fraud and forgery. This beautifully presented volume, which includes over 80 photographs, paintings, and illustrations, shines a light on the compelling personalities inside the stories, and explores the motivations behind some of the most ambitiously dishonest acts known to humankind.', 'images/978-0752457710.jpg', 2.43, 24.25, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781541618114    , 'The Great Democracy: How to Fix Our Politics, Unrig the Economy, and Unite America', 26, '2019-12-10', 272, 2, 'Since the New Deal in the 1930s, there have been two eras in our political history: the liberal era, stretching up to the 1970s, followed by the neoliberal era of privatization and austerity ever since. In each period, the dominant ideology was so strong that it united even partisan opponents. But the neoliberal era is collapsing, and the central question of our time is what comes next.', 'images/978-1541618114.jpg', 1.8, 17.99, '2020-03-07', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780226805368    , 'Democracy in America', 31, '2002-04-01', 722, 2, 'Alexis de Tocqueville (1805-59) came to America in 1831 to see what a great republic was like. What struck him most was the country\'s equality of conditions, its democracy. The book he wrote on his return to France, Democracy in America, is both the best ever written on democracy and the best ever written on America. It remains the most often quoted book about the United States, not only because it has something to interest and please everyone, but also because it has something to teach everyone.', 'images/978-0226805368.jpg', 1.23, 12.29, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780674971455    , 'Democracy: A Case Study', 32, '2017-02-21', 784, 2, 'To all who say our democracy is broken―riven by partisanship, undermined by extremism, corrupted by wealth―history offers hope. In nearly every generation since the nation’s founding, critics have lodged similar complaints, and yet the nation is still standing. In Democracy: A Case Study, Harvard Business School professor David Moss reveals that the United States has often thrived on conflict.', 'images/978-0674971455.jpg', 2.53, 25.25, '2020-01-05', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781101980972    , 'Democracy in Chains: The Deep History of the Radical Right\'s Stealth Plan for America', 24, '2018-06-05', 368, 2, 'In a brilliant and engrossing narrative, Nancy MacLean shows how Buchanan forged his ideas about government in a last gasp attempt to preserve the white elite’s power in the wake of Brown v. Board of Education. In response to the widening of American democracy, he developed a brilliant, if diabolical, plan to undermine the ability of the majority to use its numbers to level the playing field between the rich and powerful and the rest of us.', 'images/978-1101980972.jpg', 1.22, 12.19, '2020-01-04', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780061138829    , 'The Rise and Fall of Communism', 33, '2011-06-01', 752, 2, 'The Rise and Fall of Communism is the definitive history from the internationally renowned Oxford authority on the subject. Emeritus Professor of Politics at Oxford University, Archie Brown examines the origins of the most important political ideology of the 20th century, its development in different nations, its collapse in the Soviet Union following perestroika, and its current incarnations around the globe. Fans of John Lewis Gaddis, Samuel Huntington, and avid students of history will appreciate the sweep and insight of this epic and astonishing work.', 'images/978-0061138829.jpg', 2.11, 21.14, '2020-01-15', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780717802418    , 'The Communist Manifesto', 34, '2014-02-07', 48, 2, 'The Communist Manifesto, originally titled Manifesto of the Communist Party (German: Manifest der Kommunistischen Partei) is a short 1848 book written by the German Marxist political theorists Karl Marx and Friedrich Engels. It has since been recognized as one of the world\'s most influential political manuscripts. Commissioned by the Communist League, it laid out the League\'s purposes and program. It presents an analytical approach to the class struggle (historical and present) and the problems of capitalism, rather than a prediction of communism\'s potential future forms.', 'images/978-0717802418.jpg', 0.2, 2.04, '2020-02-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542047487    , 'Pride and Prejudice', 35, '2017-06-20', 446, 3, 'Jane Austen\' most popular novel, originally published in 1813, some seventeen years after it was first written, presents the Bennet family of Longbourn. Against the background of gossipy Mrs Bennet and the detached Mr Bennet, the quest is on for husbands', 'images/978-1542047487.jpg', 0, 0, '2020-01-02', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542049337    , 'Sense and Sensibility', 35, '2017-08-05', 375, 3, 'When Mrs Dashwood is forced by an avaricious daughter-in-law to leave the family home in Sussex, she takes her three daughters to live in a modest cottage in Devon. For Elinor, the eldest daughter, the move means a painful separation from the man she love', 'images/978-1542049337.jpg', 0, 0, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542049665    , 'Emma', 35, '2017-10-05', 480, 3, 'Jane Austen\' most popular novel, originally published in 1813, some seventeen years after it was first written, presents the Bennet family of Longbourn. Against the background of gossipy Mrs Bennet and the detached Mr Bennet, the quest is on for husbands', 'images/978-1542049665.jpg', 0, 0, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542049498    , 'Mansfield Park', 35, '2017-09-14', 476, 3, 'Jane Austen\' most popular novel, originally published in 1813, some seventeen years after it was first written, presents the Bennet family of Longbourn. Against the background of gossipy Mrs Bennet and the detached Mr Bennet, the quest is on for husbands', 'images/978-1542049498.jpg', 0, 0, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542049153    , 'Persuasion', 35, '2017-10-05', 253, 3, 'Anne Elliot has grieved for seven years over the loss of her first and only love, Captain Frederick Wentworth. When their paths finally cross again, Anne finds herself slighted and all traces of their former intimacy gone. As the pair continue to share the same social circle, dramatic events in Lyme Regis, and later in Bath, conspire to unravel the knots of deceit and misunderstanding in this beguiling and gently comic story of love and fidelity. Juliet Stevenson reads this unabridged recording with her customary clarity and particular understanding for the words and world of Jane Austen.', 'images/978-1542049153.jpg', 0, 0, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781542049467    , 'Northanger Abbey', 36, '2017-09-07', 235, 3, 'When Catherine Morland, a country clergyman\' daughter, is invited to spend a season in Bath with the fashionable high society, little does she imagine the delights and perils that await her. Captivated and disconcerted by what she finds, and introduced to', 'images/978-1542049467.jpg', 0, 0, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9789176371824    , 'Wuthering Heights', 37, '2016-08-18', 214, 3, 'When Mr Earnshaw brings a black-haired foundling child into his home on the Yorkshire moors, he little imagines the dramatic events which will follow. The passionate relationship between Cathy Earnshaw and the foundling, Heathcliff, is a story of love', 'images/978-9176371824.jpg', 0.1, 1, '2020-01-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781723096945    , 'The Forever In Between', 38, '2017-09-24', 108, 3, 'Born into a family of great wealth, she has never wanted for anything…except for a man who truly understood her. Her parents, constantly exasperated by her inability to find a husband, have just about given up hope. They think her irresponsible and childish, but she thinks they all have missed the point entirely. When another suitor decides that he doesn’t want to marry her, Victoria decides to take matters into her own hands. Finding an advertisement in the Matrimonial Times for a rancher in Oklahoma who is looking for a bride, she decides to write to him.', 'images/978-1723096945.jpg', 0.5, 4.99, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780191606335    , 'The Woodlanders', 39, '2005-02-10', 412, 3, 'Love, and the erratic heart, are at the centre of Hardy\'s \'woodland story\'. Set in the beautiful Blackmoor Vale, The Woodlanders concerns the fortunes of Giles Winterborne, whose love for the well-to-do Grace Melbury is challenged by the arrival of the dashing and dissolute doctor, Edred Fitzpiers. When the mysterious Felice Charmond further complicates the romantic entanglements, marital choice and class mobility become inextricably linked. Hardy\'s powerful novel depicts individuals in thrall to desire and the natural law that motivates them.', 'images/978-0191606335.jpg', 0.6, 5.99, '2020-01-05', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780099577282    , 'Adam Bede', 40, '2016-09-03', 626, 3, 'It may seem like an old tale: the beautiful village girl, her faithful admirer, a country squire\'s seduction. But seen through the eyes of any of its players, the old tale becomes one of fresh heartbreak, innocent hopes, best intentions gone awry, and better selves lost and restored. George Eliot\'s first novel shows all her humane intelligence and intimate knowledge of the richness and complexity of ordinary life.', 'images/978-0099577282.jpg', 1, 9.99, '2020-01-01', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780199682867    , 'Daniel Deronda', 39, '2014-06-10', 769, 3, 'Gwendolen Harleth gambles her happiness when she marries a sadistic aristocrat for his money. Beautiful, neurotic, and self-centred, Gwendolen is trapped in an increasingly destructive relationship, and only her chance encounter with the idealistic Deronda seems to offer the hope of a brighter future. Deronda is searching for a vocation, and in embracing the Jewish cause he finds one that is both visionary and life-changing. Damaged by their pasts, and alienated from the society around them, they must both discover the values that will give their lives meaning.', 'images/978-0199682867.jpg', 0.89, 8.88, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9788026881797    , 'Shirley', 41, '2018-03-03', 402, 3, 'Robert Moore is a mill owner hated by his workers because he has been laying many of them off, since the mill is deeply in debt and the workers are being replaced by the machines. Regardless to that, Robert is determined to restore his family\'s honor and fortune. He awaits delivery of new labour-saving machinery for the mill, but the machinery is destroyed on the way to the mill by angry millworkers. Through his sister\'s French teacher, Caroline, Robert meets Shirley, an independent heiress and a landowner. The two come closer together, but Robert\'s difficulties increase, as the angry workers start threatening his life.', 'images/978-8026881797.jpg', 0.2, 1.99, '2020-01-03', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9785006800173    , 'The Spider and the Stone', 42, '2013-10-26', 433, 3, 'Here is the story of the remarkable events following the execution of William Wallace of Braveheart fame. Set during the Bruce wars of independence, The Spider and the Stone is the unforgettable saga of the star-crossed love, religious intrigue, fierce friendship in arms, and heroic sacrifice that preserved Scotland\'s freedom during its time of greatest peril.', 'images/978-5006800173.jpg', 1, 9.99, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9788003381693    , 'Queen Hereafter', 20, '2010-10-07', 354, 3, 'Shipwrecked on the Scottish coast, a young Saxon princess and her family—including the outlawed Edgar of England—ask sanctuary of the warrior-king Malcolm Canmore, who shrewdly sees the political advantage. He promises to aid Edgar and the Saxon cause in return for the hand of Edgar’s sister, Margaret, in marriage.', 'images/978-8003381693.jpg', 1.4, 13.99, '2020-01-07', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780812989052    , 'Sisi: Empress on Her Own', 43, '2016-03-08', 465, 3, 'Through love affairs and loss, dedication and defiance, Sisi struggles against conflicting desires: to keep her family together, or to flee amid the collapse of her suffocating marriage and the gathering tumult of the First World War. In an age of crumbling monarchies, Sisi fights to assert her right to the throne beside her husband, to win the love of her people and the world, and to save an empire. But in the end, can she save herself?', 'images/978-0812989052.jpg', 1.3, 12.99, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9788072366185    , 'Before I Let You Go', 44, '2018-04-03', 384, 3, 'Before I Let You Go is a heartbreaking book about an impossible decision. Kelly Rimmer writes with wisdom and compassion about the relationships between sisters, mother and daughter…. She captures the anguish of addiction, the agonizing conflict between an addict’s best and worst selves. Above all, this is a novel about the deepest love possible.', 'images/978-8072366185.jpg', 1.2, 11.99, '2020-01-05', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9788074267149    , 'I Wanted To Tell You', 45, '2018-09-29', 334, 3, 'When Helen finds a bundle of unsent love letters, tied with a red ribbon, and signed only ‘the love you wished I could be’, she wonders who they could belong to.', 'images/978-8074267149.jpg', 0.4, 3.99, '2020-01-04', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781916011168    , 'The Gift of Happiness', 38, '2019-01-30', 262, 3, 'The beautiful new story from the bestselling author of The Summer of Chasing Dreams. Escape to a little seaside village, where the sea sparkles, snow falls and change is in the air. The perfect place to fall in love…', 'images/978-1916011168.jpg', 0.3, 2.99, '2020-01-15', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781474031363    , 'Tied Up With Love', 46, '2015-02-14', 384, 3, 'Being grabbed off the street, blind folded, tied up and thrown into a van was not what Izzy expected to happen when she stepped out the door that morning. But when an accidental kidnapping at the hands of the sexy Ethan Chase and his \'Kidnap My Wife\' sexual fantasy business leads to just that, Izzy seizes the chance to turn her misfortune into a brilliant new job opportunity…', 'images/978-1474031363.jpg', 0.16, 1.63, '2020-01-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9788077968719    , 'The French Adventure', 47, '2018-02-01', 470, 3, 'Anna gives herself 6 months to recuperate, all the while helping renovate her parents\' adjoining gites into picturesque B&Bs. But working alongside the ruggedly handsome Sam on the renovation project, she didn\'t expect for life to take an unexpected, if not unwelcome, twist...', 'images/978-8077968719.jpg', 0.46, 4.55, '2020-01-02', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062018090    , 'My Spiritual Journey', 48, '2010-10-12', 387, 4, 'In this elegant self-portrait, the world’s most outspoken and influential spiritual leader recounts his epic and engaging life story. The Dalai Lama’s most accessible and intimate book, My Spiritual Journey is an excellent introduction to the larger-than-life leader of Tibetan Buddhism—perfect for anyone curious about Eastern religion, invested in the Free Tibet movement, or simply seeking a richer spiritual life. The Dalai Lama’s riveting, deeply insightful meditations on life will resonate strongly with readers of Pema Chodron, Thich Nhat Hanh, or the His Holiness’s own The Art of Happiness and Ethics for the New Millennium.', 'images/978-0062018090.jpg', 1.8, 17.98, '2020-01-23', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780553296983    , 'The Diary of a Young Girl', 49, '2020-02-28', 361, 4, 'Discovered in the attic in which she spent the last years of her life, Anne Frank\'s remarkable diary has since become a world classic—a powerful reminder of the horrors of war and an eloquent testament to the human spirit. In 1942, with Nazis occupying Holland, a thirteen-year-old Jewish girl and her family fled their home in Amsterdam and went into hiding. For the next two years, until their whereabouts were betrayed to the Gestapo, they and another family lived cloistered in the \"Secret Annex\" of an old office building. Cut off from the outside world, they faced hunger, boredom, the constant cruelties of living in confined quarters, and the ever-present threat of discovery and death. In her diary Anne Frank recorded vivid impressions of her experiences during this period. By turns thoughtful, moving, and amusing, her account offers a fascinating commentary on human courage and frailty and a compelling self-portrait of a sensitive and spirited young woman whose promise was tragically cut short.', 'images/978-0553296983.jpg', 0.1, 0.99, '2020-01-18', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501135927    , 'Shoe Dog: A Memoir by the Creator of Nike', 50, '2016-04-26', 401, 4, 'In this instant and tenacious New York Times bestseller, Nike founder and board chairman Phil Knight “offers a rare and revealing look at the notoriously media-shy man behind the swoosh” (Booklist, starred review), illuminating his company’s early days as an intrepid start-up and its evolution into one of the world’s most iconic, game-changing, and profitable brands.', 'images/978-1501135927.jpg', 1.5, 14.99, '2020-03-16', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780593170984    , 'The Ride of a Lifetime: Lessons Learned from 15 Years as CEO of the Walt Disney Company', 43, '2019-09-23', 250, 4, 'Robert Iger became CEO of The Walt Disney Company in 2005, during a difficult time. Competition was more intense than ever and technology was changing faster than at any time in the company’s history. His vision came down to three clear ideas: Recommit to the concept that quality matters, embrace technology instead of fighting it, and think bigger—think global—and turn Disney into a stronger brand in international markets.\n\nFourteen years later, Disney is the largest, most respected media company in the world, counting Pixar, Marvel, Lucasfilm, and 21st Century Fox among its properties. Its value is nearly five times what it was when Iger took over, and he is recognized as one of the most innovative and successful CEOs of our era.\n\nIn The Ride of a Lifetime, Robert Iger shares the lessons he’s learned while running Disney and leading its 200,000 employees, and he explores the principles that are necessary for true leadership, including:\n\n• Optimism. Even in the face of difficulty, an optimistic leader will find the path toward the best possible outcome and focus on that, rather than give in to pessimism and blaming.\n• Courage. Leaders have to be willing to take risks and place big bets. Fear of failure destroys creativity.\n• Decisiveness. All decisions, no matter how difficult, can be made on a timely basis. Indecisiveness is both wasteful and destructive to morale.\n• Fairness. Treat people decently, with empathy, and be accessible to them.\n\nThis book is about the relentless curiosity that has driven Iger for forty-five years, since the day he started as the lowliest studio grunt at ABC. It’s also about thoughtfulness and respect, and a decency-over-dollars approach that has become the bedrock of every project and partnership Iger pursues, from a deep friendship with Steve Jobs in his final years to an abiding love of the Star Wars mythology.\n \n“The ideas in this book strike me as universal” Iger writes. “Not just to the aspiring CEOs of the world, but to anyone wanting to feel less fearful, more confidently themselves, as they navigate their professional and even personal lives.”', 'images/978-0593170984.jpg', 1.7, 16.99, '2020-01-10', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501139161    , 'Leonardo da Vinci', 51, '2017-10-17', 625, 4, 'Based on thousands of pages from Leonardo da Vinci’s astonishing notebooks and new discoveries about his life and work, Walter Isaacson “deftly reveals an intimate Leonardo” (San Francisco Chronicle) in a narrative that connects his art to his science. He shows how Leonardo’s genius was based on skills we can improve in ourselves, such as passionate curiosity, careful observation, and an imagination so playful that it flirted with fantasy.\n\nHe produced the two most famous paintings in history, The Last Supper and the Mona Lisa. With a passion that sometimes became obsessive, he pursued innovative studies of anatomy, fossils, birds, the heart, flying machines, botany, geology, and weaponry. He explored the math of optics, showed how light rays strike the cornea, and produced illusions of changing perspectives in The Last Supper. His ability to stand at the crossroads of the humanities and the sciences, made iconic by his drawing of Vitruvian Man, made him history’s most creative genius.\n\nIn the “luminous” (Daily Beast) Leonardo da Vinci, Isaacson describes how Leonardo’s delight at combining diverse passions remains the ultimate recipe for creativity. So, too, does his ease at being a bit of a misfit: illegitimate, gay, vegetarian, left-handed, easily distracted, and at times heretical. His life should remind us of the importance to be imaginative and, like talented rebels in any era, to think different. Here, da Vinci “comes to life in all his remarkable brilliance and oddity in Walter Isaacson’s ambitious new biography…a vigorous, insightful portrait” (The Washington Post)', 'images/978-1501139161.jpg', 1.5, 14.99, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501171383    , 'Einstein: His Life and Universe ', 51, '2007-04-10', 705, 4, 'The definitive, internationally bestselling biography of Albert Einstein. Now the basis of Genius, the ten-part National Geographic series on the life of Albert Einstein, starring the Oscar, Emmy, and Tony Award­–winning actor Geoffrey Rush as Einstein.\n\nHow did his mind work? What made him a genius? Isaacson’s biography shows how Einstein’s scientific imagination sprang from the rebellious nature of his personality. His fascinating story is a testament to the connection between creativity and freedom. Einstein explores how an imaginative, impertinent patent clerk—a struggling father in a difficult marriage who couldn’t get a teaching job or a doctorate—became the mind reader of the creator of the cosmos, the locksmith of the mysteries of the atom, and the universe. His success came from questioning conventional wisdom and marveling at mysteries that struck others as mundane. This led him to embrace a morality and politics based on respect for free minds, free spirits, and free individuals.\n\nEinstein, the classic #1 New York Times bestseller, is a brilliantly acclaimed account of the most influential scientist of the twentieth century, “an illuminating delight” (The New York Times). The basis for the National Geographic series Genius, by the author of The Innovators, Steve Jobs, and Benjamin Franklin, this is the definitive biography of Albert Einstei', 'images/978-1501171383.jpg', 1.6, 15.99, '2020-01-20', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780316323543    , 'Long Walk to Freedom: The Autobiography of Nelson Mandela', 52, '2008-03-11', 684, 4, 'Nelson Mandela was one of the great moral and political leaders of his time: an international hero whose lifelong dedication to the fight against racial oppression in South Africa won him the Nobel Peace Prize and the presidency of his country. After his triumphant release in 1990 from more than a quarter-century of imprisonment, Mandela was at the center of the most compelling and inspiring political drama in the world. As president of the African National Congress and head of South Africa\'s antiapartheid movement, he was instrumental in moving the nation toward multiracial government and majority rule. He is still revered everywhere as a vital force in the fight for human rights and racial equality.\n\nLong Walk to Freedom is his moving and exhilarating autobiography, destined to take its place among the finest memoirs of history\'s greatest figures. Here for the first time, Nelson Rolihlahla Mandela told the extraordinary story of his life -- an epic of struggle, setback, renewed hope, and ultimate triumph.\nThe book that inspired the major motion picture Mandela: Long Walk to Freedom.', 'images/978-0316323543.jpg', 1.5, 14.99, '2020-01-10', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781786033314    , 'Muhammad Ali', 53, '2019-02-07', 32, 4, 'When he was little, Muhammad Ali had his bicycle stolen. He wanted to fight the thief, but a policeman told him him to learn how to box first. After training hard in the gym, Muhammad developed a strong jab and an even stronger work ethic. His smart thinking and talking earned him the greatest title in boxing: Heavyweight Champion of the World. This moving book features stylish and quirky illustrations and extra facts at the back, including a biographical timeline with historical photos and a detailed profile of \"The Greatest\'s\" life.\n\nLittle People, BIG DREAMS is a best-selling series of books and educational games that explore the lives of outstanding people, from designers and artists to scientists and activists. All of them achieved incredible things, yet each began life as a child with a dream.\n\nThis empowering series offers inspiring messages to children of all ages, in a range of formats. The board books are told in simple sentences, perfect for reading aloud to babies and toddlers. The hardcover versions present expanded stories for beginning readers. Boxed gift sets allow you to collect a selection of the books by theme. Paper dolls, learning cards, matching games, and other fun learning tools provide even more ways to make the lives of these role models accessible to children.', 'images/978-1786033314.jpg', 1, 9.99, '2020-01-14', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780374201234    , 'The Mamba Mentality: How I Play', 54, '2018-10-23', 208, 4, 'In the wake of his retirement from professional basketball, Kobe “The Black Mamba” Bryant has decided to share his vast knowledge and understanding of the game to take readers on an unprecedented journey to the core of the legendary “Mamba mentality.” Citing an obligation and an opportunity to teach young players, hardcore fans, and devoted students of the game how to play it “the right way,” The Mamba Mentality takes us inside the mind of one of the most intelligent, analytical, and creative basketball players ever.\n\nFor the first time, and in his own words, Bryant reveals his famously detailed approach and the steps he took to prepare mentally and physically to not just succeed at the game, but to excel. Readers will learn how Bryant studied an opponent, how he channeled his passion for the game, how he played through injuries. They’ll also get fascinating granular detail as he breaks down specific plays and match-ups from throughout his career.\n\nBryant’s detailed accounts are paired with stunning photographs by the Hall of Fame photographer Andrew D. Bernstein. Bernstein, long the Lakers and NBA official photographer, captured Bryant’s very first NBA photo in 1996 and his last in 2016―and hundreds of thousands in between, the record of a unique, twenty-year relationship between one athlete and one photographer.\n\nThe combination of Bryant’s narrative and Bernstein’s photos make The Mamba Mentality an unprecedented look behind the curtain at the career of one of the world’s most celebrated and fascinating athletes.', 'images/978-0374201234.jpg', 1.8, 17.99, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781118503256    , 'The Warren Buffett Way', 55, '2013-09-20', 256, 4, 'Warren Buffett is the most famous investor of all time and one of today’s most admired business leaders. He became a billionaire and investment sage by looking at companies as businesses rather than prices on a stock screen. The first two editions of The Warren Buffett Way gave investors their first in-depth look at the innovative investment and business strategies behind Buffett’s spectacular success. The new edition updates readers on the latest investments by Buffett. And, more importantly, it draws on the new field of behavioral finance to explain how investors can overcome the common obstacles that prevent them from investing like Buffett. ', 'images/978-1118503256.jpg', 2.2, 21.99, '2020-01-16', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501158148    , 'What It Takes: Lessons in the Pursuit of Excellence', 56, '2019-09-17', 384, 4, 'People know who Stephen Schwarzman is—at least they think they do. He’s the man who took $400,000 and co-founded Blackstone, the investment firm that manages over $500 billion (as of January 2019). He’s the CEO whose views are sought by heads of state. He’s the billionaire philanthropist who founded Schwarzman Scholars, this century’s version of the Rhodes Scholarship, in China. But behind these achievements is a man who has spent his life learning and reflecting on what it takes to achieve excellence, make an impact, and live a life of consequence.', 'images/978-1501158148.jpg', 2.1, 20.99, '2020-01-17', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781250313577    , 'The Moment of Lift: How Empowering Women Changes the World', 57, '2019-04-23', 279, 4, 'For the last twenty years, Melinda Gates has been on a mission to find solutions for people with the most urgent needs, wherever they live. Throughout this journey, one thing has become increasingly clear to her: If you want to lift a society up, you need to stop keeping women down.\n\nIn this moving and compelling book, Melinda shares lessons she’s learned from the inspiring people she’s met during her work and travels around the world. As she writes in the introduction, “That is why I had to write this book―to share the stories of people who have given focus and urgency to my life. I want all of us to see ways we can lift women up where we live.”\n\nMelinda’s unforgettable narrative is backed by startling data as she presents the issues that most need our attention―from child marriage to lack of access to contraceptives to gender inequity in the workplace. And, for the first time, she writes about her personal life and the road to equality in her own marriage. Throughout, she shows how there has never been more opportunity to change the world―and ourselves.\n\nWriting with emotion, candor, and grace, she introduces us to remarkable women and shows the power of connecting with one another.\n\nWhen we lift others up, they lift us up, too.', 'images/978-1250313577.jpg', 1.5, 14.99, '2020-01-12', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501161933    , 'The Seven Husbands of Evelyn Hugo: A Novel', 58, '2017-06-13', 401, 4, 'Aging and reclusive Hollywood movie icon Evelyn Hugo is finally ready to tell the truth about her glamorous and scandalous life. But when she chooses unknown magazine reporter Monique Grant for the job, no one is more astounded than Monique herself. Why her? Why now?\r\n\r\nMonique is not exactly on top of the world. Her husband has left her, and her professional life is going nowhere. Regardless of why Evelyn has selected her to write her biography, Monique is determined to use this opportunity to jumpstart her career.\r\n\r\nSummoned to Evelyn’s luxurious apartment, Monique listens in fascination as the actress tells her story. From making her way to Los Angeles in the 1950s to her decision to leave show business in the ‘80s, and, of course, the seven husbands along the way, Evelyn unspools a tale of ruthless ambition, unexpected friendship, and a great forbidden love. Monique begins to feel a very real connection to the legendary star, but as Evelyn’s story near its concl', 'images/978-1501161933.jpg', 1.3, 12.99, '2020-01-19', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780465062881    , 'Endurance: Shackleton\'s Incredible Voyage', 26, '2015-04-29', 292, 4, 'In August 1914, polar explorer Ernest Shackleton boarded the Endurance and set sail for Antarctica, where he planned to cross the last uncharted continent on foot. In January 1915, after battling its way through a thousand miles of pack ice and only a day\'s sail short of its destination, the Endurance became locked in an island of ice. Thus began the legendary ordeal of Shackleton and his crew of twenty-seven men. When their ship was finally crushed between two ice floes, they attempted a near-impossible journey over 850 miles of the South Atlantic\'s heaviest seas to the closest outpost of civilization.', 'images/978-0465062881.jpg', 1.8, 17.99, '2020-01-18', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780143034759    , 'Alexander Hamilton', 59, '2005-03-29', 818, 4, 'Few figures in American history have been more hotly debated or more grossly misunderstood than Alexander Hamilton. Chernow’s biography gives Hamilton his due and sets the record straight, deftly illustrating that the political and economic greatness of today’s America is the result of Hamilton’s countless sacrifices to champion ideas that were often wildly disputed during his time. “To repudiate his legacy,” Chernow writes, “is, in many ways, to repudiate the modern world.” Chernow here recounts Hamilton’s turbulent life: an illegitimate, largely self-taught orphan from the Caribbean, he came out of nowhere to take America by storm, rising to become George Washington’s aide-de-camp in the Continental Army, coauthoring The Federalist Papers, founding the Bank of New York, leading the Federalist Party, and becoming the first Treasury Secretary of the United States.Historians have long told the story of America’s birth as the triumph of Jefferson’s democratic ideals over the aristocratic intentions of Hamilton. Chernow presents an entirely different man, whose legendary ambitions were motivated not merely by self-interest but by passionate patriotism and a stubborn will to build the foundations of American prosperity and power. His is a Hamilton far more human than we’ve encountered before—from his shame about his birth to his fiery aspirations, from his intimate relationships with childhood friends to his titanic feuds with Jefferson, Madison, Adams, Monroe, and Burr, and from his highly public affair with Maria Reynolds to his loving marriage to his loyal wife Eliza. And never before has there been a more vivid account of Hamilton’s famous and mysterious death in a duel with Aaron Burr in July of 1804.\nChernow’s biography is not just a portrait of Hamilton, but the story of America’s birth seen through its most central figure. At a critical time to look back to our roots, Alexander Hamilton will remind readers of the purpose of our institutions and our heritage as Americans', 'images/978-0143034759.jpg', 1.5, 14.99, '2020-01-16', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780199537822    , 'The Confessions', 60, '2008-08-14', 353, 4, 'In his own day the dominant personality of the Western Church, Augustine of Hippo today stands as perhaps the greatest thinker of Christian antiquity, and his Confessions is one of the great works of Western literature. In this intensely personal narrative, Augustine relates his rare ascent from a humble Algerian farm to the edge of the corridors of power at the imperial court in Milan, his struggle against the domination of his sexual nature, his renunciation of secular ambition and marriage, and the recovery of the faith his mother Monica had taught him during his childhood.', 'images/978-0199537822.jpg', 0.5, 4.99, '2020-01-15', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780141981253    , 'Churchill: Walking with Destiny', 61, '2018-10-04', 1044, 4, 'Winston Churchill towers over every other figure in twentieth-century British history. By the time of his death at the age of 90 in 1965, many thought him to be the greatest man in the world.\n\nThere have been over a thousand previous biographies of Churchill. Andrew Roberts now draws on over forty new sources, including the private diaries of King George VI, used in no previous Churchill biography to depict him more intimately and persuasively than any of its predecessors. The book in no way conceals Churchill\'s faults and it allows the reader to appreciate his virtues and character in full: his titanic capacity for work (and drink), his ability see the big picture, his willingness to take risks and insistence on being where the action was, his good humour even in the most desperate circumstances, the breadth and strength of his friendships and his extraordinary propensity to burst into tears at unexpected moments. Above all, it shows us the wellsprings of his personality - his lifelong desire to please his father (even long after his father\'s death) but aristocratic disdain for the opinions of almost everyone else, his love of the British Empire, his sense of history and its connection to the present.\n\nDuring the Second World War, Churchill summoned a particular scientist to see him several times for technical advice. \'It was the same whenever we met\', wrote the young man, \'I had a feeling of being recharged by a source of living power.\' Harry Hopkins, President Roosevelt\'s emissary, wrote \'Wherever he was, there was a battlefront.\' Field Marshal Sir Alan Brooke, Churchill\'s essential partner in strategy and most severe critic in private, wrote in his diary, \'I thank God I was given such an opportunity of working alongside such a man, and of having my eyes opened to the fact that occasionally supermen exist on this earth.\'', 'images/978-0141981253.jpg', 1.3, 12.99, '2020-01-20', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781982101213    , 'From the Ashes: My Story of Being Métis, Homeless, and Finding My Way', 62, '2019-08-06', 369, 4, 'From the Ashes is a remarkable memoir about hope and resilience, and a revelatory look into the life of a Métis-Cree man who refused to give up.\n\nAbandoned by his parents as a toddler, Jesse Thistle briefly found himself in the foster-care system with his two brothers, cut off from all they had known. Eventually the children landed in the home of their paternal grandparents, whose tough-love attitudes quickly resulted in conflicts. Throughout it all, the ghost of Jesse’s drug-addicted father haunted the halls of the house and the memories of every family member. Struggling with all that had happened, Jesse succumbed to a self-destructive cycle of drug and alcohol addiction and petty crime, spending more than a decade on and off the streets, often homeless. Finally, he realized he would die unless he turned his life around.\n\nIn this heart-warming and heart-wrenching memoir, Jesse Thistle writes honestly and fearlessly about his painful past, the abuse he endured, and how he uncovered the truth about his parents. Through sheer perseverance and education—and newfound love—he found his way back into the circle of his Indigenous culture and family.\n\nAn eloquent exploration of the impact of prejudice and racism, From the Ashes is, in the end, about how love and support can help us find happiness despite the odds', 'images/978-1982101213.jpg', 1.4, 13.99, '2020-01-09', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781538718469    , 'A Warning ', 63, '2019-10-19', 259, 4, 'On September 5, 2018, the New York Times published a bombshell essay and took the rare step of granting its writer anonymity. Described only as \"a senior official in the Trump administration,\" the author provided eyewitness insight into White House chaos, administration instability, and the people working to keep Donald Trump\'s reckless impulses in check.\n\nWith the 2020 election on the horizon, Anonymous is speaking out once again. In this book, the original author pulls back the curtain even further, offering a first-of-its-kind look at the president and his record -- a must-read before Election Day. It will surprise and challenge both Democrats and Republicans, motivate them to consider how we judge our nation\'s leaders, and illuminate the consequences of re-electing a commander in chief unfit for the role.\n\nThis book is a sobering assessment of the man in the Oval Office and a warning about something even more important -- who we are as a people', 'images/978-1538718469.jpg', 2, 19.99, '2020-01-08', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780399589652    , 'The Beautiful Ones', 64, '2019-10-29', 288, 4, 'Prince was a musical genius, one of the most beloved, accomplished, and acclaimed musicians of our time. He was a startlingly original visionary with an imagination deep enough to whip up whole worlds, from the sexy, gritty funk paradise of “Uptown” to the mythical landscape of Purple Rain to the psychedelia of “Paisley Park.” But his most ambitious creative act was turning Prince Rogers Nelson, born in Minnesota, into Prince, one of the greatest pop stars of any era.\n\nThe Beautiful Ones is the story of how Prince became Prince—a first-person account of a kid absorbing the world around him and then creating a persona, an artistic vision, and a life, before the hits and fame that would come to define him. The book is told in four parts. The first is the memoir Prince was writing before his tragic death, pages that bring us into his childhood world through his own lyrical prose. The second part takes us through Prince’s early years as a musician, before his first album was released, via an evocative scrapbook of writing and photos. The third section shows us Prince’s evolution through candid images that go up to the cusp of his greatest achievement, which we see in the book’s fourth section: his original handwritten treatment for Purple Rain—the final stage in Prince’s self-creation, where he retells the autobiography of the first three parts as a heroic journey.\n\nThe book is framed by editor Dan Piepenbring’s riveting and moving introduction about his profound collaboration with Prince in his final months—a time when Prince was thinking deeply about how to reveal more of himself and his ideas to the world, while retaining the mystery and mystique he’d so carefully cultivated—and annotations that provide context to the book’s images.\n\nThis work is not just a tribute to an icon, but an original and energizing literary work in its own right, full of Prince’s ideas and vision, his voice and image—his undying gift to the world.', 'images/978-0399589652.jpg', 0.52, 5.21, '2020-01-11', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501108587    , 'The Sleeping Beauty Killer', 51, '2016-11-15', 321, 5, 'Fifteen years after being convicted of murdering her fiancé—the famed philanthropist Hunter Raleigh III—Casey Carter is determined to clear her name. Though she has served her time, she finds that she is still living under suspicion. Going on the true crime show Under Suspicion seems to be her only hope to prove her innocence.\r\n\r\nThe show’s producer, Laurie Moran, also believes in her innocence and wants to help Casey. But with Alex Buckley taking a break from the show—cooling his potential romance with Laurie—Under Suspicion introduces a new on-air host in Ryan Nichols, a hot shot legal whiz with a Harvard Law degree, a Supreme Court clerkship, experience as a federal prosecutor. Ryan has no problems with steering the show, and even tries to stop Laurie from taking on Casey’s case because he’s so certain she’s guilty.\r\n\r\nAn egomaniacal new boss, a relentless gossip columnist, and Casey’s longstanding bad reputation: Laurie must face this and more to do what she believes is right, to once and for all prove Casey’s innocence—that is, if she’s innocent.', 'images/978-1501108587.jpg', 0.9, 8.99, '2020-01-21', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501108563    , 'All Dressed In White', 51, '2015-11-17', 289, 5, 'Five years ago, Amanda Pierce was excitedly preparing to marry her college sweetheart. She and Jeffrey had already battled through sickness and health, although their livelihoods looked set as Amanda was due to inherit her father’s successful garment company.\r\n\r\nThen Amanda disappeared the night of her bachelorette party.\r\n\r\nIn present-day New York, Laurie Moran realizes a missing bride is the perfect cold case for her Under Suspicion television series to investigate. By recreating the night of the disappearance at the wedding’s Florida resort with Amanda’s friends and family, Laurie hopes to solve the case. Laurie and her Under Suspicion host Alex Buckley soon find themselves overwhelmed with theories and rumors about the “beloved” bride from those who were involved with the wedding, including Amanda’s former fiancé, a jealous sister, and plenty of playboy groomsmen.\r\n\r\nOne thing is certain, whoever was behind Amanda’s vanishing plans to keep the truth hidden “until death do they part.”', 'images/978-1501108563.jpg', 1.1, 10.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501171734    , 'Every Breath You Take', 51, '2017-11-07', 305, 5, 'Laurie Moran’s professional life is a success—her television show Under Suspicion is a hit, both in the ratings and its record of solving cold cases. But her romantic break from former host Alex Buckley has left her with on-air talent she can’t stand—Ryan Nichols—and a crippling sense of loneliness.\r\n\r\nNow Ryan has suggested a new case. Three years ago, Virginia Wakeling, a member of the Board of Trustees of the Metropolitan Museum of Art and one of the museum’s most generous donors, was found dead in the snow, after being thrown from the museum’s roof on the night of the Met Gala. The leading suspect then and now is her much younger boyfriend and personal trainer, Ivan Gray.\r\n\r\nIvan runs a trendy, successful boutique gym called Punch—a business funded in no small part by the late Virginia—which happens to be the gym Ryan frequents. Laurie’s skepticism about the case is upended by a tip from her father’s NYPD connection, and soon Laurie realizes there are a bevy of suspects—including Virginia’s trusted inner circle.\r\n\r\nAs the Under Suspicion crew pries into the lives of a super wealthy real estate family with secrets to hide, danger mounts for several witnesses—and for Laurie.', 'images/978-1501171734.jpg', 1, 9.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781476763699    , 'The Cinderella Murder', 51, '2014-11-18', 401, 5, 'Television producer Laurie Moran is delighted when the pilot for her reality drama, Under Suspicion, is a success. Even more, the program—a cold case series that revisits unsolved crimes by recreating them with those affected—is off to a fantastic start when it helps solve an infamous murder in the very first episode.\r\n\r\nNow Laurie has the ideal case to feature in the next episode of Under Suspicion: the Cinderella Murder. When Susan Dempsey, a beautiful and multi-talented UCLA student, was found dead, her murder raised numerous questions. Why was her car parked miles from her body? Had she ever shown up for the acting audition she was due to attend at the home of an up-and-coming director? Why does Susan’s boyfriend want to avoid questions about their relationship? Was her disappearance connected to a controversial church? Was she close to her computer science professor because of her technological brilliance, or something more? And why was Susan missing one of her shoes when her body was discovered?\r\n\r\nWith the help of Under Suspicion host Alex Buckley, Laurie knows the case will attract great ratings, especially when the former suspects include Hollywood’s elite and tech billionaires. The suspense and drama are perfect for the silver screen—but is Cinderella’s murderer ready for a close-up?', 'images/978-1476763699.jpg', 0.8, 7.99, '2020-02-23', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781501171666    , 'You Don\'t Own Me', 51, '2018-11-16', 289, 5, 'Television producer Laurie Moran recently became engaged to her investigative television show’s former host, Alex Buckley, and since then, the two have been happily planning a summer wedding, preparing for Alex’s confirmation to a federal judicial appointment, and searching for the perfect New York City home for their new life together.\r\n\r\nBut then Laurie is approached by Robert and Cynthia Bell, parents of Dr. Martin Bell, a physician who was shot dead as he pulled into the driveway of his Greenwich Village carriage house five years ago. The Bells are sure that Martin’s disgraced and erratic wife, Kendra, carried out the murder. Determined to prove Kendra’s guilt and win custody over their grandchildren, they plead with Laurie to feature their son’s case on Under Suspicion, ensuring her that Kendra is willing to cooperate.\r\n\r\nAs Laurie dives into the case, she learns that Martin wasn’t the picture-perfect husband, father, and doctor he appeared to be and was carrying secrets of his own. And what does the web of lies ensnaring the Bell family have to do with a dangerous stranger, who gazes at Laurie from afar and thinks, She is actually quite a lovely girl, I’m sure she’s going to be missed…?', 'images/978-1501171666.jpg', 0.7, 7.01, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073471    , 'And Then There Were None', 65, '2009-03-17', 258, 5, 'Ten people, each with something to hide and something to fear, are invited to a isolated mansion on Indian Island by a host who, surprisingly, fails to appear. On the island they are cut off from everything but each other and the inescapable shadows of their own past lives. One by one, the guests share the darkest secrets of their wicked pasts. And one by one, they die…', 'images/978-0062073471.jpg', 1, 9.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073495    , 'Murder on the Orient Express', 66, '2019-02-25', 260, 5, 'Just after midnight, the famous Orient Express is stopped in its tracks by a snowdrift. By morning, the millionaire Samuel Edward Ratchett lies dead in his compartment, stabbed a dozen times, his door locked from the inside. Without a shred of doubt, one of his fellow passengers is the murderer.\r\n\r\nIsolated by the storm, detective Hercule Poirot must find the killer among a dozen of the dead man’s enemies, before the murderer decides to strike again.', 'images/978-0062073495.jpg', 0.3, 2.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073556    , 'Death on the Nile', 66, '2019-03-06', 436, 5, 'The tranquility of a cruise along the Nile was shattered by the discovery that Linnet Ridgeway had been shot through the head. She was young, stylish, and beautiful. A girl who had everything . . . until she lost her life.\r\n\r\nHercule Poirot recalled an earlier outburst by a fellow passenger: \"I\'d like to put my dear little pistol against her head and just press the trigger.\" Yet in this exotic setting nothing is ever quite what it seems.', 'images/978-0062073556.jpg', 0.3, 2.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073563    , 'The Murder of Roger Ackroyd', 66, '2019-05-16', 349, 5, 'Poirot retires to a village near the home of a friend he met in London, Roger Ackroyd, who agrees to keep him anonymous, as he pursues his retirement project of perfecting vegetable marrows. He is not long at this pursuit when his friend is murdered. Ackroyd\'s niece calls Poirot in to ensure that the guilt does not fall on Ackroyd\'s stepson; Poirot promises to find the truth, which she accepts.', 'images/978-0062073563.jpg', 0.28, 2.79, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073570    , 'Five Little Pigs', 66, '2019-05-24', 362, 5, 'Beautiful Caroline Crale was convicted of poisoning her husband, but just like the nursery rhyme, there were five other “little pigs” who could have done it: Philip Blake (the stockbroker), who went to market; Meredith Blake (the amateur herbalist), who stayed at home; Elsa Greer (the three-time divorcée), who had her roast beef; Cecilia Williams (the devoted governess), who had none; and Angela Warren (the disfigured sister), who cried all the way home.\r\n\r\nSixteen years later, Caroline’s daughter is determined to prove her mother’s innocence, and Poirot just can’t get that nursery rhyme out of his mind.', 'images/978-0062073570.jpg', 0.32, 3.19, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062074096    , 'Curtain: Poirot\'s Last Case', 66, '2019-05-18', 277, 5, 'A wheelchair-bound Poirot returns to Styles, the venue of his first investigation, where he knows another murder is going to take place...', 'images/978-0062074096.jpg', 0.32, 3.19, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073907    , 'Murder in Mesopotamia', 65, '2009-03-17', 272, 5, 'Amy Leatheram has never felt the lure of the mysterious East, but when she travels to an ancient site deep in the Iraqi desert to nurse the wife of a celebrated archaeologist, events prove stranger than she could ever have imagined. Her patient\'s bizarre visions and nervous terror seem unfounded, but as the oppressive tension in the air thickens, events come to a terrible climax--in murder.\r\n\r\nWith one spot of blood as his only clue, Hercule Poirot must embark on a journey not just across the desert, but into the darkest crevices of the human soul to unravel a mystery which taxes even his remarkable powers.', 'images/978-0062073907.jpg', 1.2, 11.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073747    , 'Death in the Clouds', 65, '2010-02-10', 240, 5, 'From seat No. 9, Hercule Poirot was ideally placed to observe his fellow air passengers on the short flight from Paris to London. Over to his right sat a pretty young woman, clearly infatuated with the man opposite; ahead, in seat No. 13, sat a countess with a poorly concealed cocaine habit; across the gangway in seat No. 8, a writer of detective fiction was being troubled by an aggressive wasp.\n\nYes, Poirot is almost ideally placed to take it all in, except what he did not yet realize was that behind him, in seat No. 2, sat the slumped, lifeless body of a woman. Murdered, and likely by someone in Poirot’s immediate proximity. ', 'images/978-0062073747.jpg', 1.2, 11.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073938    , 'Evil Under the Sun', 65, '2006-10-03', 275, 5, 'The beautiful bronzed body of Arlena Stuart lay face down on the beach. But strangely, there was no sun and Arlena was not sunbathing…she had been strangled.\r\n\r\nEver since Arlena’s arrival the air had been thick with sexual tension. Each of the guests had a motive to kill her, including Arlena’s new husband. But Hercule Poirot suspects that this apparent “crime of passion” conceals something much more evil.', 'images/978-0062073938.jpg', 1.2, 11.99, '2020-01-21', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073730    , 'Cards on the Table', 65, '2003-12-15', 336, 5, 'Mr. Shaitana is famous as a flamboyant party host. Nevertheless, he is a man of whom everybody is a little afraid. So when he boasts to Hercule Poirot that he considers murder an art form, the detective has some reservations about accepting a party invitation to view Shaitana’s “private collection.”\r\n\r\nIndeed, what begins as an absorbing evening of bridge is to turn into a more dangerous game altogether.', 'images/978-0062073730.jpg', 1.2, 11.99, '2020-03-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780062073587    , 'The ABC Murders', 66, '2019-04-30', 272, 5, 'There’s a serial killer on the loose, working his way through the alphabet and the whole country is in a state of panic.\r\n\r\nA is for Mrs. Ascher in Andover, B is for Betty Barnard in Bexhill, C is for Sir Carmichael Clarke in Churston. With each murder, the killer is getting more confident—but leaving a trail of deliberate clues to taunt the proud Hercule Poirot might just prove to be the first, and fatal, mistake.', 'images/978-0062073587.jpg', 0.32, 3.19, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780006913368    , 'The Clue of the Dancing Puppet', 67, '1962-02-01', 202, 5, 'When the eerie performances of a life-size puppet begin to haunt the old Van Pelt estate, where an amateur acting group – The Footlighters – have their theater, Nancy Drew is called upon to unravel the baffling mystery. From the moment the detective and her friends Bess and George arrive at the mansion, the dancing puppet puzzle is further complicated by Tammi Whitlock, the Footlighters’ temperamental leading lady, and Emmet Calhoun, a Shakespearean actor.\r\n\r\nNancy’s search of the mansion’s dark, musty attic for clues to the weird mystery starts a frightening chain reaction. A phone call from a stranger with a witchlike, cackling voice warns her to “Get out!” Next an encounter with two jewel theft suspects adds another perplexing angle to the puzzle. When Nancy finally sees the life-size puppet flitting across the moonlit lawn and chases it, she learns that someone with a sinister motive is determined to keep her form solving the case. Is it one of the Footlighters? Or is it an outsider?', 'images/978-0006913368.jpg', 1, 9.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780006913375    , 'The Spider Sapphire Mystery', 67, '1967-12-01', 202, 5, 'Thrilling, dangerous adventures confront Nancy Drew while on a safari in East Africa with a group of American college students. Excitement runs high as the teen-age detective delves into the theft of a fabulous sapphire formed by nature millions of years ago. The mystery starts in Nancy’s home town. Her lawyer father’s client, Floyd Ramsay, who fashions beautiful and unusual synthetic gems, is accused of stealing the magnificent spider sapphire and exhibiting it as his own creation. Mr. Ramsay’s enemies blackmail him and by their vicious acts try to deter Nancy from going on the safari.', 'images/978-0006913375.jpg', 1, 9.99, '2020-01-21', 1);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781101077474    , 'The Invisible Intruder', 67, '1969-01-01', 200, 5, '“Nancy Drew, forget the ghost hunt!” a male voice rasps on the telephone. Despite the mysterious warning, the teenage detective and a group of friends start out on a ghost-hunting expedition to investigate five places reputed to be haunted. Danger strikes at once when Nancy tries to overtake the canoe that paddles itself on Lake Sevanee. Thrills and chills mount as the ghost hunters pursue a phantom horse and ghost rider racing across the field that surrounds the Red Barn Guesthouse. During these happenings and other weird events Nancy finds herself pitted against a dangerous adversary, clever enough to operate invisibly.', 'images/978-1101077474.jpg', 1, 9.99, '2020-02-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9780006915751    , 'The Clue of the Black Keys', 67, '1951-01-01', 200, 5, 'Terry Scott, a young archaeology professor, seeks Nancy’s help in unearthing a secret of antiquity which can only be unlocked by three black keys. While on an archaeological expedition in Mexico, Terry and Dr. Joshua Pitt came across a clue to buried treasure. The clue was a cipher carved on a stone tablet. Before the professor had time to translate the cipher, the tablet disappeared – along with Dr. Pitt! Terry tells Nancy of his suspicions of the Tinos, a Mexican couple posing as scientists who vanished the same night as Dr. Pitt. Nancy and her friends follow a tangled trail of clues that lead to the Florida Keys and finally to Mexico in this suspense-filled story that will thrill readers.', 'images/978-0006915751.jpg', 1, 9.99, '2020-01-21', 0);
INSERT INTO `ga2w20`.`book` (`ISBN`, `title`, `publisherId`, `publicationDate`, `numberOfPages`, `genreId`, `description`, `imageFile`, `wholeSalePrice`, `listPrice`, `enteredDate`, `removalStatus`) VALUES (9781484200773    , 'Pro Git 2nd Edition', 68, '2014-11-09', 419, 1, 'Pro Git (Second Edition) is your fully-updated guide to Git and its usage in the modern world. Git has come a long way since it was first developed by Linus Torvalds for Linux kernel development. It has taken the open source world by storm since its inception in 2005, and this book teaches you how to use it like a pro.\n\nEffective and well-implemented version control is a necessity for successful web projects, whether large or small. With this book you’ll learn how to master the world of distributed version workflow, use the distributed features of Git to the full, and extend Git to meet your every need.\n\nWritten by Git pros Scott Chacon and Ben Straub, Pro Git (Second Edition) builds on the hugely successful first edition, and is now fully updated for Git version 2.0, as well as including an indispensable chapter on GitHub. It’s the best book for all your Git needs.', 'images/978-1484200773.jpg', 5.93, 59.28, '2020-02-10', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`book_author`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780735611313, 1);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780262510875, 2);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780262510875, 3);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780997316025, 4);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780132350884, 5);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780198733461, 6);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781848000698, 7);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781523502776, 8);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781523502776, 9);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780134076423, 10);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780134076423, 11);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781775125402, 12);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781438009193, 13);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780132569033, 14);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781284155617, 15);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781284155617, 16);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781680502688, 17);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781680502688, 18);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781680502688, 19);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9790692106715, 20);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781454926214, 22);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781676140252, 21);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781676140252, 23);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780521876582, 24);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781337685931, 25);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781337685931, 26);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781465473608, 27);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781465475855, 27);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780767908184, 28);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780195139570, 29);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780393283037, 30);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780393283037, 31);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542382595, 32);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781647483753, 33);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780143122029, 34);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781770850903, 35);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781541672529, 36);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781599869773, 37);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780670034574, 38);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781446301432, 35);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781770855878, 35);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780752457710, 35);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781541618114, 39);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780226805368, 41);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780226805368, 40);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780674971455, 42);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781101980972, 43);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780061138829, 44);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780717802418, 45);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780717802418, 46);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542047487, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542049337, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542049665, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542049498, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542049153, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781542049467, 47);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9789176371824, 48);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781723096945, 49);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780191606335, 50);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780191606335, 51);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780099577282, 52);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780199682867, 52);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9788026881797, 53);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9785006800173, 54);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9788003381693, 55);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780812989052, 56);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9788072366185, 57);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9788074267149, 58);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781916011168, 59);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781474031363, 59);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9788077968719, 60);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062018090, 61);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062018090, 62);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780553296983, 63);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501135927, 64);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780593170984, 65);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501139161, 66);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501171383, 66);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780316323543, 67);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781786033314, 68);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781786033314, 69);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780374201234, 70);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781118503256, 71);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501158148, 72);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781250313577, 73);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501161933, 74);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780465062881, 75);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780143034759, 76);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780199537822, 77);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780141981253, 78);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781982101213, 79);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781538718469, 80);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780399589652, 81);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501108587, 83);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501108587, 82);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501108563, 82);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501108563, 83);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501171734, 82);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501171734, 83);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781476763699, 82);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781476763699, 83);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501171666, 82);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781501171666, 83);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073471, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073495, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073556, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073563, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073570, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062074096, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073907, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073747, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073938, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073730, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780062073587, 84);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780006913368, 85);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780006913375, 85);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781101077474, 85);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9780006915751, 85);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781484200773, 86);
INSERT INTO `ga2w20`.`book_author` (`ISBN`, `authorId`) VALUES (9781484200773, 87);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`book_format`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780735611313, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780735611313, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780262510875, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780262510875, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780997316025, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780997316025, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780132350884, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780132350884, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780198733461, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780198733461, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781848000698, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781848000698, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781848000698, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781848000698, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781523502776, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781523502776, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781523502776, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780134076423, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780134076423, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781775125402, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781775125402, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781438009193, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781438009193, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780132569033, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780132569033, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781284155617, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781284155617, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781284155617, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781680502688, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781680502688, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9790692106715, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9790692106715, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781454926214, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781454926214, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781676140252, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781676140252, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780521876582, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780521876582, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781337685931, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781337685931, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781465473608, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781465473608, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781465475855, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781465475855, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780767908184, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780767908184, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780195139570, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780195139570, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780393283037, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542382595, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542382595, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781647483753, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781647483753, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780143122029, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780143122029, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781770850903, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781770850903, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781541672529, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781541672529, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781599869773, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781599869773, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780670034574, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780670034574, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781446301432, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781446301432, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781770855878, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781770855878, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780752457710, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780752457710, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781541618114, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781541618114, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780226805368, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780226805368, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780674971455, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780674971455, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781101980972, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781101980972, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780061138829, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780061138829, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780061138829, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780717802418, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780717802418, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542047487, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542047487, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049337, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049337, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049665, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049665, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049498, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049153, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049153, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049467, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049467, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781542049467, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9789176371824, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9789176371824, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781723096945, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781723096945, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780191606335, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780191606335, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780191606335, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780099577282, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780099577282, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780199682867, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780199682867, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788026881797, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788026881797, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9785006800173, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9785006800173, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9785006800173, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788003381693, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788003381693, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780812989052, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780812989052, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788072366185, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788072366185, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788074267149, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788074267149, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781916011168, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781916011168, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781474031363, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781474031363, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788077968719, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9788077968719, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062018090, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062018090, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780553296983, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780553296983, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501135927, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501135927, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780593170984, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780593170984, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501139161, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171383, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171383, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780316323543, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780316323543, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781786033314, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781786033314, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780374201234, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780374201234, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781118503256, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781118503256, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501158148, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501158148, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781250313577, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781250313577, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501161933, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501161933, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780465062881, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780465062881, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780143034759, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780143034759, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780199537822, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780199537822, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780141981253, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780141981253, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780141981253, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780141981253, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781982101213, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781982101213, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781538718469, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781538718469, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781538718469, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780399589652, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780399589652, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501108587, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501108563, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501108563, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171734, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171734, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781476763699, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781476763699, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781476763699, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171666, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781501171666, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073471, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073471, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073471, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073495, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073495, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073556, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073556, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073556, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073563, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073563, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073570, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073570, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062074096, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062074096, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073907, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073907, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073747, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073747, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073938, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073938, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073730, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073730, 2);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073730, 3);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073587, 5);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780062073587, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006913368, 6);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006913368, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006913375, 7);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006913375, 8);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781101077474, 9);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781101077474, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006915751, 10);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9780006915751, 4);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781484200773, 1);
INSERT INTO `ga2w20`.`book_format` (`ISBN`, `formatId`) VALUES (9781484200773, 4);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`province`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (1, 'Alberta', 5, 0, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (2, 'British Columbia', 5, 7, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (3, 'Manitoba', 5, 7, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (4, 'New Brunswick', 0, 0, 15);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (5, 'Newfoundland and Labrador', 0, 0, 15);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (6, 'Northwest Territories', 5, 0, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (7, 'Nova Scotia', 0, 0, 15);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (8, 'Nunavut', 5, 0, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (9, 'Ontario', 0, 0, 13);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (10, 'Prince Edward Island', 0, 0, 15);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (11, 'Quebec', 5, 9.975, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (12, 'Saskatchewan', 5, 6, 0);
INSERT INTO `ga2w20`.`province` (`id`, `provinceName`, `GSTpercentage`, `PSTpercentage`, `HSTpercentage`) VALUES (13, 'Yukon', 5, 0, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`client`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (1, 'mr', 'Dawson', 'Jack', '3040 Sherbrooke St W', NULL, 'Montreal', 11, 'Canada', 'H3Z1A4', '12345678910', 'Qual-e-tree', '15141234567', 'cst.receive@gmail.com', 'd13270824e5b62e8451c470b2038271fecbdf05e8cff65976d2a17f6c5a2ecd4', 'VgyhmWZ9J0yyuHZ48xXK4dXaCCZjjfGTUQ+uQDVk+1c=', 1);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (2, 'ms', 'Dallas', 'Lilu', '75 Laurier Ave E', NULL, 'Ottawa', 9, 'Canada', 'K1N6N5', '98765432110', 'SomeCompany', '16130012345', 'cst.send@gmail.com', 'e7d748a1e34cadffbd4d64ce185b12515aa769f5109e20c2bc8c4d655fd9ca13', 'gRIwSfzC9CGTldPne9MIWiJEoYXM0maDwUWSkH7RpLc=', 0);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (3, 'mr', 'Smith', 'Tom', '54 Saint-Paul rue', NULL, 'Montreal', 11, 'Canada', 'Q2W3E4', '76876546478', NULL, '12345654336', 'Smith@gmail.com', 'dfce3647d94393466048fcf0c45b5b5183de65869609a6eb7ccab475fe684746', '8ZJQy2dr+0/a1lVXNUD9Zygdmi2sNyhyIwVGEePT3Dc=', 0);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (4, 'ms', 'Pit', 'Suzan', '73 St. Denis rue', NULL, 'Montreal', 11, 'Canada', 'T6Y7U8', '86487568445', 'Qual-e-tree', '64536736256', 'Pit@gmail.com', '67565f224fb20b5f65ddd3c2fb4ec025e93a28f6c3bd5f022a43c707a99e9549', 'Zc8NJa560Hdi69eV2TR+97Abc6adFpoYANSse9tWZZw=', 1);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (5, 'mr', 'Forest', 'John', '656 Villeray Ave', NULL, 'Calgary', 1, 'Canada', 'A2S3D4', '76576576243', 'CompanyAAA', '76655346786', 'Forest@gmail.com', 'f66d246041b82accede7a436d68b56831de1fc08c7fad5f25ba2dac0b6e2dbb5', 'VN1DjVPba7jIre2G61znn9hV82Ju0Tut90M4y/EXKCU=', 0);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (6, 'mr', 'Tree', 'Sam', '97 St. Katerin', NULL, 'Kensington', 10, 'Canada', 'Z3X4C5', '87686765745', 'CompanyBBB', '76543456765', 'Tree@gmail.com', 'aea0c53d3d0f6e3ddb9f2ac697e7a60d36263a1684dd6f0200c55f55820d7ce3', 'HKwWveiEj2iAJ7Q6rAQyNhsITcJ6JRsHLmwI+1qARoY=', 0);
INSERT INTO `ga2w20`.`client` (`id`, `title`, `lastName`, `firstName`, `address1`, `address2`, `city`, `provinceId`, `country`, `postalCode`, `homeTelephone`, `companyName`, `cellTelephone`, `email`, `password`, `salt`, `manager`) VALUES (7, 'ms', 'Glass', 'Julia', '908 Escarpment st', NULL, 'Winkler', 3, 'Canada', 'V4N6M8', '87687667543', 'CompanyCCC', '68y86465464', 'Glass@gmail.com', '5fe1bb14ff964ad8c47f8fd3f503a215ea882c2e1e6a8c1e2cdf1fccbcbf9a03', 'nfXURhpc35sPFmGHs3bLwpIdjkcCgmtuSIOAA0C/e3c=', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`order`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (1, 1, '2020-02-01', 79.74, 6.92, 3.47, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (2, 2, '2020-02-02', 74.41, 0.00, 0.00, 8.56, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (3, 1, '2020-02-03', 75.71, 6.57, 3.29, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (4, 2, '2020-02-04', 23.89, 0.00, 0.00, 2.75, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (5, 1, '2020-02-04', 18.37, 1.59, 0.80, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (6, 2, '2020-02-05', 19.20, 0.00, 0.00, 2.21, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (7, 3, '2020-02-28', 45.99, 3.99, 2.00, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (8, 3, '2020-03-01', 11.49, 1.00, 0.50, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (9, 3, '2020-03-08', 2.35, 0.20, 0.10, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (10, 4, '2020-02-17', 11.49, 1.00, 0.50, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (11, 4, '2020-03-01', 8.06, 0.70, 0.35, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (12, 4, '2020-03-04', 12.64, 1.10, 0.55, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (13, 5, '2020-02-17', 11.54, 0.00, 0.55, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (14, 5, '2020-02-27', 42.00, 0.00, 2.00, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (15, 5, '2020-03-09', 20.99, 0.00, 1.00, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (16, 6, '2020-02-10', 3.44, 0.00, 0.00, 0.45, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (17, 6, '2020-02-14', 17.24, 0.00, 0.00, 2.25, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (18, 6, '2020-02-17', 32.19, 0.00, 0.00, 4.20, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (19, 7, '2020-02-27', 16.79, 1.05, 0.75, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (20, 7, '2020-02-29', 11.19, 0.70, 0.50, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (21, 7, '2020-03-08', 16.12, 1.01, 0.72, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (22, 6, '2020-03-01', 7.97, 0.00, 0.00, 1.04, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (23, 6, '2020-03-03', 73.59, 0.00, 0.00, 9.60, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (24, 6, '2020-03-05', 47.35, 0.00, 0.00, 6.18, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (25, 6, '2020-03-06', 20.67, 0.00, 0.00, 2.70, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (26, 6, '2020-03-07', 18.16, 0.00, 0.00, 2.37, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (27, 6, '2020-03-08', 29.04, 0.00, 0.00, 3.79, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (28, 7, '2020-03-02', 0.00, 0.00, 0.00, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (29, 7, '2020-03-08', 1.12, 0.07, 0.05, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (30, 7, '2020-03-11', 16.79, 1.05, 0.75, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (31, 7, '2020-03-16', 13.43, 0.84, 0.60, 0.00, 0);
INSERT INTO `ga2w20`.`order` (`id`, `clientId`, `orderDate`, `totalPrice`, `totalPST`, `totalGST`, `totalHST`, `removalStatus`) VALUES (32, 7, '2020-03-17', 11.19, 0.70, 0.50, 0.00, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`order_book`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (1, 9780735611313    , 14.39, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (1, 9781465475855    , 27.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (1, 9781542047487    , 0, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (1, 9780062018090    , 17.98, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (1, 9781501108587    , 8.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (2, 9780262510875    , 40, 0, 0, 13, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (2, 9780997316025    , 25.85, 0, 0, 13, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (3, 9780262510875    , 40, 9.975, 5, 0, 1);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (3, 9780997316025    , 25.85, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (4, 9780061138829    , 21.14, 0, 0, 13, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (5, 9780191606335    , 5.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (5, 9780099577282    , 9.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (6, 9780593170984    , 16.99, 0, 0, 13, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (7, 9780262510875    , 40, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (8, 9780767908184    , 9.99, 9.975, 5, 0, 1);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (9, 9780717802418    , 2.04, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (10, 9781786033314    , 9.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (11, 9781501171666    , 7.01, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (12, 9781501108563    , 10.99, 9.975, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (13, 9781501108563    , 10.99, 0, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (14, 9780262510875    , 40, 0, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (15, 9781538718469    , 19.99, 0, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (16, 9781916011168    , 2.99, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (17, 9781501135927    , 14.99, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (18, 9781465475855    , 27.99, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (19, 9781250313577    , 14.99, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (20, 9780767908184    , 9.99, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (21, 9780735611313    , 14.39, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (22, 9780198733461    , 6.93, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (23, 9780134076423    , 63.99, 0, 0, 15, 1);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (24, 9781775125402    , 41.17, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (25, 9781542382595    , 17.97, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (26, 9780143122029    , 15.79, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (27, 9780674971455    , 25.25, 0, 0, 15, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (28, 9781542049337    , 0, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (29, 9789176371824    , 1, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (30, 9780143034759    , 14.99, 7, 5, 0, 0);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (31, 9780062073747    , 11.99, 7, 5, 0, 1);
INSERT INTO `ga2w20`.`order_book` (`orderId`, `ISBN`, `price`, `PSTpercentage`, `GSTpercentage`, `HSTpercentage`, `removalStatus`) VALUES (32, 9780062073471    , 9.99, 7, 5, 0, 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`review`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780735611313, 4, 1, '2020-02-08', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780062018090, 5, 1, '2020-02-09', 'I found this book to be very well thought out and well written.');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9780262510875, 2, 1, '2020-02-10', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780262510875, 4, 1, '2020-02-11', 'it is not bad, but still missing something');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9780593170984, 4, 0, '2020-02-11', 'good book for reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9780735611313, 1, 0, '2020-02-10', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9780262510875, 2, 1, '2020-02-11', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9780997316025, 3, 0, '2020-02-11', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780132350884, 4, 0, '2020-02-11', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9780198733461, 5, 1, '2020-02-10', 'great book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9781446301432, 1, 1, '2020-02-09', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9781770855878, 2, 1, '2020-02-11', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9780752457710, 3, 1, '2020-02-11', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9781541618114, 4, 1, '2020-02-09', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780226805368, 5, 1, '2020-02-11', 'gread book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9788026881797, 1, 1, '2020-02-10', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9785006800173, 2, 0, '2020-02-09', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9788003381693, 3, 0, '2020-02-09', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9780812989052, 4, 0, '2020-02-09', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9788072366185, 1, 1, '2020-02-09', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9781250313577, 2, 1, '2020-02-09', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9781501161933, 3, 1, '2020-02-09', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9780465062881, 4, 1, '2020-02-09', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9780143034759, 1, 1, '2020-02-09', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9780199537822, 2, 1, '2020-02-10', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9781465475855, 3, 1, '2020-02-10', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9780767908184, 4, 1, '2020-02-10', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9780195139570, 5, 0, '2020-02-08', 'great book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9780393283037, 1, 0, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9781542382595, 2, 0, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9781647483753, 3, 1, '2020-02-08', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9781542049337, 4, 1, '2020-02-08', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9781542049665, 5, 1, '2020-02-08', 'gread book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9781542049498, 1, 1, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9781542049153, 2, 1, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9781542049467, 3, 1, '2020-02-08', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9780553296983, 4, 1, '2020-02-10', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9780593170984, 5, 1, '2020-02-08', 'great book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9781501139161, 1, 1, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9781501171383, 2, 1, '2020-02-10', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (3, 9781501108563, 3, 1, '2020-02-08', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (4, 9781501171734, 4, 1, '2020-02-08', 'good book');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9781501171666, 5, 1, '2020-02-08', 'great');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (6, 9780062073471, 1, 1, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9780062073471, 2, 1, '2020-02-08', 'the book is not worth reading');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (5, 9781501171734, 3, 1, '2020-02-08', 'so-so');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (1, 9781542049467, 4, 1, '2020-02-10', 'good gook');
INSERT INTO `ga2w20`.`review` (`clientId`, `ISBN`, `rating`, `approved`, `reviewDate`, `reviewText`) VALUES (2, 9781647483753, 5, 1, '2020-02-08', 'great book');

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`special_book`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (1, 9780262510875, '2020-01-22', '2020-05-22', 20);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (2, 9780198733461, '2020-01-12', '2020-05-12', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (3, 9781438009193, '2020-01-17', '2020-05-17', 25);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (4, 9781676140252, '2020-01-22', '2020-05-22', 17);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (5, 9781542382595, '2020-01-15', '2020-05-15', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (6, 9781770855878, '2020-01-03', '2020-05-03', 17);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (7, 9780226805368, '2020-01-09', '2020-05-09', 14);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (8, 9780061138829, '2020-01-15', '2020-05-15', 20);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (9, 9780099577282, '2020-01-01', '2020-05-01', 35);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (10, 9788003381693, '2020-01-07', '2020-05-07', 28);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (11, 9788077968719, '2020-01-02', '2020-05-02', 25);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (12, 9780593170984, '2020-01-10', '2020-05-10', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (13, 9781250313577, '2020-01-12', '2020-05-12', 20);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (14, 9781501108587, '2020-01-21', '2020-05-21', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (15, 9781501171734, '2020-01-21', '2020-05-21', 18);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (16, 9780062073747, '2020-01-21', '2020-05-21', 25);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (17, 9780006913375, '2020-01-21', '2020-05-21', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (18, 9780735611313    , '2020-03-11', '2020-05-21', 17);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (19, 9780006915751, '2020-03-26', '2020-05-26', 20);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (20, 9780143122029, '2020-03-26', '2020-05-26', 30);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (21, 9790692106715, '2020-03-27', '2020-05-27', 15);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (22, 9788072366185, '2020-03-27', '2020-05-27', 18);
INSERT INTO `ga2w20`.`special_book` (`id`, `ISBN`, `startDate`, `endDate`, `percentage`) VALUES (23, 9780062073570, '2020-03-27', '2020-05-27', 40);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`survey`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (1, 'How many books do you think you read over the year?', 'More than 20 books.', 'About 10 books.', 'One book.', 'I do not read book at all.', 1);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (2, 'Who more often reads books?', 'Teenagers.', 'Middle-aged people.', 'The elderly.', 'Nobody.', 0);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (3, 'Do you like to read? ', 'Yes, I like.', 'I read when there is nothing to do.', 'No, I do not like.', 'It is difficult to say.', 0);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (4, 'Most of all I like to read...', 'Books.', 'Newspapers.', 'Magazines.', 'Blogs.', 0);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (5, 'The place l like to read is...', 'Home.', 'Metro.', 'Library.', 'It does not matter.', 0);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (6, 'What topics do you enjoy reading about?', 'About animals.', 'About people.', 'About science and technology.', 'About sports.', 0);
INSERT INTO `ga2w20`.`survey` (`id`, `surveyText`, `answer1`, `answer2`, `answer3`, `answer4`, `surveyActive`) VALUES (7, 'About how many hours a day do you read?', 'All day.', 'More than 2 hour.', 'Less than 1 hour.', 'I do not read at all.', 0);

COMMIT;


-- -----------------------------------------------------
-- Data for table `ga2w20`.`survey_client`
-- -----------------------------------------------------
START TRANSACTION;
USE `ga2w20`;
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (1, 1, 1, 2);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (2, 1, 2, 3);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (3, 2, 1, 4);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (4, 2, 2, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (5, 3, 1, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (6, 3, 2, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (7, 4, 1, 2);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (8, 4, 2, 3);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (9, 5, 1, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (10, 5, 2, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (11, 6, 1, 4);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (12, 6, 2, 4);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (13, 7, 1, 2);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (14, 7, 2, 3);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (15, 1, 3, 4);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (16, 2, 4, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (17, 3, 5, 3);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (18, 4, 6, 1);
INSERT INTO `ga2w20`.`survey_client` (`id`, `surveyId`, `clientId`, `answer`) VALUES (19, 5, 7, 2);

COMMIT;

