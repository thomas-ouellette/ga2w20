
-- ONLY FOR TESTING

DROP DATABASE IF EXISTS ga2w20;
CREATE DATABASE ga2w20;

USE ga2w20;

DROP USER IF EXISTS ga2w20user@localhost;
CREATE USER ga2w20user@'localhost' IDENTIFIED WITH mysql_native_password BY 'ga2w20password' REQUIRE NONE;
GRANT ALL ON ga2w20.* TO ga2w20user@'localhost';


FLUSH PRIVILEGES;
