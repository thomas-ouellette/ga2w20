package com.ga2w20.backingbeans;

import com.ga2w20.entities.Advertisement;
import com.ga2w20.jpacontroller.AdvertisementJpaController;
import java.util.Date;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The AdvertisementBackingBean contains all additional functionality for
 * advertisements, and is managed by a single advertisement attribute.
 * 
 * @author Liam Harbec
 */
@Named("advertisementBackingBean")
@RequestScoped
public class AdvertisementBackingBean {

    @Inject
    private AdvertisementJpaController advertisementJpaController;

    private Advertisement advertisement;
    
    private final static Logger LOG = LoggerFactory.getLogger(AdvertisementBackingBean.class);
    
    
    /**
     * Gets the current advertisement, and initializes a new one if it is null
     * 
     * @return The advertisement
     */
    public Advertisement getAdvertisement() {
        if (advertisement == null) {
            LOG.info("New advertisement initialized");
            advertisement = new Advertisement();
        }
        return advertisement;
    }
    
    
    /**
     * Sets the current advertisement
     * 
     * @param advertisement The advertisement
     */
    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }
    
    
    /**
     * Randomly selects one advertisement that is currently active
     * 
     * @author Liam Harbec
     */
    public void generateAdvertisement() {
        advertisement = advertisementJpaController.getRandomAdvertisement(new Date());
        LOG.info("New advertisement generated");
    }
    
    
}
