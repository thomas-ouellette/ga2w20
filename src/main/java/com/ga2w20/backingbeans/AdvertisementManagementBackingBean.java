
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Advertisement;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AdvertisementJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 *This is the backing bean that allows to edit current ads,
 * deleting ads and creating new ads in the management side
 * @author Cadin Londono
 */
@Named
@ViewScoped
public class AdvertisementManagementBackingBean implements Serializable{
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(AdvertisementManagementBackingBean.class);
    
    private List<Advertisement> advertisements;
    
    private ResourceBundle resourceBundle;
    
    @Inject
    private AdvertisementJpaController advertisementJpaController;
    /**
     * This will refresh the List of advertisement that are stored as class variables 
     * and instiante the resource bundle to display messages after submission
     */
    @PostConstruct
    public void init(){
        refreshAdvertisements();
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }
    /**
     * This method will edit the advertisement that is given with the new values into the database
     * @param advertisement 
     */
    public void editAdvertisement(Advertisement advertisement){
        try {
            advertisementJpaController.edit(advertisement);
            refreshAdvertisements();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Ad_Edit_Success"), null));
        } catch (Exception ex) {
            LOG.error("Unable to modify an advertisement", ex);
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Ad_Edit_Error"), null));
        }
    }
    /**
     * This method will add a new advertisement to the database that the manager can edit 
     * with the values he wants (by default the ads active is false)
     */
    public void addNewAdvertisement(){
        try {
            Advertisement blankAdvertisement = new Advertisement();
            blankAdvertisement.setAdsActive(false);
            blankAdvertisement.setImgUrl(" ");
            blankAdvertisement.setSiteUrl(" ");
            blankAdvertisement.setStartDate(new Date());
            blankAdvertisement.setEndDate(new Date());
            advertisementJpaController.create(blankAdvertisement);
            refreshAdvertisements();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Ad_Create_Success"), null));

        } catch (Exception ex) {
            LOG.error("Couldn't create blank advertisement", ex);
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Ad_Create_Error"), null));
        }
    }
    /**
     * This method will delete an advertisement from the database.
     * (used when too many ads are created)
     * @param advertisement 
     */
    public void deleteAdvertisement(Advertisement advertisement){
        try {
            advertisementJpaController.destroy(advertisement.getId());
            refreshAdvertisements();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Ad_Delete_Success"), null));
        } catch (Exception ex) {
            LOG.error("unable to delete advertisement", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Ad_Delete_Error"), null));

        }
    }
    /**
     * Getter of the List of advertisements (all advertisements in database)
     * @return List of Advertisement
     */
    public List<Advertisement> getAdvertisements(){
        return this.advertisements;
    }
    /**
     * Setters for the advertisements
     * @param advertisements 
     */
    public void setAdvertisements(List<Advertisement> advertisements){
        this.advertisements = advertisements;
    }
    /**
     * This method repopulates the list of advertisement with the latest values 
     * in the database
     */
    private void refreshAdvertisements(){
        this.advertisements = advertisementJpaController.findAdvertisementEntities();
    }
    
}
