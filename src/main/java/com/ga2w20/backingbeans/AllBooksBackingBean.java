package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.jpacontroller.BookJpaController;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * This is the backing bean that keeps a list of all books in the database and the 
 * filtered books (search through books)
 * @author cadin
 */
@Named("allBooksBackingBean")
@ViewScoped
public class AllBooksBackingBean implements Serializable {
    
    private List<Book> books;
    private List<Book> filteredBooks;
    
    @Inject
    private BookJpaController bookJpaController;
    
    /**
     * Constructor that gets all books from the database 
     * and puts them in a list of books
     */
    @PostConstruct
    public void init() {
        books = bookJpaController.findBookEntities();
    }
    /**
     * Getter for the list of books
     * @return List of books
     */
    public List<Book> getBooks(){
        return books;
    }
    /**
     * Returns the filtered books (Search of books)
     * @return List of books
     */
    public List<Book> getFilteredBooks(){
        return this.filteredBooks;
    }
    /**
     * Sets a List of filtered books (Books that qualify with the search query)
     * @param filteredBooks 
     */
    public void setFilteredBooks(List<Book> filteredBooks){
        this.filteredBooks = filteredBooks;
    }
}
