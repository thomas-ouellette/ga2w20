
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Client;
import com.ga2w20.jpacontroller.ClientJpaController;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *This is the backing bean to accessing all of the clients 
 * in the database
 * @author cadin
 */
@Named
@ViewScoped
public class AllClientsBackingBean implements Serializable{
    
    private List<Client> allClients;
    private List<Client> filteredClients;
    
    @Inject
    private ClientJpaController clientJpaController;
    /**
     * Getting the latest clients from the client
     * in the constructor.
     */
    @PostConstruct
    public void init(){
        this.allClients = clientJpaController.findClientEntities();
    }
    /**
     * Getter for allClients class variable.
     * returns all the clients in the database
     * @return List of clients
     */
    public List<Client> getAllClients(){
        return this.allClients;
    }
    /**
     * Returns a filtered list of clients (clients filtered by name)
     * @return List of clients
     */
    public List<Client> getFilteredClients(){
        return this.filteredClients;
    }
    /**
     * Setting a filtered list of clients (clients filtered by name)
     * @param filteredClients 
     */
    public void setFilteredClients(List<Client> filteredClients){
        this.filteredClients = filteredClients;
    }
}
