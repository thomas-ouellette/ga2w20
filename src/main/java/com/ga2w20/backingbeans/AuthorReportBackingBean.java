
package com.ga2w20.backingbeans;

import com.ga2w20.jpacontroller.AuthorJpaController;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *This is the backing bean for the reports related to the 
 * authors of the book. It uses the sales bean to display the results
 * after loading the records for the requested report
 * @author cadin
 */
@Named
@ViewScoped
public class AuthorReportBackingBean implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(AuthorReportBackingBean.class);

    private ResourceBundle resourceBundle;

    private int authorId;
    private Date startDate;
    private Date endDate;

    List<SalesBean> authorSalesBeans;

    @Inject
    AuthorJpaController authorJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }

    /**
     * this method display all of the total sales between the specified date for a specific author.
     * It will only display one record since is that total of all books.
     */
    public void loadAuthorTotalSales() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        authorSalesBeans = new ArrayList();
        //BigDecimal totalSales = orderJpaController.getGeneralTotalSales(startDate, endDate);
        SalesBean authorSalesBean = authorJpaController.getTotalSales(authorId, startDate, endDate);
        //checking to see if the query returned a result if not we just return
        if(authorSalesBean.getCost() == null){
            LOG.info("No orders found for authorId: " + this.authorId);
            return;
        }
        authorSalesBean.setTitle(this.resourceBundle.getString("All_Books"));
        authorSalesBeans.add(authorSalesBean);
    }

    /**
     * This is the method that display the sales between a specified date and display
     * the sales grouped by books
     */
    public void loadAuthorTotalSalesGroupedByBooks() {
        if (startDate == null || endDate == null ) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.authorSalesBeans = authorJpaController.getTotalSalesAllBooks(authorId, startDate, endDate);
    }
    
    
    /**
     * Getter of the author's Id that we want 
     * to create a report for
     * @return int
     */
    public int getAuthorId(){
        return this.authorId;
    }
    /**
     * Setter for the author's id 
     * we want to create a report for
     * @param authorId 
     */
    public void setAuthorId(int authorId){
        this.authorId = authorId;
    }
    
    /**
     * Getter for the start date of the wanted report
     * @return Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Setter for the starting date of the wanted report
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the end date of the wanted report
     * @return 
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Setters for the end date of the wanted report
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter of the sales beans that contain the info for 
     * the records of the author report
     * @return List of SalesBean
     */
    public List<SalesBean> getAuthorSaleBeans() {
        return this.authorSalesBeans;
    }
   
}
