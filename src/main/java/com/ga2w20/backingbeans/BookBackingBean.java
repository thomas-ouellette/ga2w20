package com.ga2w20.backingbeans;

import com.ga2w20.entities.Author;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Review;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.BookJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The BookBackingBean contains all additional functionality for books, and is
 * managed by a single book attribute.
 * 
 * @author Liam Harbec, Svitlana Myronova 
 */
@Named("bookBackingBean")
@RequestScoped
public class BookBackingBean {

    
    @Inject
    private BookJpaController bookJpaController;

    private Book book;
    
    private List<Author> authors;

    
    private final static Logger LOG = LoggerFactory.getLogger(BookBackingBean.class);

    /**
     * Gets the isbn of the current book
     * 
     * @return The isbn
     */
    public long getIsbn() {
        if(book == null)
            return -1;
        return book.getIsbn();
    }
    
    
    /**
     * Sets the isbn of the current book
     * 
     * @param isbn The isbn to set
     */
    public void setIsbn(long isbn) {
        this.book = bookJpaController.findBook(isbn);
    }
    
    
    /**
     * Gets the book object, and creates a blank one if it does not already exist
     *
     * @return The fetched book
     */
    public Book getBook() {
        return this.book;
    }
    

    
    /**
     * Sets the current book object
     * 
     * @param book The book
     */
    public void setBook(Book book) {
        this.book = book;
    }
    
    
    /**
     * Fetches all of the authors of the current book
     * 
     * @return The authors of the book
     */
    public List<Author> getAuthors() {
        if (this.authors == null) {
            LOG.info("authors instantiated to the book's authors");
            this.authors = this.book.getAuthorList();
        }
        return this.authors;
    }
    
    
    /**
     * Calculates the average review rating of the book
     * 
     * @author Liam Harbec
     * @return The calculated book average
     */
    
    public int getAverageReview() {
        List<Review> reviews = getApprovedReviews();
        int total = 0;
        int reviewAmount = 0;
        for (Review r : reviews) {
            total += r.getRating();
            reviewAmount++;
        }
        return reviewAmount > 0 ? total/reviewAmount : total;
    }
    
    
    /**
     * Gets the genre of the current book object
     * 
     * @author Liam Harbec
     * @return The genre title
     */
    
    public String getGenre() {
        return this.book.getGenreId().getGenreTitle();
    }
    
    
    /**
     * Gets the publisher of the current book object
     * 
     * @author Liam Harbec
     * @return The publisher name
     */
    public String getPublisher() {
        return this.book.getPublisherId().getPublisherName();
    }
    
    
    /**
     * Gets similar books to the current book object.
     * 3 of these books are of the same genre.
     * At most 3 of these books are by the same author.
     * 
     * @author Liam Harbec
     * @return The similar books
     */
    
    public List<Book> getSimilarBooks() {
        String bookGenre = getGenre();
        String firstAuthor = getAuthors().get(0).getAuthorName();
        List<Book> booksWithGenre = this.bookJpaController.getBooksWithGenre(bookGenre, this.book.getIsbn());
        List<Book> booksWithAuthor = this.bookJpaController.getBooksWithAuthor(firstAuthor, this.book.getIsbn());
        
        ArrayList<Book> similarBooks = new ArrayList<>();
        
        // Removes the potential of duplicates
        Iterator<Book> itr = booksWithAuthor.iterator();
        while (itr.hasNext()) {
            Book bookByAuthor = itr.next();
            for (Book b : booksWithGenre) {
                if (bookByAuthor.equals(b)) {
                    itr.remove();
                }
            }
        }
        
        // Books by the same author
        if (booksWithAuthor.size() > 0) {
            // Maximum of 3 books per author
            if (booksWithAuthor.size() < 3) {
                for (int i = 0; i < booksWithAuthor.size(); i++) {
                    similarBooks.add(booksWithAuthor.get(i));
                }
            } else {
                for (int i = 0; i < 3; i++) {
                    similarBooks.add(booksWithAuthor.get(i));
                }
            }
        }
        
        // Books with the same genre
        for (int i = 0; similarBooks.size() < 6; i++) {
            similarBooks.add(booksWithGenre.get(i));
        }
        return similarBooks;
    }
    
    
    /**
     * Gets the price of a book with a sale price applied to it.
     * 
     * @author Liam Harbec
     * @return The price to be displayed
     */
    public String getBookSale() {
        BigDecimal percentageSale = calculateSalePercentage();
        if (percentageSale == BigDecimal.ZERO) {
            return null;
        } else {
            BigDecimal percent = percentageSale.divide(BigDecimal.valueOf(100));
            percent = BigDecimal.valueOf(1).subtract(percent);
            BigDecimal calculated = this.book.getListPrice().multiply(percent);
            calculated = calculated.setScale(2, RoundingMode.HALF_EVEN);
            return calculated + "";
        }
    }
    
    
    /**
     * Gets the sale percentage as a whole number
     * 
     * @author Liam Harbec
     * @return The percentage sale as a whole number
     */
    public BigDecimal getSalePercent() {
        BigDecimal percent = calculateSalePercentage();
        percent = percent.setScale(0, RoundingMode.DOWN);
        return percent;
    }
    
    
    /**
     * Gets the sale percentage of the current book object
     * 
     * @author Liam Harbec
     * @return The sale percentage
     */
    public BigDecimal calculateSalePercentage() {
        return bookJpaController.getSpecialBook(book.getIsbn(), new Date());
    }
    
    
    /**
     * Gets all reviews that have been approved by a manager. If the review has
     * not been approved, it will not be returned.
     * 
     * @author Liam Harbec
     * @return The reviews that have been approved
     */
    public List<Review> getApprovedReviews() {
        ArrayList<Review> list = new ArrayList<>();
        for (Review r : book.getReviewList()) {
            if (r.getApproved()) {
                list.add(r);
            }
        }
        return list;
    }
    
    /**
     * Method writes the LastGenreIdCookie cookie
     * 
     * @author Svitlana Myronova
     */
    public void writeCookie() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().addResponseCookie("LastGenreIdCookie", this.book.getGenreId().getId().toString(), null);
    }
}
