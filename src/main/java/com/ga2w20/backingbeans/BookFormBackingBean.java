
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Author;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Format;
import com.ga2w20.entities.Genre;
import com.ga2w20.entities.Publisher;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AuthorJpaController;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.FormatJpaController;
import com.ga2w20.jpacontroller.GenreJpaController;
import com.ga2w20.jpacontroller.PublisherJpaController;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import org.primefaces.model.UploadedFile;
import org.primefaces.shaded.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the backing bean, for the edit book and the add a book form.
 * Since this two forms are 90% similar(the only difference in edit book is that
 * we cannot change the isbn of the book), they share the same backing bean for
 * code reusability purposes. With this backing bean we are able to upload a image,
 * create a new genre, new publisher, new format, new author and most importantly a new 
 * book.
 * The menus handle the entities via their name instead of the actual bean object. This is because
 * it allows to create new entities and also not create hacky converters that need to be managed beans(Bad Code).
 * @author cadin londono
 */
@Named("bookFormBackingBean")
@ViewScoped
public class BookFormBackingBean implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(BookFormBackingBean.class);
    
    private ResourceBundle resourceBundle;      
    
    //Form fields
    private Book book;
    private String publisherName;
    private String genreTitle;
    //Image
    private boolean newImageUploaded = false;
    private UploadedFile image;
    private String fileName;
    private Path tempImagePath;
    //Optional Form fields
    private String newAuthor;
    private String newFormat;
    //JPA Controllers
    @Inject
    private BookJpaController bookJpaController;
    
    @Inject
    private GenreJpaController genreJpaController;
    
    @Inject
    private PublisherJpaController publisherJpaController;
    
    @Inject
    private AuthorJpaController authorJpaController;
    
    @Inject
    private FormatJpaController formatJpaController;
    /**
     * Constructor will automatically create a new book when accessing a page
     */
    @PostConstruct
    public void init(){
        this.book = new Book();
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }
    /**
     * Returns the backing bean book.
     * @return Book
     */
    public Book getBook(){
        return this.book;
    }
    /**
     * Sets the book of the backing bean.
     * It is important to call this method at the start
     * of the page to edit a book to set the book.
     * @param book 
     */
    public void setBook(Book book){
        this.book = book;
        this.genreTitle = book.getGenreId().getGenreTitle();
        this.publisherName = book.getPublisherId().getPublisherName();
    }
    /**
     * This is the method called when we want to submit the form 
     * to add a new book. If an error occurs a global message will be displayed.
     * @return String since its the redirect url that is supposed to be returned.(Returns null most of the time)
     */
    public String saveBook()
    {
        try {
            saveBookProperties(false);
            bookJpaController.create(book);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Book_Add_Success"), null));
        } catch (RollbackFailureException ex) {
            LOG.error("Rollback Transaction error", ex);
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to create book"));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Rollback_Transaction_Error"), null));
        }catch(PreexistingEntityException ex){
            LOG.error("Book PK already used by another book", ex);
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("A book already has this isbn"));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Book_PK_Error"), null));
        }
        catch (Exception ex) {
            LOG.error("Unable to create book", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Book_Creation_Error"), null));
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to create book"));
        }
        return null;
    }
    /**
     * This is the method called when we want to modify and existing book.
     * It will edit the existing book with new properties.
     * @return String since its the redirect url that is supposed to be returned.(Returns null most of the time)
     */
    public String editBook()
    {
        try {
            saveBookProperties(true);
            bookJpaController.edit(book);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Book_Edit_Success"), null));
        } catch (RollbackFailureException ex) {
            LOG.error("Rollback Transaction error", ex);
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to create book"));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Rollback_Transaction_Error"), null));
        }catch(PreexistingEntityException ex){
            LOG.error("Book PK already used by another book", ex);
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("A book already has this isbn"));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Book_PK_Error"), null));
        }
        catch (Exception ex) {
            LOG.error("Unable to create book", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Book_Craetion_Error"), null));
            //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Unable to create book"));
        }
        return null;
    }
    /**
     * This method is used to gather all the info that want's to be added or modified for a book and
     * puts it into the book object. Checks if the genre,publisher, author or format are new and creates them and attaches
     * it to the book.
     * @param editBook When we want to edit book we don't want to throw
     * an error if the user didn't upload a new picture. We want to throw an error
     * if the user is creating a book and hasn't added a picture.
     * @throws Exception 
     */
    private void saveBookProperties(boolean editBook) throws Exception{
        if(this.newImageUploaded || !editBook)
        {
            try {
                saveFile();
                book.setImageFile("images/" + tempImagePath.getFileName());
            }
            catch(NullPointerException ex){
                LOG.error("image is null", ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Image_Null_Error"), null));
                throw new Exception("No new Image");
            }
            catch (IOException ex) {
                LOG.error("unable to save image", ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Image_Save_Error"), null));
                throw new Exception("Error saving image to file");
            }
        }
        Publisher bookPublisher;
        try
        {
            bookPublisher = publisherJpaController.getPublisherByName(publisherName);
        }
        catch(NoResultException noResultException){
            bookPublisher = new Publisher();
            bookPublisher.setPublisherName(publisherName);
            try {
                publisherJpaController.create(bookPublisher);
            } catch (Exception ex) {
                LOG.error("Publisher controller unable to create publisher", ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Publisher_Creation_Error"), null));
                throw ex;
            }
        }
        
        book.setPublisherId(bookPublisher);
        
        Genre bookGenre;
        try{
            bookGenre = genreJpaController.getGenreByTitle(genreTitle);
        }
        catch(NoResultException noResultException){
            bookGenre = new Genre();
            bookGenre.setGenreTitle(genreTitle);
            try{
                genreJpaController.create(bookGenre);
            } catch (Exception ex) {
                LOG.error("unable to create genre", ex);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Genre_Creation_Error"), null));
                throw ex;
            }
        }
        book.setGenreId(bookGenre);
        book.setEnteredDate(new Date());
    }
    /**
     * This is the method called when the user wants to upload a picture to use 
     * for a book cover image. It will create temp file which will be empty.
     * @return String redirect url
     */
    public String uploadFile()
    {
        try
        {
            FacesContext context = FacesContext.getCurrentInstance();
            ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
            String rootpath = scontext.getRealPath("/images");
            Path tempFolder = Paths.get(rootpath);
            
            String filename = FilenameUtils.getBaseName(image.getFileName()); 
            String extension = FilenameUtils.getExtension(image.getFileName());
            tempImagePath = Files.createTempFile(tempFolder, filename + "-", "." + extension);
            this.fileName = image.getFileName();
            this.newImageUploaded = true;
            return null;
        }
        catch(IOException e)
        {
            LOG.error("Error creating temp file",e);
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Image_Upload_Error"), null));
        }
        return "bookedit?faces-redirect=true&includeViewParams=true";
    }
    
    /**
     * this method will be called when we want to save the image to the file we created
     * in upload picture. It will use the image inputStream and write to the file.
     * @throws IOException File not created before
     * @throws NullPointerException No image has been uploaded.
     */
    private void saveFile() throws IOException{
        if(image == null)
        {
            throw new NullPointerException("Image is null");
        }
        try (InputStream input = image.getInputstream()) {
            Files.copy(input, tempImagePath, StandardCopyOption.REPLACE_EXISTING);
        }
    }
    /**
     * This method will take the name of the author that wants to be added to the database and doesnt exist yet.
     * It creates a new author in the db
     */
    public void createNewAuthor(){
        Author author = new Author();
        author.setAuthorName(newAuthor);
        try {
            authorJpaController.create(author);
        }
        //we catch it here since this method is called by its own command button
        catch (Exception ex) {
            LOG.error("unable to create new author", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Author_Creation_Error"), null));
        }
    }
    /**
     * This method will take the name of the format that wants to be added to the database and doesnt exist yet.
     * It creates a new format in the db
     */
    public void createNewFormat(){
        Format format = new Format();
        format.setFormatTitle(newFormat);
        try{
            formatJpaController.create(format);
            //we catch it here since this method is called by its own command button
        } catch(Exception ex){
            LOG.error("unable to create new format", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Format_Creation_Error"), null));
        }
    }
    /**
     * returns the name of the publisher chosen. It returns string instead of 
     * the publisher object because jsf doesn't convert well objects in a select one menu.
     * It also allow us to create a new publisher if it doens't exist.
     * @return String
     */
    public String getPublisherName(){
        return this.publisherName;
    }
    /**
     * setting the name of the publisher of the book
     * @param publisherName 
     */
    public void setPublisherName(String publisherName){
        this.publisherName = publisherName;
    }
     /**
     * returns the name of the genre chosen. It returns string instead of 
     * the genre object because jsf doesn't convert well objects in a select one menu.
     * It also allow us to create a new genre if it doens't exist.
     * @return String
     */
    public String getGenreTitle(){
        return this.genreTitle;
    }
    /**
     * Sets the name of the genre of the book.
     * @param genreTitle 
     */
    public void setGenreTitle(String genreTitle){
        this.genreTitle = genreTitle;
    }
    /**
     * returns the image uploaded(The cover image)
     * @return UploadedFile
     */
    public UploadedFile getImage(){
        return this.image;
    }
    /**
     * Sets the image of the book(cover image)
     * @param image 
     */
    public void setImage(UploadedFile image){
        this.image = image;
    }
    /**
     * Returns the filename of the image uploaded. This is use to display
     * after uploading the image, the image uploaded
     * @return 
     */
    public String getFileName(){
        return this.fileName;
    }
    /**
     * We can set the name of a new author we want to create and add to our select many options
     * @param newAuthor 
     */
    public void setNewAuthor(String newAuthor){
        this.newAuthor = newAuthor;
    }
    /**
     * Returns the name of the new author chosen
     * @return 
     */
    public String getNewAuthor(){
        return this.newAuthor;
    }
    /**
     * Sets the name of the new format chosen. It allow us to create a new format and 
     * added to the list of select many options.
     * @param newFormat 
     */
    public void setNewFormat(String newFormat){
        this.newFormat = newFormat;
    } 
    /**
     * Returns the new format title chosen for the book
     * @return String
     */
    public String getNewFormat(){
        return this.newFormat;
    }
}
