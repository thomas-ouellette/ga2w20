
package com.ga2w20.backingbeans;

import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.querybeans.SummaryBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *The backing bean for all the books reports.
 * It has a list of beans that it loads after performing the
 * query for the requested report
 * @author cadin
 */
@Named
@ViewScoped
public class BookReportBackingBean implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(BookReportBackingBean.class);

    private ResourceBundle resourceBundle;

    private Date startDate;
    private Date endDate;

    List<SummaryBean> bookSalesBeans;

    @Inject
    BookJpaController bookJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }


    /**
     * This is the method that display the top selling books between a specified date and display
     * the books order by the times it has been sold.
     */
    public void loadTopSellingBooks() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.bookSalesBeans = bookJpaController.getTopSellingBooks(startDate, endDate);
    }
    
    /**
     * This is the method that display the books that have 0 sales between a specified date and display
     */
    public void loadZeroSalesBooks() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.bookSalesBeans = bookJpaController.getZeroSalesBooks(startDate, endDate);
    }

    /**
     * Getter for the start date of the wanted report
     * @return Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Setter for the starting date of the wanted report
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the end date of the wanted report
     * @return 
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Setters for the end date of the wanted report
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter of the sale beans that represent the records
     * in the book reports
     * @return List of summary bean
     */
    public List<SummaryBean> getBookSaleBeans() {
        return this.bookSalesBeans;
    }
}
