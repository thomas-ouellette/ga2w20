package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Genre;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.utils.Paginator;
import com.ga2w20.utils.PaginatorData;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The BooksOfGenreBackingBean is a layer between UI and DB. 
 * It allows to generate necessary data (books with a specific genre) and use it in xhtml file.
 * This data will be displayed  on a page. 
 * 
 * @author Svitlana Myrornova
 */
@Named("booksOfGenreBackingBean")
@SessionScoped
public class BooksOfGenreBackingBean implements Serializable, PaginatorData<Book> {

    @Inject
    private BookJpaController bookJpaController;

    @PersistenceContext
    private EntityManager entityManager;

    private Integer genreId = -1;
    private Paginator<Book> paginator;

    private final static Logger LOG = LoggerFactory.getLogger(BooksOfGenreBackingBean.class);

    private String genreTitle;
    
    /**
     * Default number of retrieved top sellers
     */
    private static final int NUMBER_OF_BOOKS = 5;

    /**
     * Show the page
     * 
     * @author Svitlana Myrornova
     * @param genreId
     * @return page
     */
    public String show(int genreId)
    {
        this.genreId = genreId;
        this.paginator = new Paginator<>(this);
        if(this.genreId == -1)
        {
            this.genreTitle = null;
        }
        else
        {
            this.genreTitle = entityManager.createNamedQuery("Genre.findByGenreId", Genre.class).setParameter("genreId", genreId).getSingleResult().getGenreTitle();
        }
        return "booksofgenre";
    }
    
    /**
     * Getter for GenreId
     * 
     * @author Svitlana Myrornova
     * @return genreId
     */
    public Integer getGenreId() {
        return genreId;
    }

    /**
     * Setter for GenreId
     *
     * @author Svitlana Myrornova
     * @param genreId
     */
    public void setGenreId(Integer genreId)
    {
        this.genreId = genreId;
    }
    
    /**
     * Getter for Paginator
     * 
     * @author Svitlana Myrornova
     * @return paginator
     */
    public Paginator getPaginator() {
        return this.paginator;
    }
    
    /**
     * Getting a genre title
     * 
     * @author Svitlana Myrornova
     * @return genreTitle
     */
    public String getGenreTitle() {
        LOG.info("Getting a genre title");
        return this.genreTitle;

    }    
    
    /**
     * Get the number of all books of all genres or with specific genreId 
     * except the first 5 best sellers 
     * 
     * @author Svitlana Myrornova
     * @return the number of books
     */
    @Override
    public int getTotalItems() {
        int total = 0;
        if (genreId == -1) {
            total = bookJpaController.getBookCountActive();
        } else {
            total = bookJpaController.getBooksWithGenreCount(genreId);
        }
        return total > NUMBER_OF_BOOKS ? total - NUMBER_OF_BOOKS : 0;
    }

    /**
     * Get the list of all topSellers of all gernes or with specific genreId
     * except the first 5 best sellers 
     * 
     * @author Svitlana Myrornova
     * @return the number of books
     */
    @Override
    public List<Book> getPageData(int firstItem, int maxItems) {
        if (genreId == -1) {
            return bookJpaController.getTopSellersWithAllGenres(firstItem + NUMBER_OF_BOOKS, maxItems);
        } else {
            return bookJpaController.getTopSellersWithGenre(genreId, firstItem + NUMBER_OF_BOOKS, maxItems);
        }
    }
    
    /**
     * Get the list of 5 best sellers of all genres or with specific genreId
     * 
     * @author Svitlana Myrornova
     * @return the number of books
     */
    public List<Book> getBestSellers() {
        if (genreId == -1) {
            return bookJpaController.getTopSellersWithAllGenres(0, NUMBER_OF_BOOKS);
        } else {
            return bookJpaController.getTopSellersWithGenre(genreId, 0, NUMBER_OF_BOOKS);
        }
    }  
}
