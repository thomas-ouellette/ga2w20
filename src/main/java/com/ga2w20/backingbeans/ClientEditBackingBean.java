
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Client;
import com.ga2w20.entities.Province;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.jpacontroller.ProvinceJpaController;
import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 *This is the backing bean that allows us to 
 * edit a client's different information.
 * @author cadin
 */
@Named
@ViewScoped
public class ClientEditBackingBean implements Serializable {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ClientEditBackingBean.class);
    
    private Client client;
    private String provinceName;
    
    private ResourceBundle resourceBundle;
    
    @Inject
    private ClientJpaController clientJpaController;
    
    @Inject
    private ProvinceJpaController provinceJpaController;
    /**
     * This constructor is where we initialize the resource bundle to display
     * growl messages.
     */
    @PostConstruct
    public void init(){
      this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }
    /**
     * This is the method that allow us to set up the client that we want to edit, allowing us 
     * to display the right province since is separate because it has its own entity
     * @param client 
     */
    public void setUpClientToEdit(Client client){
        setClient(client);
        setProvinceName(client.getProvinceId().getProvinceName());
    }
    /**
     * Getter for the client
     * @return client
     */
    public Client getClient(){
        return this.client;
    }
    /**
     * Setter for the client
     * @param client 
     */
    public void setClient(Client client){
        this.client = client;
    }
    /**
     * Getter for the province name (use for select menu)
     * @return String
     */
    public String getProvinceName(){
        return this.provinceName;
    }
    /**
     * Setter for the province name (use for select menu)
     * @param provinceName 
     */
    public void setProvinceName(String provinceName){
        this.provinceName = provinceName;
    }
    /**
     * This is the method that will save all the info from the client that has been edited 
     * and will check that the values are valid
     */
    public void saveEditingOfClientInfo(){
        try{
            //checking that the province inputed is part of the database
        Province province = provinceJpaController.getProvinceFromName(provinceName);
        if(province == null){
            throw new IllegalArgumentException("The province that was tried to be submited is not part of the saved provinces");
        }
        this.client.setProvinceId(province);    
            clientJpaController.edit(client);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Succesful_Edited_Client"), null));
        } catch (Exception ex) {
            LOG.error("Unable to edit a client (invalid form)", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Client_Edit_Error"), null));
        }
    }
    
}
