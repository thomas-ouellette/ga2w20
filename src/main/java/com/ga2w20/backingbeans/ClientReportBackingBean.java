
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Client;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *The backing bean for all the client reports.
 * It has a list of beans that it loads after performing the
 * query for the requested report
 * @author cadin
 */
@Named
@ViewScoped
public class ClientReportBackingBean implements Serializable{
    
    private final static Logger LOG = LoggerFactory.getLogger(ClientReportBackingBean.class);

    private ResourceBundle resourceBundle;

    private int clientId;
    private Date startDate;
    private Date endDate;

    List<SalesBean> clientSalesBeans;
    List<SummaryBean> clientBeans;

    @Inject
    ClientJpaController clientJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }

    /**
     * this method display all of the total sales between the specified date for a specific client.
     * It will only display one record since is that total of all books.
     */
    public void loadClientTotalSales() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        clientSalesBeans = new ArrayList();
        //BigDecimal totalSales = orderJpaController.getGeneralTotalSales(startDate, endDate);
        SalesBean clientSalesBean = clientJpaController.getTotalSales(clientId, startDate, endDate);
        //checking to see if the query returned a result if not we just return
        if(clientSalesBean.getCost() == null){
            LOG.info("No orders found for clientId: " + this.clientId);
            return;
        }
        clientSalesBean.setTitle(this.resourceBundle.getString("All_Books"));
        clientSalesBeans.add(clientSalesBean);
    }

    /**
     * This is the method that display the purchases of a client between a specified date and display
     * the sales grouped by books
     */
    public void loadClientTotalSalesGroupedByBooks() {
        if (startDate == null || endDate == null ) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.clientSalesBeans = clientJpaController.getTotalSalesAllBooks(clientId, startDate, endDate);
    }
    /**
     * This method will display all of the clients that made a purchase between the time period specified.
     * It is order desc by the amount of purchases the client has made
     * 
     */
    public void loadTopClients() {
        if (startDate == null || endDate == null ) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.clientBeans = clientJpaController.getTopClients(startDate, endDate);
    }

    /**
     * Getter for the client's Id that 
     * we want to create a report for
     * @return int
     */
    public int getClientId(){
        return this.clientId;
    }
    /**
     * Setter for the client's Id that we want to 
     * create a report for
     * @param clientId int
     */
    public void setClientId(int clientId){
        this.clientId = clientId;
    }
    
    /**
     * Getter for the start date of the wanted report
     * @return Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Setter for the starting date of the wanted report
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the end date of the wanted report
     * @return 
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Setters for the end date of the wanted report
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter for the sales beans that represents the records
     * in the client reports
     * @return List of sales bean
     */
    public List<SalesBean> getClientSaleBeans() {
        return this.clientSalesBeans;
    }
    
    /**
     * Getter for the summary beans used for the 
     * top clients report
     * @return List of Summary beans
     */
    public List<SummaryBean> getClientBeans(){
        return this.clientBeans;
    }
    
}
