package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.SpecialBook;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.SpecialBookJpaController;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.Cookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The IndexBackingBean is a layer between UI and DB. It allows to generate
 * necessary data (special books, trending books and newest book) and use it in
 * xhtml file. This data will be displayed on a page.
 *
 * @author Svitlana Myronova
 */
@Named("indexBackingBean")
@RequestScoped
public class IndexBackingBean {

    @Inject
    private BookJpaController bookJpaController;

    @Inject
    private SpecialBookJpaController specialBookJpaController;

    @PersistenceContext
    private EntityManager entityManager;

    private final static Logger LOG = LoggerFactory.getLogger(IndexBackingBean.class);

    private List<SpecialBook> specialBooks;
    private List<Book> trendingBooks;
    private List<Book> newestBooks;
    private List<Book> fromCookieBooks;

    private int lastGenreIdCookie = -1;

    /**
     * The default number of retrieved books
     */
    private static final int NUMBER_OF_BOOKS = 20;

    /**
     * Will be executed olny once by CDI after class is constructed
     * 
     * @author Svitlana Myrornova
     */
    @PostConstruct
    private void OnPostConstruct() {

        this.specialBooks = getSpecialBooksAllGenreActive();

        this.trendingBooks = getTrendingBooksInternalAllGenre();

        this.newestBooks = entityManager.createNamedQuery("Book.findAllOrderedByEnteredDate", Book.class).setMaxResults(NUMBER_OF_BOOKS).getResultList();
    }

    /**
     * This method gets a list of 20 active special books
     *
     * @author Svitlana Myrornova
     * @return results - list of books
     */
    private List<SpecialBook> getSpecialBooksAllGenreActive() {
        LOG.info("Getting a list of active special books");
        List results = specialBookJpaController.getSpecialBooksAllGenreActive(NUMBER_OF_BOOKS);
        Collections.shuffle(results);
        return results;
    }

    /**
     * This method gets a list of 20 trending books
     *
     * @author Svitlana Myrornova
     * @return results - list of books
     */
    private List<Book> getTrendingBooksInternalAllGenre() {
        LOG.info("Getting a list of trending books");
        List results = bookJpaController.getTopSellersWithAllGenres(0, NUMBER_OF_BOOKS);
        Collections.shuffle(results);
        return results;
    }

    /**
     * This method gets a list of 20 trending books
     *
     * @author Svitlana Myrornova
     * @return results - list of books
     */
    private List<Book> getFromCookieBooksInternal() {
        LOG.info("Getting a list of books from cookie");
        List results = bookJpaController.getBooksWithGenreActive(lastGenreIdCookie, NUMBER_OF_BOOKS);
        Collections.shuffle(results);
        return results;
    }

    /**
     * Get list of specialBook
     *
     * @author Svitlana Myrornova
     * @return specialBooks - list of books
     */
    public List<SpecialBook> getSpecialBooks() {
        //it produses a lot of lines of logs because of using Bootstrap carousel
        //LOG.info("Getting a list of active special books");
        return this.specialBooks;
    }

    /**
     * Get list of trending books - the most popular
     *
     * @author Svitlana Myrornova
     * @return trendingBooks - list of books
     */
    public List<Book> getTrendingBooks() {
        //it produses a lot of lines of logs because of using Bootstrap carousel
        //LOG.info("Getting a list of active trending books");
        return this.trendingBooks;
    }

    /**
     * Get list of the newest books
     *
     * @author Svitlana Myrornova
     * @return newestBooks - list of books
     */
    public List<Book> getNewestBooks() {
        //it produses a lot of lines of logs because of using Bootstrap carousel
        //LOG.info("Getting a list of active newest books");
        return this.newestBooks;
    }

    /**
     * Get list of the newest books
     * 
     * @author Svitlana Myrornova
     * @return fromCookieBooks  - list of books
     */
    public List<Book> getFromCookieBooks() {
        //it produses a lot of lines of logs because of using Bootstrap carousel
        //LOG.info("Getting a list of active newest books");
        if (fromCookieBooks == null) {
            fromCookieBooks = getFromCookieBooksInternal();
        }
        return this.fromCookieBooks;
    }

    /**
     * Get lastGenreIdCookie
     *
     * @author Svitlana Myrornova
     * @return lastGenreIdCookie
     */
    public int getLastGenreIdCookie() {
        //it produses a lot of lines of logs because of using Bootstrap carousel
        //LOG.info("Getting a list of active newest books");
        return this.lastGenreIdCookie;
    }

    /**
     * Look for cookies
     * 
     * @author Svitlana Myrornova
     */
    public void checkCookies() {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, Object> cookieMap = context.getExternalContext().getRequestCookieMap();

        // Retrieve all cookies
        if (cookieMap == null || cookieMap.isEmpty()) {
            LOG.info("No cookies");
        } else {
            ArrayList<Object> allCookies = new ArrayList<>(cookieMap.values());

            // Streams coding to print out the contenst of the cookies found
            allCookies.stream().map((c) -> {
                LOG.info(((Cookie) c).getName());
                return c;
            }).forEach((c) -> {
                LOG.info(((Cookie) c).getValue());
            });
        }

        // Retrieve a specific cookie
        Object my_cookie = context.getExternalContext().getRequestCookieMap().get("LastGenreIdCookie");
        if (my_cookie != null) {
            try {
                String strGenreId = ((Cookie) my_cookie).getValue();
                lastGenreIdCookie = Integer.parseInt(strGenreId);
            } catch (Exception e) {
                lastGenreIdCookie = -1;
            }
        }
    }

}
