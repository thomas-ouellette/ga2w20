package com.ga2w20.backingbeans;

import java.io.Serializable;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The LanguageBackingBean is a layer between UI and DB. 
 * It allows to switch  between languages on a page and show appropriate data.
 *
 * @author Svitlana Myronova
 */
@Named("languageBackingBean")
@SessionScoped
public class LanguageBackingBean implements Serializable {

    /**
     * It is a mechanism to for identifying the language
     */
    private Locale locale;

    private final static Logger LOG = LoggerFactory.getLogger(LanguageBackingBean.class);

    /**
     * Will be executed once by CDI after class is constructed
     * 
     * @author Svitlana Myrornova
     */
    @PostConstruct
    public void init() {
        locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
    }  

    /**
     * Method that switches the language
     * 
     * @author Svitlana Myrornova
     * @param lang
     * @return page with new language
     */
    public String switchTo(String lang) {
        LOG.info("switching to " + lang);
        this.locale = new Locale(lang);
        FacesContext.getCurrentInstance().getViewRoot().setLocale(this.locale);
        //retirect on a page with chosen language and parameters from the request
        return FacesContext.getCurrentInstance().getViewRoot().getViewId().substring(1) + 
                "?faces-redirect=true&includeViewParams=true";
    }

    /**
     * Method that checks the language on a page. Check that current language is English.
     *
     * @author Svitlana Myrornova
     * @return true or false 
     */
    public boolean isCurrentLanguageEnglish() {
        LOG.info("checking the language");
        return this.locale.getLanguage() == "en";
    }

    /**
     * Getter for locale
     *
     * @author Svitlana Myrornova
     * @return locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Setter for locale
     *
     * @author Svitlana Myrornova
     * @param locale
     */
    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
}
