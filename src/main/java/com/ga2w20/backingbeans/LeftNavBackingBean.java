package com.ga2w20.backingbeans;

import com.ga2w20.entities.Genre;
import com.ga2w20.jpacontroller.GenreJpaController;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The LeftNavBackingBean is a layer between UI and DB. 
 * It gets all genres from the DB and allows to use it in xhtml file to show on a LeftNav.
 * 
 * @author Svitlana Myronova
 */
@Named("leftNavBackingBean")
@RequestScoped
public class LeftNavBackingBean {

    @Inject
    private GenreJpaController genreJpaController;
    
    private final static Logger LOG = LoggerFactory.getLogger(LeftNavBackingBean.class);

    /**
     * This method gets a list of all genre.
     * 
     * @author Svitlana Myrornova
     * @return list of genres
     */
    public List<Genre> getGenres() {
        LOG.info("Getting a list of all genres");
        return genreJpaController.findGenreEntities();
    }
}
