package com.ga2w20.backingbeans;

import com.ga2w20.utils.PasswordUtils;
import com.ga2w20.entities.Client;
import com.ga2w20.jpacontroller.ClientJpaController;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The LoginBackingBean is used in the loginpage.xhtml to use inputted data and
 * verify if the inputted user exists
 * 
 * @author Liam Harbec
 */
@Named("loginBackingBean")
@RequestScoped
public class LoginBackingBean {
    
    @Inject
    private ClientJpaController clientJpaController;
    
    private String email;
    private String password;
    
    private UIComponent loginButton;
    
    private final static Logger LOG = LoggerFactory.getLogger(LoginBackingBean.class);
    
    
    /**
     * Gets the email that was inputted by the user
     * 
     * @return The email
     */
    public String getEmail() {
        return email;
    }
    
    
    /**
     * Sets the email value to what the user inputted
     * 
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    
    /**
     * Gets the password that was inputted by the user
     * 
     * @return The password
     */
    public String getPassword() {
        return password;
    }
    
    /**
     * Sets the password value to what the user inputted
     * 
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    
    /**
     * Gets the login commandButton component
     * 
     * @return The button
     */
    public UIComponent getLoginButton() {
        return loginButton;
    }
    
    
    /**
     * Sets the login commandButton component
     * 
     * @param loginButton The button
     */
    public void setLoginButton(UIComponent loginButton) {
        this.loginButton = loginButton;
    }
    
    
    /**
     * Attempts to log the user in with their provided email and password.
     * If the user cannot be found based on their email, or if their inputted
     * password does not match the hashed and salted password stored in the
     * database, they will be met with an error message.
     * 
     * @author Liam Harbec
     * @throws IOException
     * @throws NoSuchAlgorithmException 
     */
    public void login() throws IOException, NoSuchAlgorithmException {
        Client client = clientJpaController.findClientFromLogin(email);
        // If client is found
        if (client.getId() != -1) {
            PasswordUtils utils = new PasswordUtils();
            String input = utils.hashAndSaltPassword(password, client.getSalt());
            if (input.equals(client.getPassword())) {
                LOG.info(client.getFirstName() + " " + client.getLastName() + " has successfuly logged in");
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.getSessionMap().put("client", client);
                ec.redirect("index.xhtml");
                return;
            }
        }
        // Will happen only if client cannot be found or the credentials are invalid 
        LOG.info("Invalid user credentials were entered");
        FacesContext context = FacesContext.getCurrentInstance();
        String errorMessage = context.getApplication().evaluateExpressionGet(context, "#{msg.Login_LoginError}", String.class);
        FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);
        facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
        context.addMessage(loginButton.getClientId(context), facesMessage);
    }
}
