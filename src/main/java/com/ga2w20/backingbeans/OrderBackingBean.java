package com.ga2w20.backingbeans;

import com.ga2w20.entities.Order;
import com.ga2w20.jpacontroller.OrderJpaController;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The OrderBackingBean is used in the manageorders.xhtml file to act as the bridge between
 * xhtml and the database, fetching from the Orders table.
 * 
 * @author Thomas Ouellette
 */
@Named("orderBackingBean")
@RequestScoped
public class OrderBackingBean {
    
    private List<Order> orders;
    
    private Order selectedOrder;
    
    @Inject
    private OrderJpaController orderJpaController;
    
    private final static Logger LOG = LoggerFactory.getLogger(OrderBackingBean.class);
    
    @PostConstruct
    public void init() {
        this.orders = orderJpaController.findOrderEntities();
        this.selectedOrder = new Order();
    }
    
    public List<Order> getOrders() {
        return this.orders;
    }
    
    public Order getSelectedOrder(){
        return this.selectedOrder;
    }
    
    public void setSelectedOrder(Order order){
        this.selectedOrder = order;
    }
    
    public void deleteOrder(Order order) throws Exception {
        orderJpaController.destroy(order.getId());
        this.orders.remove(order);
    }
    
    /**
     * Used to update an order when it is marked as "removed" in the table.
     * 
     * @author Thomas Ouellette
     */
    public void editOrder(Order order) throws Exception {
        LOG.debug("Modifying order #" + order.getId());
        orderJpaController.edit(order);
    }
    
}
