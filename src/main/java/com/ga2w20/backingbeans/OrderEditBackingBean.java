package com.ga2w20.backingbeans;

import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Province;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.OrderBookJpaController;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.jpacontroller.ProvinceJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ResourceBundle; 
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used as the action bean for the edit order page. I holds the 
 * order that is being edited and has the methods that update the order. It will
 * re-calculate the order's total price and taxes after modifications are made
 * and update it in the database by calling controller methods.
 * 
 * @author Thomas Ouellette
 */
@Named("orderEditBackingBean")
@ViewScoped
public class OrderEditBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(OrderEditBackingBean.class);

    private ResourceBundle resourceBundle;

    private Order order;

    private String provincialTaxCharged;
    private int provinceId;

    BigDecimal pstPercent, gstPercent, hstPercent;

    @Inject
    private OrderJpaController orderJpaController;

    @Inject
    private ProvinceJpaController provinceJpaController;

    @Inject
    private OrderBookJpaController orderBookJpaController;

    @PostConstruct
    public void init() {
        this.order = new Order();
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        this.provincialTaxCharged = "N/A";
        provinceId = 0;
        pstPercent = BigDecimal.ZERO;
        gstPercent = BigDecimal.ZERO;
        hstPercent = BigDecimal.ZERO;
    }

    public Order getOrder() {
        return this.order;
    }

    public void setOrder(Order order) {
        LOG.info("Order beiong viewed: #" + order.getId());
        this.order = order;
        LOG.info("Order beiong viewed: #" + this.order.getId());
        setProvincialTaxCharged(order.getOrderBookList().get(0).getGSTpercentage().add(order.getOrderBookList().get(0).getPSTpercentage()).add(order.getOrderBookList().get(0).getHSTpercentage()).toString());
    }

    public String getProvincialTaxCharged() {
        return this.provincialTaxCharged;
    }

    public void setProvincialTaxCharged(String taxCharged) {
        this.provincialTaxCharged = taxCharged;
    }

    public int getProvinceId() {
        return this.provinceId;
    }

    public void setProvinceId(int provinceId) {
        LOG.info("Province Id changed to " + provinceId);
        this.provinceId = provinceId;
    }

    /**
     * Called when the update button is pressed. Will re-calculate order total 
     * price and then update the order record in the database
     * 
     * @author Thomas Ouellette
     */
    public void editOrder() {
        LOG.debug("Modifying order");
        try {
            retotalOrder();
            orderJpaController.edit(this.order);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Order_Update_Successful"), null));
        } catch (RollbackFailureException e) {
            LOG.error("Rollback Transaction error", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Error_modifying_order" + e.getLocalizedMessage()), null));
        } catch (PreexistingEntityException e) {
            LOG.error("Order PK already used by another book", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Error_modifying_order" + e.getLocalizedMessage()), null));
        } catch (Exception e) {
            LOG.error("Order PK already used by another book", e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Error_modifying_order" + e.getLocalizedMessage()), null));
        }
    }
    
    /**
     * Re-calculates order total price. Checks if the tax has been changed and 
     * then will total the order, excluding books marked as "removed"
     * 
     * @author Thomas Ouellette
     */
    private void retotalOrder() throws Exception {
        
        updateTaxPercentage();
        
        LOG.debug("Updated taxes on order #" + order.getId() + ", right now: pst = " + pstPercent + ", gst = " + gstPercent + ", hst = " + hstPercent);

        BigDecimal subtotal = new BigDecimal(0.0);
        
        LOG.debug("retotaling order");
        
        for (OrderBook orderBook : this.order.getOrderBookList()) {
            if (provinceId != 0) {
                orderBook.setPSTpercentage(pstPercent);
                orderBook.setGSTpercentage(gstPercent);
                orderBook.setHSTpercentage(hstPercent);
            }

            if (!orderBook.getRemovalStatus()) {
                subtotal = subtotal.add(orderBook.getPrice());
            }

            orderBookJpaController.edit(orderBook);
        }

        BigDecimal pstTotal = BigDecimal.ZERO;
        BigDecimal gstTotal = BigDecimal.ZERO;
        BigDecimal hstTotal = BigDecimal.ZERO;

        // Taxes are calculated if the subtotal is not 0
        if (!subtotal.equals(BigDecimal.ZERO)) {
            pstTotal = subtotal.multiply(pstPercent.divide(new BigDecimal(100.0)));
            gstTotal = subtotal.multiply(gstPercent.divide(new BigDecimal(100.0)));
            hstTotal = subtotal.multiply(hstPercent.divide(new BigDecimal(100.0)));
        }

        BigDecimal total = subtotal.add(pstTotal).add(gstTotal).add(hstTotal);
        
        LOG.debug("subtotal = " + subtotal);
        LOG.debug("total pst = " + pstTotal);
        LOG.debug("total gst = " + gstTotal);
        LOG.debug("total hst = " + hstTotal);
        LOG.debug("total = " + total);
        
        order.setTotalPST(pstTotal);
        order.setTotalGST(gstTotal);
        order.setTotalHST(hstTotal);
        order.setTotalPrice(total);
    }

    /**
     * Updates the tax being charged. Either changes it to zero percent, keeps 
     * the current rate, or changes it to another province's rate.
     * 
     * @author Thomas Ouellette
     */
    public void updateTaxPercentage() {
        LOG.debug("Updating order #" + order.getId() + " taxes");
        switch (this.provinceId) {
            case -1:
                LOG.debug("Removing taxes from order #" + order.getId());
                pstPercent = BigDecimal.ZERO;
                gstPercent = BigDecimal.ZERO;
                hstPercent = BigDecimal.ZERO;
                break;
            case 0:
                LOG.debug("Keeping taxes the same on order #" + order.getId());
                OrderBook orderBook = this.order.getOrderBookList().get(0);
                pstPercent = orderBook.getPSTpercentage();
                gstPercent = orderBook.getGSTpercentage();
                hstPercent = orderBook.getHSTpercentage();
                break;
            default:
                LOG.debug("Changing taxes on order #" + order.getId());
                Province province = provinceJpaController.findProvince(provinceId);
                pstPercent = province.getPSTpercentage();
                gstPercent = province.getGSTpercentage();
                hstPercent = province.getHSTpercentage();
                break;
        }
    }
}
