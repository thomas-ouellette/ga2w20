
package com.ga2w20.backingbeans;

import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *The backing bean for all the order reports.
 * It has a list of beans that it loads after performing the
 * query for the requested report
 * @author cadin
 */
@Named
@ViewScoped
public class OrderReportBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(OrderReportBackingBean.class);

    private ResourceBundle resourceBundle;

    private Date startDate;
    private Date endDate;

    List<SalesBean> salesBeans;

    @Inject
    OrderJpaController orderJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }

    /**
     * this method display all of the total sales between the specified date.
     * It will only display one record since is that total of all books.
     */
    public void loadTotalSalesForAllItems() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report total sales of all items");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        salesBeans = new ArrayList();
        SalesBean salesBeanTotal = orderJpaController.getGeneralTotalSales(startDate, endDate);
        salesBeanTotal.setTitle(this.resourceBundle.getString("All_Books"));
        salesBeans.add(salesBeanTotal);
    }

    /**
     * This is the method that display the sales between a specified date and display
     * the sales grouped by books
     */
    public void loadTotalSalesGroupedByBooks() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report total sales grouped by books");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.salesBeans = orderJpaController.getSalesAllBooks(startDate, endDate);
    }

    /**
     * Getter for the start date of the wanted report
     * @return Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Setter for the starting date of the wanted report
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the end date of the wanted report
     * @return 
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Setters for the end date of the wanted report
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    /**
     * Getter for the sales beans that represent the records 
     * in the book reports
     * @return List of sales beans
     */
    public List<SalesBean> getSalesBeans() {
        return this.salesBeans;
    }

}
