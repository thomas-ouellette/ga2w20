
package com.ga2w20.backingbeans;

import com.ga2w20.jpacontroller.AuthorJpaController;
import com.ga2w20.jpacontroller.PublisherJpaController;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The backing bean for all the publisher reports.
 * It has a list of beans that it loads after performing the
 * query for the requested report
 * @author cadin
 */
@Named
@ViewScoped
public class PublisherReportBackingBean implements Serializable{
    private final static Logger LOG = LoggerFactory.getLogger(AuthorReportBackingBean.class);

    private ResourceBundle resourceBundle;

    //We use publisherId since it is what is needed to retrieve the publisher in a query
    private int publisherId;
    private Date startDate;
    private Date endDate;

    List<SalesBean> publisherSalesBeans;

    @Inject
    PublisherJpaController publisherJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }

    /**
     * this method display all of the total sales between the specified date for a specific publisher.
     * It will only display one record since is that total of all books.
     */
    public void publisherTotalSales() {
        if (startDate == null || endDate == null) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        publisherSalesBeans = new ArrayList();
        SalesBean publisherSalesBean = publisherJpaController.getTotalSales(publisherId, startDate, endDate);
        //checking to see if the query returned a result if not we just return
        if(publisherSalesBean.getCost() == null){
            LOG.info("No orders found for publisherId: " + this.publisherId);
            return;
        }
        publisherSalesBean.setTitle(this.resourceBundle.getString("All_Books"));
        publisherSalesBeans.add(publisherSalesBean);
    }

    /**
     * This is the method that display the sales of a publisher between a specified date and display
     * the sales grouped by their books
     */
    public void publisherTotalSalesGroupedByBooks() {
        if (startDate == null || endDate == null ) {
            LOG.error("Dates are null when trying to submit for report");
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Date_Null_Error"), null));
            return;
        }
        this.publisherSalesBeans = publisherJpaController.getTotalSalesAllBooks(publisherId, startDate, endDate);
    }

    /**
     * Getter for the publisher id
     * @return int
     */
    public int getPublisherId(){
        return this.publisherId;
    }
    /**
     * Setter for the publisherId
     * @param publisherId 
     */
    public void setPublisherId(int publisherId){
        this.publisherId = publisherId;
    }
    /**
     * Getter for the start date of the wanted report
     * @return Date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Setter for the starting date of the wanted report
     * @param startDate 
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Getter for the end date of the wanted report
     * @return 
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Setters for the end date of the wanted report
     * @param endDate 
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Getter of the sales beans that represent the records
     * of the publisher reports
     * @return 
     */
    public List<SalesBean> getPublisherSaleBeans() {
        return this.publisherSalesBeans;
    }
}
