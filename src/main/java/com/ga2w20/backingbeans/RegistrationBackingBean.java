package com.ga2w20.backingbeans;

import com.ga2w20.utils.PasswordUtils;
import com.ga2w20.entities.Client;
import com.ga2w20.entities.Province;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.jpacontroller.ProvinceJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The RegistrationBackingBean is used in the registrationpage.xhtml to send inputted
 * data to create a Client in the database
 * 
 * @author Liam Harbec
 */
@Named("registrationBackingBean")
@ViewScoped
public class RegistrationBackingBean implements Serializable {
    
    @Inject
    private ClientJpaController clientJpaController;
    @Inject
    private ProvinceJpaController provinceJpaController;

    private Client client;
    private String title;
    private String province;
    private String password;
    private String passwordConfirm;
    private List<String> allEmails;
    
    private final static Logger LOG = LoggerFactory.getLogger(RegistrationBackingBean.class);
    
    
    /**
     * Required for the creation of a new user
     */
    @PostConstruct
    public void init() {
        client = new Client();
        client.setCountry("Canada");
        client.setManager(false);
        this.allEmails = getEmailsAsStrings();
    }
    
    
    /**
     * Gets the current client
     * 
     * @return The client
     */
    public Client getClient() {
        return client;
    }
    
    
    /**
     * Gets the province that was inputted by the user
     * 
     * @return The province
     */
    public String getProvince() {
        return province;
    }
    
    
    /**
     * Sets the province value to what the user inputted
     * 
     * @param province The province
     */
    public void setProvince(String province) {
        this.province = province;
    }
    
    
    /**
     * Gets the password that was inputted by the user
     * 
     * @return The password
     */
    public String getPassword() {
        return password;
    }
    
    
    /**
     * Sets the password value to what the user inputted
     * 
     * @param password The password
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
    
    /**
     * Gets the password confirmation value that was inputted by the user
     * 
     * @return The password confirmation 
     */
    public String getPasswordConfirm() {
        return passwordConfirm;
    }
    
    
    /**
     * Sets the password confirmation value to what the user inputted
     * 
     * @param passwordConfirm The password confirmation
     */
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
    
    
    /**
     * Gets the title value that was inputted by the user
     * 
     * @return The title
     */
    public String getTitle() {
        return title;
    }
    
    
    /**
     * Sets the title value to what the user inputted
     * 
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    
    /**
     * Gets the allEmails value that is fetched by the database
     */
    public List<String> getAllEmails() {
        return this.allEmails;
    }
    
    
    /**
     * Sets the allEmails value to what is inputted
     */
    public void setAllEmails(List<String> allEmails) {
        this.allEmails = allEmails;
    }
    
    
    /**
     * Gets all users' emails in the database
     * 
     * @author Liam Harbec
     * @return The list of emails
     */
    private List<String> getEmailsAsStrings() {
        List<String> list = new ArrayList<>();
        for (Client client : clientJpaController.findClientEntities()) {
            list.add(client.getEmail());
        }
        return list;
    }
    
    
    /**
     * Creates a new client in the database based on all of the user's inputted
     * information.
     * As the title and province are select menus, if the field is invalid, the
     * error message will only be displayed after the submission
     * 
     * @author Liam Harbec
     * @throws java.lang.Exception
     */
    public void createClient() throws Exception {
        int validated = 0;
        validated += validateTitle();
        validated += validateProvince();
        
        if (validated == 2) {
            LOG.info("Validated");
            Province p = provinceJpaController.getProvinceFromName(province);
            client.setTitle(title);
            client.setProvinceId(p);
            
            // Password protection
            PasswordUtils utils = new PasswordUtils();
            client.setSalt(utils.getSalt());
            client.setPassword(utils.hashAndSaltPassword(password, client.getSalt()));
            
            clientJpaController.create(client);
            LOG.info("Client " + client.getId() + " was successfully created");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.getSessionMap().put("client", client);
            LOG.info("Added the logged in client to the session");
            ec.redirect("index.xhtml");
        }
    }
    
    /**
     * Validates that the user selected one of the titles, and did not leave
     * it as the default select option.
     * 
     * @author Liam Harbec
     * @return 1 if it was validated
     * @return 0 if it was not validated
     */
    public int validateTitle() {
        if (title.equals("0")) {
            FacesContext context = FacesContext.getCurrentInstance();
            String errorMessage = context.getApplication().evaluateExpressionGet(context, "#{msg.Registration_Required}", String.class);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("titleInputError", facesMessage);
        } else {
            return 1;
        }
        return 0;
    }
    
    /**
     * Validates that the user selected one of the provinces/territories, and did
     * not leave it as the default select option
     * 
     * @author Liam Harbec
     * @return 1 if it was validated
     * @return 0 if it was not validated
     */
    public int validateProvince() {
        if (province.equals("0")) {
            FacesContext context = FacesContext.getCurrentInstance();
            String errorMessage = context.getApplication().evaluateExpressionGet(context, "#{msg.Registration_Required}", String.class);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            context.addMessage("provinceTerritoryInputError", facesMessage);
        } else {
            return 1;
        }
        return 0;
    }
    
    
    /**
     * Verifies that the password and password confirmation fields match
     * 
     * @author Liam Harbec
     * @param context
     * @param component
     * @param value 
     */
    public void verifyPasswordConfirmMatches(FacesContext context,
            UIComponent component, Object value) {
        String passwordConfirm = (String) value;
        if (!password.equals(passwordConfirm)) {
            LOG.info("Passwords match");
            String message = context.getApplication().evaluateExpressionGet(context,
                "#{msg.Registration_PasswordNotMatch}", String.class);
            throw new ValidatorException(new FacesMessage(message));
        }
    }
    
    
}
