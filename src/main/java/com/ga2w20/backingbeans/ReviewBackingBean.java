package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Client;
import com.ga2w20.entities.Review;
import com.ga2w20.entities.ReviewPK;
import com.ga2w20.jpacontroller.ReviewJpaController;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ReviewBackingBean is used in various xhtml files to act as the bridge between
 * xhtml and the database, fetching from the Review table.
 * 
 * @author Thomas Ouellette, Liam Harbec
 */
@Named("reviewBackingBean")
@RequestScoped
public class ReviewBackingBean implements Serializable {
    
    private Review review;
    private List<Review> reviews;
    private int rating;
    private String reviewText;
    
    @Inject
    private ReviewJpaController reviewJpaController;
    
    private UIComponent reviewButton;
    
    private final static Logger LOG = LoggerFactory.getLogger(ReviewBackingBean.class);
    
    @PostConstruct
    public void init() {
        reviews = reviewJpaController.findReviewEntities();
        review = new Review();
        review.setApproved(false);
        review.setReviewDate(new Date());
    }

    /**
     *
     * @author Thomas
     */
    public List<Review> getReviews() {
        return reviews;
    }

    /**
     *
     * @author Thomas
     */
    public void deleteReview(Review review) throws Exception {
        reviewJpaController.destroy(review.getReviewPK());
        reviews.remove(review);
    }
    
    /**
     * Review created if it does not exist.
     *
     * @return
     */
    public Review getReview() {
        if (review == null) {
            review = new Review();
        }
        return review;
    }
    
    
    /**
     * Sets the review to the inputted value
     * 
     * @param review The review
     */
    public void setReview(Review review) {
        this.review = review;
    }
    
    
    /**
     * Gets the rating value
     * 
     * @return The rating
     */
    public int getRating() {
        return review.getRating();
    }
    
    
    /**
     * Sets the rating to the inputted value
     * 
     * @param rating The rating
     */
    public void setRating(int rating) {
        this.rating = rating;
    }
    
    
    /**
     * Gets the inputted text of the rating
     * 
     * @return The text
     */
    public String getReviewText() {
        return review.getReviewText();
    }
    
    
    /**
     * Sets the text to the inputted value
     * 
     * @param reviewText The text
     */
    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }
    
    
    /**
     * Gets the registration commandButton component
     * 
     * @return The button
     */
    public UIComponent getReviewButton() {
        return reviewButton;
    }
    
    
    /**
     * Sets the login commandButton component
     * 
     * @param loginButton The button
     */
    public void setReviewButton(UIComponent reviewButton) {
        this.reviewButton = reviewButton;
    }
    
    
    /**
     * Gets the isbn of the last browsed book
     * 
     * @author Liam Harbec
     * @return The isbn
     */
    public Long getIsbnFromMap() {
        String isbn = (String) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("isbn");
        return Long.parseLong(isbn);
    }
    
    
    /**
     * Adds a review to the list of reviews of a given book
     * 
     * @author Liam Harbec
     * @return The url to redirect to
     * @throws java.lang.Exception 
     */
    public String createReview(Book book) throws Exception {
        LOG.info("A review is attempting to be created");
        
        FacesContext context = FacesContext.getCurrentInstance();
        if (review.getRating() != 0 && !(review.getReviewText().isEmpty())) {
            LOG.info("Both fields were filled");
            ReviewPK pk = new ReviewPK();
            Client client = (Client) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("client");
            pk.setClientId(client.getId());
            pk.setIsbn(book.getIsbn());
            if (reviewJpaController.findReview(pk) == null) {
                LOG.info("Client did not leave review yet, will leave one now");
                review.setClient(client);
                review.setReviewPK(pk);
                review.setBook(book);
                reviewJpaController.create(review);
                context.getExternalContext().redirect("reviewconfirm.xhtml");
            } else {
                LOG.info("Client has already left a review for this book");
                String errorMessage = context.getApplication().evaluateExpressionGet(context, "#{msg.Review_AlreadyLeftReview}", String.class);
                FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                context.addMessage(reviewButton.getClientId(context), facesMessage);
            }
        }
        return null;
    }

    /**
     *
     * @author Thomas
     */
    public void editReview(Review review) throws Exception {
        reviewJpaController.edit(review);
    }
}
