package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.SpecialBook;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.SpecialBookJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *This is the backing bean for the page of creating sales for a specific book.
 * It verifies that the special book created is valid
 * before trying to insert it into the DB
 * 
 * @author cadin
 */
@Named
@ViewScoped
public class SalesBackingBean implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(SalesBackingBean.class);

    //i18n
    private ResourceBundle resourceBundle;

    private Book book;
    private Date startingDate;
    private Date endDate;
    private SpecialBook specialBook;

    @Inject
    private SpecialBookJpaController specialBookJpaController;

    /**
     * Setting up the resource bundle for the error message with the right locale.
     * Called when the page loads
     */
    @PostConstruct
    public void init() {
        this.book = new Book();
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
    }

    /**
     * This is the method called when we want to add the special book to the db.
     * It verifies that it is valid before doing so.
     * Display error message if there is an error that needs to be connected
     * @return String Redirect
     * @throws Exception 
     */
    public String addNewSale() throws Exception {
        try
        {
        //is the starting date after the end date of the sale
        if (specialBook.getStartDate().after(specialBook.getEndDate())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Start_Date_End_Date_Sale_Error"), null));
            throw new IllegalArgumentException("The start date of the sale is after the end date of the special book we want to create");     
        }
        
        if (!isTheSalePercentageValid(specialBook.getPercentage())) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Sale_Percentage_Error"), null));
            throw new IllegalArgumentException("The sale percentage inputed for the special book is invalid(smaller or equal to 0 or bigger than 100)");
        }
        //Loops through every single sale to see if theres already a sale in place in the timeframe that we want to create the new sale
        List<SpecialBook> allSalesOfTheBook = book.getSpecialBookList();
        allSalesOfTheBook.stream().filter((sale) -> (doesTheTwoDateRangeOverlap(sale.getStartDate(), sale.getEndDate(), specialBook.getStartDate(), specialBook.getEndDate()))).forEachOrdered((_item) -> {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Previous_Sale_Date_Range_Error"), null));
            throw new IllegalArgumentException("There is already a sale inplace in the date range we want to create a new sale");
        });
        
        specialBookJpaController.create(specialBook);
        return null;
        }
        catch(IllegalArgumentException ex){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, this.resourceBundle.getString("Invalid_Sale"), null));
            return null;
        }
    }
   
    public void deleteSpecialBookSale(SpecialBook specialBook){
        try {
            specialBookJpaController.destroy(specialBook.getId());
        } catch (Exception ex) {
            LOG.error("Unable to delete a special book entity", ex);
        }
    }

    /**
     * Getter for the book we want to add a sale for
     * @return Book 
     */
    public Book getBook() {
        return this.book;
    }

    /**
     * Setter for the book we want to add a sale for
     * @param book 
     */
    public void setBook(Book book) {
        this.book = book;
    }

    /**
     * Getter of the specialBook(Sale) we want to create
     * @return SpecialBook(Sale)
     */
    public SpecialBook getSpecialBook() {
        if (this.specialBook == null) {
            this.specialBook = new SpecialBook();
            this.specialBook.setIsbn(book);
        }
        return this.specialBook;
    }

    /**
     * Setter for the SpecialBook(Sale) we want to create
     * @param specialBook 
     */
    public void setSpecialBook(SpecialBook specialBook) {
        this.specialBook = specialBook;
    }

    /**
     * This method verifies that the date ranges do not overlap (1 and 2 dates are the first date range, 3 and 4 are the second date range)
     * @param startDate1 starting date of first date range
     * @param endDate1 end date of the first date range
     * @param startDate2 starting date of second date range
     * @param endDate2 end date of second date range
     * @return true if the two date range overlap
     */
    private boolean doesTheTwoDateRangeOverlap(Date startDate1, Date endDate1, Date startDate2, Date endDate2) {
        return startDate1.compareTo(endDate2) <= 0 && startDate2.compareTo(endDate1) <= 0;
    }

    /**
     * Verifies that the sale percentage is greater than 0 and less than equal to 100
     * @param salePercentage
     * @return true if the sale percentage is valid
     */
    private boolean isTheSalePercentageValid(BigDecimal salePercentage) {
        return !(salePercentage.compareTo(BigDecimal.ZERO) <= 0 || salePercentage.compareTo(new BigDecimal(100)) > 0);
    }
}
