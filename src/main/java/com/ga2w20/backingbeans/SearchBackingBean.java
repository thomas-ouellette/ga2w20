package com.ga2w20.backingbeans;

import com.ga2w20.entities.Book;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.utils.Paginator;
import com.ga2w20.utils.PaginatorData;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SearchBackingBean is a layer between UI and DB. 
 * It allows to find and generate necessary data (books) and use it in xhtml file.
 * This data will be displayed  on a page. 
 *
 * @author Svitlana Myronova
 */
@Named("searchBackingBean")
@SessionScoped
public class SearchBackingBean implements Serializable, PaginatorData<Book> {

    private final static Logger LOG = LoggerFactory.getLogger(SearchBackingBean.class);

    @Inject
    BookJpaController bookJpaController;
    
    private Paginator paginator;

    @PersistenceContext
    private EntityManager entityManager;

    private String filter;
    private String query;

    private List<Book> searchResults;

    /**
     * Getter for search results
     *
     * @author Svitlana Myrornova
     * @return searchResults - list of books
     */
    public List<Book> getSearchResults() {
        return this.searchResults;
    }

    /**
     * Setter for filter: title, author, publisher, isbn, description
     *
     * @author Svitlana Myrornova
     * @param filter
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * Getter for filter: title, author, publisher, isbn, description
     *
     * @author Svitlana Myrornova
     * @return filter
     */
    public String getFilter() {
        return this.filter;
    }

    /**
     * Setter for query
     *
     * @author Svitlana Myrornova
     * @param query
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * Getter for query
     *
     * @author Svitlana Myrornova
     * @return query
     */
    public String getQuery() {
        return this.query;
    }

    /**
     * It returns on search page or on a book page if result is only one book
     *
     * @author Svitlana Myrornova
     * @return page
     */
    public String search() {
        paginator = new Paginator(this);
        
        if(paginator.getModel() == null)
            return "search";

        if (paginator.getModel().size() == 1) {
            Book book = (Book) paginator.getModel().get(0);
            long isbn = book.getIsbn();
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("book.xhtml?isbn=" + isbn);
            } catch (IOException ex) {
                LOG.error("redirect to book.xhtml with isbn=" + isbn);
            }
            return null;
        }

        return "search";
    }

    /**
     * This method returns list of the books that are filtered by ISBN
     *
     * @author Svitlana Myrornova
     * @return books - list of books
     */
    private List<Book> getBooksByIsbn() {
        LOG.info("Getting books by ISBN");
        ArrayList<Book> books = new ArrayList<Book>(); 
        try{
            Long isbn = Long.parseLong(query);
            books.add(bookJpaController.findBook(isbn));
        }
        catch(Exception e)
        {
        }
        return books;
    }

    /**
     * Getter for Paginator
     * 
     * @author Svitlana Myrornova
     * @return paginator
     */
    public Paginator getPaginator() {
        return this.paginator;
    }

    /**
     * Setter for Paginator
     *
     * @author Svitlana Myrornova
     * @param paginator
     */
    public void setPaginator(Paginator paginator) {
        this.paginator = paginator;
    }

     /**
     * Get the number of all books depending on the filter
     * 
     * @author Svitlana Myrornova
     * @return the number of books
     */
    @Override
    public int getTotalItems() {
        if (filter == null) {
            return 0;
        } else if (filter.equals("title")) {
            return bookJpaController.getBooksLikeTitleCount(query);
        } else if (filter.equals("author")) {
            return bookJpaController.getBooksLikeAuthorCount(query);
        } else if (filter.equals("publisher")) {
            return bookJpaController.getBooksLikePublisherCount(query);
        } else if (filter.equals("isbn")) {
            return getBooksByIsbn().size();
        } else {
            return 0;
        }
    }

     /**
     * Get the list of all books depending on the filter
     * 
     * @author Svitlana Myrornova
     * @return the number of books
     */
    @Override
    public List<Book> getPageData(int firstItem, int maxItems) {
        if (filter == null) {
            return null;
        } else if (filter.equals("title")) {
            return bookJpaController.getBooksLikeTitle(query, firstItem, maxItems);
        } else if (filter.equals("author")) {
            return bookJpaController.getBooksLikeAuthor(query, firstItem, maxItems);
        } else if (filter.equals("publisher")) {
            return bookJpaController.getBooksLikePublisher(query, firstItem, maxItems);
        } else if (filter.equals("isbn")) {
            return getBooksByIsbn();
        } else {
            return null;
        }
    }
}
