package com.ga2w20.backingbeans;

import java.io.IOException;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SessionBackingBean is used in several xhtml files to act as the bridge
 * between xhtml and the session object.
 *
 * @author Liam Harbec, Svitlana Myronova
 */
@Named("sessionBackingBean")
@SessionScoped
public class SessionBackingBean implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(SessionBackingBean.class);

    /**
     * Clears the client attribute and redirects the user to the home page
     *
     * @author Liam Harbec
     * @return The URL to redirect to
     * @throws IOException
     */
    public String logOutUser() throws IOException {
        LOG.info("Logging out the user");
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        
        //reset cookie - Svitlana Myronova
        Cookie c = new Cookie("LastGenreIdCookie", "");
        c.setMaxAge(0);
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addCookie(c);
        return "index.xhtml?faces-redirect=true";
    }

    /**
     * Redirects the user based on whether or not they are logged in
     *
     * @author Liam Harbec
     * @param isbn The isbn of the browsed book
     * @return
     */
    public String leaveReviewRedirect(String isbn) {
        // If the user is logged in
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("client") != null) {
            // To be used to render the book
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("isbn", isbn);
            // Parameters are lost in submit of review, getting them from the
            // request parameter map instead
            return "review.xhtml";
        } else {
            return "login.xhtml";
        }
    }

}
