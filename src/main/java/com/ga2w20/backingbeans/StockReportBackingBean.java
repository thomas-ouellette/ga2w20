
package com.ga2w20.backingbeans;

import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.StockBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The backing bean for the stock report view.
 * It needs its own backing bean as it has its own 
 * special query bean
 * @author cadin
 */
@Named
@ViewScoped
public class StockReportBackingBean implements Serializable {
     private final static Logger LOG = LoggerFactory.getLogger(StockReportBackingBean.class);

    private ResourceBundle resourceBundle;

    List<StockBean> stockBeans;

    @Inject
    BookJpaController bookJpaController;

    /**
     * The constructor called when we load the page. it loads the resource bundle
     * and the current stock report of the inventory
     */
    @PostConstruct
    public void init() {
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());
        this.stockBeans = bookJpaController.getStockReport();
    }


    /**
     * Getter for the stock beans that represent the records in 
     * the stock report
     * @return List of stock beans
     */
    public List<StockBean> getStockBeans() {
        return this.stockBeans;
    }

}
