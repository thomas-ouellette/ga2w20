package com.ga2w20.backingbeans;

import com.ga2w20.entities.Client;
import com.ga2w20.entities.Survey;
import com.ga2w20.entities.SurveyClient;
import com.ga2w20.jpacontroller.SurveyClientJpaController;
import com.ga2w20.jpacontroller.SurveyJpaController;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SurveyBackingBean is used to act as the bridge between xhtml and the
 * database, fetching from the Survey and Survey_Client tables to represent a
 * survey that the user can vote for, and see the total results.
 * 
 * @author Liam Harbec
 */
@Named("surveyBackingBean")
@SessionScoped
public class SurveyBackingBean implements Serializable {

    @Inject
    private SurveyJpaController surveyJpaController;
    @Inject
    private SurveyClientJpaController surveyClientJpaController;

    private Survey survey;
    private int answer;
    private List<Integer> surveyResults;
    
    private final static Logger LOG = LoggerFactory.getLogger(SurveyBackingBean.class);
    
    @PostConstruct
    public void init() {
        survey = new Survey();
    }
    
    
    /**
     * Gets the survey
     * 
     * @return The survey
     */
    public Survey getSurvey() {
        if (survey == null) {
            LOG.info("New survey initialized");
            survey = new Survey();
        }
        return survey;
    }
    
    
    /**
     * Sets the survey to the inputted value
     * 
     * @param survey The survey
     */
    public void setSurvey(Survey survey) {
        this.survey = survey;
    }
    
    
    /**
     * Gets the user's inputted answer
     * 
     * @return The answer
     */
    public int getAnswer() {
        return answer;
    }
    
    
    /**
     * Sets the answer to the inputted value
     * 
     * @param answer The answer
     */
    public void setAnswer(int answer) {
        this.answer = answer;
    }
    
    
    /**
     * Gets the results of the given survey
     * 
     * @return The results
     */
    public List<Integer> getSurveyResults() {
        return surveyResults;
    }
    
    
    /**
     * Sets the results of the given survey
     * 
     * @param surveyResults The results
     */
    public void setSurveyResults(List<Integer> surveyResults) {
        this.surveyResults = surveyResults;
    }
    
    
    /**
     * Randomly selects one of the surveys, adds it to the session, and
     * initializes the survey variable in the bean.
     * 
     * @author Liam Harbec
     */
    public void generateSurvey() {
        if (FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("survey") == null) {
            Survey generatedSurvey = surveyJpaController.generateSurvey();
            if (generatedSurvey == null) {
                LOG.info("There are no active surveys");
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("surveyGenerated", false);
            } else {
                LOG.info("Survey " + generatedSurvey.getId() + " was successfully generated");
                survey = generatedSurvey;
                FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("survey", survey);
            }
        } else {
            LOG.info("A survey is already in the session");
        }
    }
    
    
    /**
     * Creates a new record with the user's selection to the loaded survey and
     * gets all of the results of the survey, storing them in the surveyResults
     * variable in the bean.
     * 
     * @author Liam Harbec
     * @throws Exception 
     */
    public void submitSurvey() throws Exception {
        LOG.info("Submitting the survey");
        Client client = (Client) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("client");
        SurveyClient surveyClient = new SurveyClient();
        surveyClient.setSurveyId(survey);
        if (client != null) {
            surveyClient.setClientId(client);
        }
        surveyClient.setAnswer(answer);
        surveyClientJpaController.create(surveyClient);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("surveySubmitted", true);
        surveyResults = surveyClientJpaController.getSurveyResults(survey.getId());
    }
    
    
    /**
     * Gets the percentage that the particular option weighs in comparison to the
     * total amount of votes.
     * 
     * @author Liam Harbec
     * @param option The option to calculate
     * @return The percentage that the option weighs
     */
    public BigDecimal getResultAsPercent(Integer option) {
        Integer total = 0;
        for (Integer i : surveyResults) {
            total += i;
        }
        double calculated = (option.doubleValue() / total.doubleValue()) * 100;
        BigDecimal bd = BigDecimal.valueOf(calculated);
        return bd.setScale(2, RoundingMode.HALF_EVEN);
    }
}
