
package com.ga2w20.backingbeans;

import com.ga2w20.entities.Survey;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.SurveyJpaController;
import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.slf4j.LoggerFactory;

/**
 *This is the backing bean for managing surveys.
 * Add survey, edit survey and delete survey.
 * @author cadin
 */
@Named
@ViewScoped
public class SurveysManagementBackingBean implements Serializable {
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(SurveysManagementBackingBean.class);

    private List<Survey> surveys;
     
    private ResourceBundle resourceBundle;
    
    @Inject
    private SurveyJpaController surveyJpaController;
    /**
     * This will refresh the List of surveys that are stored as class variables 
     * and instantiate the resource bundle to display messages after submission
     */
    @PostConstruct
    public void init(){
        refreshSurveys();
        this.resourceBundle = ResourceBundle.getBundle("com.ga2w20.messagebundles.MessageBundle", FacesContext.getCurrentInstance().getViewRoot().getLocale());

    }
    /**
     * Getter for the surveys
     * @return List of surveys
     */
    public List<Survey> getSurveys(){
        return this.surveys;
    }
    /**
     * This getter will not only return the surveys.
     * but will refresh the surveys so they have the right update surveys
     * from the database.
     * @return List of surveys
     */
    public List<Survey> getCurrentSurveys(){
        refreshSurveys();
        return this.surveys;
    }
    /**
     * This method will add a new blank survey to the database for the manager
     * to edit and create his own survey. (by default the survey is not active)
     */
    public void addANewBlankSurvey(){
        try {
            Survey survey = new Survey();
            survey.setSurveyText(" ");
            survey.setAnswer1(" ");
            survey.setAnswer2(" ");
            survey.setAnswer3(" ");
            survey.setAnswer4(" ");
            survey.setSurveyActive(false);
            surveyJpaController.create(survey);
            refreshSurveys();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Succesful_Add_Survey"), null));
        } catch (Exception ex) {
            LOG.error("Unable to create new Survey", ex);
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Error_Add_Survey"), null));
        }
    }
    /**
     * There can only be one survey that is active in the website.
     * In this method we change the active survey to be the one inputed in.
     * If the input is the same active survey it will remain active.
     * @param survey 
     */
    public void setSurveyAsTheActiveSurvey(Survey survey){
        try
        {
            if(survey.getSurveyActive())
            {
                surveyJpaController.setNewCurrentActiveSurvey(survey);
                refreshSurveys();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Succesful_Change_Active_Survey"), null));
            }
            else
            {
                survey.setSurveyActive(true);
            }
        }
        catch(Exception e){
            LOG.error("error occured while changing the survey active",e);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Error_Change_Active_Survey"), null));
        }
    }
    /**
     * This method is for deleting surveys from the database.
     * (used only when too many survey are created)
     * @param survey 
     */
    public void deleteSurvey(Survey survey){
        try {
            surveyJpaController.destroy(survey.getId());
            refreshSurveys();
        } catch (Exception ex) {
            LOG.error("Unable to destroy Survey", ex);
            ex.printStackTrace();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Error_Delete_Survey"), null));
        }
    }
    /**
     * This method will refresh the surveys with the latest values from the database, to the 
     * surveys class variable
     */
    public void refreshSurveys(){
        this.surveys = surveyJpaController.findSurveyEntities();
    }
    /**
     * This method will save all the changes done to a survey 
     * into the database
     * @param survey 
     */
    public void saveSurveyChanges(Survey survey){
        try {
            surveyJpaController.edit(survey);
            refreshSurveys();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, this.resourceBundle.getString("Succesful_Edit_Survey"), null));
        } catch (Exception ex) {
            LOG.error("unable to edit survey", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, this.resourceBundle.getString("Error_Edit_Survey"), null));
        }
    }
}
