package com.ga2w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Book entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "book", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Book.findAll", query = "SELECT b FROM Book b"),
    @NamedQuery(name = "Book.findByIsbn", query = "SELECT b FROM Book b WHERE b.isbn = :isbn"),
    @NamedQuery(name = "Book.findByTitle", query = "SELECT b FROM Book b WHERE b.title = :title"),
    @NamedQuery(name = "Book.findByPublicationDate", query = "SELECT b FROM Book b WHERE b.publicationDate = :publicationDate"),
    @NamedQuery(name = "Book.findByNumberOfPages", query = "SELECT b FROM Book b WHERE b.numberOfPages = :numberOfPages"),
    @NamedQuery(name = "Book.findByImageFile", query = "SELECT b FROM Book b WHERE b.imageFile = :imageFile"),
    @NamedQuery(name = "Book.findByWholeSalePrice", query = "SELECT b FROM Book b WHERE b.wholeSalePrice = :wholeSalePrice"),
    @NamedQuery(name = "Book.findByListPrice", query = "SELECT b FROM Book b WHERE b.listPrice = :listPrice"),
    @NamedQuery(name = "Book.findByEnteredDate", query = "SELECT b FROM Book b WHERE b.enteredDate = :enteredDate"),
    @NamedQuery(name = "Book.findByRemovalStatus", query = "SELECT b FROM Book b WHERE b.removalStatus = :removalStatus"),
    //for index page
    @NamedQuery(name = "Book.findAllOrderedByEnteredDate", query = "SELECT b FROM Book b WHERE b.removalStatus = false ORDER BY b.enteredDate DESC")
})
public class Book implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBN")
    private Long isbn;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 300)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Column(name = "publicationDate")
    @Temporal(TemporalType.DATE)
    private Date publicationDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberOfPages")
    private int numberOfPages;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "imageFile")
    private String imageFile;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "wholeSalePrice")
    private BigDecimal wholeSalePrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "listPrice")
    private BigDecimal listPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "enteredDate")
    @Temporal(TemporalType.DATE)
    private Date enteredDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "removalStatus")
    private boolean removalStatus;
    @JoinTable(name = "book_author", joinColumns = {
        @JoinColumn(name = "ISBN", referencedColumnName = "ISBN")}, inverseJoinColumns = {
        @JoinColumn(name = "authorId", referencedColumnName = "id")})
    @ManyToMany
    private List<Author> authorList;
    @JoinTable(name = "book_format", joinColumns = {
        @JoinColumn(name = "ISBN", referencedColumnName = "ISBN")}, inverseJoinColumns = {
        @JoinColumn(name = "formatId", referencedColumnName = "id")})
    @ManyToMany
    private List<Format> formatList;
    @JoinColumn(name = "genreId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Genre genreId;
    @JoinColumn(name = "publisherId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Publisher publisherId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book")
    private List<Review> reviewList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "book")
    private List<OrderBook> orderBookList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "isbn")
    private List<SpecialBook> specialBookList;

    public Book() {
    }

    public Book(Long isbn) {
        this.isbn = isbn;
    }

    public Book(Long isbn, String title, Date publicationDate, int numberOfPages, String description, String imageFile, BigDecimal wholeSalePrice, BigDecimal listPrice, Date enteredDate, boolean removalStatus) {
        this.isbn = isbn;
        this.title = title;
        this.publicationDate = publicationDate;
        this.numberOfPages = numberOfPages;
        this.description = description;
        this.imageFile = imageFile;
        this.wholeSalePrice = wholeSalePrice;
        this.listPrice = listPrice;
        this.enteredDate = enteredDate;
        this.removalStatus = removalStatus;
    }

    public Long getIsbn() {
        return isbn;
    }

    public void setIsbn(Long isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public BigDecimal getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(BigDecimal wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public Date getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(Date enteredDate) {
        this.enteredDate = enteredDate;
    }

    public boolean getRemovalStatus() {
        return removalStatus;
    }

    public void setRemovalStatus(boolean removalStatus) {
        this.removalStatus = removalStatus;
    }

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public List<Format> getFormatList() {
        return formatList;
    }

    public void setFormatList(List<Format> formatList) {
        this.formatList = formatList;
    }

    public Genre getGenreId() {
        return genreId;
    }

    public void setGenreId(Genre genreId) {
        this.genreId = genreId;
    }

    public Publisher getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Publisher publisherId) {
        this.publisherId = publisherId;
    }

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public List<OrderBook> getOrderBookList() {
        return orderBookList;
    }

    public void setOrderBookList(List<OrderBook> orderBookList) {
        this.orderBookList = orderBookList;
    }

    public List<SpecialBook> getSpecialBookList() {
        return specialBookList;
    }

    public void setSpecialBookList(List<SpecialBook> specialBookList) {
        this.specialBookList = specialBookList;
    }

    /**
     * This method finds current sale price of a book
     * 
     * @return salePrice
     */
    public BigDecimal getSalePrice() {
        List<SpecialBook> specialBooks = this.getSpecialBookList();
        if (specialBooks.isEmpty()) {
            return new BigDecimal(0);
        } else {
            Date currentDate = new Date();            
            for(SpecialBook sb: specialBooks){
                if (currentDate.compareTo(sb.getStartDate())>=0  && currentDate.compareTo(sb.getEndDate())<=0 ){
                    BigDecimal sale = sb.getPercentage().multiply(this.listPrice).divide(new BigDecimal(100));
                    return this.getListPrice().subtract(sale);
                }
            }
            return new BigDecimal(0);
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (isbn != null ? isbn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Book)) {
            return false;
        }
        Book other = (Book) object;
        if ((this.isbn == null && other.isbn != null) || (this.isbn != null && !this.isbn.equals(other.isbn))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.Book[ isbn=" + isbn + " ]";
    }

}
