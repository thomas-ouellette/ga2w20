package com.ga2w20.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Format entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "format", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Format.findAll", query = "SELECT f FROM Format f"),
    @NamedQuery(name = "Format.findById", query = "SELECT f FROM Format f WHERE f.id = :id"),
    @NamedQuery(name = "Format.findByFormatTitle", query = "SELECT f FROM Format f WHERE f.formatTitle = :formatTitle")})
public class Format implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "formatTitle")
    private String formatTitle;
    @ManyToMany(mappedBy = "formatList")
    private List<Book> bookList;

    public Format() {
    }

    public Format(Integer id) {
        this.id = id;
    }

    public Format(Integer id, String formatTitle) {
        this.id = id;
        this.formatTitle = formatTitle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFormatTitle() {
        return formatTitle;
    }

    public void setFormatTitle(String formatTitle) {
        this.formatTitle = formatTitle;
    }

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Format)) {
            return false;
        }
        Format other = (Format) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.formatTitle;
    }
    
}
