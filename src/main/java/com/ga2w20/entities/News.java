package com.ga2w20.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * News entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "news", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "News.findAll", query = "SELECT n FROM News n"),
    @NamedQuery(name = "News.findById", query = "SELECT n FROM News n WHERE n.id = :id"),
    @NamedQuery(name = "News.findByImgUrl", query = "SELECT n FROM News n WHERE n.imgUrl = :imgUrl"),
    @NamedQuery(name = "News.findBySiteUrl", query = "SELECT n FROM News n WHERE n.siteUrl = :siteUrl"),
    @NamedQuery(name = "News.findByStartDate", query = "SELECT n FROM News n WHERE n.startDate = :startDate"),
    @NamedQuery(name = "News.findByEndDate", query = "SELECT n FROM News n WHERE n.endDate = :endDate"),
    @NamedQuery(name = "News.findByNewsActive", query = "SELECT n FROM News n WHERE n.newsActive = :newsActive")})
public class News implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "img_url")
    private String imgUrl;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "site_url")
    private String siteUrl;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "endDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "newsActive")
    private boolean newsActive;

    public News() {
    }

    public News(Integer id) {
        this.id = id;
    }

    public News(Integer id, String imgUrl, String siteUrl, Date startDate, Date endDate, boolean newsActive) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.siteUrl = siteUrl;
        this.startDate = startDate;
        this.endDate = endDate;
        this.newsActive = newsActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getSiteUrl() {
        return siteUrl;
    }

    public void setSiteUrl(String siteUrl) {
        this.siteUrl = siteUrl;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public boolean getNewsActive() {
        return newsActive;
    }

    public void setNewsActive(boolean newsActive) {
        this.newsActive = newsActive;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof News)) {
            return false;
        }
        News other = (News) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.News[ id=" + id + " ]";
    }
    
}
