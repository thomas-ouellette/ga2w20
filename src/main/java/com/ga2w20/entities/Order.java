package com.ga2w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * Order entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "order", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Order.findAll", query = "SELECT o FROM Order o"),
    @NamedQuery(name = "Order.findById", query = "SELECT o FROM Order o WHERE o.id = :id"),
    @NamedQuery(name = "Order.findByOrderDate", query = "SELECT o FROM Order o WHERE o.orderDate = :orderDate"),
    @NamedQuery(name = "Order.findByTotalPrice", query = "SELECT o FROM Order o WHERE o.totalPrice = :totalPrice"),
    @NamedQuery(name = "Order.findByTotalPST", query = "SELECT o FROM Order o WHERE o.totalPST = :totalPST"),
    @NamedQuery(name = "Order.findByTotalGST", query = "SELECT o FROM Order o WHERE o.totalGST = :totalGST"),
    @NamedQuery(name = "Order.findByTotalHST", query = "SELECT o FROM Order o WHERE o.totalHST = :totalHST"),
    @NamedQuery(name = "Order.findByRemovalStatus", query = "SELECT o FROM Order o WHERE o.removalStatus = :removalStatus")})
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "orderDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date orderDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalPrice")
    private BigDecimal totalPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalPST")
    private BigDecimal totalPST;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalGST")
    private BigDecimal totalGST;
    @Basic(optional = false)
    @NotNull
    @Column(name = "totalHST")
    private BigDecimal totalHST;
    @Basic(optional = false)
    @NotNull
    @Column(name = "removalStatus")
    private boolean removalStatus;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "Order")
    private List<OrderBook> orderBookList;
    @JoinColumn(name = "clientId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Client clientId;

    public Order() {
    }

    public Order(Integer id) {
        this.id = id;
    }

    public Order(Integer id, Date orderDate, BigDecimal totalPrice, BigDecimal totalPST, BigDecimal totalGST, BigDecimal totalHST, boolean removalStatus) {
        this.id = id;
        this.orderDate = orderDate;
        this.totalPrice = totalPrice;
        this.totalPST = totalPST;
        this.totalGST = totalGST;
        this.totalHST = totalHST;
        this.removalStatus = removalStatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPST() {
        return totalPST;
    }

    public void setTotalPST(BigDecimal totalPST) {
        this.totalPST = totalPST;
    }

    public BigDecimal getTotalGST() {
        return totalGST;
    }

    public void setTotalGST(BigDecimal totalGST) {
        this.totalGST = totalGST;
    }

    public BigDecimal getTotalHST() {
        return totalHST;
    }

    public void setTotalHST(BigDecimal totalHST) {
        this.totalHST = totalHST;
    }

    public boolean getRemovalStatus() {
        return removalStatus;
    }

    public void setRemovalStatus(boolean removalStatus) {
        this.removalStatus = removalStatus;
    }

    public List<OrderBook> getOrderBookList() {
        return orderBookList;
    }

    public void setOrderBookList(List<OrderBook> orderBookList) {
        this.orderBookList = orderBookList;
    }

    public Client getClientId() {
        return clientId;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Order)) {
            return false;
        }
        Order other = (Order) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.Order[ id=" + id + " ]";
    }
    
}
