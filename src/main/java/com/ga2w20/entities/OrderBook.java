package com.ga2w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * OrderBook entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "order_book", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "OrderBook.findAll", query = "SELECT o FROM OrderBook o"),
    @NamedQuery(name = "OrderBook.findByOrderId", query = "SELECT o FROM OrderBook o WHERE o.orderBookPK.orderId = :orderId"),
    @NamedQuery(name = "OrderBook.findByIsbn", query = "SELECT o FROM OrderBook o WHERE o.orderBookPK.isbn = :isbn"),
    @NamedQuery(name = "OrderBook.findByPrice", query = "SELECT o FROM OrderBook o WHERE o.price = :price"),
    @NamedQuery(name = "OrderBook.findByPSTpercentage", query = "SELECT o FROM OrderBook o WHERE o.pSTpercentage = :pSTpercentage"),
    @NamedQuery(name = "OrderBook.findByGSTpercentage", query = "SELECT o FROM OrderBook o WHERE o.gSTpercentage = :gSTpercentage"),
    @NamedQuery(name = "OrderBook.findByHSTpercentage", query = "SELECT o FROM OrderBook o WHERE o.hSTpercentage = :hSTpercentage"),
    @NamedQuery(name = "OrderBook.findByRemovalStatus", query = "SELECT o FROM OrderBook o WHERE o.removalStatus = :removalStatus")
})
public class OrderBook implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrderBookPK orderBookPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private BigDecimal price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PSTpercentage")
    private BigDecimal pSTpercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "GSTpercentage")
    private BigDecimal gSTpercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HSTpercentage")
    private BigDecimal hSTpercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "removalStatus")
    private boolean removalStatus;
    @JoinColumn(name = "ISBN", referencedColumnName = "ISBN", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Book book;
    @JoinColumn(name = "orderId", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Order Order;

    public OrderBook() {
    }

    public OrderBook(OrderBookPK orderBookPK) {
        this.orderBookPK = orderBookPK;
    }

    public OrderBook(OrderBookPK orderBookPK, BigDecimal price, BigDecimal pSTpercentage, BigDecimal gSTpercentage, BigDecimal hSTpercentage, boolean removalStatus) {
        this.orderBookPK = orderBookPK;
        this.price = price;
        this.pSTpercentage = pSTpercentage;
        this.gSTpercentage = gSTpercentage;
        this.hSTpercentage = hSTpercentage;
        this.removalStatus = removalStatus;
    }

    public OrderBook(int orderId, long isbn) {
        this.orderBookPK = new OrderBookPK(orderId, isbn);
    }

    public OrderBookPK getOrderBookPK() {
        return orderBookPK;
    }

    public void setOrderBookPK(OrderBookPK orderBookPK) {
        this.orderBookPK = orderBookPK;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPSTpercentage() {
        return pSTpercentage;
    }

    public void setPSTpercentage(BigDecimal pSTpercentage) {
        this.pSTpercentage = pSTpercentage;
    }

    public BigDecimal getGSTpercentage() {
        return gSTpercentage;
    }

    public void setGSTpercentage(BigDecimal gSTpercentage) {
        this.gSTpercentage = gSTpercentage;
    }

    public BigDecimal getHSTpercentage() {
        return hSTpercentage;
    }

    public void setHSTpercentage(BigDecimal hSTpercentage) {
        this.hSTpercentage = hSTpercentage;
    }

    public boolean getRemovalStatus() {
        return removalStatus;
    }

    public void setRemovalStatus(boolean removalStatus) {
        this.removalStatus = removalStatus;
    }
    
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Order getOrder() {
        return Order;
    }

    public void setOrder(Order Order) {
        this.Order = Order;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderBookPK != null ? orderBookPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderBook)) {
            return false;
        }
        OrderBook other = (OrderBook) object;
        if ((this.orderBookPK == null && other.orderBookPK != null) || (this.orderBookPK != null && !this.orderBookPK.equals(other.orderBookPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.OrderBook[ orderBookPK=" + orderBookPK + " ]";
    }
    
}
