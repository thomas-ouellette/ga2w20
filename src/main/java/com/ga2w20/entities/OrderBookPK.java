package com.ga2w20.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * OrderBook primary key entity
 *
 * @author Svitlana Myronova
 */
@Embeddable
public class OrderBookPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "orderId")
    private int orderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBN")
    private long isbn;

    public OrderBookPK() {
    }

    public OrderBookPK(int orderId, long isbn) {
        this.orderId = orderId;
        this.isbn = isbn;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) orderId;
        hash += (int) isbn;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrderBookPK)) {
            return false;
        }
        OrderBookPK other = (OrderBookPK) object;
        if (this.orderId != other.orderId) {
            return false;
        }
        if (this.isbn != other.isbn) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.OrderBookPK[ orderId=" + orderId + ", isbn=" + isbn + " ]";
    }
    
}
