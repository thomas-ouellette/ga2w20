package com.ga2w20.entities;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Province entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "province", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Province.findAll", query = "SELECT p FROM Province p"),
    @NamedQuery(name = "Province.findById", query = "SELECT p FROM Province p WHERE p.id = :id"),
    @NamedQuery(name = "Province.findByProvinceName", query = "SELECT p FROM Province p WHERE p.provinceName = :provinceName"),
    @NamedQuery(name = "Province.findByGSTpercentage", query = "SELECT p FROM Province p WHERE p.gSTpercentage = :gSTpercentage"),
    @NamedQuery(name = "Province.findByPSTpercentage", query = "SELECT p FROM Province p WHERE p.pSTpercentage = :pSTpercentage"),
    @NamedQuery(name = "Province.findByHSTpercentage", query = "SELECT p FROM Province p WHERE p.hSTpercentage = :hSTpercentage")})
public class Province implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "provinceName")
    private String provinceName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "GSTpercentage")
    private BigDecimal gSTpercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PSTpercentage")
    private BigDecimal pSTpercentage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HSTpercentage")
    private BigDecimal hSTpercentage;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provinceId")
    private List<Client> clientList;

    public Province() {
    }

    public Province(Integer id) {
        this.id = id;
    }

    public Province(Integer id, String provinceName, BigDecimal gSTpercentage, BigDecimal pSTpercentage, BigDecimal hSTpercentage) {
        this.id = id;
        this.provinceName = provinceName;
        this.gSTpercentage = gSTpercentage;
        this.pSTpercentage = pSTpercentage;
        this.hSTpercentage = hSTpercentage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public List<Client> getClientList() {
        return clientList;
    }

    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Province)) {
            return false;
        }
        Province other = (Province) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.Province[ id=" + id + " ]";
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public BigDecimal getGSTpercentage() {
        return gSTpercentage;
    }

    public void setGSTpercentage(BigDecimal gSTpercentage) {
        this.gSTpercentage = gSTpercentage;
    }

    public BigDecimal getPSTpercentage() {
        return pSTpercentage;
    }

    public void setPSTpercentage(BigDecimal pSTpercentage) {
        this.pSTpercentage = pSTpercentage;
    }

    public BigDecimal getHSTpercentage() {
        return hSTpercentage;
    }

    public void setHSTpercentage(BigDecimal hSTpercentage) {
        this.hSTpercentage = hSTpercentage;
    }
    
}
