package com.ga2w20.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 * Review primary key entity
 *
 * @author Svitlana Myronova
 */
@Embeddable
public class ReviewPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "clientId")
    private int clientId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISBN")
    private long isbn;

    public ReviewPK() {
    }

    public ReviewPK(int clientId, long isbn) {
        this.clientId = clientId;
        this.isbn = isbn;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public long getIsbn() {
        return isbn;
    }

    public void setIsbn(long isbn) {
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) clientId;
        hash += (int) isbn;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReviewPK)) {
            return false;
        }
        ReviewPK other = (ReviewPK) object;
        if (this.clientId != other.clientId) {
            return false;
        }
        if (this.isbn != other.isbn) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.ReviewPK[ clientId=" + clientId + ", isbn=" + isbn + " ]";
    }
    
}
