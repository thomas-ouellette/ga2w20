package com.ga2w20.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * SpecialBook entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "special_book", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "SpecialBook.findAll", query = "SELECT s FROM SpecialBook s"),
    @NamedQuery(name = "SpecialBook.findById", query = "SELECT s FROM SpecialBook s WHERE s.id = :id"),
    @NamedQuery(name = "SpecialBook.findByStartDate", query = "SELECT s FROM SpecialBook s WHERE s.startDate = :startDate"),
    @NamedQuery(name = "SpecialBook.findByEndDate", query = "SELECT s FROM SpecialBook s WHERE s.endDate = :endDate"),
    @NamedQuery(name = "SpecialBook.findByPercentage", query = "SELECT s FROM SpecialBook s WHERE s.percentage = :percentage")
})
public class SpecialBook implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "startDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "endDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "percentage")
    private BigDecimal percentage;
    @JoinColumn(name = "ISBN", referencedColumnName = "ISBN")
    @ManyToOne(optional = false)
    private Book isbn;

    public SpecialBook() {
    }

    public SpecialBook(Integer id) {
        this.id = id;
    }

    public SpecialBook(Integer id, Date startDate, Date endDate, BigDecimal percentage) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.percentage = percentage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }

    public Book getIsbn() {
        return isbn;
    }

    public void setIsbn(Book isbn) {
        this.isbn = isbn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SpecialBook)) {
            return false;
        }
        SpecialBook other = (SpecialBook) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.SpecialBook[ id=" + id + " ]";
    }
    
}
