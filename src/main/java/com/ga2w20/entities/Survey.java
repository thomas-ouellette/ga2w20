package com.ga2w20.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Survey entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "survey", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "Survey.findAll", query = "SELECT s FROM Survey s"),
    @NamedQuery(name = "Survey.findById", query = "SELECT s FROM Survey s WHERE s.id = :id"),
    @NamedQuery(name = "Survey.findByAnswer1", query = "SELECT s FROM Survey s WHERE s.answer1 = :answer1"),
    @NamedQuery(name = "Survey.findByAnswer2", query = "SELECT s FROM Survey s WHERE s.answer2 = :answer2"),
    @NamedQuery(name = "Survey.findByAnswer3", query = "SELECT s FROM Survey s WHERE s.answer3 = :answer3"),
    @NamedQuery(name = "Survey.findByAnswer4", query = "SELECT s FROM Survey s WHERE s.answer4 = :answer4"),
    @NamedQuery(name = "Survey.findBySurveyActive", query = "SELECT s FROM Survey s WHERE s.surveyActive = :surveyActive")
})
public class Survey implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "surveyText")
    private String surveyText;    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer1")
    private String answer1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer2")
    private String answer2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer3")
    private String answer3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "answer4")
    private String answer4;
    @Basic(optional = false)
    @NotNull
    @Column(name = "surveyActive")
    private boolean surveyActive;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "surveyId")
    private List<SurveyClient> surveyClientList;

    public Survey() {
    }

    public Survey(Integer id) {
        this.id = id;
    }

    public Survey(Integer id, String surveyText, String answer1, String answer2, String answer3, String answer4, boolean surveyActive ) {
        this.id = id;
        this.surveyText = surveyText;
        this.answer1 = answer1;
        this.answer2 = answer2;
        this.answer3 = answer3;
        this.answer4 = answer4;
        this.surveyActive = surveyActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSurveyText() {
        return surveyText;
    }

    public void setSurveyText(String surveyText) {
        this.surveyText = surveyText;
    }    

    public String getAnswer1() {
        return answer1;
    }

    public void setAnswer1(String answer1) {
        this.answer1 = answer1;
    }

    public String getAnswer2() {
        return answer2;
    }

    public void setAnswer2(String answer2) {
        this.answer2 = answer2;
    }

    public String getAnswer3() {
        return answer3;
    }

    public void setAnswer3(String answer3) {
        this.answer3 = answer3;
    }

    public String getAnswer4() {
        return answer4;
    }

    public void setAnswer4(String answer4) {
        this.answer4 = answer4;
    }
    
    public boolean getSurveyActive() {
        return surveyActive;
    }

    public void setSurveyActive(boolean surveyActive) {
        this.surveyActive = surveyActive;
    }

    public List<SurveyClient> getSurveyClientList() {
        return surveyClientList;
    }

    public void setSurveyClientList(List<SurveyClient> surveyClientList) {
        this.surveyClientList = surveyClientList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Survey)) {
            return false;
        }
        Survey other = (Survey) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.Survey[ id=" + id + " ]";
    }
    
}
