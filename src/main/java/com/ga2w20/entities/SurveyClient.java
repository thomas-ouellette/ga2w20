package com.ga2w20.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * SurveyClient entity
 *
 * @author Svitlana Myronova
 */
@Entity
@Table(name = "survey_client", catalog = "ga2w20", schema = "")
@NamedQueries({
    @NamedQuery(name = "SurveyClient.findAll", query = "SELECT s FROM SurveyClient s"),
    @NamedQuery(name = "SurveyClient.findById", query = "SELECT s FROM SurveyClient s WHERE s.id = :id"),
    @NamedQuery(name = "SurveyClient.findByAnswer", query = "SELECT s FROM SurveyClient s WHERE s.answer = :answer")})
public class SurveyClient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "answer")
    private int answer;
    @JoinColumn(name = "clientId", referencedColumnName = "id")
    @ManyToOne
    private Client clientId;
    @JoinColumn(name = "surveyId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Survey surveyId;

    public SurveyClient() {
    }

    public SurveyClient(Integer id) {
        this.id = id;
    }

    public SurveyClient(Integer id, int answer) {
        this.id = id;
        this.answer = answer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }

    public Client getClientId() {
        return clientId;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    public Survey getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(Survey surveyId) {
        this.surveyId = surveyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SurveyClient)) {
            return false;
        }
        SurveyClient other = (SurveyClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ga2w20.entities.SurveyClient[ id=" + id + " ]";
    }
    
}
