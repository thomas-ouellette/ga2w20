package com.ga2w20.exceptions;

import java.util.ArrayList;
import java.util.List;

/**
 * Illegal Orphan Exception
 * 
 * @author Svitlana Myronova
 */
public class IllegalOrphanException extends Exception {
    
    private List<String> messages;
    
    /**
     * Constructor with a parameter for IllegalOrphanException
     * 
     * @param messages
     */
    public IllegalOrphanException(List<String> messages) {
        super((messages != null && messages.size() > 0 ? messages.get(0) : null));
        if (messages == null) {
            this.messages = new ArrayList<String>();
        }
        else {
            this.messages = messages;
        }
    }
    
    /**
     * Getter messages
     *
     * @return messages
     */
    public List<String> getMessages() {
        return messages;
    }
}
