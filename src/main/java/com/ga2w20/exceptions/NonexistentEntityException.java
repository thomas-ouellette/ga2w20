package com.ga2w20.exceptions;

/**
 * Nonexistent Entity Exception
 *
 * @author Svitlana Myronova
 */
public class NonexistentEntityException extends Exception {
    
    /**
     * Constructor with parameters for NonexistentEntityException
     *
     * @param message
     * @param cause
     */
    public NonexistentEntityException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor with a parameter for NonexistentEntityException
     *
     * @param message
     */
    public NonexistentEntityException(String message) {
        super(message);
    }
}
