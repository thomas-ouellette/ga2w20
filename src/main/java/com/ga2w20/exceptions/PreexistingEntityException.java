package com.ga2w20.exceptions;

/**
 * Preexisting Entity Exception
 *
 * @author Svitlana Myronova
 */
public class PreexistingEntityException extends Exception {
    
    /**
     * Constructor with parameters for PreexistingEntityException
     *
     * @param message
     * @param cause
     */
    public PreexistingEntityException(String message, Throwable cause) {
        super(message, cause);
    }
    
    /**
     * Constructor with a parameter for PreexistingEntityException
     *
     * @param message
     */
    public PreexistingEntityException(String message) {
        super(message);
    }
}
