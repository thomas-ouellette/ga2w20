package com.ga2w20.exceptions;

/**
 * Rollback Failure Exception
 *
 * @author Ken Fogel
 */
public class RollbackFailureException extends Exception {

    /**
     * Constructor with parameters for RollbackFailureException
     *
     * @param message
     * @param cause
     */
    public RollbackFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor with a parameter for RollbackFailureException
     *
     * @param message
     */
    public RollbackFailureException(String message) {
        super(message);
    }
}
