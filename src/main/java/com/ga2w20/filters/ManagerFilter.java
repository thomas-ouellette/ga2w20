/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga2w20.filters;

import com.ga2w20.entities.Client;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cadin, Thomas Ouellette
 */
@WebFilter(filterName = "ManagerFilter")
public class ManagerFilter implements Filter {

    private final static Logger LOG = LoggerFactory.getLogger(ManagerFilter.class);

    /**
     * Filters the management side pages. You must be logged in and be a manager
     * to access any pages in the management directory.
     *
     * Client are redirected to the client home page.
     *
     * Unauthenticated users will be redirected to the login page.
     *
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        Client client = (Client) ((HttpServletRequest) request).getSession().getAttribute("client");

        if (client != null && client.getManager()) {
            chain.doFilter(request, response);
        } else {
            LOG.info("Unauthorized access attempt to management site, redirecting user");

            String redirectTo = "/index.xhtml";
            if (client == null) {
                redirectTo = "/login.xhtml";
            }

            String contextPath = ((HttpServletRequest) request)
                    .getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath
                    + redirectTo);
        }
    }

    @Override
    public void destroy() {
        // Nothing to do here!
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do here but must be overloaded
    }

}
