package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Advertisement;
import com.ga2w20.entities.Advertisement_;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The AdvertisementJpaController is responsible for all queries and database actions
 * relevant to the Advertisement entity
 * 
 * @author Svitlana Myronova, Liam Harbec
 */
@Named
@RequestScoped
public class AdvertisementJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(AdvertisementJpaController.class);
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Advertisement entity
     * 
     * @author Svitlana Myronova
     * @param advertisement
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Advertisement advertisement) throws RollbackFailureException, Exception {

        try {
            utx.begin();
            em.persist(advertisement);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit Advertisement entity
     *
     * @author Svitlana Myronova
     * @param ad
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Advertisement ad) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            ad = em.merge(ad);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = ad.getId();
                if (findAdvertisement(id) == null) {
                    throw new NonexistentEntityException("The advertisement with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Advertisement entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {

        try {

            utx.begin();
            Advertisement ad;
            try {
                ad = em.getReference(Advertisement.class, id);
                ad.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The advertisement with id " + id + " no longer exists.", enfe);
            }
            em.remove(ad);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Advertisement entities
     *
     * @author Svitlana Myronova
     * @return list of Advertisement 
     */
    public List<Advertisement> findAdvertisementEntities() {
        LOG.info("getting all the advertisements");
        return findAdvertisementEntities(true, -1, -1);
    }

    /**
     * Find Advertisement entities
     *
     * @author Svitlana Myronova      
     * @param maxResults
     * @param firstResult
     * @return list of Advertisement
     */
    public List<Advertisement> findAdvertisementEntities(int maxResults, int firstResult) {
        return findAdvertisementEntities(false, maxResults, firstResult);
    }

    /**
     * Find Advertisement entities
     *
     * @author Svitlana Myronova    
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Advertisement
     */
    private List<Advertisement> findAdvertisementEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Advertisement.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find Advertisement entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return Advertisement entity
     */
    public Advertisement findAdvertisement(Integer id) {
        return em.find(Advertisement.class, id);

    }

    /**
     * Get count of Advertisement entities
     *
     * @author Svitlana Myronova
     * @return number of Advertisement entities 
     */
    public int getAdvertisementCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Advertisement> rt = cq.from(Advertisement.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }    
    
    /**
     * Randomly selects an active advertisement from the database
     * 
     * @author Liam Harbec
     * @param currentDate The current date to search for
     * @return The selected advertisement
     */
    public Advertisement getRandomAdvertisement(Date currentDate) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Advertisement> query = cb.createQuery(Advertisement.class);
        Root<Advertisement> ad = query.from(Advertisement.class);
        
        query.select(ad).where(cb.equal(ad.get(Advertisement_.adsActive), true),
                cb.between(cb.literal(currentDate),
                ad.get(Advertisement_.startDate),
                ad.get(Advertisement_.endDate)));
        TypedQuery<Advertisement> allAds = em.createQuery(query);
        
        List<Advertisement> ads = allAds.getResultList();
        Random r = new Random();
        int adIndex = 0;
        try {
            adIndex = r.nextInt(ads.size());
        } catch (IllegalArgumentException e) {
            LOG.info("Query returned 0 ads");
            return null;
        }
        Advertisement adToReturn = ads.get(adIndex);
        return adToReturn;
    }
}
