package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Author;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Book_;
import com.ga2w20.entities.Order_;
import com.ga2w20.entities.OrderBook_;
import com.ga2w20.entities.Author_;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.math.BigDecimal;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The AuthorJpaController is responsible for all queries and database actions
 * relevant to the Author entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class AuthorJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(AuthorJpaController.class);
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Author 
     *
     * @author Svitlana Myronova
     * @param author
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Author author) throws RollbackFailureException, Exception {
        try {
            if (author.getBookList() == null) {
                author.setBookList(new ArrayList<Book>());
            }
            utx.begin();
            List<Book> attachedBookList = new ArrayList<Book>();
            for (Book bookListBookToAttach : author.getBookList()) {
                bookListBookToAttach = em.getReference(bookListBookToAttach.getClass(), bookListBookToAttach.getIsbn());
                attachedBookList.add(bookListBookToAttach);
            }
            author.setBookList(attachedBookList);
            em.persist(author);
            for (Book bookListBook : author.getBookList()) {
                bookListBook.getAuthorList().add(author);
                bookListBook = em.merge(bookListBook);
            }
            utx.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException | NotSupportedException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Edit Author
     *
     * @author Svitlana Myronova
     * @param author
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void edit(Author author) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Author persistentAuthor = em.find(Author.class, author.getId());
            List<Book> bookListOld = persistentAuthor.getBookList();
            List<Book> bookListNew = author.getBookList();
            List<Book> attachedBookListNew = new ArrayList<Book>();
            for (Book bookListNewBookToAttach : bookListNew) {
                bookListNewBookToAttach = em.getReference(bookListNewBookToAttach.getClass(), bookListNewBookToAttach.getIsbn());
                attachedBookListNew.add(bookListNewBookToAttach);
            }
            bookListNew = attachedBookListNew;
            author.setBookList(bookListNew);
            author = em.merge(author);
            for (Book bookListOldBook : bookListOld) {
                if (!bookListNew.contains(bookListOldBook)) {
                    bookListOldBook.getAuthorList().remove(author);
                    bookListOldBook = em.merge(bookListOldBook);
                }
            }
            for (Book bookListNewBook : bookListNew) {
                if (!bookListOld.contains(bookListNewBook)) {
                    bookListNewBook.getAuthorList().add(author);
                    bookListNewBook = em.merge(bookListNewBook);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = author.getId();
                if (findAuthor(id) == null) {
                    throw new NonexistentEntityException("The author with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Author
     *
     * @author Svitlana Myronova
     * @param id
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Author author;
            try {
                author = em.getReference(Author.class, id);
                author.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The author with id " + id + " no longer exists.", enfe);
            }
            List<Book> bookList = author.getBookList();
            for (Book bookListBook : bookList) {
                bookListBook.getAuthorList().remove(author);
                bookListBook = em.merge(bookListBook);
            }
            em.remove(author);
            utx.commit();
        } catch (NonexistentEntityException | NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find Author Entities
     * 
     * @author Svitlana Myronova
     * @return list of Author
     */
    public List<Author> findAuthorEntities() {
        LOG.info("getting all the authors");
        return findAuthorEntities(true, -1, -1);
    }

    /**
     * Find Author Entities
     * 
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Author
     */
    public List<Author> findAuthorEntities(int maxResults, int firstResult) {
        return findAuthorEntities(false, maxResults, firstResult);
    }

    /**
     * Find Author Entities
     * 
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Author
     */
    private List<Author> findAuthorEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Author.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Author entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return Author entity
     */
    public Author findAuthor(Integer id) {
        return em.find(Author.class, id);
    }

    /**
     * Get count of Author entities
     * 
     * @author Svitlana Myronova
     * @return
     */
    public int getAuthorCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Author> rt = cq.from(Author.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    /**
     * This query returns the names of all authors in a list
     * @author Cadin
     * @return List of author names (String)
     */
    public List<String> getAuthorsNames(){
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Author> author = cq.from(Author.class);
        cq.select(author.get("authorName"));
        Query query = em.createQuery(cq);
        return query.getResultList();
    }

    /**
     * This query returns the specified authors total of how much its books have sold,
     * the cost of the books, the profits made (sales - cost), last order date and amount of sales withing the Date range specified
     * and the number of sales
     * @param authorId int
     * @param from Date
     * @param to Date
     * @return SalesBean ( sales, cost, profit, last order date and number of sales)
     * @author Cadin Londono
     */
    public SalesBean getTotalSales(int authorId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Author> authors = cq.from(Author.class);
        Join<Author,Book> books = authors.join(Author_.bookList);
        Join<Book, OrderBook> orderBooks = books.join(Book_.orderBookList);
        Join<OrderBook,Order> orders = orderBooks.join(OrderBook_.Order);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<Date> lastOrder = cb.greatest(orders.get(Order_.orderDate));
        
        cq.multiselect( sales, cost, profit, numberOfSales, lastOrder);
        cq.where(cb.between(orders.get(Order_.orderDate), from, to),
                 cb.equal(authors.get("id"), authorId));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        
        return q.getSingleResult();
    }
    
    /**
     * This query returns the books of the author specified with their sales,
     * cost, profits (sales - cost), order date and amount of sales with the Date range 
     * specified
     * @param authorId
     * @param from
     * @param to
     * @return SalesBean (  book title,sales, cost, profit and number of sales)
     * @author Cadin Londono
     */
    public List<SalesBean> getTotalSalesAllBooks(int authorId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Author> authors = cq.from(Author.class);
        Join<Author,Book> books = authors.join(Author_.bookList);
        Join<Book, OrderBook> orderBooks = books.join(Book_.orderBookList);
        Join<OrderBook,Order> orders = orderBooks.join(OrderBook_.Order);
        
        Expression<BigDecimal> sale = orderBooks.get("price");
        Expression<BigDecimal> cost = books.get("wholeSalePrice");
        Expression<BigDecimal> profit = cb.diff(sale, cost);
        Expression<Long> numberOfSales = cb.literal(1L);
        Expression<Date> orderDate = orders.get(Order_.orderDate);
        
        cq.multiselect(books.get("title"), sale, cost, profit, numberOfSales, orderDate);
        cq.orderBy(cb.desc(orderDate), cb.asc(books.get("isbn")));
        cq.where(cb.between(orders.get(Order_.orderDate), from, to),
                 cb.equal(authors.get("id"), authorId));
        cq.groupBy(books.get("isbn"));
        cq.orderBy(cb.desc(orderDate));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        
        return q.getResultList();
    }
}
