package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Author;
import com.ga2w20.entities.Author_;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Book_;
import com.ga2w20.entities.Format;
import com.ga2w20.entities.Genre;
import com.ga2w20.entities.Genre_;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.Review;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Order_;
import com.ga2w20.entities.OrderBook_;
import com.ga2w20.entities.Publisher;
import com.ga2w20.entities.Publisher_;
import com.ga2w20.entities.Review;
import com.ga2w20.entities.SpecialBook;
import com.ga2w20.entities.SpecialBook_;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.StockBean;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaBuilder.Coalesce;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The BookJpaController is responsible for all queries and database actions
 * relevant to the Book entity
 * 
 * @author Svitlana Myronova, Liam Harbec
 */
@Named
@RequestScoped
public class BookJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(BookJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Book entity
     *
     * @author Svitlana Myronova
     * @param book
     * @throws PreexistingEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Book book) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (book.getAuthorList() == null) {
            book.setAuthorList(new ArrayList<Author>());
        }
        if (book.getFormatList() == null) {
            book.setFormatList(new ArrayList<Format>());
        }
        if (book.getReviewList() == null) {
            book.setReviewList(new ArrayList<Review>());
        }
        if (book.getOrderBookList() == null) {
            book.setOrderBookList(new ArrayList<OrderBook>());
        }
        if (book.getSpecialBookList() == null) {
            book.setSpecialBookList(new ArrayList<SpecialBook>());
        }

        try {
            utx.begin();
            Genre genreId = book.getGenreId();
            if (genreId != null) {
                genreId = em.getReference(genreId.getClass(), genreId.getId());
                book.setGenreId(genreId);
            }
            Publisher publisherId = book.getPublisherId();
            if (publisherId != null) {
                publisherId = em.getReference(publisherId.getClass(), publisherId.getId());
                book.setPublisherId(publisherId);
            }
            List<Author> attachedAuthorList = new ArrayList<Author>();
            for (Author authorListAuthorToAttach : book.getAuthorList()) {
                authorListAuthorToAttach = em.getReference(authorListAuthorToAttach.getClass(), authorListAuthorToAttach.getId());
                attachedAuthorList.add(authorListAuthorToAttach);
            }
            book.setAuthorList(attachedAuthorList);
            List<Format> attachedFormatList = new ArrayList<Format>();
            for (Format formatListFormatToAttach : book.getFormatList()) {
                formatListFormatToAttach = em.getReference(formatListFormatToAttach.getClass(), formatListFormatToAttach.getId());
                attachedFormatList.add(formatListFormatToAttach);
            }
            book.setFormatList(attachedFormatList);
            List<Review> attachedReviewList = new ArrayList<Review>();
            for (Review reviewListReviewToAttach : book.getReviewList()) {
                reviewListReviewToAttach = em.getReference(reviewListReviewToAttach.getClass(), reviewListReviewToAttach.getReviewPK());
                attachedReviewList.add(reviewListReviewToAttach);
            }
            book.setReviewList(attachedReviewList);
            List<OrderBook> attachedOrderBookList = new ArrayList<OrderBook>();
            for (OrderBook orderBookListOrderBookToAttach : book.getOrderBookList()) {
                orderBookListOrderBookToAttach = em.getReference(orderBookListOrderBookToAttach.getClass(), orderBookListOrderBookToAttach.getOrderBookPK());
                attachedOrderBookList.add(orderBookListOrderBookToAttach);
            }
            book.setOrderBookList(attachedOrderBookList);
            List<SpecialBook> attachedSpecialBookList = new ArrayList<SpecialBook>();
            for (SpecialBook specialBookListSpecialBookToAttach : book.getSpecialBookList()) {
                specialBookListSpecialBookToAttach = em.getReference(specialBookListSpecialBookToAttach.getClass(), specialBookListSpecialBookToAttach.getId());
                attachedSpecialBookList.add(specialBookListSpecialBookToAttach);
            }
            book.setSpecialBookList(attachedSpecialBookList);
            em.persist(book);
            if (genreId != null) {
                genreId.getBookList().add(book);
                genreId = em.merge(genreId);
            }
            if (publisherId != null) {
                publisherId.getBookList().add(book);
                publisherId = em.merge(publisherId);
            }
            for (Author authorListAuthor : book.getAuthorList()) {
                authorListAuthor.getBookList().add(book);
                authorListAuthor = em.merge(authorListAuthor);
            }
            for (Format formatListFormat : book.getFormatList()) {
                formatListFormat.getBookList().add(book);
                formatListFormat = em.merge(formatListFormat);
            }
            for (Review reviewListReview : book.getReviewList()) {
                Book oldBookOfReviewListReview = reviewListReview.getBook();
                reviewListReview.setBook(book);
                reviewListReview = em.merge(reviewListReview);
                if (oldBookOfReviewListReview != null) {
                    oldBookOfReviewListReview.getReviewList().remove(reviewListReview);
                    oldBookOfReviewListReview = em.merge(oldBookOfReviewListReview);
                }
            }
            for (OrderBook orderBookListOrderBook : book.getOrderBookList()) {
                Book oldBookOfOrderBookListOrderBook = orderBookListOrderBook.getBook();
                orderBookListOrderBook.setBook(book);
                orderBookListOrderBook = em.merge(orderBookListOrderBook);
                if (oldBookOfOrderBookListOrderBook != null) {
                    oldBookOfOrderBookListOrderBook.getOrderBookList().remove(orderBookListOrderBook);
                    oldBookOfOrderBookListOrderBook = em.merge(oldBookOfOrderBookListOrderBook);
                }
            }
            for (SpecialBook specialBookListSpecialBook : book.getSpecialBookList()) {
                Book oldIsbnOfSpecialBookListSpecialBook = specialBookListSpecialBook.getIsbn();
                specialBookListSpecialBook.setIsbn(book);
                specialBookListSpecialBook = em.merge(specialBookListSpecialBook);
                if (oldIsbnOfSpecialBookListSpecialBook != null) {
                    oldIsbnOfSpecialBookListSpecialBook.getSpecialBookList().remove(specialBookListSpecialBook);
                    oldIsbnOfSpecialBookListSpecialBook = em.merge(oldIsbnOfSpecialBookListSpecialBook);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findBook(book.getIsbn()) != null) {
                throw new PreexistingEntityException("Book " + book + " already exists.", ex);
            }
            throw ex;
        }

    }

    /**
     * Edit Book entity
     *
     * @author Svitlana Myronova
     * @param book
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Book book) throws IllegalOrphanException, NonexistentEntityException, Exception {

        try {
            utx.begin();
            Book persistentBook = em.find(Book.class, book.getIsbn());
            Genre genreIdOld = persistentBook.getGenreId();
            Genre genreIdNew = book.getGenreId();
            Publisher publisherIdOld = persistentBook.getPublisherId();
            Publisher publisherIdNew = book.getPublisherId();
            List<Author> authorListOld = persistentBook.getAuthorList();
            List<Author> authorListNew = book.getAuthorList();
            List<Format> formatListOld = persistentBook.getFormatList();
            List<Format> formatListNew = book.getFormatList();
            List<Review> reviewListOld = persistentBook.getReviewList();
            List<Review> reviewListNew = book.getReviewList();
            List<OrderBook> orderBookListOld = persistentBook.getOrderBookList();
            List<OrderBook> orderBookListNew = book.getOrderBookList();
            List<SpecialBook> specialBookListOld = persistentBook.getSpecialBookList();
            List<SpecialBook> specialBookListNew = book.getSpecialBookList();
            List<String> illegalOrphanMessages = null;
            for (Review reviewListOldReview : reviewListOld) {
                if (!reviewListNew.contains(reviewListOldReview)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Review " + reviewListOldReview + " since its book field is not nullable.");
                }
            }
            for (OrderBook orderBookListOldOrderBook : orderBookListOld) {
                if (!orderBookListNew.contains(orderBookListOldOrderBook)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OrderBook " + orderBookListOldOrderBook + " since its book field is not nullable.");
                }
            }
            for (SpecialBook specialBookListOldSpecialBook : specialBookListOld) {
                if (!specialBookListNew.contains(specialBookListOldSpecialBook)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SpecialBook " + specialBookListOldSpecialBook + " since its isbn field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (genreIdNew != null) {
                genreIdNew = em.getReference(genreIdNew.getClass(), genreIdNew.getId());
                book.setGenreId(genreIdNew);
            }
            if (publisherIdNew != null) {
                publisherIdNew = em.getReference(publisherIdNew.getClass(), publisherIdNew.getId());
                book.setPublisherId(publisherIdNew);
            }
            List<Author> attachedAuthorListNew = new ArrayList<Author>();
            for (Author authorListNewAuthorToAttach : authorListNew) {
                authorListNewAuthorToAttach = em.getReference(authorListNewAuthorToAttach.getClass(), authorListNewAuthorToAttach.getId());
                attachedAuthorListNew.add(authorListNewAuthorToAttach);
            }
            authorListNew = attachedAuthorListNew;
            book.setAuthorList(authorListNew);
            List<Format> attachedFormatListNew = new ArrayList<Format>();
            for (Format formatListNewFormatToAttach : formatListNew) {
                formatListNewFormatToAttach = em.getReference(formatListNewFormatToAttach.getClass(), formatListNewFormatToAttach.getId());
                attachedFormatListNew.add(formatListNewFormatToAttach);
            }
            formatListNew = attachedFormatListNew;
            book.setFormatList(formatListNew);
            List<Review> attachedReviewListNew = new ArrayList<Review>();
            for (Review reviewListNewReviewToAttach : reviewListNew) {
                reviewListNewReviewToAttach = em.getReference(reviewListNewReviewToAttach.getClass(), reviewListNewReviewToAttach.getReviewPK());
                attachedReviewListNew.add(reviewListNewReviewToAttach);
            }
            reviewListNew = attachedReviewListNew;
            book.setReviewList(reviewListNew);
            List<OrderBook> attachedOrderBookListNew = new ArrayList<OrderBook>();
            for (OrderBook orderBookListNewOrderBookToAttach : orderBookListNew) {
                orderBookListNewOrderBookToAttach = em.getReference(orderBookListNewOrderBookToAttach.getClass(), orderBookListNewOrderBookToAttach.getOrderBookPK());
                attachedOrderBookListNew.add(orderBookListNewOrderBookToAttach);
            }
            orderBookListNew = attachedOrderBookListNew;
            book.setOrderBookList(orderBookListNew);
            List<SpecialBook> attachedSpecialBookListNew = new ArrayList<SpecialBook>();
            for (SpecialBook specialBookListNewSpecialBookToAttach : specialBookListNew) {
                specialBookListNewSpecialBookToAttach = em.getReference(specialBookListNewSpecialBookToAttach.getClass(), specialBookListNewSpecialBookToAttach.getId());
                attachedSpecialBookListNew.add(specialBookListNewSpecialBookToAttach);
            }
            specialBookListNew = attachedSpecialBookListNew;
            book.setSpecialBookList(specialBookListNew);
            book = em.merge(book);
            if (genreIdOld != null && !genreIdOld.equals(genreIdNew)) {
                genreIdOld.getBookList().remove(book);
                genreIdOld = em.merge(genreIdOld);
            }
            if (genreIdNew != null && !genreIdNew.equals(genreIdOld)) {
                genreIdNew.getBookList().add(book);
                genreIdNew = em.merge(genreIdNew);
            }
            if (publisherIdOld != null && !publisherIdOld.equals(publisherIdNew)) {
                publisherIdOld.getBookList().remove(book);
                publisherIdOld = em.merge(publisherIdOld);
            }
            if (publisherIdNew != null && !publisherIdNew.equals(publisherIdOld)) {
                publisherIdNew.getBookList().add(book);
                publisherIdNew = em.merge(publisherIdNew);
            }
            for (Author authorListOldAuthor : authorListOld) {
                if (!authorListNew.contains(authorListOldAuthor)) {
                    authorListOldAuthor.getBookList().remove(book);
                    authorListOldAuthor = em.merge(authorListOldAuthor);
                }
            }
            for (Author authorListNewAuthor : authorListNew) {
                if (!authorListOld.contains(authorListNewAuthor)) {
                    authorListNewAuthor.getBookList().add(book);
                    authorListNewAuthor = em.merge(authorListNewAuthor);
                }
            }
            for (Format formatListOldFormat : formatListOld) {
                if (!formatListNew.contains(formatListOldFormat)) {
                    formatListOldFormat.getBookList().remove(book);
                    formatListOldFormat = em.merge(formatListOldFormat);
                }
            }
            for (Format formatListNewFormat : formatListNew) {
                if (!formatListOld.contains(formatListNewFormat)) {
                    formatListNewFormat.getBookList().add(book);
                    formatListNewFormat = em.merge(formatListNewFormat);
                }
            }
            for (Review reviewListNewReview : reviewListNew) {
                if (!reviewListOld.contains(reviewListNewReview)) {
                    Book oldBookOfReviewListNewReview = reviewListNewReview.getBook();
                    reviewListNewReview.setBook(book);
                    reviewListNewReview = em.merge(reviewListNewReview);
                    if (oldBookOfReviewListNewReview != null && !oldBookOfReviewListNewReview.equals(book)) {
                        oldBookOfReviewListNewReview.getReviewList().remove(reviewListNewReview);
                        oldBookOfReviewListNewReview = em.merge(oldBookOfReviewListNewReview);
                    }
                }
            }
            for (OrderBook orderBookListNewOrderBook : orderBookListNew) {
                if (!orderBookListOld.contains(orderBookListNewOrderBook)) {
                    Book oldBookOfOrderBookListNewOrderBook = orderBookListNewOrderBook.getBook();
                    orderBookListNewOrderBook.setBook(book);
                    orderBookListNewOrderBook = em.merge(orderBookListNewOrderBook);
                    if (oldBookOfOrderBookListNewOrderBook != null && !oldBookOfOrderBookListNewOrderBook.equals(book)) {
                        oldBookOfOrderBookListNewOrderBook.getOrderBookList().remove(orderBookListNewOrderBook);
                        oldBookOfOrderBookListNewOrderBook = em.merge(oldBookOfOrderBookListNewOrderBook);
                    }
                }
            }
            for (SpecialBook specialBookListNewSpecialBook : specialBookListNew) {
                if (!specialBookListOld.contains(specialBookListNewSpecialBook)) {
                    Book oldIsbnOfSpecialBookListNewSpecialBook = specialBookListNewSpecialBook.getIsbn();
                    specialBookListNewSpecialBook.setIsbn(book);
                    specialBookListNewSpecialBook = em.merge(specialBookListNewSpecialBook);
                    if (oldIsbnOfSpecialBookListNewSpecialBook != null && !oldIsbnOfSpecialBookListNewSpecialBook.equals(book)) {
                        oldIsbnOfSpecialBookListNewSpecialBook.getSpecialBookList().remove(specialBookListNewSpecialBook);
                        oldIsbnOfSpecialBookListNewSpecialBook = em.merge(oldIsbnOfSpecialBookListNewSpecialBook);
                    }
                }
            }

            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = book.getIsbn();
                if (findBook(id) == null) {
                    throw new NonexistentEntityException("The book with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Book entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Long id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Book book;
            try {
                book = em.getReference(Book.class, id);
                book.getIsbn();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The book with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Review> reviewListOrphanCheck = book.getReviewList();
            for (Review reviewListOrphanCheckReview : reviewListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Book (" + book + ") cannot be destroyed since the Review " + reviewListOrphanCheckReview + " in its reviewList field has a non-nullable book field.");
            }
            List<OrderBook> orderBookListOrphanCheck = book.getOrderBookList();
            for (OrderBook orderBookListOrphanCheckOrderBook : orderBookListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Book (" + book + ") cannot be destroyed since the OrderBook " + orderBookListOrphanCheckOrderBook + " in its orderBookList field has a non-nullable book field.");
            }
            List<SpecialBook> specialBookListOrphanCheck = book.getSpecialBookList();
            for (SpecialBook specialBookListOrphanCheckSpecialBook : specialBookListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Book (" + book + ") cannot be destroyed since the SpecialBook " + specialBookListOrphanCheckSpecialBook + " in its specialBookList field has a non-nullable isbn field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Genre genreId = book.getGenreId();
            if (genreId != null) {
                genreId.getBookList().remove(book);
                genreId = em.merge(genreId);
            }
            Publisher publisherId = book.getPublisherId();
            if (publisherId != null) {
                publisherId.getBookList().remove(book);
                publisherId = em.merge(publisherId);
            }
            List<Author> authorList = book.getAuthorList();
            for (Author authorListAuthor : authorList) {
                authorListAuthor.getBookList().remove(book);
                authorListAuthor = em.merge(authorListAuthor);
            }
            List<Format> formatList = book.getFormatList();
            for (Format formatListFormat : formatList) {
                formatListFormat.getBookList().remove(book);
                formatListFormat = em.merge(formatListFormat);
            }
            em.remove(book);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find Book Entities
     *
     * @author Svitlana Myronova
     * @return list of Book
     */
    public List<Book> findBookEntities() {
        LOG.info("getting all the books");
        return findBookEntities(true, -1, -1);
    }

    /**
     * Find Book Entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Book
     */
    public List<Book> findBookEntities(int maxResults, int firstResult) {
        return findBookEntities(false, maxResults, firstResult);
    }

    /**
     * Find Book Entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Book
     */
    private List<Book> findBookEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Book.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Book entity by id
     *
     * @author Svitlana Myronova
     * @param isbn
     * @return
     */
    public Book findBook(Long isbn) {
        Book b = em.find(Book.class, isbn);
        return b;
    }
   
    /**
     * Get count of Book entities
     *
     * @author Svitlana Myronova
     * @return number of Book entities
     */
    public int getBookCount() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> rt = cq.from(Book.class);
        cq.select(cb.count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * Get the number of all books that have removalStatus = 0
     * 
     * @author Svitlana Myronova
     * @return the number of books
     */    
    public int getBookCountActive() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> rt = cq.from(Book.class);
        cq.select(cb.count(rt));
        cq.where(cb.equal(rt.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
        
    /**
     * Get the list of books that are active now (removalStatus = 0) with the special genre 
     * 
     * @author Svitlana Myronova
     * @param genreId
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getBooksWithGenreActive(int genreId, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        Join<Book, Genre> bookGenre = book.join(Book_.genreId);
        cq.select(book);
        cq.where(cb.equal(bookGenre.get(Genre_.id), genreId),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        q.setMaxResults(maxItems);
        return q.getResultList();
    }    
    
    /**
     * Get the list of top sellers (all genres) that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param firstItem
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getTopSellersWithAllGenres(int firstItem, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        Root<Book> book = cq.from(Book.class);        
        Join<Book, OrderBook> bookOrderBook = book.join(Book_.orderBookList, JoinType.LEFT);        
        cq.select(cb.tuple(book, cb.count(bookOrderBook))); 
        cq.where(cb.equal(book.get(Book_.removalStatus), false));
        cq.groupBy(book.get(Book_.isbn));
        cq.orderBy(cb.desc(cb.count(bookOrderBook)));
        Query q = em.createQuery(cq);
        q.setFirstResult(firstItem);
        q.setMaxResults(maxItems);
        
        ArrayList<Book> books = new ArrayList<Book>();
        List<Tuple> result = q.getResultList();
        for (Tuple t : result) {
            Book b = (Book) t.get(0);
            books.add(b);
        }
        return books;
    }

    /**
     * Get the list of top sellers with the genreId that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param genreId
     * @param firstItem
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getTopSellersWithGenre(int genreId, int firstItem, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq = cb.createTupleQuery();
        Root<Book> book = cq.from(Book.class);        
        Join<Book, OrderBook> bookOrderBook = book.join(Book_.orderBookList, JoinType.LEFT);        
        cq.select(cb.tuple(book, cb.count(bookOrderBook)));        
        cq.where(cb.equal(book.get(Book_.genreId).get(Genre_.id), genreId),
                 cb.equal(book.get(Book_.removalStatus), false));        
        cq.groupBy(book.get(Book_.isbn));
        cq.orderBy(cb.desc(cb.count(bookOrderBook)));
        Query q = em.createQuery(cq);
        q.setFirstResult(firstItem);
        q.setMaxResults(maxItems);
        
        ArrayList<Book> books = new ArrayList<Book>();
        List<Tuple> result = q.getResultList();
        for (Tuple t : result) {
            Book b = (Book) t.get(0);
            books.add(b);
        }
        return books;
    }

    /**
     * Get the number of books with the genreId that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param genreId
     * @return the number of books
     */
    public int getBooksWithGenreCount(Integer genreId) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        cq.select(cb.count(book));
        cq.where(cb.equal(book.get(Book_.genreId).get(Genre_.id), genreId),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Get the list of books with the Title that fits the request from the that have removalStatus = 0
     * search query
     *
     * @author Svitlana Myronova
     * @param title
     * @param firstItem
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getBooksLikeTitle(String title, int firstItem, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        cq.select(book);
        cq.where(cb.like(book.get(Book_.title), "%" + title + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        q.setFirstResult(firstItem);
        q.setMaxResults(maxItems);
        return q.getResultList();
    }

    /**
     * Get the number of books with the Title that fits the request from the
     * search query that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param title
     * @return the number of books
     */
    public int getBooksLikeTitleCount(String title) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        cq.select(cb.count(book));
        cq.where(cb.like(book.get(Book_.title), "%" + title + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Get the list of books with the Author that fits the request from the
     * search query that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param author
     * @param firstItem
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getBooksLikeAuthor(String author, int firstItem, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        Join<Book, Author> bookAuthor = book.join(Book_.authorList);
        cq.select(book);
        cq.where(cb.like(bookAuthor.get(Author_.authorName), "%" + author + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        q.setFirstResult(firstItem);
        q.setMaxResults(maxItems);
        return q.getResultList();
    }

    /**
     * Get the number of books with the Author that fits the request from the
     * search query that have removalStatus = 0
     *
     * @author Svitlana Myoronova
     * @param author
     * @return the number of books
     */
    public int getBooksLikeAuthorCount(String author) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        Join<Book, Author> bookAuthor = book.join(Book_.authorList);
        cq.select(cb.count(book));
        cq.where(cb.like(bookAuthor.get(Author_.authorName), "%" + author + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * Get the list of books with the Publisher that fits the request from the
     * search query that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param publisher
     * @param firstItem
     * @param maxItems
     * @return the list of books
     */
    public List<Book> getBooksLikePublisher(String publisher, int firstItem, int maxItems) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        Join<Book, Publisher> bookPublisher = book.join(Book_.publisherId);
        cq.select(book);
        cq.where(cb.like(bookPublisher.get(Publisher_.publisherName), "%" + publisher + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        q.setFirstResult(firstItem);
        q.setMaxResults(maxItems);
        return q.getResultList();
    }

    /**
     * Get the number of books with the Publisher that fits the request from the
     * search query that have removalStatus = 0
     *
     * @author Svitlana Myronova
     * @param publisher
     * @return the number of books
     */
    public int getBooksLikePublisherCount(String publisher) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Book> book = cq.from(Book.class);
        Join bookPublisher = book.join(Book_.publisherId);
        cq.select(cb.count(book));
        cq.where(cb.like(bookPublisher.get(Publisher_.publisherName), "%" + publisher + "%"),
                cb.equal(book.get(Book_.removalStatus), false));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    
    /**
     * Gets a list of books with the same genre
     *
     * @author Liam Harbec
     * @param genre The genre to search for
     * @param isbn The isbn of the current book
     * @return The list of books
     */
    public List<Book> getBooksWithGenre(String genre, Long isbn) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> query = cb.createQuery(Book.class);
        Root<Book> book = query.from(Book.class);
        Join bookGenre = book.join(Book_.genreId, JoinType.INNER);
        query.select(book)
                .where(cb.notEqual(book.get(Book_.isbn), isbn),
                        cb.equal(bookGenre.get(Genre_.genreTitle.getName()), genre));
        TypedQuery<Book> genreBooks = em.createQuery(query);
        List<Book> books = genreBooks.getResultList();

        Collections.shuffle(books);

        return books;
    }

    
    /**
     * Gets a list of books with the same author
     *
     * @author Liam Harbec
     * @param author The author to search for
     * @param isbn The isbn of the current book
     * @return The list of books
     */
    public List<Book> getBooksWithAuthor(String author, Long isbn) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Book> query = cb.createQuery(Book.class);
        Root<Book> book = query.from(Book.class);
        Join bookAuthor = book.join(Book_.authorList);
        query.select(book)
                .where(cb.notEqual(book.get(Book_.isbn), isbn),
                        cb.equal(bookAuthor.get(Author_.authorName), author));

        TypedQuery<Book> authorBooks = em.createQuery(query);
        List<Book> books = authorBooks.getResultList();
        Collections.shuffle(books);
        return books;
    }

    /**
     * This query returns the books that have been order withing a date range
     * ordered desc by the amount of times it has sold.
     * @param from
     * @param to
     * @return List of SummaryBean (book title, sales, cost, profit and number of sales)
     * @author Cadin Londono
     */
    public List<SummaryBean> getTopSellingBooks(Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SummaryBean> cq = cb.createQuery(SummaryBean.class);
        Root<Order> order =  cq.from(Order.class);
        Join <Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        orderBooks.on(cb.between(order.get(Order_.orderDate), from, to));
        Join<OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        
        cq.multiselect(books.get("title"), sales, cost, profit, numberOfSales);
        cq.groupBy(books.get("isbn"));
        
        cq.orderBy(cb.desc(cb.count(orderBooks.get("price"))), cb.asc(books.get("isbn")));
        TypedQuery<SummaryBean> query = em.createQuery(cq);
        List<SummaryBean> bookSalesBeanList = query.getResultList();
        return bookSalesBeanList;
    }
    /**
     * This query returns a list of all the books that haven't sold
     * with the date range specified
     * @param from Date
     * @param to Date
     * @return List of SummaryBean (book title, sales, cost, profit and number of sales)
     * @author Cadin Londono
     */
    public List<SummaryBean> getZeroSalesBooks(Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SummaryBean> cq = cb.createQuery(SummaryBean.class);     
        Root<Book> books = cq.from(Book.class);
        Join<Book,OrderBook> orderBooks = books.join(Book_.orderBookList, JoinType.LEFT);
        Join<OrderBook,Order> order = orderBooks.join(OrderBook_.Order, JoinType.LEFT);
        
        Expression<Long> numberOfSales = cb.literal(0L);
        Expression<BigDecimal> sales = cb.literal(new BigDecimal(0));
        Expression<BigDecimal> cost = cb.literal(new BigDecimal(0));
        Expression<BigDecimal> profit = cb.literal(new BigDecimal(0));
        
        //SUBQUERY FOR GETTING ALL BOOKS THAT HAVE BEEN SOLD DURIGN THIS TIME FRAME
        //IN ORDER TO REMOVE THEM FROM THE LIST
        Subquery sub = cq.subquery(Long.class);
        Root subRoot = sub.from(Book.class);
        Join<Book, OrderBook> subOrderBooks = subRoot.join(Book_.orderBookList);
        Join<OrderBook, Order> subOrder = subOrderBooks.join(OrderBook_.Order);
        sub.select(subRoot.get("isbn"));
        sub.where(cb.between(subOrder.get("orderDate"), from, to));
        
        cq.multiselect(books.get("title"), sales, cost, profit, numberOfSales);
        cq.groupBy(books.get("isbn"));
        cq.where(cb.or(cb.isNull(order.get("orderDate")), cb.not(cb.in(books.get("isbn")).value(sub)))  );
        cq.orderBy(cb.asc(books.get("isbn")));
        TypedQuery<SummaryBean> query = em.createQuery(cq);
        List<SummaryBean> bookSalesBeanList = query.getResultList();
        
        return bookSalesBeanList;
    }
    
    /**
     * This books returns all of the books in inventory along with their
     * wholesale price, list price, sales, cost, profit and number of sales
     * @return List of StockBeans (book title,wholesale price, list price, sales, cost, profit and number of sales)
     * @author Cadin Londono
     */
    public List<StockBean> getStockReport(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockBean> cq = cb.createQuery(StockBean.class);     
        Root<Book> books = cq.from(Book.class);
        Join<Book,OrderBook> orderBooks = books.join(Book_.orderBookList, JoinType.LEFT);
        Join<OrderBook,Order> order = orderBooks.join(OrderBook_.Order, JoinType.LEFT);
        
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        //We use coalesce to get 0 instead of null in the sales field if the book has not sold
        Coalesce <BigDecimal> sales = cb.coalesce();
        sales.value(cb.sum(orderBooks.get("price")));
        sales.value(new BigDecimal(0));
        Expression<BigDecimal> cost = cb.prod(books.get("wholeSalePrice"), numberOfSales).as(BigDecimal.class);
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        
        cq.multiselect(books.get("title"),books.get("wholeSalePrice"), books.get("listPrice"),sales, cost, profit, numberOfSales);
        cq.groupBy(books.get("isbn"));
        cq.orderBy(cb.asc(books.get("isbn")));
        TypedQuery<StockBean> query = em.createQuery(cq);
        List<StockBean> bookSalesBeanList = query.getResultList();
        
        return bookSalesBeanList;
    }
    /**
     * This method will return the total sales $ of a book up to now
     * @author Cadin Londono
     * @param book
     * @return BigDecimal total sales
     */
    public BigDecimal getBookTotalSales(Book book){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<BigDecimal> cq = cb.createQuery(BigDecimal.class);     
        Root<Book> books = cq.from(Book.class);
        Join<Book,OrderBook> orderBooks = books.join(Book_.orderBookList, JoinType.LEFT);
        Join<OrderBook,Order> order = orderBooks.join(OrderBook_.Order, JoinType.LEFT);
        
        Coalesce <BigDecimal> sales = cb.coalesce();
        sales.value(cb.sum(orderBooks.get("price")));
        sales.value(new BigDecimal(0));
        
        cq.select(sales);
        cq.where(cb.equal(books.get("isbn"), book.getIsbn()));
        cq.groupBy(books.get("isbn"));
        TypedQuery<BigDecimal> query = em.createQuery(cq);
        return query.getSingleResult();
    }
    
    /* 
     * Gets the percentage sale of a book that is currently on sale.
     *
     * @author Liam Harbec
     * @param book The book
     * @return The percentage of sale, if any at all
     */
    public BigDecimal getSpecialBook(Long isbn, Date currentDate) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SpecialBook> query = cb.createQuery(SpecialBook.class);
        Root<SpecialBook> specialBook = query.from(SpecialBook.class);
        query.select(specialBook)
                .where(cb.equal(specialBook.get(SpecialBook_.isbn).get(Book_.isbn), isbn),
                        cb.between(cb.literal(currentDate),
                                specialBook.get(SpecialBook_.startDate),
                                specialBook.get(SpecialBook_.endDate)));

        TypedQuery<SpecialBook> specialBookQuery = em.createQuery(query);

        try {
            SpecialBook specialBookResult = specialBookQuery.getSingleResult();
            return specialBookResult.getPercentage();
        } catch (NoResultException e) {
            return BigDecimal.ZERO;
        }
    }
}
