package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Order_;
import com.ga2w20.entities.OrderBook_;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga2w20.entities.Client;
import com.ga2w20.entities.Client_;
import com.ga2w20.entities.Province;
import com.ga2w20.entities.SurveyClient;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.math.BigDecimal;
import java.util.Date;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**

 * The ClientJpaController is responsible for all queries and database actions
 * relevant to the Client entity
 * 
 * @author Svitlana Myronova, Liam Harbec, Cadin Londono
 */
@Named
@RequestScoped
public class ClientJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ClientJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Client entity
     *
     * @author Svitlana Myronova
     * @param client
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Client client) throws RollbackFailureException, Exception {
        try {
            if (client.getSurveyClientList() == null) {
                client.setSurveyClientList(new ArrayList<SurveyClient>());
            }
            utx.begin();
            Province provinceId = client.getProvinceId();
            if (provinceId != null) {
                provinceId = em.getReference(provinceId.getClass(), provinceId.getId());
                client.setProvinceId(provinceId);
            }
            List<SurveyClient> attachedSurveyClientList = new ArrayList<SurveyClient>();
            for (SurveyClient surveyClientListSurveyClientToAttach : client.getSurveyClientList()) {
                surveyClientListSurveyClientToAttach = em.getReference(surveyClientListSurveyClientToAttach.getClass(), surveyClientListSurveyClientToAttach.getId());
                attachedSurveyClientList.add(surveyClientListSurveyClientToAttach);
            }
            client.setSurveyClientList(attachedSurveyClientList);
            em.persist(client);
            if (provinceId != null) {
                provinceId.getClientList().add(client);
                provinceId = em.merge(provinceId);
            }
            for (SurveyClient surveyClientListSurveyClient : client.getSurveyClientList()) {
                Client oldClientIdOfSurveyClientListSurveyClient = surveyClientListSurveyClient.getClientId();
                surveyClientListSurveyClient.setClientId(client);
                surveyClientListSurveyClient = em.merge(surveyClientListSurveyClient);
                if (oldClientIdOfSurveyClientListSurveyClient != null) {
                    oldClientIdOfSurveyClientListSurveyClient.getSurveyClientList().remove(surveyClientListSurveyClient);
                    oldClientIdOfSurveyClientListSurveyClient = em.merge(oldClientIdOfSurveyClientListSurveyClient);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit Client entity
     *
     * @author Svitlana Myronova
     * @param client
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Client client) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            Client persistentClient = em.find(Client.class, client.getId());
            Province provinceIdOld = persistentClient.getProvinceId();
            Province provinceIdNew = client.getProvinceId();
            List<SurveyClient> surveyClientListOld = persistentClient.getSurveyClientList();
            List<SurveyClient> surveyClientListNew = client.getSurveyClientList();
            if (provinceIdNew != null) {
                provinceIdNew = em.getReference(provinceIdNew.getClass(), provinceIdNew.getId());
                client.setProvinceId(provinceIdNew);
            }
            List<SurveyClient> attachedSurveyClientListNew = new ArrayList<SurveyClient>();
            for (SurveyClient surveyClientListNewSurveyClientToAttach : surveyClientListNew) {
                surveyClientListNewSurveyClientToAttach = em.getReference(surveyClientListNewSurveyClientToAttach.getClass(), surveyClientListNewSurveyClientToAttach.getId());
                attachedSurveyClientListNew.add(surveyClientListNewSurveyClientToAttach);
            }
            surveyClientListNew = attachedSurveyClientListNew;
            client.setSurveyClientList(surveyClientListNew);
            client = em.merge(client);
            if (provinceIdOld != null && !provinceIdOld.equals(provinceIdNew)) {
                provinceIdOld.getClientList().remove(client);
                provinceIdOld = em.merge(provinceIdOld);
            }
            if (provinceIdNew != null && !provinceIdNew.equals(provinceIdOld)) {
                provinceIdNew.getClientList().add(client);
                provinceIdNew = em.merge(provinceIdNew);
            }
            for (SurveyClient surveyClientListOldSurveyClient : surveyClientListOld) {
                if (!surveyClientListNew.contains(surveyClientListOldSurveyClient)) {
                    surveyClientListOldSurveyClient.setClientId(null);
                    surveyClientListOldSurveyClient = em.merge(surveyClientListOldSurveyClient);
                }
            }
            for (SurveyClient surveyClientListNewSurveyClient : surveyClientListNew) {
                if (!surveyClientListOld.contains(surveyClientListNewSurveyClient)) {
                    Client oldClientIdOfSurveyClientListNewSurveyClient = surveyClientListNewSurveyClient.getClientId();
                    surveyClientListNewSurveyClient.setClientId(client);
                    surveyClientListNewSurveyClient = em.merge(surveyClientListNewSurveyClient);
                    if (oldClientIdOfSurveyClientListNewSurveyClient != null && !oldClientIdOfSurveyClientListNewSurveyClient.equals(client)) {
                        oldClientIdOfSurveyClientListNewSurveyClient.getSurveyClientList().remove(surveyClientListNewSurveyClient);
                        oldClientIdOfSurveyClientListNewSurveyClient = em.merge(oldClientIdOfSurveyClientListNewSurveyClient);
                    }
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = client.getId();
                if (findClient(id) == null) {
                    throw new NonexistentEntityException("The client with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Client entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Client client;
            try {
                client = em.getReference(Client.class, id);
                client.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The client with id " + id + " no longer exists.", enfe);
            }
            Province provinceId = client.getProvinceId();
            if (provinceId != null) {
                provinceId.getClientList().remove(client);
                provinceId = em.merge(provinceId);
            }
            List<SurveyClient> surveyClientList = client.getSurveyClientList();
            for (SurveyClient surveyClientListSurveyClient : surveyClientList) {
                surveyClientListSurveyClient.setClientId(null);
                surveyClientListSurveyClient = em.merge(surveyClientListSurveyClient);
            }
            em.remove(client);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find Client entities
     *
     * @author Svitlana Myronova
     * @return list of Client entities
     */
    public List<Client> findClientEntities() {
        LOG.info("getting all the clients");
        return findClientEntities(true, -1, -1);
    }

    /**
     * Find Client entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Client entities
     */
    public List<Client> findClientEntities(int maxResults, int firstResult) {
        return findClientEntities(false, maxResults, firstResult);
    }

    /**
     * Find Client entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Client entities
     */
    private List<Client> findClientEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Client.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Client entity by id
     * 
     * @author Svitlana Myronova
     * @param id
     * @return Client entity
     */
    public Client findClient(Integer id) {

        return em.find(Client.class, id);
    }

    /**
     * Get count of Client entities
     *
     * @author Svitlana Myronova
     * @return number of Client entities
     */
    public int getClientCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Client> rt = cq.from(Client.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    /**
     * Get the total sales ($ spent) of the specified clietn within
     * the date range
     * @param clientId
     * @param from Date
     * @param to Date
     * @return SalesBean ( sales, cost, profit, last order date and number of sales)
     * @author Cadin Londono
     */
    public SalesBean getTotalSales(int clientId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Client> client = cq.from(Client.class);
        Join<Client,Order> order = client.join(Client_.OrderList);
        Join<Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        Join <OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<Date> lastOrder = cb.greatest(order.get(Order_.orderDate));
        
        cq.multiselect( sales, cost, profit, numberOfSales, lastOrder);
        cq.where(cb.between(order.get(Order_.orderDate), from, to),
                 cb.equal(client.get("id"), clientId));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        return q.getSingleResult();
    }
    /**
     * Get all the orders of the books that the client has bought with its sales,cost, profits, amount of sales and order date made
     * withing the date range specified
     * @param clientId
     * @param from
     * @param to
     * @return List of SalesBean ( book title,sales, cost, profit and number of sales, order date)
     * @author Cadin Londono
     */
    public List<SalesBean> getTotalSalesAllBooks(int clientId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Client> client = cq.from(Client.class);
        Join<Client,Order> order = client.join(Client_.OrderList);
        Join<Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        Join <OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sale = orderBooks.get("price");
        Expression<BigDecimal> cost = books.get("wholeSalePrice");
        Expression<BigDecimal> profit = cb.diff(sale, cost);
        Expression<Long> numberOfSales = cb.literal(1L);
        Expression<Date> orderDate = order.get(Order_.orderDate);
        
        cq.multiselect(books.get("title"), sale, cost, profit, numberOfSales, orderDate);
        cq.orderBy(cb.desc(orderDate), cb.asc(books.get("isbn")));
        cq.where(cb.between(order.get(Order_.orderDate), from, to),
                 cb.equal(client.get("id"), clientId));
        cq.groupBy(books.get("isbn"));
        cq.orderBy(cb.desc(orderDate));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        List<SalesBean> salesBeans =  q.getResultList();
        return salesBeans;
    }
    /**
     * Returns a list of sales beans containing the full names of the clients 
     * along with their sales ($ spent), cost from books, profit made from them and
     * amount of sales. Ordered desc by amount of sales
     * @param from Date
     * @param to Date
     * @return List of SummaryBean ( client full name,sales, cost, profit and number of sales)
     * @author Cadin Londono
     */
    public List<SummaryBean> getTopClients(Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SummaryBean> cq = cb.createQuery(SummaryBean.class);
        Root<Client> client = cq.from(Client.class);
        Join<Client,Order> order = client.join(Client_.OrderList);
        Join<Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        Join <OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<String> clientFullName = cb.concat(cb.concat(cb.concat(cb.concat(client.get("firstName"), " "), client.get("lastName")), " #"), client.get("id"));
        
        cq.multiselect( clientFullName ,sales, cost, profit, numberOfSales);
        cq.groupBy(client.get("id"));
        cq.where(cb.between(order.get(Order_.orderDate), from, to));
        cq.orderBy(cb.desc(numberOfSales), cb.asc(client.get("id")));
        TypedQuery<SummaryBean> q = em.createQuery(cq);
        return q.getResultList();
    }

    
    /**
     * Gets a Client object from an email and password
     * 
     * @author Liam Harbec
     * @param email The email address of the supposed user
     * @return The Client object
     */
    public Client findClientFromLogin(String email) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Client> query = cb.createQuery(Client.class);
        Root<Client> client = query.from(Client.class);
        query.select(client)
                .where(cb.equal(client.get(Client_.email), email));
        TypedQuery<Client> clientQuery = em.createQuery(query);
        try {
            Client clientResult = clientQuery.getSingleResult();
            return clientResult;
        } catch (NoResultException e) {
            return new Client(-1);
        }
    }
}
