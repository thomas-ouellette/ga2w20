package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Format;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The FormatJpaController is responsible for all queries and database actions
 * relevant to the Format entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class FormatJpaController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(FormatJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Format entity 
     *
     * @author Svitlana Myronova
     * @param format
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Format format) throws RollbackFailureException, Exception {
        try {
            if (format.getBookList() == null) {
                format.setBookList(new ArrayList<Book>());
            }
            utx.begin();
            List<Book> attachedBookList = new ArrayList<Book>();
            for (Book bookListBookToAttach : format.getBookList()) {
                bookListBookToAttach = em.getReference(bookListBookToAttach.getClass(), bookListBookToAttach.getIsbn());
                attachedBookList.add(bookListBookToAttach);
            }
            format.setBookList(attachedBookList);
            em.persist(format);
            for (Book bookListBook : format.getBookList()) {
                bookListBook.getFormatList().add(format);
                bookListBook = em.merge(bookListBook);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Edit Format entity
     *
     * @author Svitlana Myronova
     * @param format
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Format format) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            Format persistentFormat = em.find(Format.class, format.getId());
            List<Book> bookListOld = persistentFormat.getBookList();
            List<Book> bookListNew = format.getBookList();
            List<Book> attachedBookListNew = new ArrayList<Book>();
            for (Book bookListNewBookToAttach : bookListNew) {
                bookListNewBookToAttach = em.getReference(bookListNewBookToAttach.getClass(), bookListNewBookToAttach.getIsbn());
                attachedBookListNew.add(bookListNewBookToAttach);
            }
            bookListNew = attachedBookListNew;
            format.setBookList(bookListNew);
            format = em.merge(format);
            for (Book bookListOldBook : bookListOld) {
                if (!bookListNew.contains(bookListOldBook)) {
                    bookListOldBook.getFormatList().remove(format);
                    bookListOldBook = em.merge(bookListOldBook);
                }
            }
            for (Book bookListNewBook : bookListNew) {
                if (!bookListOld.contains(bookListNewBook)) {
                    bookListNewBook.getFormatList().add(format);
                    bookListNewBook = em.merge(bookListNewBook);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = format.getId();
                if (findFormat(id) == null) {
                    throw new NonexistentEntityException("The format with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Format entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Format format;
            try {
                format = em.getReference(Format.class, id);
                format.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The format with id " + id + " no longer exists.", enfe);
            }
            List<Book> bookList = format.getBookList();
            for (Book bookListBook : bookList) {
                bookListBook.getFormatList().remove(format);
                bookListBook = em.merge(bookListBook);
            }
            em.remove(format);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find Format entities
     *
     * @author Svitlana Myronova
     * @return list of Format entities
     */
    public List<Format> findFormatEntities() {
        LOG.info("getting all the formats");
        return findFormatEntities(true, -1, -1);
    }

    /**
     * Find Format Entities
     * 
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Format entities
     */
    public List<Format> findFormatEntities(int maxResults, int firstResult) {
        return findFormatEntities(false, maxResults, firstResult);
    }

    /**
     * Find Format Entities
     * 
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Format entities
     */
    private List<Format> findFormatEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Format.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Format entity by id
     * 
     * @author Svitlana Myronova
     * @param id
     * @return Format entity
     */
    public Format findFormat(Integer id) {
        return em.find(Format.class, id);
    }

    /**
     * Get count of Format entities
     *
     * @author Svitlana Myrornova
     * @return number of Format entities
     */
    public int getFormatCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Format> rt = cq.from(Format.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    public List<String> getFormatsTitles(){
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Format> author = cq.from(Format.class);
        cq.select(author.get("formatTitle"));
        Query query = em.createQuery(cq);
        return query.getResultList();
    }

}
