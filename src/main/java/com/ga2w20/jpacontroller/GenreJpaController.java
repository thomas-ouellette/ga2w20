package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Genre;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The GenreJpaController is responsible for all queries and database actions
 * relevant to the Genre entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class GenreJpaController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(GenreJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Genre entity
     * 
     * @author Svitlana Myronova
     * @param genre
     * @throws Exception
     */
    public void create(Genre genre) throws Exception {
        try {
            if (genre.getBookList() == null) {
                genre.setBookList(new ArrayList<Book>());
            }
            utx.begin();
            List<Book> attachedBookList = new ArrayList<Book>();
            for (Book bookListBookToAttach : genre.getBookList()) {
                bookListBookToAttach = em.getReference(bookListBookToAttach.getClass(), bookListBookToAttach.getIsbn());
                attachedBookList.add(bookListBookToAttach);
            }
            genre.setBookList(attachedBookList);
            em.persist(genre);
            for (Book bookListBook : genre.getBookList()) {
                Genre oldGenreIdOfBookListBook = bookListBook.getGenreId();
                bookListBook.setGenreId(genre);
                bookListBook = em.merge(bookListBook);
                if (oldGenreIdOfBookListBook != null) {
                    oldGenreIdOfBookListBook.getBookList().remove(bookListBook);
                    oldGenreIdOfBookListBook = em.merge(oldGenreIdOfBookListBook);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit Genre entity
     *
     * @author Svitlana Myronova
     * @param genre
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Genre genre) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Genre persistentGenre = em.find(Genre.class, genre.getId());
            List<Book> bookListOld = persistentGenre.getBookList();
            List<Book> bookListNew = genre.getBookList();
            List<String> illegalOrphanMessages = null;
            for (Book bookListOldBook : bookListOld) {
                if (!bookListNew.contains(bookListOldBook)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Book " + bookListOldBook + " since its genreId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Book> attachedBookListNew = new ArrayList<Book>();
            for (Book bookListNewBookToAttach : bookListNew) {
                bookListNewBookToAttach = em.getReference(bookListNewBookToAttach.getClass(), bookListNewBookToAttach.getIsbn());
                attachedBookListNew.add(bookListNewBookToAttach);
            }
            bookListNew = attachedBookListNew;
            genre.setBookList(bookListNew);
            genre = em.merge(genre);
            for (Book bookListNewBook : bookListNew) {
                if (!bookListOld.contains(bookListNewBook)) {
                    Genre oldGenreIdOfBookListNewBook = bookListNewBook.getGenreId();
                    bookListNewBook.setGenreId(genre);
                    bookListNewBook = em.merge(bookListNewBook);
                    if (oldGenreIdOfBookListNewBook != null && !oldGenreIdOfBookListNewBook.equals(genre)) {
                        oldGenreIdOfBookListNewBook.getBookList().remove(bookListNewBook);
                        oldGenreIdOfBookListNewBook = em.merge(oldGenreIdOfBookListNewBook);
                    }
                }
            }
            utx.commit();
        } catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = genre.getId();
                if (findGenre(id) == null) {
                    throw new NonexistentEntityException("The genre with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Genre entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Genre genre;
            try {
                genre = em.getReference(Genre.class, id);
                genre.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The genre with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Book> bookListOrphanCheck = genre.getBookList();
            for (Book bookListOrphanCheckBook : bookListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Genre (" + genre + ") cannot be destroyed since the Book " + bookListOrphanCheckBook + " in its bookList field has a non-nullable genreId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(genre);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Genre entities
     *
     * @author Svitlana Myronova
     * @return list of Genre entities
     */
    public List<Genre> findGenreEntities() {
        LOG.info("getting all the genres");
        return findGenreEntities(true, -1, -1);
    }

    /**
     * Find Genre entities
     *
     * @author Svitlana Myronova     
     * @param maxResults
     * @param firstResult
     * @return list of Genre entities
     */
    public List<Genre> findGenreEntities(int maxResults, int firstResult) {
        return findGenreEntities(false, maxResults, firstResult);
    }

    /**
     * Find Genre entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Genre entities
     */
    private List<Genre> findGenreEntities(boolean all, int maxResults, int firstResult) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<Genre> g = cq.from(Genre.class);
        cq.select(cq.from(Genre.class)).orderBy(cb.asc(g.get("genreTitle")));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Genre entity
     *
     * @author Svitlana Myronova
     * @param id
     * @return Genre entity
     */
    public Genre findGenre(Integer id) {
        return em.find(Genre.class, id);

    }

    /**
     * Get count of Genre entities
     *
     * @author Svitlana Myronova
     * @return number of Genre entities
     */
    public int getGenreCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Genre> rt = cq.from(Genre.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }
    
    public Genre getGenreByTitle(String genreTitle){
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Genre> genre = cq.from(Genre.class);
        cq.select(genre);
        cq.where(em.getCriteriaBuilder().equal(genre.get("genreTitle"), genreTitle));
        Query query = em.createQuery(cq);
        return (Genre) query.getSingleResult();
        
    }

}
