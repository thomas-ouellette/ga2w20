package com.ga2w20.jpacontroller;

import com.ga2w20.entities.News;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The NewsJpaController is responsible for all queries and database actions
 * relevant to the News entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class NewsJpaController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(NewsJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create News entity
     *
     * @author Svitlana Myronova
     * @param news
     * @throws Exception
     */
    public void create(News news) throws Exception {
        try {
            utx.begin();
            em.persist(news);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit News entity
     *
     * @author Svitlana Myronova
     * @param news
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(News news) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            news = em.merge(news);
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = news.getId();
                if (findNews(id) == null) {
                    throw new NonexistentEntityException("The news with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy News entity
     *
     * @author Svitlana Myronva
     * @param id
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            News news;
            try {
                news = em.getReference(News.class, id);
                news.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The news with id " + id + " no longer exists.", enfe);
            }
            em.remove(news);
            utx.commit();
            
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
             try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**    
     * Find News entities
     * 
     * @author Svitlana Myronova
     * @return list of News entities 
     */
    public List<News> findNewsEntities() {
        LOG.info("getting all the news");
        return findNewsEntities(true, -1, -1);
    }

    /**
     * Find News entities
     * 
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of News entities
     */
    public List<News> findNewsEntities(int maxResults, int firstResult) {
        return findNewsEntities(false, maxResults, firstResult);
    }

    /**
     * Find News entities
     * 
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of News entities
     */
    private List<News> findNewsEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(News.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find News entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return News entity
     */
    public News findNews(Integer id) {
        return em.find(News.class, id);

    }

    /**
     * Get count of News entities
     *
     * @author Svitlana Myronova
     * @return number of News entities
     */
    public int getNewsCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<News> rt = cq.from(News.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

}
