package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.OrderBookPK;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.LoggerFactory;

/**
 * The OrderBookJpaController is responsible for all queries and database actions
 * relevant to the Order_Book entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class OrderBookJpaController implements Serializable {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(OrderBookJpaController.class);
    
    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create OrderBook entity
     *
     * @author Svitlana Myronova
     * @param orderBook
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(OrderBook orderBook) throws PreexistingEntityException, Exception {
        if (orderBook.getOrderBookPK() == null) {
            orderBook.setOrderBookPK(new OrderBookPK());
        }
        orderBook.getOrderBookPK().setIsbn(orderBook.getBook().getIsbn());
        orderBook.getOrderBookPK().setOrderId(orderBook.getOrder().getId());
        try {
            utx.begin();
            Book book = orderBook.getBook();
            if (book != null) {
                book = em.getReference(book.getClass(), book.getIsbn());
                orderBook.setBook(book);
            }
            Order order = orderBook.getOrder();
            if (order != null) {
                order = em.getReference(order.getClass(), order.getId());
                orderBook.setOrder(order);
            }
            em.persist(orderBook);
            if (book != null) {
                book.getOrderBookList().add(orderBook);
                book = em.merge(book);
            }
            if (order != null) {
                order.getOrderBookList().add(orderBook);
                order = em.merge(order);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findOrderBook(orderBook.getOrderBookPK()) != null) {
                throw new PreexistingEntityException("OrderBook " + orderBook + " already exists.", ex);
            }
            throw ex;
        }
    }

    /**
     * Edit OrderBook entity
     *
     * @author Svitlana Myronova
     * @param orderBook
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(OrderBook orderBook) throws NonexistentEntityException, Exception {
        orderBook.getOrderBookPK().setIsbn(orderBook.getBook().getIsbn());
        orderBook.getOrderBookPK().setOrderId(orderBook.getOrder().getId());
        try {
            utx.begin();
            OrderBook persistentOrderBook = em.find(OrderBook.class, orderBook.getOrderBookPK());
            Book bookOld = persistentOrderBook.getBook();
            Book bookNew = orderBook.getBook();
            Order orderOld = persistentOrderBook.getOrder();
            Order orderNew = orderBook.getOrder();
            if (bookNew != null) {
                bookNew = em.getReference(bookNew.getClass(), bookNew.getIsbn());
                orderBook.setBook(bookNew);
            }
            if (orderNew != null) {
                orderNew = em.getReference(orderNew.getClass(), orderNew.getId());
                orderBook.setOrder(orderNew);
            }
            orderBook = em.merge(orderBook);
            if (bookOld != null && !bookOld.equals(bookNew)) {
                bookOld.getOrderBookList().remove(orderBook);
                bookOld = em.merge(bookOld);
            }
            if (bookNew != null && !bookNew.equals(bookOld)) {
                bookNew.getOrderBookList().add(orderBook);
                bookNew = em.merge(bookNew);
            }
            if (orderOld != null && !orderOld.equals(orderNew)) {
                orderOld.getOrderBookList().remove(orderBook);
                orderOld = em.merge(orderOld);
            }
            if (orderNew != null && !orderNew.equals(orderOld)) {
                orderNew.getOrderBookList().add(orderBook);
                orderNew = em.merge(orderNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                OrderBookPK id = orderBook.getOrderBookPK();
                if (findOrderBook(id) == null) {
                    throw new NonexistentEntityException("The orderBook with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy OrderBook entity
     * 
     * @author Svitlana  Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(OrderBookPK id) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            OrderBook orderBook;
            try {
                orderBook = em.getReference(OrderBook.class, id);
                orderBook.getOrderBookPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The orderBook with id " + id + " no longer exists.", enfe);
            }
            Book book = orderBook.getBook();
            if (book != null) {
                book.getOrderBookList().remove(orderBook);
                book = em.merge(book);
            }
            Order order = orderBook.getOrder();
            if (order != null) {
                order.getOrderBookList().remove(orderBook);
                order = em.merge(order);
            }
            em.remove(orderBook);
            utx.commit();
            
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find OrderBook entities
     *
     * @author Svitlana Myronova
     * @return list of OrderBook entities
     */
    public List<OrderBook> findOrderBookEntities() {
        LOG.info("getting all the orderbooks");
        return findOrderBookEntities(true, -1, -1);
    }

    /**
     * Find OrderBook entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of OrderBook entities
     */
    public List<OrderBook> findOrderBookEntities(int maxResults, int firstResult) {
        return findOrderBookEntities(false, maxResults, firstResult);
    }

    /**
     * Find OrderBook entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of OrderBook entities
     */
    private List<OrderBook> findOrderBookEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(OrderBook.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find OrderBook entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return OrderBook entity
     */
    public OrderBook findOrderBook(OrderBookPK id) {
        return em.find(OrderBook.class, id);

    }

    /**
     * Get count of OrderBook entities
     *
     * @author Svitlana Myronova
     * @return number of OrderBook entities
     */
    public int getOrderBookCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<OrderBook> rt = cq.from(OrderBook.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }
}
