package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import java.util.Date;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga2w20.entities.Client;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Order_;
import com.ga2w20.entities.OrderBook_;
import com.ga2w20.entities.Book_;
import com.ga2w20.entities.OrderBookPK;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The OrderJpaController is responsible for all queries and database actions
 * relevant to the Order entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class OrderJpaController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(OrderJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Order entity
     *
     * @author Svitlana Myronova
     * @param order
     * @throws Exception
     */
    public void create(Order order) throws Exception {

        if (order.getOrderBookList() == null) {
            order.setOrderBookList(new ArrayList<OrderBook>());
        }
        try {
            utx.begin();
            Client clientId = order.getClientId();
            if (clientId != null) {
                clientId = em.getReference(clientId.getClass(), clientId.getId());
                order.setClientId(clientId);
            }
            List<OrderBook> attachedOrderBookList = new ArrayList<OrderBook>();
            for (OrderBook orderBookListOrderBookToAttach : order.getOrderBookList()) {
                orderBookListOrderBookToAttach = em.getReference(orderBookListOrderBookToAttach.getClass(), orderBookListOrderBookToAttach.getOrderBookPK());
                attachedOrderBookList.add(orderBookListOrderBookToAttach);
            }
            order.setOrderBookList(attachedOrderBookList);
            em.persist(order);
            if (clientId != null) {
                clientId.getOrderList().add(order);
                clientId = em.merge(clientId);
            }
            for (OrderBook orderBookListOrderBook : order.getOrderBookList()) {
                Order oldOrderOfOrderBookListOrderBook = orderBookListOrderBook.getOrder();
                orderBookListOrderBook.setOrder(order);
                orderBookListOrderBook = em.merge(orderBookListOrderBook);
                if (oldOrderOfOrderBookListOrderBook != null) {
                    oldOrderOfOrderBookListOrderBook.getOrderBookList().remove(orderBookListOrderBook);
                    oldOrderOfOrderBookListOrderBook = em.merge(oldOrderOfOrderBookListOrderBook);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findOrder(order.getId()) != null) {
                throw new PreexistingEntityException("Book " + order + " already exists.", ex);
            }
            throw ex;
        }
    }

    /**
     * Edit Order entity
     *
     * @author Svitlana Myronova
     * @param order
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Order order) throws IllegalOrphanException, NonexistentEntityException, Exception {
        
        try {
            utx.begin();
            Order persistentOrder = em.find(Order.class, order.getId());
            Client clientIdOld = persistentOrder.getClientId();
            Client clientIdNew = order.getClientId();
            List<OrderBook> orderBookListOld = persistentOrder.getOrderBookList();
            List<OrderBook> orderBookListNew = order.getOrderBookList();
            List<String> illegalOrphanMessages = null;
            for (OrderBook orderBookListOldOrderBook : orderBookListOld) {
                if (!orderBookListNew.contains(orderBookListOldOrderBook)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain OrderBook " + orderBookListOldOrderBook + " since its order field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientIdNew != null) {
                clientIdNew = em.getReference(clientIdNew.getClass(), clientIdNew.getId());
                order.setClientId(clientIdNew);
            }
            List<OrderBook> attachedOrderBookListNew = new ArrayList<OrderBook>();
            for (OrderBook orderBookListNewOrderBookToAttach : orderBookListNew) {
                orderBookListNewOrderBookToAttach = em.getReference(orderBookListNewOrderBookToAttach.getClass(), orderBookListNewOrderBookToAttach.getOrderBookPK());
                attachedOrderBookListNew.add(orderBookListNewOrderBookToAttach);
            }
            orderBookListNew = attachedOrderBookListNew;
            order.setOrderBookList(orderBookListNew);
            order = em.merge(order);
            if (clientIdOld != null && !clientIdOld.equals(clientIdNew)) {
                clientIdOld.getOrderList().remove(order);
                clientIdOld = em.merge(clientIdOld);
            }
            if (clientIdNew != null && !clientIdNew.equals(clientIdOld)) {
                clientIdNew.getOrderList().add(order);
                clientIdNew = em.merge(clientIdNew);
            }
            for (OrderBook orderBookListNewOrderBook : orderBookListNew) {
                if (!orderBookListOld.contains(orderBookListNewOrderBook)) {
                    Order oldOrderOfOrderBookListNewOrderBook = orderBookListNewOrderBook.getOrder();
                    orderBookListNewOrderBook.setOrder(order);
                    orderBookListNewOrderBook = em.merge(orderBookListNewOrderBook);
                    if (oldOrderOfOrderBookListNewOrderBook != null && !oldOrderOfOrderBookListNewOrderBook.equals(order)) {
                        oldOrderOfOrderBookListNewOrderBook.getOrderBookList().remove(orderBookListNewOrderBook);
                        oldOrderOfOrderBookListNewOrderBook = em.merge(oldOrderOfOrderBookListNewOrderBook);
                    }
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = order.getId();
                if (findOrder(id) == null) {
                    throw new NonexistentEntityException("The order with id " + id + " no longer exists.");
                }
            }
            throw ex;

        }

    }

    /**
     * Destroy Order entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {

            utx.begin();
            Order order;
            try {
                order = em.getReference(Order.class, id);
                order.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The order with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<OrderBook> orderBookListOrphanCheck = order.getOrderBookList();
            for (OrderBook orderBookListOrphanCheckOrderBook : orderBookListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Order (" + order + ") cannot be destroyed since the OrderBook " + orderBookListOrphanCheckOrderBook + " in its orderBookList field has a non-nullable order field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client clientId = order.getClientId();
            if (clientId != null) {
                clientId.getOrderList().remove(order);
                clientId = em.merge(clientId);
            }
            em.remove(order);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Order entities
     *
     * @author Svitlana Myronova
     * @return list of Order entities
     */
    public List<Order> findOrderEntities() {
        LOG.info("getting all the orders");
        return findOrderEntities(true, -1, -1);
    }

    /**
     * Find Order entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Order entities
     */
    public List<Order> findOrderEntities(int maxResults, int firstResult) {
        return findOrderEntities(false, maxResults, firstResult);
    }

    /**
     * Find Order entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Order entities
     */
    private List<Order> findOrderEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Order.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find Order entity by id
     * 
     * @author Svitlana Myronova
     * @param id
     * @return Order entity
     */
    public Order findOrder(Integer id) {

        return em.find(Order.class, id);
    }

    /**
     * Get count of Order entities
     *
     * @author Svitlana Myronova
     * @return number of Order entities
     */
    public int getOrderCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Order> rt = cq.from(Order.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }
    /**
     * Returns the totals sales, cost, profit and amount of sales and order date
     * made within the specified date range
     * @param from Date
     * @param to Date
     * @return SalesBean ( sales, cost, profit, last order date and number of sales)
     * @author Cadin Londono
     */
    public SalesBean getGeneralTotalSales(Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Order> order = cq.from(Order.class);
        Join<Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        Join <OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<Date> lastOrder = cb.greatest(order.get(Order_.orderDate));
        
        cq.multiselect( sales, cost, profit, numberOfSales, lastOrder);
        cq.where(cb.between(order.get(Order_.orderDate), from, to));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        return q.getSingleResult();
    }
    
    /**
     * Returns all of the book orders  withing the 
     * Date range specified along with their sales, cost, profit(sales - cost) , order date and 
     * amount of sales
     * @param from Date
     * @param to Date
     * @return List of SalesBean ( book title,sales, cost, profit and number of sales, order date)
     * @author Cadin Londono
     */
    
    public List<SalesBean> getSalesAllBooks(Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Order> order =  cq.from(Order.class);
        Join <Order, OrderBook> orderBooks = order.join(Order_.orderBookList);
        orderBooks.on(cb.between(order.get(Order_.orderDate), from, to));
        Join<OrderBook, Book> books = orderBooks.join(OrderBook_.book);
        
        Expression<BigDecimal> sale = orderBooks.get("price");
        Expression<BigDecimal> cost = books.get("wholeSalePrice");
        Expression<BigDecimal> profit = cb.diff(sale, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<Date> lastOrder = order.get(Order_.orderDate);
        
        cq.multiselect(books.get("title"), sale, cost, profit, numberOfSales, lastOrder);
        cq.groupBy(books.get("isbn"));
        cq.orderBy(cb.desc(lastOrder), cb.asc(books.get("isbn")));
        TypedQuery<SalesBean> query = em.createQuery(cq);
        List<SalesBean> salesBeanList = query.getResultList();
        return salesBeanList;
    }
    

}
