package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Client;
import com.ga2w20.entities.Province;
import com.ga2w20.entities.Province_;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ProvinceJpaController is responsible for all queries and database actions
 * relevant to the Province entity
 * 
 * @author Svitlana Myronova, Liam Harbec
 */
@Named
@RequestScoped
public class ProvinceJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(ProvinceJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Province entity
     *
     * @author Svitlana Myronova
     * @param province
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Province province) throws RollbackFailureException, Exception {
        try {
            if (province.getClientList() == null) {
                province.setClientList(new ArrayList<Client>());
            }
            utx.begin();
            List<Client> attachedClientList = new ArrayList<Client>();
            for (Client clientListClientToAttach : province.getClientList()) {
                clientListClientToAttach = em.getReference(clientListClientToAttach.getClass(), clientListClientToAttach.getId());
                attachedClientList.add(clientListClientToAttach);
            }
            province.setClientList(attachedClientList);
            em.persist(province);
            for (Client clientListClient : province.getClientList()) {
                Province oldProvinceIdOfClientListClient = clientListClient.getProvinceId();
                clientListClient.setProvinceId(province);
                clientListClient = em.merge(clientListClient);
                if (oldProvinceIdOfClientListClient != null) {
                    oldProvinceIdOfClientListClient.getClientList().remove(clientListClient);
                    oldProvinceIdOfClientListClient = em.merge(oldProvinceIdOfClientListClient);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            LOG.error("Rollback");
            } catch (Exception er) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", er);

            }
            throw ex;
        }

    }

    /**
     * Edit Province entity
     *
     * @author Svitlana Myronova
     * @param province
     * @throws IllegalOrphanException
     * @throws RollbackFailureException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Province province) throws IllegalOrphanException, RollbackFailureException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Province persistentProvince = em.find(Province.class, province.getId());
            List<Client> clientListOld = persistentProvince.getClientList();
            List<Client> clientListNew = province.getClientList();
            List<String> illegalOrphanMessages = null;
            for (Client clientListOldClient : clientListOld) {
                if (!clientListNew.contains(clientListOldClient)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Client " + clientListOldClient + " since its provinceId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Client> attachedClientListNew = new ArrayList<Client>();
            for (Client clientListNewClientToAttach : clientListNew) {
                clientListNewClientToAttach = em.getReference(clientListNewClientToAttach.getClass(), clientListNewClientToAttach.getId());
                attachedClientListNew.add(clientListNewClientToAttach);
            }
            clientListNew = attachedClientListNew;
            province.setClientList(clientListNew);
            province = em.merge(province);
            for (Client clientListNewClient : clientListNew) {
                if (!clientListOld.contains(clientListNewClient)) {
                    Province oldProvinceIdOfClientListNewClient = clientListNewClient.getProvinceId();
                    clientListNewClient.setProvinceId(province);
                    clientListNewClient = em.merge(clientListNewClient);
                    if (oldProvinceIdOfClientListNewClient != null && !oldProvinceIdOfClientListNewClient.equals(province)) {
                        oldProvinceIdOfClientListNewClient.getClientList().remove(clientListNewClient);
                        oldProvinceIdOfClientListNewClient = em.merge(oldProvinceIdOfClientListNewClient);
                    }
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = province.getId();
                if (findProvince(id) == null) {
                    throw new NonexistentEntityException("The province with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Province entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws IllegalOrphanException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, IllegalOrphanException, RollbackFailureException, Exception {

        try {
            utx.begin();
            Province province;
            try {
                province = em.getReference(Province.class, id);
                province.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The province with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Client> clientListOrphanCheck = province.getClientList();
            for (Client clientListOrphanCheckClient : clientListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Province (" + province + ") cannot be destroyed since the Client " + clientListOrphanCheckClient + " in its clientList field has a non-nullable provinceId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(province);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Province entities
     * 
     * @author Svitlana Myronova
     * @return list of entities 
     */
    public List<Province> findProvinceEntities() {
        LOG.info("getting all the province");
        return findProvinceEntities(true, -1, -1);
    }

    /**
     * Find Province entities
     * 
     * @author Svitlana Myronov
     * @param maxResults
     * @param firstResult
     * @return list of Province entities
     */
    public List<Province> findProvinceEntities(int maxResults, int firstResult) {
        return findProvinceEntities(false, maxResults, firstResult);
    }

    /**
     * Find Province entities
     * 
     * @author Svitlana Myronov
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Province entities
     */
    private List<Province> findProvinceEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Province.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find Province Entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return Province entity
     */
    public Province findProvince(Integer id) {

        return em.find(Province.class, id);

    }

    /**
     * Get count of Province entities
     *
     * @author Svitlana Myronova
     * @return number of Province entities
     */
    public int getProvinceCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Province> rt = cq.from(Province.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }
    
    /**
     * Gets a Province object based on the name of the province
     * 
     * @author Liam Harbec
     * @param provinceName The province to get
     * @return The province object
     */
    public Province getProvinceFromName(String provinceName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Province> query = cb.createQuery(Province.class);
        Root<Province> province = query.from(Province.class);
        query.select(province)
                .where(cb.equal(province.get(Province_.provinceName), provinceName));
        TypedQuery<Province> provinceQuery = em.createQuery(query);
        try {
            Province provinceResult = provinceQuery.getSingleResult();
            return provinceResult;
        } catch (NoResultException e) {
            return null;
        }
    }
}
