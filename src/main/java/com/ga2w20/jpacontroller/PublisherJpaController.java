package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Author;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.Order_;
import com.ga2w20.entities.Publisher;
import com.ga2w20.entities.Publisher_;
import com.ga2w20.entities.Book_;
import com.ga2w20.entities.OrderBook_;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.querybeans.SummaryBean;
import com.ga2w20.querybeans.SalesBean;
import java.math.BigDecimal;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The PublishedJpaController is responsible for all queries and database actions
 * relevant to the Publisher entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class PublisherJpaController implements Serializable {
    private final static Logger LOG = LoggerFactory.getLogger(PublisherJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Publisher entity
     *
     * @author Svitlana Myronova
     * @param publisher
     * @throws Exception
     */
    public void create(Publisher publisher) throws Exception {
        try {
            if (publisher.getBookList() == null) {
                publisher.setBookList(new ArrayList<Book>());
            }
            utx.begin();
            List<Book> attachedBookList = new ArrayList<Book>();
            for (Book bookListBookToAttach : publisher.getBookList()) {
                bookListBookToAttach = em.getReference(bookListBookToAttach.getClass(), bookListBookToAttach.getIsbn());
                attachedBookList.add(bookListBookToAttach);
            }
            publisher.setBookList(attachedBookList);
            em.persist(publisher);
            for (Book bookListBook : publisher.getBookList()) {
                Publisher oldPublisherIdOfBookListBook = bookListBook.getPublisherId();
                bookListBook.setPublisherId(publisher);
                bookListBook = em.merge(bookListBook);
                if (oldPublisherIdOfBookListBook != null) {
                    oldPublisherIdOfBookListBook.getBookList().remove(bookListBook);
                    oldPublisherIdOfBookListBook = em.merge(oldPublisherIdOfBookListBook);
                }
            }
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit Publisher entity
     *
     * @author Svitlana Myronova
     * @param publisher
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Publisher publisher) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Publisher persistentPublisher = em.find(Publisher.class, publisher.getId());
            List<Book> bookListOld = persistentPublisher.getBookList();
            List<Book> bookListNew = publisher.getBookList();
            List<String> illegalOrphanMessages = null;
            for (Book bookListOldBook : bookListOld) {
                if (!bookListNew.contains(bookListOldBook)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Book " + bookListOldBook + " since its publisherId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Book> attachedBookListNew = new ArrayList<Book>();
            for (Book bookListNewBookToAttach : bookListNew) {
                bookListNewBookToAttach = em.getReference(bookListNewBookToAttach.getClass(), bookListNewBookToAttach.getIsbn());
                attachedBookListNew.add(bookListNewBookToAttach);
            }
            bookListNew = attachedBookListNew;
            publisher.setBookList(bookListNew);
            publisher = em.merge(publisher);
            for (Book bookListNewBook : bookListNew) {
                if (!bookListOld.contains(bookListNewBook)) {
                    Publisher oldPublisherIdOfBookListNewBook = bookListNewBook.getPublisherId();
                    bookListNewBook.setPublisherId(publisher);
                    bookListNewBook = em.merge(bookListNewBook);
                    if (oldPublisherIdOfBookListNewBook != null && !oldPublisherIdOfBookListNewBook.equals(publisher)) {
                        oldPublisherIdOfBookListNewBook.getBookList().remove(bookListNewBook);
                        oldPublisherIdOfBookListNewBook = em.merge(oldPublisherIdOfBookListNewBook);
                    }
                }
            }
            utx.commit();

        } catch (IllegalOrphanException | IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = publisher.getId();
                if (findPublisher(id) == null) {
                    throw new NonexistentEntityException("The publisher with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Publisher entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, Exception {
        try {
            utx.begin();
            Publisher publisher;
            try {
                publisher = em.getReference(Publisher.class, id);
                publisher.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The publisher with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Book> bookListOrphanCheck = publisher.getBookList();
            for (Book bookListOrphanCheckBook : bookListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Publisher (" + publisher + ") cannot be destroyed since the Book " + bookListOrphanCheckBook + " in its bookList field has a non-nullable publisherId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(publisher);
            utx.commit();
            

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Publisher entities
     * 
     * @author Svitlna Myronova
     * @return list of Publisher entities
     */
    public List<Publisher> findPublisherEntities() {
        LOG.info("getting all the publishers");
        return findPublisherEntities(true, -1, -1);
    }

    /**
     * Find Publisher entities
     * 
     * @author Svitlna Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Publisher entities
     */
    public List<Publisher> findPublisherEntities(int maxResults, int firstResult) {
        return findPublisherEntities(false, maxResults, firstResult);
    }

    /**
     * Find Publisher entities
     * 
     * @author Svitlna Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Publisher entities
     */
    private List<Publisher> findPublisherEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Publisher.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find Publisher entity by id
     *
     * @author Svitlana  Myronova
     * @param id
     * @return Publisher entity
     */
    public Publisher findPublisher(Integer id) {
        return em.find(Publisher.class, id);

    }

    /**
     * Get count of Publisher entities 
     * 
     * @author Svitlana Myronova
     * @return number of Publisher entities
     */
    public int getPublisherCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Publisher> rt = cq.from(Publisher.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }
    
    public List<String> getPublishersNames(){
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Publisher> publisher = cq.from(Publisher.class);
        cq.select(publisher.get("publisherName"));
        Query query = em.createQuery(cq);
        return query.getResultList();
    }
    
    public Publisher getPublisherByName(String publisherName){
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Publisher> publisher = cq.from(Publisher.class);
        cq.select(publisher);
        cq.where(em.getCriteriaBuilder().equal(publisher.get("publisherName"), publisherName));
        Query query = em.createQuery(cq);
        return (Publisher) query.getSingleResult();
    }

    /**
     * Returns the total sales, cost ,profit, last order date and amount of sales
     * made by the specified publisher withing the specified date range
     * @param publisherId
     * @param from Date
     * @param to Date
     * @return SalesBean ( sales, cost, profit, last order date and number of sales)
     * @author Cadin Londono
     */
    public SalesBean getTotalSales(int publisherId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Publisher> publishers = cq.from(Publisher.class);
        Join<Publisher,Book> books = publishers.join(Publisher_.bookList);
        Join<Book, OrderBook> orderBooks = books.join(Book_.orderBookList);
        Join<OrderBook,Order> orders = orderBooks.join(OrderBook_.Order);
        
        Expression<BigDecimal> sales = cb.sum(orderBooks.get("price"));
        Expression<BigDecimal> cost = cb.sum(books.get("wholeSalePrice"));
        Expression<BigDecimal> profit = cb.diff(sales, cost);
        Expression<Long> numberOfSales = cb.count(orderBooks.get("price"));
        Expression<Date> lastOrder = cb.greatest(orders.get(Order_.orderDate));
        
        cq.multiselect( sales, cost, profit, numberOfSales, lastOrder);
        cq.where(cb.between(orders.get(Order_.orderDate), from, to),
                 cb.equal(publishers.get("id"), publisherId));
        TypedQuery<SalesBean> q = em.createQuery(cq);
        return q.getSingleResult();
    }
    /**
     * Returns the total sales, cost, profit, order date amount of sales of each of the specified publisher
     * books orders within the specified Date range
     * @param publisherId
     * @param from Date
     * @param to Date
     * @return List of SalesBean ( book title,sales, cost, profit, order date and number of sales)
     * @author Cadin Londono
     */
    public List<SalesBean> getTotalSalesAllBooks(int publisherId, Date from, Date to){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SalesBean> cq = cb.createQuery(SalesBean.class);
        Root<Publisher> publishers = cq.from(Publisher.class);
        Join<Publisher,Book> books = publishers.join(Publisher_.bookList);
        Join<Book, OrderBook> orderBooks = books.join(Book_.orderBookList);
        Join<OrderBook,Order> orders = orderBooks.join(OrderBook_.Order);
        
        Expression<BigDecimal> sale = orderBooks.get("price");
        Expression<BigDecimal> cost = books.get("wholeSalePrice");
        Expression<BigDecimal> profit = cb.diff(sale, cost);
        Expression<Long> numberOfSales = cb.literal(1L);
        Expression<Date> orderDate = orders.get(Order_.orderDate);
        
        cq.multiselect(books.get("title"), sale, cost, profit, numberOfSales, orderDate);
        cq.orderBy(cb.desc(orderDate));
        cq.where(cb.between(orders.get(Order_.orderDate), from, to),
                 cb.equal(publishers.get("id"), publisherId));
        cq.orderBy(cb.desc(orderDate), cb.asc(books.get("isbn")));
        
        TypedQuery<SalesBean> q = em.createQuery(cq);
        return q.getResultList();
    }
}
