package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Client;
import com.ga2w20.entities.Review;
import com.ga2w20.entities.ReviewPK;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.PreexistingEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.LoggerFactory;

/**
 * The ReviewJpaController is responsible for all queries and database actions
 * relevant to the Review entity
 * 
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class ReviewJpaController implements Serializable {
    
    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(ReviewJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Review entity
     *
     * @author Svitlana Myronova
     * @param review
     * @throws PreexistingEntityException
     * @throws Exception
     */
    public void create(Review review) throws PreexistingEntityException, Exception {
        if (review.getReviewPK() == null) {
            review.setReviewPK(new ReviewPK());
        }
        review.getReviewPK().setClientId(review.getClient().getId());
        review.getReviewPK().setIsbn(review.getBook().getIsbn());
        try {
            utx.begin();
            Book book = review.getBook();
            if (book != null) {
                book = em.getReference(book.getClass(), book.getIsbn());
                review.setBook(book);
            }
            Client client = review.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                review.setClient(client);
            }
            em.persist(review);
            if (book != null) {
                book.getReviewList().add(review);
                book = em.merge(book);
            }
            if (client != null) {
                client.getReviewList().add(review);
                client = em.merge(client);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findReview(review.getReviewPK()) != null) {
                throw new PreexistingEntityException("Review " + review + " already exists.", ex);
            }
            throw ex;
        }
    }

    /**
     * Edit Review entity
     *
     * @author Svitlana Myronova
     * @param review
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Review review) throws NonexistentEntityException, Exception {
        review.getReviewPK().setClientId(review.getClient().getId());
        review.getReviewPK().setIsbn(review.getBook().getIsbn());
        try {
            utx.begin();
            Review persistentReview = em.find(Review.class, review.getReviewPK());
            Book bookOld = persistentReview.getBook();
            Book bookNew = review.getBook();
            Client clientOld = persistentReview.getClient();
            Client clientNew = review.getClient();
            if (bookNew != null) {
                bookNew = em.getReference(bookNew.getClass(), bookNew.getIsbn());
                review.setBook(bookNew);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                review.setClient(clientNew);
            }
            review = em.merge(review);
            if (bookOld != null && !bookOld.equals(bookNew)) {
                bookOld.getReviewList().remove(review);
                bookOld = em.merge(bookOld);
            }
            if (bookNew != null && !bookNew.equals(bookOld)) {
                bookNew.getReviewList().add(review);
                bookNew = em.merge(bookNew);
            }
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getReviewList().remove(review);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getReviewList().add(review);
                clientNew = em.merge(clientNew);
            }
            utx.commit();

        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ReviewPK id = review.getReviewPK();
                if (findReview(id) == null) {
                    throw new NonexistentEntityException("The review with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Review entity 
     * 
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void destroy(ReviewPK id) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            Review review;
            try {
                review = em.getReference(Review.class, id);
                review.getReviewPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The review with id " + id + " no longer exists.", enfe);
            }
            Book book = review.getBook();
            if (book != null) {
                book.getReviewList().remove(review);
                book = em.merge(book);
            }
            Client client = review.getClient();
            if (client != null) {
                client.getReviewList().remove(review);
                client = em.merge(client);
            }
            em.remove(review);
            utx.commit();
            
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Find Review entities
     *
     * @author Svitlana Myronova
     * @return list of Review entities
     */
    public List<Review> findReviewEntities() {
        LOG.info("getting all the reviews");
        return findReviewEntities(true, -1, -1);
    }

    /**
     * Find Review entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Review entities
     */
    public List<Review> findReviewEntities(int maxResults, int firstResult) {
        return findReviewEntities(false, maxResults, firstResult);
    }

    /**
     * Find Review entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Review entities
     */
    private List<Review> findReviewEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Review.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();

    }

    /**
     * Find Review entities by id
     * 
     * @author Svitlana Myronova 
     * @param id
     * @return Review entity
     */
    public Review findReview(ReviewPK id) {
        return em.find(Review.class, id);

    }

    /**
     * Get count of Review entities
     *
     * @author Svitlana Myronova
     * @return number of Review entities
     */
    public int getReviewCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Review> rt = cq.from(Review.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();

    }

}
