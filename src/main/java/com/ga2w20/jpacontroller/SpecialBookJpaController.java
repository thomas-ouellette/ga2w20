package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Book;
import com.ga2w20.entities.Book_;
import com.ga2w20.entities.SpecialBook;
import com.ga2w20.entities.SpecialBook_;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SpecialBookJpaController is responsible for all queries and database actions
 * relevant to the Special_Book entity
 * @author Svitlana Myronova
 */
@Named
@RequestScoped
public class SpecialBookJpaController implements Serializable {
    
    private final static Logger LOG = LoggerFactory.getLogger(SpecialBookJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create SpecialBook entities 
     *
     * @author Svitlana Myronova
     * @param specialBook
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(SpecialBook specialBook) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Book isbn = specialBook.getIsbn();
            if (isbn != null) {
                isbn = em.getReference(isbn.getClass(), isbn.getIsbn());
                specialBook.setIsbn(isbn);
            }
            em.persist(specialBook);
            if (isbn != null) {
                isbn.getSpecialBookList().add(specialBook);
                isbn = em.merge(isbn);
            }
            utx.commit();
        } catch (RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException | SystemException | NotSupportedException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }

    }

    /**
     * Edit SpecialBook entity  
     *
     * @author Svitlana Myronova
     * @param specialBook
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(SpecialBook specialBook) throws NonexistentEntityException, Exception {

        try {
            utx.begin();
            SpecialBook persistentSpecialBook = em.find(SpecialBook.class, specialBook.getId());
            Book isbnOld = persistentSpecialBook.getIsbn();
            Book isbnNew = specialBook.getIsbn();
            if (isbnNew != null) {
                isbnNew = em.getReference(isbnNew.getClass(), isbnNew.getIsbn());
                specialBook.setIsbn(isbnNew);
            }
            specialBook = em.merge(specialBook);
            if (isbnOld != null && !isbnOld.equals(isbnNew)) {
                isbnOld.getSpecialBookList().remove(specialBook);
                isbnOld = em.merge(isbnOld);
            }
            if (isbnNew != null && !isbnNew.equals(isbnOld)) {
                isbnNew.getSpecialBookList().add(specialBook);
                isbnNew = em.merge(isbnNew);
            }
            utx.commit();
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }

            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = specialBook.getId();
                if (findSpecialBook(id) == null) {
                    throw new NonexistentEntityException("The specialBook with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy SpecialBook entity
     * 
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            SpecialBook specialBook;
            try {
                specialBook = em.getReference(SpecialBook.class, id);
                specialBook.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The specialBook with id " + id + " no longer exists.", enfe);
            }
            Book isbn = specialBook.getIsbn();
            if (isbn != null) {
                isbn.getSpecialBookList().remove(specialBook);
                isbn = em.merge(isbn);
            }
            em.remove(specialBook);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find SpecialBook entities
     * 
     * @author Svitlana Myronova
     * @return list of SpecialBook entities
     */
    public List<SpecialBook> findSpecialBookEntities() {
        LOG.info("getting all the specialbooks");
        return findSpecialBookEntities(true, -1, -1);
    }

    /**
     * Find SpecialBook entities
     * 
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of SpecialBook entities
     */
    public List<SpecialBook> findSpecialBookEntities(int maxResults, int firstResult) {
        return findSpecialBookEntities(false, maxResults, firstResult);
    }

    /**
     * Find SpecialBook entities
     * 
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of SpecialBook entities 
     */
    private List<SpecialBook> findSpecialBookEntities(boolean all, int maxResults, int firstResult) {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SpecialBook.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find SpecialBook entity by id
     * 
     * @author Svitlana Myronova
     * @param id
     * @return SpecialBook entity
     */
    public SpecialBook findSpecialBook(Integer id) {
        return em.find(SpecialBook.class, id);
    }

    /**
     * Get count of SpecialBook entities
     *
     * @author Svitlana Myronova
     * @return number of SpecialBook entities
     */
    public int getSpecialBookCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SpecialBook> rt = cq.from(SpecialBook.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    /**
     * This method gets a list of active special books that have removalStatus = 0
     * 
     * @author Svitlana Myronova
     * @param maxItems
     * @return list of special books
     */
    public List<SpecialBook> getSpecialBooksAllGenreActive(int maxItems){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root<SpecialBook> specialBook = cq.from(SpecialBook.class);
        cq.select(specialBook);
        cq.where(cb.equal(specialBook.get(SpecialBook_.isbn).get(Book_.removalStatus), false),
                 cb.between(cb.currentDate(), specialBook.get(SpecialBook_.startDate), specialBook.get(SpecialBook_.endDate)));
        Query q = em.createQuery(cq);
        q.setMaxResults(maxItems);
        return q.getResultList();
    }

}
