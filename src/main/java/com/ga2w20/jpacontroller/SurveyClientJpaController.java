package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Client;
import com.ga2w20.entities.Survey;
import com.ga2w20.entities.SurveyClient;
import com.ga2w20.entities.SurveyClient_;
import com.ga2w20.entities.Survey_;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SurveyClientJpaController is responsible for all queries and database actions
 * relevant to the Survey_Client entity
 * 
 * @author Svitlana Myronova, Liam Harbec
 */
@Named
@RequestScoped
public class SurveyClientJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(SurveyClientJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create SurveyClient entity
     *
     * @author Svitlana Myronova
     * @param surveyClient
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(SurveyClient surveyClient) throws RollbackFailureException, Exception {
        try {
            utx.begin();
            Client clientId = surveyClient.getClientId();
            if (clientId != null) {
                clientId = em.getReference(clientId.getClass(), clientId.getId());
                surveyClient.setClientId(clientId);
            }
            Survey surveyId = surveyClient.getSurveyId();
            if (surveyId != null) {
                surveyId = em.getReference(surveyId.getClass(), surveyId.getId());
                surveyClient.setSurveyId(surveyId);
            }
            em.persist(surveyClient);
            if (clientId != null) {
                clientId.getSurveyClientList().add(surveyClient);
                clientId = em.merge(clientId);
            }
            if (surveyId != null) {
                surveyId.getSurveyClientList().add(surveyClient);
                surveyId = em.merge(surveyId);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Edit SurveyClient entity
     *
     * @author Svitlana Myronova
     * @param surveyClient
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(SurveyClient surveyClient) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            SurveyClient persistentSurveyClient = em.find(SurveyClient.class, surveyClient.getId());
            Client clientIdOld = persistentSurveyClient.getClientId();
            Client clientIdNew = surveyClient.getClientId();
            Survey surveyIdOld = persistentSurveyClient.getSurveyId();
            Survey surveyIdNew = surveyClient.getSurveyId();
            if (clientIdNew != null) {
                clientIdNew = em.getReference(clientIdNew.getClass(), clientIdNew.getId());
                surveyClient.setClientId(clientIdNew);
            }
            if (surveyIdNew != null) {
                surveyIdNew = em.getReference(surveyIdNew.getClass(), surveyIdNew.getId());
                surveyClient.setSurveyId(surveyIdNew);
            }
            surveyClient = em.merge(surveyClient);
            if (clientIdOld != null && !clientIdOld.equals(clientIdNew)) {
                clientIdOld.getSurveyClientList().remove(surveyClient);
                clientIdOld = em.merge(clientIdOld);
            }
            if (clientIdNew != null && !clientIdNew.equals(clientIdOld)) {
                clientIdNew.getSurveyClientList().add(surveyClient);
                clientIdNew = em.merge(clientIdNew);
            }
            if (surveyIdOld != null && !surveyIdOld.equals(surveyIdNew)) {
                surveyIdOld.getSurveyClientList().remove(surveyClient);
                surveyIdOld = em.merge(surveyIdOld);
            }
            if (surveyIdNew != null && !surveyIdNew.equals(surveyIdOld)) {
                surveyIdNew.getSurveyClientList().add(surveyClient);
                surveyIdNew = em.merge(surveyIdNew);
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = surveyClient.getId();
                if (findSurveyClient(id) == null) {
                    throw new NonexistentEntityException("The surveyClient with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy SurveyClient entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            SurveyClient surveyClient;
            try {
                surveyClient = em.getReference(SurveyClient.class, id);
                surveyClient.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The surveyClient with id " + id + " no longer exists.", enfe);
            }
            Client clientId = surveyClient.getClientId();
            if (clientId != null) {
                clientId.getSurveyClientList().remove(surveyClient);
                clientId = em.merge(clientId);
            }
            Survey surveyId = surveyClient.getSurveyId();
            if (surveyId != null) {
                surveyId.getSurveyClientList().remove(surveyClient);
                surveyId = em.merge(surveyId);
            }
            em.remove(surveyClient);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find SurveyClient entities
     * 
     * @author Svitlana Myronova
     * @return list of SurveyClient entities
     */
    public List<SurveyClient> findSurveyClientEntities() {
        LOG.info("getting all the surveyClients");
        return findSurveyClientEntities(true, -1, -1);
    }

    /**
     * Find SurveyClient entities
     * 
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of SurveyClient entities
     */
    public List<SurveyClient> findSurveyClientEntities(int maxResults, int firstResult) {
        return findSurveyClientEntities(false, maxResults, firstResult);
    }

    /**
     * Find SurveyClient entities
     * 
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of SurveyClient entities
     */
    private List<SurveyClient> findSurveyClientEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(SurveyClient.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find SurveyClient entity by id
     *
     * @author Svitlana Myronova
     * @param id
     * @return SurveyClient entity
     */
    public SurveyClient findSurveyClient(Integer id) {
        return em.find(SurveyClient.class, id);

    }

    /**
     * Get count of SurveyClient entities
     * 
     * @author Svitlana Myronova
     * @return number of SurveyClient entities
     */
    public int getSurveyClientCount() {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<SurveyClient> rt = cq.from(SurveyClient.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    
    /**
     * Will return the number of votes for each option for a given survey.
     * Index 0 is the amount for option 1.
     * Index 1 is the amount for option 2.
     * Index 2 is the amount for option 3.
     * Index 3 is the amount for option 4.
     * 
     * @author Liam Harbec
     * @param survey The survey id to get the results for
     * @return The amount of votes for each option of the given survey
     */
    public List<Integer> getSurveyResults(Integer survey) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<SurveyClient> query = cb.createQuery(SurveyClient.class);
        Root<SurveyClient> surveys = query.from(SurveyClient.class);
        query.select(surveys)
                .where(cb.equal(surveys.get(SurveyClient_.surveyId).get(Survey_.id), survey));
        TypedQuery<SurveyClient> surveyQuery = em.createQuery(query);
        List<SurveyClient> surveyList = surveyQuery.getResultList();
        
        ArrayList<Integer> resultListCounter = new ArrayList<>();
        // Adding four potential options
        resultListCounter.add(0);
        resultListCounter.add(0);
        resultListCounter.add(0);
        resultListCounter.add(0);
        for (SurveyClient sc : surveyList) {
            switch (sc.getAnswer()) {
                case 1:
                    resultListCounter.set(0, resultListCounter.get(0) + 1);
                    break;
                case 2:
                    resultListCounter.set(1, resultListCounter.get(1) + 1);
                    break;
                case 3:
                    resultListCounter.set(2, resultListCounter.get(2) + 1);
                    break;
                case 4:
                    resultListCounter.set(3, resultListCounter.get(3) + 1);
                    break;
                default:
                    LOG.info("SurveyClient option invalid");
                    break;
            }
        }
        return resultListCounter;
    }
}
