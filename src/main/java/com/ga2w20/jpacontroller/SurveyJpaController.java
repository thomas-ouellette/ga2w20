package com.ga2w20.jpacontroller;

import com.ga2w20.entities.Survey;
import com.ga2w20.entities.SurveyClient;
import com.ga2w20.entities.Survey_;
import com.ga2w20.exceptions.IllegalOrphanException;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The SurveyJpaController is responsible for all queries and database actions
 * relevant to the Survey entity
 * 
 * @author Svitlana Myronova, Liam Harbec
 */
@Named
@RequestScoped
public class SurveyJpaController implements Serializable {

    private final static Logger LOG = LoggerFactory.getLogger(SurveyJpaController.class);

    @Resource
    private UserTransaction utx;

    @PersistenceContext
    private EntityManager em;

    /**
     * Create Survey entity
     *
     * @author Svitlana Myronova
     * @param survey
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void create(Survey survey) throws RollbackFailureException, Exception {
        try {
            if (survey.getSurveyClientList() == null) {
                survey.setSurveyClientList(new ArrayList<SurveyClient>());
            }
            utx.begin();
            List<SurveyClient> attachedSurveyClientList = new ArrayList<SurveyClient>();
            for (SurveyClient surveyClientListSurveyClientToAttach : survey.getSurveyClientList()) {
                surveyClientListSurveyClientToAttach = em.getReference(surveyClientListSurveyClientToAttach.getClass(), surveyClientListSurveyClientToAttach.getId());
                attachedSurveyClientList.add(surveyClientListSurveyClientToAttach);
            }
            survey.setSurveyClientList(attachedSurveyClientList);
            em.persist(survey);
            for (SurveyClient surveyClientListSurveyClient : survey.getSurveyClientList()) {
                Survey oldSurveyIdOfSurveyClientListSurveyClient = surveyClientListSurveyClient.getSurveyId();
                surveyClientListSurveyClient.setSurveyId(survey);
                surveyClientListSurveyClient = em.merge(surveyClientListSurveyClient);
                if (oldSurveyIdOfSurveyClientListSurveyClient != null) {
                    oldSurveyIdOfSurveyClientListSurveyClient.getSurveyClientList().remove(surveyClientListSurveyClient);
                    oldSurveyIdOfSurveyClientListSurveyClient = em.merge(oldSurveyIdOfSurveyClientListSurveyClient);
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Edit Survey entity
     *
     * @author Svitlana Myronova
     * @param survey
     * @throws NonexistentEntityException
     * @throws Exception
     */
    public void edit(Survey survey) throws NonexistentEntityException, Exception {
        try {
            utx.begin();
            Survey persistentSurvey = em.find(Survey.class, survey.getId());
            List<SurveyClient> surveyClientListOld = persistentSurvey.getSurveyClientList();
            List<SurveyClient> surveyClientListNew = survey.getSurveyClientList();
            List<String> illegalOrphanMessages = null;
            for (SurveyClient surveyClientListOldSurveyClient : surveyClientListOld) {
                if (!surveyClientListNew.contains(surveyClientListOldSurveyClient)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain SurveyClient " + surveyClientListOldSurveyClient + " since its surveyId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<SurveyClient> attachedSurveyClientListNew = new ArrayList<SurveyClient>();
            for (SurveyClient surveyClientListNewSurveyClientToAttach : surveyClientListNew) {
                surveyClientListNewSurveyClientToAttach = em.getReference(surveyClientListNewSurveyClientToAttach.getClass(), surveyClientListNewSurveyClientToAttach.getId());
                attachedSurveyClientListNew.add(surveyClientListNewSurveyClientToAttach);
            }
            surveyClientListNew = attachedSurveyClientListNew;
            survey.setSurveyClientList(surveyClientListNew);
            survey = em.merge(survey);
            for (SurveyClient surveyClientListNewSurveyClient : surveyClientListNew) {
                if (!surveyClientListOld.contains(surveyClientListNewSurveyClient)) {
                    Survey oldSurveyIdOfSurveyClientListNewSurveyClient = surveyClientListNewSurveyClient.getSurveyId();
                    surveyClientListNewSurveyClient.setSurveyId(survey);
                    surveyClientListNewSurveyClient = em.merge(surveyClientListNewSurveyClient);
                    if (oldSurveyIdOfSurveyClientListNewSurveyClient != null && !oldSurveyIdOfSurveyClientListNewSurveyClient.equals(survey)) {
                        oldSurveyIdOfSurveyClientListNewSurveyClient.getSurveyClientList().remove(surveyClientListNewSurveyClient);
                        oldSurveyIdOfSurveyClientListNewSurveyClient = em.merge(oldSurveyIdOfSurveyClientListNewSurveyClient);
                    }
                }
            }
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                int id = survey.getId();
                if (findSurvey(id) == null) {
                    throw new NonexistentEntityException("The survey with id " + id + " no longer exists.");
                }
            }
            throw ex;
        }
    }

    /**
     * Destroy Survey entity
     *
     * @author Svitlana Myronova
     * @param id
     * @throws IllegalOrphanException
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        try {
            utx.begin();
            Survey survey;
            try {
                survey = em.getReference(Survey.class, id);
                survey.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The survey with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<SurveyClient> surveyClientListOrphanCheck = survey.getSurveyClientList();
            for (SurveyClient surveyClientListOrphanCheckSurveyClient : surveyClientListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Survey (" + survey + ") cannot be destroyed since the SurveyClient " + surveyClientListOrphanCheckSurveyClient + " in its surveyClientList field has a non-nullable surveyId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(survey);
            utx.commit();

        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }

    /**
     * Find Survey entities
     *
     * @author Svitlana Myronova
     * @return list of Survey entities
     */
    public List<Survey> findSurveyEntities() {
        LOG.info("getting all the surveys");
        return findSurveyEntities(true, -1, -1);
    }

    /**
     * Find Survey entities
     *
     * @author Svitlana Myronova
     * @param maxResults
     * @param firstResult
     * @return list of Survey entities
     */
    public List<Survey> findSurveyEntities(int maxResults, int firstResult) {
        return findSurveyEntities(false, maxResults, firstResult);
    }

    /**
     * Find Survey entities
     *
     * @author Svitlana Myronova
     * @param all
     * @param maxResults
     * @param firstResult
     * @return list of Survey entities
     */
    private List<Survey> findSurveyEntities(boolean all, int maxResults, int firstResult) {

        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(Survey.class));
        Query q = em.createQuery(cq);
        if (!all) {
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
        }
        return q.getResultList();
    }

    /**
     * Find Survey entity by id
     *
     * @author Svitlana Myronova 
     * @param id
     * @return Survey entity
     */
    public Survey findSurvey(Integer id) {

        return em.find(Survey.class, id);
    }

    /**
     * Get count of Survey entities
     *
     * @author Svitlana Myronova
     * @return number of Survey entities
     */
    public int getSurveyCount() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        Root<Survey> rt = cq.from(Survey.class);
        cq.select(em.getCriteriaBuilder().count(rt));
        Query q = em.createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
    
    /**
     * Will select a random active survey to be displayed to the user and set it
     * in the session map variable.
     * 
     * @author Liam Harbec
     * @return The survey that was generated
     */
    public Survey generateSurvey() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Survey> query = cb.createQuery(Survey.class);
        Root<Survey> s = query.from(Survey.class);
        query.select(s).where(cb.isTrue(s.get(Survey_.surveyActive)));
        TypedQuery<Survey> surveyQuery = em.createQuery(query);

        List<Survey> surveys = surveyQuery.getResultList();
        // Validation already occurs on management side
        // This is in case there is somehow more than one active
        for (Survey sur : surveys) {
            LOG.info("----------------");
            LOG.info("Survey is: " + sur.getSurveyActive());
            LOG.info("Survey id: " + sur.getId());
        }
        try {
            Survey survey = surveys.get(0);
            
            return survey;
        } catch (NullPointerException e) {
            LOG.info("No survey was found to be active");
            return null;
        }
    }
    
    public Survey getActiveSurvey(){
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Survey> query = cb.createQuery(Survey.class);
        Root<Survey> survey = query.from(Survey.class);
        query.select(survey);
        query.where(cb.equal(survey.get("surveyActive"), true));
        TypedQuery<Survey> surveyQuery = em.createQuery(query);
        return surveyQuery.getSingleResult();
    }
    
    public void setNewCurrentActiveSurvey(Survey survey) throws Exception{
        try{
        utx.begin();
        Survey currentActiveSurvey = getActiveSurvey();
        currentActiveSurvey.setSurveyActive(false);
        em.merge(currentActiveSurvey);
        
        Survey newActiveSurvey = findSurvey(survey.getId());
        newActiveSurvey.setSurveyActive(true);
        em.merge(newActiveSurvey);
        utx.commit();
        } 
        catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
                LOG.error("Rollback");
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        }
    }
}
