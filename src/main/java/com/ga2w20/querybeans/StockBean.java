/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga2w20.querybeans;

import java.math.BigDecimal;

/**
 *
 * This is the stock bean use to hold the results of the custom query
 * to display a report on the store current stock
 * This stock bean represents a book
 * @author cadin
 */
public class StockBean {
    private String title;
    private BigDecimal wholesalePrice;
    private BigDecimal listPrice;
    private BigDecimal sales;
    private BigDecimal cost;
    private BigDecimal profit;
    private Long amountOfSales;
    
    /**
     * Constructor initiates all of the properties of this bean
     * @param title
     * @param wholesalePrice
     * @param listPrice
     * @param sales
     * @param cost
     * @param profit
     * @param amountOfSales 
     */
    public StockBean(String title,BigDecimal wholesalePrice, BigDecimal listPrice,BigDecimal sales, BigDecimal cost, BigDecimal profit, Long amountOfSales){
        this.title = title;
        this.wholesalePrice = wholesalePrice;
        this.listPrice = listPrice;
        this.sales = sales;
        this.cost = cost;
        this.profit = profit;
        this.amountOfSales = amountOfSales;
    }
    /**
     * Gets the title of the book
     * @return  String
     */
    public String getTitle(){
        return this.title;
    }
    /**
     * Sets the title of the book
     * @param title 
     */
    public void setTitle(String title){
        this.title = title;
    }
    /**
     * Gets the wholesale price of the book
     * @return BigDecimal money
     */
    public BigDecimal getWholesalePrice(){
        return this.wholesalePrice;
    }
    /**
     * Sets the wholesale price of the book
     * @param wholesalePrice 
     */
    public void setWholesalePrice(BigDecimal wholesalePrice){
        this.wholesalePrice = wholesalePrice;
    }
    /**
     * Gets the list price of the book
     * @return BigDecimal money
     */
    public BigDecimal getListPrice(){
        return this.listPrice;
    }
    /**
     * Sets the list price of the book
     * @param listPrice 
     */
    public void setListPrice(BigDecimal listPrice){
        this.listPrice = listPrice;
    }
    
    /**
     * Gets the sales of the record
     * @return BigDecimal Money
     */
    public BigDecimal getSales(){
        return this.sales;
    }
    /**
     * Sets the sale of the record/bean
     * @param sales 
     */
    public void setSales(BigDecimal sales){
        this.sales = sales;
    }
    /**
     * Gets the cost of the record/bean
     * @return BigDecimal Money
     */
    public BigDecimal getCost(){
        return this.cost;
    }
    /**
     * Sets the cost of the record/bean
     * @param cost 
     */
    public void setCost(BigDecimal cost){
        this.cost = cost;
    }
     /**
     * Gets the profit of the record/bean
     * @return BigDecimal Money
     */  
    public BigDecimal getProfit(){
        return this.profit;
    }
    /**
     * Sets the profit of the record/bean
     * @param profit
     */
    public void setProfit(BigDecimal profit){
        this.profit = profit;
    }
    /**
     * Gets the amount of sales/bean
     * @return Long count
     */
    public Long getAmountOfSales(){
        return this.amountOfSales;
    }
    /**
     * Sets the amount of sales of the record/bean
     * @param amountOfSales 
     */
    public void setAmountOfSales(Long amountOfSales){
        this.amountOfSales = amountOfSales;
    }
}
