/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ga2w20.querybeans;

import java.math.BigDecimal;
import java.util.Date;

/**
 * This is the bean that holds the result for most of the report queries
 * It contains the title of the record along with the information that 
 * all of them most display
 * @author cadin
 */
public class SummaryBean {
    private String title;
    private BigDecimal sales;
    private BigDecimal cost;
    private BigDecimal profit;
    private Long amountOfSales;

    /**
     * This constructor allows to initiate every single property of the bean
     * @param title
     * @param sales
     * @param cost
     * @param profit
     * @param amountOfSales 
     */
    public SummaryBean(String title,BigDecimal sales, BigDecimal cost, BigDecimal profit, Long amountOfSales){
        this.title = title;
        this.sales = sales;
        this.cost = cost;
        this.profit = profit;
        this.amountOfSales = amountOfSales;
    }
    /**
     * This constructor will allow to initiate all of the necessary properties that need to be display 
     * for a record. The only missing one is the title which is left empty in the case we want to change
     * it later on and not set it when we create the bean. (Custom titles)
     * @param sales
     * @param cost
     * @param profit
     * @param amountOfSales 
     */
    public SummaryBean(BigDecimal sales, BigDecimal cost, BigDecimal profit, Long amountOfSales){
        this.sales = sales;
        this.cost = cost;
        this.profit = profit;
        this.amountOfSales = amountOfSales;
    }
    /**
     * Gets the title of the record/ the bean
     * @return String title
     */
    public String getTitle(){
        return this.title;
    }
    /**
     * Sets the title of the record/ bean
     * @param title 
     */
    public void setTitle(String title){
        this.title = title;
    }
    /**
     * Gets the sales of the record
     * @return BigDecimal Money
     */
    public BigDecimal getSales(){
        return this.sales;
    }
    /**
     * Sets the sale of the record/bean
     * @param sales 
     */
    public void setSales(BigDecimal sales){
        this.sales = sales;
    }
    /**
     * Gets the cost of the record/bean
     * @return BigDecimal Money
     */
    public BigDecimal getCost(){
        return this.cost;
    }
    /**
     * Sets the cost of the record/bean
     * @param cost 
     */
    public void setCost(BigDecimal cost){
        this.cost = cost;
    }
     /**
     * Gets the profit of the record/bean
     * @return BigDecimal Money
     */  
    public BigDecimal getProfit(){
        return this.profit;
    }
    /**
     * Sets the profit of the record/bean
     * @param profit
     */
    public void setProfit(BigDecimal profit){
        this.profit = profit;
    }
    /**
     * Gets the amount of sales/bean
     * @return Long count
     */
    public Long getAmountOfSales(){
        return this.amountOfSales;
    }
    /**
     * Sets the amount of sales of the record/bean
     * @param amountOfSales 
     */
    public void setAmountOfSales(Long amountOfSales){
        this.amountOfSales = amountOfSales;
    }
    
}
