package com.ga2w20.utils;

import java.util.List;

/**
 * The Paginator class helps to implements pagination in app
 *
 * @author Svitlana Myronova
 * @param <T> specified type of object
 */
public class Paginator<T> {

    /**
     * Default numbers of items per page
     */
    private static final int DEFAULT_PER_PAGE_NUMBER = 10;

    /**
     * Default index on a page
     */
    private static final int DEFAULT_PAGE_INDEX = 1;

    private final PaginatorData paginatorData;

    private final int itemsPerPage;
    private int pageIndex;
    private final int pagesTotal;
    private List<T> model;

    /**
     *  Constructor for Paginator
     * 
     * @author Svitlana Myrornova
     * @param paginatorData
     */
    public Paginator(PaginatorData paginatorData) {
        this.paginatorData = paginatorData;
        this.itemsPerPage = DEFAULT_PER_PAGE_NUMBER;
        this.pageIndex = DEFAULT_PAGE_INDEX;

        this.pagesTotal = (paginatorData.getTotalItems() + itemsPerPage - 1) / itemsPerPage;
        this.model = paginatorData.getPageData((pageIndex - 1) * itemsPerPage, itemsPerPage);
        updateModel();
    }
    
    /**
     * Update model depending on pageIndex and itemsPerPage
     * 
     * @author Svitlana Myrornova
     */
    private void updateModel() {
        if (pagesTotal == 0) {
            this.model = null;
        } else {
            this.model = paginatorData.getPageData((pageIndex - 1) * itemsPerPage, itemsPerPage);
        }
    }

    /**
     * Check the index and go to next one
     * 
     * @author Svitlana Myrornova
     */
    public void next() {
        if (this.pageIndex < this.pagesTotal) {
            this.pageIndex++;
        }

        updateModel();
    }

    /**
     * Check the index and go to previous one
     * 
     * @author Svitlana Myrornova
     */
    public void prev() {
        if (this.pageIndex > 1) {
            this.pageIndex--;
        }

        updateModel();
    }

    /**
     * Getter for pageIndex
     *
     * @author Svitlana Myrornova
     * @return pageIndex
     */
    public int getPageIndex() {
        return pageIndex;
    }
    
    /**
     * Setter for pageIndex
     *
     * @author Svitlana Myrornova
     * @param pageIndex
     */
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
        updateModel();
    }

    /**
     * Getter for pagesTotal
     *
     * @author Svitlana Myrornova
     * @return pagesTotal
     */
    public int getPagesTotal() {
        return pagesTotal;
    }

    /**
     * Getter for model
     *
     * @author Svitlana Myrornova
     * @return
     */
    public List<T> getModel() {
        return model;
    }    
}
