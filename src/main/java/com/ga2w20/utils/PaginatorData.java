package com.ga2w20.utils;

import java.util.List;

/**
 * PaginatorData interface  
 *
 * @author Svitlana Myronova
 * @param <T> specified type of object
 */
public interface PaginatorData<T> {

    /**
     * Getter for total number of items
     *
     * @author Svitlana Myrornova
     * @return number or items
     */
    int getTotalItems();

    /**
     * Getter for a data per one page
     *
     * @author Svitlana Myrornova
     * @param firstItem
     * @param maxItems
     * @return list of items
     */
    List<T> getPageData(int firstItem, int maxItems);
}
