package com.ga2w20.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

/**
 * Used to protect the passwords of clients in the database by using a salt value
 * as well as hashing the passwords.
 * 
 * @author Ken Fogel, Liam Harbec
 */
public class PasswordUtils {

    /**
     * Generates a random salt value
     * 
     * @return The salt
     */
    public String getSalt() {
        Random r = new SecureRandom();
        byte[] saltBytes = new byte[32];
        r.nextBytes(saltBytes);
        return Base64.getEncoder().encodeToString(saltBytes);
    }

    
    /**
     * Calls the hashPassword method with the combined password and salt value
     * 
     * @param password The password
     * @param salt The salt
     * @return The hashed and salted password
     * @throws NoSuchAlgorithmException 
     */
    public String hashAndSaltPassword(String password, String salt) throws NoSuchAlgorithmException {
        return hashPassword(password + salt);
    }
    
    
    /**
     * Hashes the inputted password
     * 
     * @param password The password
     * @return The hashed password
     * @throws NoSuchAlgorithmException 
     */
    private String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.reset();
        md.update(password.getBytes());
        byte[] mdArray = md.digest();
        StringBuilder sb = new StringBuilder(mdArray.length * 2);
        for (byte b : mdArray) {
            int v = b & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString();
    }
}
