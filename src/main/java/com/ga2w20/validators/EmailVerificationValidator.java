package com.ga2w20.validators;

import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author liamharbec
 */
@FacesValidator("emailVerificationValidator")
public class EmailVerificationValidator implements Validator {
    
    private final static Logger LOG = LoggerFactory.getLogger(EmailVerificationValidator.class);
    
    @Override
    public void validate(FacesContext context,
            UIComponent component, Object value) throws ValidatorException {
        UIInput emailInput = (UIInput) component.getAttributes().get("email");
        String email = ((String) emailInput.getValue()).isBlank() ? null : (String) emailInput.getValue();
        
        List<String> allEmails = (List<String>) component.getAttributes().get("allEmails");
        
        if (email != null && allEmails.contains(email)) {
            LOG.info("Email is already being used");
            String errorMessage = context.getApplication().evaluateExpressionGet(context, "#{msg.Registration_EmailUsed}", String.class);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, errorMessage);
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(facesMessage);
        }
    }
    
}
