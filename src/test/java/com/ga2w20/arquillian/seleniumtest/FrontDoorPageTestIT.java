package com.ga2w20.arquillian.seleniumtest;

import io.github.bonigarcia.wdm.WebDriverManager;
import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Testing the front door page with Selenium
 *
 * @author Svitlana Myronova, Ken Fogel
 */
public class FrontDoorPageTestIT {

    private final static Logger LOG = LoggerFactory.getLogger(FrontDoorPageTestIT.class);

    private WebDriver driver;
    static private ResourceBundle resourceBundle;

    /**
     * Setup before doing all tests.
     * 
     * @author Svitlana Myrornova
     * @throws IOException
     */
    @BeforeClass
    public static void setupClass() throws IOException {
        // Normally an executable that matches the browser you are using must
        // be in the classpath. The webdrivermanager library by Boni Garcia
        // downloads the required driver and makes it available
        WebDriverManager.chromedriver().setup();
        //Get contentStream to be able to get message from MessageBundle_en
        InputStream contentStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/ga2w20/messagebundles/MessageBundle_en.properties");
        assertThat(contentStream).isNotNull();
        resourceBundle = new PropertyResourceBundle(contentStream);
    }

    /**
     * Setup before doing each separate test
     * 
     * @author Svitlana Myrornova
     */
    @Before
    public void setupTest() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("-lang=en");
        driver = new ChromeDriver(options);
    }

    /**
     * Test for Page title:
     * Welcome to Qual-e-tree Bookstore.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testPageTitle() throws Exception {
        LOG.info("testPageTitle method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check title
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Test for Logo. When you click on it, the front door page opens with the title:
     * Welcome to Qual-e-tree Bookstore.
     * 
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testLogo() throws Exception {
        LOG.info("testLogo method");
        
        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("logo")).click();
        //Check title
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Test for BrowseByGenre. When you click on it, the title will be Books of Genre.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testBrowseByGenre() throws Exception {
        LOG.info("testBrowseByGenre method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the submit button
        driver.findElement(By.id("browseByGenreUiId:0:browseByGenreFormId:genreId")).click();
        //Check title
        pageTitle = resourceBundle.getString("Title_BooksOfGenre");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }    

    /**
     * Test for Search for one result. When you click on it, Book page appears.
     * 
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testSearchForOneResult() throws Exception {
        LOG.info("testSearchForOneResult method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Find search input field
        WebElement inputElement = driver.findElement(By.id("searchFormId:searchQuery"));
        // Clear out anything currently in the field
        inputElement.clear();
        // Enter text into the input field
        inputElement.sendKeys("Murder on the Orient Express");

        // Click the submit button
        driver.findElement(By.id("searchFormId:searchBtn")).click();
        //Check title
        pageTitle = resourceBundle.getString("Title_Book");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Test for Search for many results. When you click on it, Search page appears.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testSearchForManyResults() throws Exception {
        LOG.info("testSearchForManyResults method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Find search input field
        WebElement inputElement = driver.findElement(By.id("searchFormId:searchQuery"));
        // Clear out anything currently in the field
        inputElement.clear();
        // Enter text into the input field
        inputElement.sendKeys("the");

        // Click the submit button
        driver.findElement(By.id("searchFormId:searchBtn")).click();
        //Check title
        pageTitle = resourceBundle.getString("Title_Search");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Test for Search without input. When you click on it, the page stays the same.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testSearchWithoutInput() throws Exception {
        LOG.info("testSearchWithoutInput method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Find search input field
        WebElement inputElement = driver.findElement(By.id("searchFormId:searchQuery"));
        // Clear out anything currently in the field
        inputElement.clear();
        // Enter text into the input field
        inputElement.sendKeys("");

        // Click the submit button
        driver.findElement(By.id("searchFormId:searchBtn")).click();
        //Check title
        pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }

    /**
     * Test for Language. When you click on it, the title is on French:
     * Bienvenue à Qual-e-tree.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testLanguage() throws Exception {
        LOG.info("testLanguage method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("languageFrFormId:languageFrComLink")).click();
        //Get french contentStream
        InputStream contentStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/ga2w20/messagebundles/MessageBundle_fr.properties");
        assertThat(contentStream).isNotNull();
        resourceBundle = new PropertyResourceBundle(contentStream);
        //Get french title
        String pageTitleFr = resourceBundle.getString("Title_ClientIndex");
        //return contentStream and resourceBundle to English
        contentStream = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("com/ga2w20/messagebundles/MessageBundle_en.properties");
        assertThat(contentStream).isNotNull();
        resourceBundle = new PropertyResourceBundle(contentStream);
        //Check title
        wait.until(ExpectedConditions.titleIs(pageTitleFr));
    }
    
    /**
     * Test for LogIn. When you click on it, LogIn page appears.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testLogIn() throws Exception {
        LOG.info("testLogIn method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("logInOutputLinkId")).click();
        //Check title
        pageTitle = resourceBundle.getString("Title_Login");
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }
    
    /**
     * Test for left Ads. When you click on it, this redirects to another app.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testLeftAds() throws Exception {
        LOG.info("testLeftAds method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("leftAdsId")).click();
        //Check title
        wait.until(ExpectedConditions.not(ExpectedConditions.titleIs(pageTitle)));
    }

    /**
     * Test for right Ads. When you click on it, this redirects to another app.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testRightAds() throws Exception {
        LOG.info("testRightAds method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("rightAdsId")).click();
        //Check title
        wait.until(ExpectedConditions.not(ExpectedConditions.titleIs(pageTitle)));
    }
    
    /**
     * Test for Specials. When you click on a book, Book page appears.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testSpecials() throws Exception {
        LOG.info("testSpecials method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("specialBookRepeatId:0:specialBookId")).click();
        pageTitle = resourceBundle.getString("Title_Book");
        //Check title
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }    
    
    /**
     * Test for Newest Arrivals. When you click on a book, Book page appears.
     *
     * @author Svitlana Myrornova
     * @throws Exception
     */
    @Test
    public void testNewestArrivals() throws Exception {
        LOG.info("testNewestArrivals method");

        // And now use this to visit a web site
        driver.get("http://localhost:8080/ga2w20/");

        // Wait for the page to load, timeout after 10 seconds
        WebDriverWait wait = new WebDriverWait(driver, 10);
        // Check the title of the page
        String pageTitle = resourceBundle.getString("Title_ClientIndex");
        wait.until(ExpectedConditions.titleIs(pageTitle));

        // Click the logo button
        driver.findElement(By.id("newestBookRepeatId:0:newestBookId")).click();
        pageTitle = resourceBundle.getString("Title_Book");
        //Check title
        wait.until(ExpectedConditions.titleIs(pageTitle));
    }
    
    @After
    public void shutdownTest() {
        //Close the browser
        driver.quit();
    }

}
