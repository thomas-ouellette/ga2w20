package com.ga2w20.arquillian.test;

import com.ga2w20.entities.Advertisement;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AdvertisementJpaController;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import static org.assertj.core.api.Assertions.assertThat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ArquillianAdvertisementTest executes queries that take no parameters
 * in the AdvertisementJpaController.
 * 
 * 
 * @author Svitlana Myronova
 */
@RunWith(Arquillian.class)
public class ArquillianAdvertisementTest {

    private final static Logger LOG = LoggerFactory.getLogger(ArquillianAdvertisementTest.class);

    /**
     * Deploy
     * 
     * @author Svitlana Myrornova
     * @return webArchive
     */
    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(AdvertisementJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Advertisement.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private AdvertisementJpaController ajc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;
    
    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    /**
     * Check of creating of Advertisement entity. 
     *
     * @author Svitlana Myrornova
     * @throws RollbackFailureException
     * @throws IllegalStateException
     * @throws Exception
     */
    @Test
    public void testCreate() throws RollbackFailureException, IllegalStateException, Exception {
        Advertisement advertisement = new Advertisement();
        advertisement.setImgUrl("https://icdn2.digitaltrends.com/image/digitaltrends/oneplus-7-pro-review-1.jpg");
        advertisement.setSiteUrl("https://www.oneplus.com/ca_en");

        java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-04-01");
        java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        advertisement.setStartDate(timestamp);

        date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-05-01");
        timestamp = new java.sql.Timestamp(date.getTime());
        advertisement.setEndDate(timestamp);

        advertisement.setAdsActive(false);

        ajc.create(advertisement);

        Advertisement ad1 = ajc.findAdvertisement(advertisement.getId());

        LOG.debug("ID " + advertisement.getId());

        assertEquals("Not the same", ad1, advertisement);
    }

    /**
     * Verify that the AdvertisementController is returning the 8 advertisement in the database
     *
     * @author Svitlana Myrornova
     * @throws SQLException
     */
    @Test
    public void should_find_all_advertisement() throws SQLException {
        LOG.info("should_find_all_advertisement()");

        List<Advertisement> advertisementAll = ajc.findAdvertisementEntities();
        assertThat(advertisementAll).hasSize(8);
    }
    

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     * 
     * @author Svitlana Myrornova
     */
    @Before 
    public void seedDatabase() throws Exception {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }    
}
