package com.ga2w20.arquillian.test;

import com.ga2w20.arquillian.test.bookcontroller.params.ArquillianBookControllerParameterizedTestParam;
import com.ga2w20.arquillian.test.bookcontroller.params.BooksLikeAuthorParam;
import com.ga2w20.arquillian.test.bookcontroller.params.BooksLikePublisherParam;
import com.ga2w20.arquillian.test.bookcontroller.params.BooksLikeTitleParam;
import com.ga2w20.arquillian.test.bookcontroller.params.BooksWithGenreParam;
import com.ga2w20.arquillian.test.bookcontroller.params.TopSellersWithAllGenresParam;
import com.ga2w20.arquillian.test.bookcontroller.params.TopSellersWithGenreParam;
import com.ga2w20.entities.Book;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for testing methods with parameters from BookJpaController class
 *
 * 
 * @author Svitlana Myronova
 */
@RunWith(Arquillian.class)
public class ArquillianBookControllerParameterizedTest {

    private final static Logger LOG = LoggerFactory.getLogger(ArquillianBookControllerParameterizedTest.class);

    /**
     * Deploy
     * 
     * @author Svitlana Myrornova
     * @return webArchive
     */
    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Book.class.getPackage())
                .addPackage(BookJpaController.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ArquillianBookControllerParameterizedTestParam.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private BookJpaController bjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    /**
     * ParameterRule creates ArquillianBookControllerParameterizedTestParam objects then runs the test methods.
     * 
     * @author Svitlana Myrornova
     */
    @Rule
    public ParameterRule rule = new ParameterRule("testParam",            
            new ArquillianBookControllerParameterizedTestParam(
                    new BooksLikePublisherParam("HarperOne", 1, 1, 0, 1),
                    new BooksLikeAuthorParam("Sofia Stril-Rever", 1, 1, 0, 1),
                    new BooksLikeTitleParam("Death on the Nile", 1, 1, 0, 1),
                    new TopSellersWithGenreParam(1, 19, 17, 2, 20),
                    new TopSellersWithAllGenresParam(94, 0, 94),
                    new BooksWithGenreParam(1, 19, 19)
            ),
            new ArquillianBookControllerParameterizedTestParam(
                    new BooksLikePublisherParam("House", 4, 3, 1, 3),
                    new BooksLikeAuthorParam("Robert", 7, 3, 2, 3),
                    new BooksLikeTitleParam("learn", 4, 4, 0, 4),
                    new TopSellersWithGenreParam(4, 20, 18, 2, 20),
                    new TopSellersWithAllGenresParam(80, 14, 94),
                    new BooksWithGenreParam(2, 19, 19)
            ),
            new ArquillianBookControllerParameterizedTestParam(
                    new BooksLikePublisherParam("One", 2, 1, 1, 2),
                    new BooksLikeAuthorParam("Mary", 4, 3, 1, 4),
                    new BooksLikeTitleParam("life", 2, 1, 1, 2),
                    new TopSellersWithGenreParam(2, 19, 18, 1, 19),    
                    new TopSellersWithAllGenresParam(50, 0, 50),
                    new BooksWithGenreParam(1, 10, 10)
            ),
            new ArquillianBookControllerParameterizedTestParam(
                    new BooksLikePublisherParam("world", 2, 1, 1, 2),
                    new BooksLikeAuthorParam("Jane", 6, 5, 1, 6),
                    new BooksLikeTitleParam("Code", 2, 1, 1, 2),
                    new TopSellersWithGenreParam(3, 19, 18, 1, 19),    
                    new TopSellersWithAllGenresParam(90, 4, 94),
                    new BooksWithGenreParam(1, 1, 1)    
            ),
            new ArquillianBookControllerParameterizedTestParam(
                    new BooksLikePublisherParam("qwert", 0, 0, 0, 0),
                    new BooksLikeAuthorParam("123", 0, 0, 0, 0),
                    new BooksLikeTitleParam("poiuy", 0, 0, 0, 0),
                    new TopSellersWithGenreParam(10, 0, 0, 0, 0),
                    new TopSellersWithAllGenresParam(1, 93, 94),
                    new BooksWithGenreParam(10, 0, 0)
            )            
    );

    private ArquillianBookControllerParameterizedTestParam testParam;

    /**
     * Test for getBooksLikePublisherCount(String publisher) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikePublisherCount() {
        LOG.info("FindBookLikePublisherCount");
        
        int countEval = bjc.getBooksLikePublisherCount(testParam.booksLikePublisherParam.publisher);
        int countExpec = testParam.booksLikePublisherParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Test for getBooksLikePublisher(String publisher, int firstItem, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikePublisher() {
        LOG.info("FindBookLikePublisher");

        List<Book> books = bjc.getBooksLikePublisher(
                testParam.booksLikePublisherParam.publisher,
                testParam.booksLikePublisherParam.firstItem,
                testParam.booksLikePublisherParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.booksLikePublisherParam.countInterval;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }
    
    /**
     * Test for getBooksLikeAuthorCount(String author) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikeAuthorCount() {
        LOG.info("FindBookLikeAuthorCount");
        
        int countEval = bjc.getBooksLikeAuthorCount(testParam.booksLikeAuthorParam.author);
        int countExpec = testParam.booksLikeAuthorParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Test for getBooksLikeAuthor(String author, int firstItem, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikeAuthor() {
        LOG.info("FindBookLikeAuthor");

        List<Book> books = bjc.getBooksLikeAuthor(
                testParam.booksLikeAuthorParam.author,
                testParam.booksLikeAuthorParam.firstItem,
                testParam.booksLikeAuthorParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.booksLikeAuthorParam.countInterval;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }
    
    /**
     * Test for getBooksLikeTitleCount(String title) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikeTitleCount() {
        LOG.info("FindBookLikeTitleCount");
        
        int countEval = bjc.getBooksLikeTitleCount(testParam.booksLikeTitleParam.title);
        int countExpec = testParam.booksLikeTitleParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Test for getBooksLikeTitle(String title, int firstItem, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBookLikeTitle() {
        LOG.info("FindBookLikeTitle");

        List<Book> books = bjc.getBooksLikeTitle(
                testParam.booksLikeTitleParam.title,
                testParam.booksLikeTitleParam.firstItem,
                testParam.booksLikeTitleParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.booksLikeTitleParam.countInterval;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }
    
    /**
     * Test for getBooksWithGenreCount(String genre) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBooksWithGenreCount() {
        LOG.info("FindBooksWithGenreCount");
        
        int countEval = bjc.getBooksWithGenreCount(testParam.topSellersWithGenreParam.genreId);
        int countExpec = testParam.topSellersWithGenreParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Test for getTopSellersWithGenre(String genre, int firstItem, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindTopSellersWithGenre() {
        LOG.info("FindTopSellersWithGenre");

        List<Book> books = bjc.getTopSellersWithGenre(
                testParam.topSellersWithGenreParam.genreId,
                testParam.topSellersWithGenreParam.firstItem,
                testParam.topSellersWithGenreParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.topSellersWithGenreParam.countInterval;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Test for getTopSellersWithAllGenres(int firstItem, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindTopSellersWithAllGenres() {
        LOG.info("FindTopSellersWithAllGenres");

        List<Book> books = bjc.getTopSellersWithAllGenres(
                testParam.topSellersWithAllGenresParam.firstItem,
                testParam.topSellersWithAllGenresParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.topSellersWithAllGenresParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }
    
    /**
     * Test for getBooksWithGenreActive(int genreId, int maxItems) method from BookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindBooksWithGenreActive() {
        LOG.info("FindBooksWithGenreActive");

        List<Book> books = bjc.getBooksWithGenreActive(
                testParam.booksWithGenreParam.genreId,
                testParam.booksWithGenreParam.maxItems);
        
        int countEval = books.size();
        int countExpec = testParam.booksWithGenreParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
    }

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     * 
     * @author Svitlana Myrornova
     */
    @Before
    public void seedDatabase() throws Exception {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
