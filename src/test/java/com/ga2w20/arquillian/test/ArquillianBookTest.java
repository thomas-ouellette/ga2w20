package com.ga2w20.arquillian.test;

import com.ga2w20.entities.Author;
import com.ga2w20.entities.Book;
import com.ga2w20.entities.Format;
import com.ga2w20.entities.Genre;
import com.ga2w20.entities.Publisher;
import com.ga2w20.exceptions.NonexistentEntityException;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AuthorJpaController;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.FormatJpaController;
import com.ga2w20.jpacontroller.GenreJpaController;
import com.ga2w20.jpacontroller.PublisherJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import static org.assertj.core.api.Assertions.assertThat;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ArquillianBookTest executes queries that take no parameters
 * in multiple controllers.
 * 
 * @author Svitlana Myronova
 */
@RunWith(Arquillian.class)
public class ArquillianBookTest {
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianBookTest.class);    

    /**
     * Deploy
     * 
     * @author Svitlana Myrornova
     * @return webArchive
     */
    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(BookJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Book.class.getPackage())
                .addPackage(Genre.class.getPackage())
                .addPackage(Publisher.class.getPackage())
                .addPackage(Author.class.getPackage())
                .addPackage(Format.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private BookJpaController bjc;

    @Inject
    private PublisherJpaController pjc;

    @Inject
    private GenreJpaController gjc;

    @Inject
    private AuthorJpaController ajc;

    @Inject
    private FormatJpaController fjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    /**
     * Check of creating of Book entity. 
     *
     * @author Svitlana Myrornova
     * @throws RollbackFailureException
     * @throws IllegalStateException
     * @throws Exception
     */
    @Test
    public void testCreate() throws RollbackFailureException, IllegalStateException, Exception {
        Book book = new Book();

        long isbn = 1111111111;

        book.setIsbn(isbn);
        book.setTitle("new title");

        Publisher publisher = pjc.findPublisher(1);
        book.setPublisherId(publisher);

        java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-01");
        java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        book.setPublicationDate(timestamp);

        int nunberOfPage = 100;
        book.setNumberOfPages(nunberOfPage);

        Genre genre = gjc.findGenre(1);
        book.setGenreId(genre);

        String description = "some description";
        book.setDescription(description);

        String imageFile = "some where";
        book.setImageFile(imageFile);

        BigDecimal wholeSalePrice = new BigDecimal(10.23);
        book.setWholeSalePrice(wholeSalePrice);

        BigDecimal listPrice = new BigDecimal(10.23);
        book.setListPrice(listPrice);

        Author author = ajc.findAuthor(1);
        List<Author> authors = new ArrayList<Author>();
        authors.add(author);
        book.setAuthorList(authors);

        Format format = fjc.findFormat(1);
        List<Format> formats = new ArrayList<Format>();
        formats.add(format);
        book.setFormatList(formats);

        date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-01");
        timestamp = new java.sql.Timestamp(date.getTime());
        book.setEnteredDate(timestamp);

        book.setRemovalStatus(false);

        bjc.create(book);

        Book book1 = bjc.findBook(book.getIsbn());

        LOG.debug("ID " + book.getIsbn());

        assertEquals("Not the same", book1, book);
    }
    
    @Test
    public void editBookTest() throws NonexistentEntityException, Exception{
        Book book = bjc.findBook(9780006913368l);
        book.setTitle("1");
        bjc.edit(book);
        Book book2 = bjc.findBook(9780006913368l);
        assertEquals("1", book2.getTitle());
    }

    /**
     * Verify that the BookController is returning the 100 books in the database
     *
     * @author Svitlana Myrornova
     * @throws SQLException
     */
    @Test
    public void should_find_all_book() throws SQLException {
        LOG.info("should_find_all_book()");

        List<Book> bookAll = bjc.findBookEntities();
        assertThat(bookAll).hasSize(100);
    }
    
    
    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     * 
     * @author Svitlana Myronova
     */
    @Before 
    public void seedDatabase() throws Exception {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
