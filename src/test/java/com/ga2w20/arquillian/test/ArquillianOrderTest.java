package com.ga2w20.arquillian.test;

import com.ga2w20.entities.Client;
import com.ga2w20.entities.Order;
import com.ga2w20.entities.OrderBook;
import com.ga2w20.entities.OrderBookPK;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.jpacontroller.OrderBookJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * This class contain test for the orderJpaController.
 * These test were done before we knew that we should only test
 * the methods we create in the jpa controller and not the defaults
 * @author Cadin Londono
 */
@RunWith(Arquillian.class)
public class ArquillianOrderTest {
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianOrderTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(OrderJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Order.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private OrderJpaController ojc;
    
    @Inject
    private ClientJpaController cjc;
    
    @Inject
    private BookJpaController bjc;
    
    @Inject
    private OrderBookJpaController objc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;
    
    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    /**
     * Test that the create method of orderJpaController works properly
     * @throws RollbackFailureException
     * @throws IllegalStateException
     * @throws Exception 
     */
    @Test
    public void testCreate() throws RollbackFailureException, IllegalStateException, Exception {
        //Id
        Order order = new Order();
        int orderId = 7;
        order.setId(orderId);
        //Client
        Client client = cjc.findClient(1);
        order.setClientId(client);
        //Price and Taxes
        order.setTotalGST(new BigDecimal(7));
        order.setTotalPST(new BigDecimal(5)); 
        order.setTotalHST(BigDecimal.ZERO);
        order.setTotalPrice(new BigDecimal(18));
        //Date
        java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-02-24");
        java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        order.setOrderDate(timestamp);
        
        OrderBookPK orderBookPK = new OrderBookPK();
        orderBookPK.setIsbn(9780593170984l);
        orderBookPK.setOrderId(6);
        OrderBook orderBook = objc.findOrderBook(orderBookPK);
        orderBook.setOrder(order);
        List<OrderBook> orderBooks = new ArrayList<OrderBook>();
        orderBooks.add(orderBook);
        order.setOrderBookList(orderBooks);
       
        ojc.create(order);
        Order orderFromDB = ojc.findOrder(33);
        LOG.debug("ID " + order.getId());
        assertEquals("Not same the same", orderFromDB, order);
    }

    /**
     * Verify that the OrderController is returning the 6 orders in the database
     *
     * @throws SQLException
     */
    
    @Test
    public void should_find_all_orders_01() throws SQLException {
        List<Order> listOrders = ojc.findOrderEntities();
        assertEquals(listOrders.size(), 32);
    }
    
    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() throws Exception {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    } 
}
