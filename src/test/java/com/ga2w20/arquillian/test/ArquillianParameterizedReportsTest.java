
package com.ga2w20.arquillian.test;

import com.ga2w20.arquillian.test.reports.params.ArquillianReportsTestsParam;
import com.ga2w20.arquillian.test.reports.params.ReportAuthorParam;
import com.ga2w20.arquillian.test.reports.params.ReportBooksParam;
import com.ga2w20.arquillian.test.reports.params.ReportClientsParam;
import com.ga2w20.arquillian.test.reports.params.ReportPublisherParam;
import com.ga2w20.arquillian.test.reports.params.ReportSalesParam;
import com.ga2w20.entities.Order;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AuthorJpaController;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.jpacontroller.ClientJpaController;
import java.io.File;
import javax.inject.Inject;
import com.ga2w20.jpacontroller.OrderJpaController;
import com.ga2w20.jpacontroller.PublisherJpaController;
import com.ga2w20.querybeans.SalesBean;
import com.ga2w20.querybeans.StockBean;
import com.ga2w20.querybeans.SummaryBean;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *These are all the test for all the methods related to reports on the managers side.
 * It is all parameterised testing 
 * 
 * 
 * @author cadin
 */
@RunWith(Arquillian.class)
public class ArquillianParameterizedReportsTest {
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianParameterizedReportsTest.class);

    
    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // This is a simpler ShrinkWrap because this example does not use JPA
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
        .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                // Add all classes that share the package this class is found in
                .addPackage(OrderJpaController.class.getPackage())
                .addPackage(ReportSalesParam.class.getPackage())
                // Add only this class from the package it is found in
                .addClass(ParameterRule.class)
                .addPackage(SalesBean.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Order.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    /**
     * The ParameterRule class does not support an array of values that are 
     * assigned by the test class constructor as regular parameterized are done.
     * 
     * ParameterRule creates FinanceBean objects initialized through the 
     * FinanceBeani constructor one at a time and then runs the test methods.
     * 
     * In this example I was able to use the FinanceBean but for your tests you 
     * will need to create a special test class that contains the parameters to 
     * test and the result
     */
    @Rule
    public ParameterRule rule = new ParameterRule("dynamic",
            new ArquillianReportsTestsParam(
                    new ReportSalesParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-01"),1, 5, new BigDecimal(62.41), new BigDecimal(12.95)),
                    new ReportBooksParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-01"),"My Spiritual Journey", 95,4, new BigDecimal(16.18), new BigDecimal(17.98), 1, new BigDecimal(0.00), 9780006913368l),
                    new ReportClientsParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-01"), 1, 1, new BigDecimal(16.18), new BigDecimal(62.41), "Jack Dawson #1"),
                    new ReportPublisherParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-01"), 1, new BigDecimal(12.95), 1, new BigDecimal(12.95)),
                    new ReportAuthorParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-01"), 1,1, new BigDecimal(12.95), new BigDecimal(12.95))),
            new ArquillianReportsTestsParam(
                    new ReportSalesParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-31"),6, 17, new BigDecimal(246.12), new BigDecimal(12.95)),
                    new ReportBooksParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-31"),"And Then There Were None", 83,12, new BigDecimal(10.79), new BigDecimal(11.99), 1, new BigDecimal(21.14), 9780061138829l),
                    new ReportClientsParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-31"), 5, 4, new BigDecimal(9.89), new BigDecimal(16.20), "Sam Tree #6"),
                    new ReportPublisherParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-31"), 5, new BigDecimal(6.24), 1, new BigDecimal(6.24)),
                    new ReportAuthorParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-31"), 6,1, new BigDecimal(6.24), new BigDecimal(6.24))),
            new ArquillianReportsTestsParam(
                    new ReportSalesParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-08"),3, 13, new BigDecimal(194.86), new BigDecimal(0.90)),
                    new ReportBooksParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-08"),"Computer Science: An Interdisciplinary Approach", 87,99, new BigDecimal(0.00), new BigDecimal(15.70), 0, new BigDecimal(63.99), 9780134076423l),
                    new ReportClientsParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-08"), 4, 3, new BigDecimal(1.84), new BigDecimal(10.83), "Sam Tree #6"),
                    new ReportPublisherParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-08"), 8, new BigDecimal(57.59), 1, new BigDecimal(57.59)),
                    new ReportAuthorParam(Date.valueOf("2020-03-01"), Date.valueOf("2020-03-08"), 10,1, new BigDecimal(57.59), new BigDecimal(57.59))),
            new ArquillianReportsTestsParam(
                    new ReportSalesParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-29"),3, 17, new BigDecimal(384.36), new BigDecimal(8.99)),
                    new ReportBooksParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-29"),"Structure and Interpretation of Computer Programs",83,96, new BigDecimal(0.00), new BigDecimal(3.99), 0, new BigDecimal(0.00), 9780195139570l),
                    new ReportClientsParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-29"), 7, 2, new BigDecimal(15.29), new BigDecimal(93.58), "Jack Dawson #1"),
                    new ReportPublisherParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-29"), 3, new BigDecimal(46.52), 2, new BigDecimal(23.26)),
                    new ReportAuthorParam(Date.valueOf("2020-02-01"), Date.valueOf("2020-02-29"), 4,1, new BigDecimal(46.52), new BigDecimal(23.26))),
            new ArquillianReportsTestsParam(
                    new ReportSalesParam(Date.valueOf("2020-01-01"), Date.valueOf("2020-04-12"), 3,31, new BigDecimal(630.48), new BigDecimal(17.99)),
                    new ReportBooksParam(Date.valueOf("2020-01-01"), Date.valueOf("2020-04-12"), "Structure and Interpretation of Computer Programs", 69, 60, new BigDecimal(19.78), new BigDecimal(10.99), 2l, new BigDecimal(1.00), 9789176371824l),
                    new ReportClientsParam(Date.valueOf("2020-01-01"), Date.valueOf("2020-04-12"),7,7,new BigDecimal(8.99), new BigDecimal(69.60),"Jack Dawson #1"),
                    new ReportPublisherParam(Date.valueOf("2020-01-01"), Date.valueOf("2020-04-12"),24, new BigDecimal(14.21), 1, new BigDecimal(14.21)),
                    new ReportAuthorParam(Date.valueOf("2020-01-01"), Date.valueOf("2020-04-12"),69, 1, new BigDecimal(8.99), new BigDecimal(8.99))
            )
       );
    // The identifier for this object must match the string that is the first 
    // parameter of the ParameterRule object
    private ArquillianReportsTestsParam dynamic;
    
    @Inject
    OrderJpaController orderJpaController;
    
    @Inject
    BookJpaController bookJpaController;
    
    @Inject
    ClientJpaController clientJpaController;
    
    @Inject
    PublisherJpaController publisherJpaController;
    
    @Inject
    AuthorJpaController authorJpaController;
    
    /**
     *This test will test if getSalesAllBooks gets the right amount of books
     * that have sold. (grouped by book meaning even if the book has sold multiple times, it will
     * only appear once)
     */
    @Test
    public void testGetAllTotalSalesForSpecificDateRange(){
        List<SalesBean> salesBeans = orderJpaController.getSalesAllBooks(dynamic.getReportSalesParam().getFrom(), dynamic.getReportSalesParam().getTo());
        assertEquals(dynamic.getReportSalesParam().getAmountOfSales(), salesBeans.size());
    }
    /**
     * This test will test that the total profit from all the sales in 
     * a specific time period is right. (I test the profit since is depending on sales and cost)
     */
    @Test
    public void testGetTotalSalesProfitForSpecifictDateRange(){
       SalesBean saleBean = orderJpaController.getGeneralTotalSales(dynamic.getReportSalesParam().getFrom(), dynamic.getReportSalesParam().getTo());
       assertEquals(dynamic.getReportSalesParam().getTotalProfit().floatValue(),saleBean.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will test if a specific book that is returned from the getSalesALlBooks has
     * the right profit amount (I test the profit since is depending on sales and cost)
     */
    @Test
    public void testGetProfitOfASpecificBookForASpecifitDateRange(){
        List<SalesBean> salesBeans = orderJpaController.getSalesAllBooks(dynamic.getReportSalesParam().getFrom(), dynamic.getReportSalesParam().getTo());
        assertEquals( dynamic.getReportSalesParam().getProfitOfSingleBook().floatValue(),salesBeans.get(dynamic.getReportSalesParam().getBookPlacement()).getProfit().floatValue(), 0.001);
    }
    /**
     * This test will test if the most sold book in a specific time frame is the first book returned on the list
     */
    @Test
    public void testGetTheMostSoldBook(){
        List<SummaryBean> summaryBeans = bookJpaController.getTopSellingBooks(dynamic.getReportBooksParam().getFrom(), dynamic.getReportBooksParam().getTo());
        SummaryBean topSellingBookSummaryBean = summaryBeans.get(0);
        assertEquals(dynamic.getReportBooksParam().getTitleOfMostSoldBook(), topSellingBookSummaryBean.getTitle());
    }
    /**
     * This test will test if the right amount of books that have not sold in a specific time frame is returned
     */
    @Test
    public void testCountZeroSaleBooks(){
        List<SummaryBean> summaryBeans = bookJpaController.getZeroSalesBooks(dynamic.getReportBooksParam().getFrom(), dynamic.getReportBooksParam().getTo());
        assertEquals(dynamic.getReportBooksParam().getTotalBooksThatHaveNotSold(),summaryBeans.size());
    }
    /**
     * This test will check if a specific book in the stock report has the right 
     * amount of profit (I test the profit since is depending on sales and cost)
     */
    @Test
    public void testGetTheRightProfitFromBookInTheStock(){
        List<StockBean> stockBeans = bookJpaController.getStockReport();
        StockBean stockBean = stockBeans.get(dynamic.getReportBooksParam().getPlaceOfSpecificBookInStock());
        assertEquals(dynamic.getReportBooksParam().getTotalProfitOfSpecificBookInStock().floatValue(), stockBean.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will check that a specific book in the stock report
     * has the right list price 
     */
    @Test
    public void testGetListPriceFromBookInStock(){
        List<StockBean> stockBeans = bookJpaController.getStockReport();
        StockBean stockBean = stockBeans.get(dynamic.getReportBooksParam().getPlaceOfSpecificBookInStock());
        assertEquals( dynamic.getReportBooksParam().getListPriceOfBookInStock().floatValue(), stockBean.getListPrice().floatValue(), 0.001);
    }
    /**
     * This test will check that a specific book in the stock report
     * has the right amount of sales to it.
     */
    @Test
    public void testGetAmountOfSalesFromBookInStock(){
        List<StockBean> stockBeans = bookJpaController.getStockReport();
        StockBean stockBean = stockBeans.get(dynamic.getReportBooksParam().getPlaceOfSpecificBookInStock());
        assertEquals(dynamic.getReportBooksParam().getAmountOfSalesOfBookInStock(), stockBean.getAmountOfSales().longValue());
    }
    /**
     * This test will check that the method getBookTotalSales used to display the total sales in the inventory management
     * returns the right amount of total sales of the book (not the count but it is how much money was made from the book without
     * substracting the cost).
     */
    @Test
    public void testTotalSalesOfBook(){
        BigDecimal totalSales = bookJpaController.getBookTotalSales(bookJpaController.findBook(dynamic.getReportBooksParam().getIsbnOfBook()));
        assertEquals(dynamic.getReportBooksParam().getTotalSalesOfBook().floatValue(), totalSales.floatValue(), 0.001); 
    }
    /**
     * This test will check that the method getTopClients returns all of the top clients (all the clients that have
     * bought something during a timeframe).
     */
    @Test
    public void testCountOfTopClients(){
        List<SummaryBean> summaryBeans = clientJpaController.getTopClients(dynamic.getReportClientsParam().getFrom(), dynamic.getReportClientsParam().getTo());
        assertEquals(dynamic.getReportClientsParam().getCountTopClients(), summaryBeans.size());
    }
    /**
     * This test will check that the most recent bought book from the client is the 
     * first one on the list and that it has the right amount of profit (I test the profit since is depending on sales and cost)
     */
    @Test
    public void testProfitMadeOutOfMostRecentBoughtBookOfClient(){
        List<SalesBean> salesBeans = clientJpaController.getTotalSalesAllBooks(dynamic.getReportClientsParam().getClientId(), dynamic.getReportClientsParam().getFrom(), dynamic.getReportClientsParam().getTo());
        SalesBean clientSalesBean = salesBeans.get(0);
        assertEquals(dynamic.getReportClientsParam().getProfitMadeOutOfClientMostRecentBoughtBook().floatValue(), clientSalesBean.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will check that the right amount of total profit made
     * from a specific client will be returned (I test the profit since is depending on sales and cost)
     */
    @Test
    public void testProfitTotalSalesOfClient(){
        SalesBean salesBeans = clientJpaController.getTotalSales(dynamic.getReportClientsParam().getClientId(), dynamic.getReportClientsParam().getFrom(), dynamic.getReportClientsParam().getTo());
        assertEquals(dynamic.getReportClientsParam().getProfitOutOfTotalSales().floatValue(), salesBeans.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will check that the getTopClients method returns the right top client
     * at top of the list.
     */
    @Test
    public void testFullNameOfTopClient(){
        List<SummaryBean> summaryBeans = clientJpaController.getTopClients(dynamic.getReportClientsParam().getFrom(), dynamic.getReportClientsParam().getTo());
        String topClientFullName = summaryBeans.get(0).getTitle();
        assertEquals(dynamic.getReportClientsParam().getFullNameOfTopClient(), topClientFullName);
    }
    
    /**
     * This test will check that a specific publisher has the right amount of sales returned 
     * by the getTotalSales method
     */
    @Test
    public void testProfitPublisherTotalSales(){
        SalesBean salesBean = publisherJpaController.getTotalSales(dynamic.getReportPublisherParam().getPublisherId(), dynamic.getReportPublisherParam().getFrom(), dynamic.getReportPublisherParam().getTo());
        assertEquals(dynamic.getReportPublisherParam().getTotalProfitOfTotalSales().floatValue(), salesBean.getProfit().floatValue(), 00.001);
    }
    /**
     * This test will check that the right amount of books of the publisher sold
     * is returned by the getTotalSalesAllBooks.
     */
    @Test
    public void testCountHowManyBookPublisherSold(){
        List<SalesBean> salesBeans = publisherJpaController.getTotalSalesAllBooks(dynamic.getReportPublisherParam().getPublisherId(), dynamic.getReportPublisherParam().getFrom(), dynamic.getReportPublisherParam().getTo());
        assertEquals(dynamic.getReportPublisherParam().getCountOfBooks(), salesBeans.size());
    }
    /**
     * This test will check that the right amount of profit gets returned 
     * by the most recent order made of a book by the publisher.
     */
    @Test
    public void testProfitMadeOfMostRecentOrderOfPublisherBook(){
        List<SalesBean> salesBeans = publisherJpaController.getTotalSalesAllBooks(dynamic.getReportPublisherParam().getPublisherId(), dynamic.getReportPublisherParam().getFrom(), dynamic.getReportPublisherParam().getTo());
        SalesBean mostRecentSoldBookSalesBean = salesBeans.get(0);
        assertEquals(dynamic.getReportPublisherParam().getTotalProfitForMostRecentOrderSold().floatValue(), mostRecentSoldBookSalesBean.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will check that all of the books of the author that have sold 
     * in a time range are returned;
     */
    @Test
    public void testCountOfAuthorBooksThatHaveSold(){
        List<SalesBean> salesBeans = authorJpaController.getTotalSalesAllBooks(dynamic.getReportAuthorParam().getAuthorId(), dynamic.getReportAuthorParam().getFrom(), dynamic.getReportAuthorParam().getTo());
        assertEquals(dynamic.getReportAuthorParam().getCountHowManyBooksSold(), salesBeans.size());
    }
    /**
     * This test will check that the right amount of total profit made from 
     * a specific authors books is returned
     */
    @Test
    public void testTotalProfitMadeByAuthor(){
        SalesBean salesBean = authorJpaController.getTotalSales(dynamic.getReportAuthorParam().getAuthorId(), dynamic.getReportAuthorParam().getFrom(), dynamic.getReportAuthorParam().getTo());
        assertEquals(dynamic.getReportAuthorParam().getProfitMadeOutOfTotalSales().floatValue(), salesBean.getProfit().floatValue(), 0.001);
    }
    /**
     * This test will check that the right amount of profit is returned 
     * from the most recent bought book by the author.
    */
    @Test
    public void testProfitMadeOutOfMostRecenOrderOfABookByTheAuthor(){
        List<SalesBean> salesBeans = authorJpaController.getTotalSalesAllBooks(dynamic.getReportAuthorParam().getAuthorId(), dynamic.getReportAuthorParam().getFrom(), dynamic.getReportAuthorParam().getTo());
        SalesBean mostRecentBoughtBookSalesBean = salesBeans.get(0);
        assertEquals(dynamic.getReportAuthorParam().getProfitMadeOutMostRecentOrder().floatValue(), mostRecentBoughtBookSalesBean.getProfit().floatValue(), 0.001);
    }
    
        /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    
    @Before 
    public void seedDatabase() throws Exception {
        LOG.info("seedDatabase");
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    } 


}
