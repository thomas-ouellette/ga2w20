package com.ga2w20.arquillian.test;

import com.ga2w20.entities.Format;
import com.ga2w20.entities.Province;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.ProvinceJpaController;
import com.ga2w20.jpacontroller.SurveyJpaController;
import java.io.File;
import java.math.BigDecimal;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ArquillianProvinceTest executes queries that take no parameters
 * in the ProvinceJpaController.
 * 
 * @author Liam Harbec
 */
@RunWith(Arquillian.class)
public class ArquillianProvinceTest {
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianProvinceTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(SurveyJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(Format.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private ProvinceJpaController pjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;
    
    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    
    /**
     * Tests the getProvinceFromName method to make sure it successfully returns
     * a province based on an inputted name
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetProvinceFromName() {
        String provinceName = "Quebec";
        
        Integer id = 11;
        BigDecimal gSTpercentage = BigDecimal.valueOf(5.000);
        BigDecimal pSTpercentage = BigDecimal.valueOf(9.975);
        BigDecimal hSTpercentage = BigDecimal.valueOf(0.000);
        
        Province province = new Province(id, provinceName, gSTpercentage, pSTpercentage, hSTpercentage);
        
        Province result = pjc.getProvinceFromName(provinceName);
        assertEquals(province, result);
    }
    
    
    /**
     * Tests the getProvinceFromName method to make sure it successfully returns
     * null when an invalid province name is inputted
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetProvinceFromName_Invalid() {
        String provinceName = "My Province Is Cool";
        
        Province result = pjc.getProvinceFromName(provinceName);
        assertNull(result);
    }
    
    
    /**
     * Tests the getProvinceFromName method to make sure it successfully returns
     * null when a null String is inputted
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetProvinceFromName_Null() {
        String provinceName = null;
        
        Province result = pjc.getProvinceFromName(provinceName);
        assertNull(result);
    }
    

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }    
}
