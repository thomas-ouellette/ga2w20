package com.ga2w20.arquillian.test;

import com.ga2w20.arquillian.test.specialbookcontroller.params.ArquillianSpecialBookControllerParameterizedTestParam;
import com.ga2w20.arquillian.test.specialbookcontroller.params.SpecialBooksAllGenreParam;
import com.ga2w20.entities.SpecialBook;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.SpecialBookJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for testing the method with parameter from SpecialBookJpaController class
 * 
 * 
 * @author Svitlana Myronova
 */
@RunWith(Arquillian.class)
public class ArquillianSpecialBookControllerParameterizedTest {
    
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianSpecialBookControllerParameterizedTest.class);
    
    /**
     * Deploy
     * 
     * @author Svitlana Myrornova
     * @return webArchive
     */
    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(SpecialBook.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addPackage(SpecialBookJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ArquillianSpecialBookControllerParameterizedTestParam.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }
    
    @Inject
    private SpecialBookJpaController sbjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;
    
    /**
     * ParameterRule creates ArquillianSpecialBookControllerParameterizedTestParam objects then runs the test methods.
     * 
     * @author Svitlana Myrornova
     */
    @Rule
    public ParameterRule rule = new ParameterRule("testParam",            
            new ArquillianSpecialBookControllerParameterizedTestParam(
                    new SpecialBooksAllGenreParam( 20, 20)),
            new ArquillianSpecialBookControllerParameterizedTestParam(
                   new SpecialBooksAllGenreParam( 20, 50)),
            new ArquillianSpecialBookControllerParameterizedTestParam(
                   new SpecialBooksAllGenreParam( 20, 0)),
            new ArquillianSpecialBookControllerParameterizedTestParam(
                    new SpecialBooksAllGenreParam( 1, 1)),
            new ArquillianSpecialBookControllerParameterizedTestParam(
                    new SpecialBooksAllGenreParam( 10, 10))
    );

    private ArquillianSpecialBookControllerParameterizedTestParam testParam;
    
    /**
     * Test for getSpecialBooksAllGenreActive(int maxItems) function from SpecialBookJpaController
     * 
     * @author Svitlana Myrornova
     */
    @Test
    public void FindSpecialBooksAllGenre() {
        LOG.info("FindSpecialBooksAllGenre");       
        
        List<SpecialBook> specialBooks = sbjc.getSpecialBooksAllGenreActive(testParam.specialBooksAllGenreParam.maxItems);          
        
        int countEval = specialBooks.size();
        int countExpec = testParam.specialBooksAllGenreParam.count;
        LOG.debug(" evaluated result - " + countEval + " expected result - " + countExpec);
        assertEquals(countExpec, countEval);
        
    }
    
        /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before 
    public void seedDatabase() throws Exception {
        LOG.info("seedDatabase");
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }    
}
