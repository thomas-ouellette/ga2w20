package com.ga2w20.arquillian.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.sql.DataSource;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.slf4j.LoggerFactory;

/**
 * This class is helper class that contains methods for restoring the database
 * to a known state before testing.
 *
 * @author Svitlana Myronova, Ken Fogel
 */
public class TestUtil {

    private final static org.slf4j.Logger LOG = LoggerFactory.getLogger(TestUtil.class);

    DataSource ds;
    UserTransaction utx;
    EntityManager em;

    /**
     * Constructor for TestUtil
     *
     * @author Svitlana Myronova
     * @param dataSource
     * @param utx
     * @param em
     */
    public TestUtil(DataSource dataSource, UserTransaction utx, EntityManager em) {
        this.ds = dataSource;
        this.utx = utx;
        this.em = em;
    }

    /**
     * This method splits the createDatabase.sql and, using this data, restores 
     * the database to a known state before testing.
     * 
     * @author Svitlana Myronova
     */
    public void seedDatabase() {
        clearEntityManager();
        final String seedDataScript = loadAsString("createDatabase.sql");
        try (Connection connection = ds.getConnection()) {
            for (String statement : splitStatements(new StringReader(seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
        clearEntityManager();
    }

    private void clearEntityManager() {
        try {
            utx.begin();
            em.flush();
            em.clear();
            utx.commit();
            //Now we need to clear Second Level cache in EntityManagerFactory
            em.getEntityManagerFactory().getCache().evictAll();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            throw new RuntimeException("Failed seeding database", ex);
        }
    }

    /**
     * The following methods support the seedDatabse method
     * 
     * @author Svitlana Myronova
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path)) {
            return new Scanner(inputStream).useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    /**
     * Split statements
     *
     * @author Svitlana Myronova
     * @param reader
     * @param statementDelimiter
     * @return list of strings
     */
    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.stripTrailing();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    /**
     * Check if it is a comment 
     *
     * @author Svitlana Myronova
     * @param line
     * @return true or false 
     */
    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
