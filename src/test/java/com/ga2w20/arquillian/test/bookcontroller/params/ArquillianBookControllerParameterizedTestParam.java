package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that generates parameters for ArquillianBookControllerParameterizedTest.java
 * 
 * @author Svitlana Myronova
 */
public class ArquillianBookControllerParameterizedTestParam {
    /**
     * this attribute for testing:
     * getBooksLikePublisherCount(String publisher) that returns int
     * getBooksLikePublisher(String publisher, int firstItem, int maxItems) that returns List<Book> 
     */
    public final BooksLikePublisherParam booksLikePublisherParam;
    
    /**
     * this attribute for testing:
     * getBooksLikeAuthorCount(String author) that returns int
     * getBooksLikeAuthor(String author, int firstItem, int maxItems) that returns List<Book>
     */
    public final BooksLikeAuthorParam booksLikeAuthorParam;
    
    /**
     * this attribute for testing:
     * getBooksLikeTitleCount(String title) that returns int
     * getBooksLikeTitle(String title, int firstItem, int maxItems) that returns List<Book>
     */
    public final BooksLikeTitleParam booksLikeTitleParam;
    
    /**
     * this attribute for testing:
     * getBooksWithGenreCount(Integer genreId) that returns int
     * getTopSellersWithGenre(int genreId, int firstItem, int maxItems) that returns List<Book>
     */
    public final TopSellersWithGenreParam topSellersWithGenreParam;
    
    // this attribute for testing getTopSellersWithAllGenres(int firstItem, int maxItems) that returns List<Book>
    public final TopSellersWithAllGenresParam topSellersWithAllGenresParam;
    
    // this attribute for testing getBooksWithGenreActive(int genreId, int maxItems) that returns List<Book>
    public final BooksWithGenreParam booksWithGenreParam;
    
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianBookControllerParameterizedTestParam.class);
    
    /**
     * Constructor for ArquillianBookControllerParameterizedTestParam
     *
     * @author Svitlana Myronova
     * @param booksLikePublisherParam
     * @param booksLikeAuthorParam
     * @param booksLikeTitleParam
     * @param topSellersWithGenreParam
     * @param topSellersWithAllGenresParam
     * @param booksWithGenreParam
     */
    public ArquillianBookControllerParameterizedTestParam(BooksLikePublisherParam booksLikePublisherParam,
                                                          BooksLikeAuthorParam booksLikeAuthorParam,
                                                          BooksLikeTitleParam booksLikeTitleParam,
                                                          TopSellersWithGenreParam topSellersWithGenreParam,
                                                          TopSellersWithAllGenresParam topSellersWithAllGenresParam,
                                                          BooksWithGenreParam booksWithGenreParam) 
    {
        // a lot of data in logs
        //LOG.info("creating ArquillianBookControllerParameterizedTestParam");
        this.booksLikePublisherParam = booksLikePublisherParam;
        this.booksLikeAuthorParam = booksLikeAuthorParam;
        this.booksLikeTitleParam = booksLikeTitleParam;
        this.topSellersWithGenreParam = topSellersWithGenreParam;
        this.topSellersWithAllGenresParam = topSellersWithAllGenresParam;
        this.booksWithGenreParam = booksWithGenreParam;
    }
}
