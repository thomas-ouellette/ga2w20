package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for two functions from BookJpaController: getBooksLikeAuthorCount(String author), getBooksLikeAuthor(String author, int firstItem, int maxItems)
 * 
 * @author Svitlana Myronova
 */
public class BooksLikeAuthorParam {
     
    public final String author;
    public final int count;
    public final int countInterval;
    
    public final int firstItem;
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(BooksLikeAuthorParam.class);
    
    /**
     * Constructor for BooksLikeAuthorParam
     * 
     * @author Svitlana Myronova
     * @param author
     * @param count
     * @param countInterval
     * @param firstItem
     * @param maxItems
     */
    public BooksLikeAuthorParam(String author, int count, int countInterval, int firstItem, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating BooksLikeAuthorParam");
        this.author = author;
        this.count = count;
        this.countInterval = countInterval;
        this.firstItem = firstItem;
        this.maxItems = maxItems;
    }
    
}
