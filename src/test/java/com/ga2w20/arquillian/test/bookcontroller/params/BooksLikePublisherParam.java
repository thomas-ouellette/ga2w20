package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for two functions from BookJpaController: getBooksLikePublisherCount(String publisher), getBooksLikePublisher(String publisher, int firstItem, int maxItems) 
 * 
 * @author Svitlana Myronova
 */
public class BooksLikePublisherParam {

    public final String publisher;
    public final int count;
    public final int countInterval;
    
    public final int firstItem;
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(BooksLikePublisherParam.class);
    
    /**
     * Constructor for BooksLikePublisherParam
     *
     * @author Svitlana Myronova
     * @param publisher
     * @param count
     * @param countInterval
     * @param firstItem
     * @param maxItems
     */
    public BooksLikePublisherParam(String publisher, int count, int countInterval, int firstItem, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating BooksLikePublisherParam");
        this.publisher = publisher;
        this.count = count;
        this.countInterval = countInterval;
        this.firstItem = firstItem;
        this.maxItems = maxItems;
    }
    
}
