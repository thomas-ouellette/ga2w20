package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for two functions from BookJpaController: getBooksLikeTitleCount(String title), getBooksLikeTitle(String title, int firstItem, int maxItems)
 * 
 * @author Svitlana Myronova
 */
public class BooksLikeTitleParam {
    
    public final String title;
    public final int count;
    public final int countInterval;
    
    public final int firstItem;
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(BooksLikeTitleParam.class);
    
    /**
     * Constructor for BooksLikeTitleParam
     *
     * @author Svitlana Myronova
     * @param title
     * @param count
     * @param countInterval
     * @param firstItem
     * @param maxItems
     */
    public BooksLikeTitleParam(String title, int count, int countInterval, int firstItem, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating BooksLikeTitleParam");
        this.title = title;
        this.count = count;
        this.countInterval = countInterval;
        this.firstItem = firstItem;
        this.maxItems = maxItems;
    }
}
