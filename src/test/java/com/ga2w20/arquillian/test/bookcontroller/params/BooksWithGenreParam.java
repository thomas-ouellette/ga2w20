package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for the getBooksWithGenreActive(int genreId, int maxItems) function from BookJpaController
 * 
 * @author Svitlana Myronova
 */
public class BooksWithGenreParam {
    public final Integer genreId;
    public final int count;
    
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(BooksWithGenreParam.class);
    
    /**
     * Constructor for BooksWithGenreParam
     *
     * @author Svitlana Myronova
     * @param genreId
     * @param count
     * @param maxItems
     */
    public BooksWithGenreParam(Integer genreId, int count, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating BooksWithGenreParam");
        this.genreId = genreId;
        this.count = count;
        this.maxItems = maxItems;
    }
}
