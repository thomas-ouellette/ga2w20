package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for the getTopSellersWithAllGenres(int firstItem, int maxItems) function from BookJpaController
 * 
 * @author Svitlana Myronova
 */
public class TopSellersWithAllGenresParam {
    public final int count;
    
    public final int firstItem;
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(TopSellersWithAllGenresParam.class);
    
    /**
     * Constructor for TopSellersWithAllGenresParam
     * 
     * @author Svitlana Myronova
     * @param count
     * @param firstItem
     * @param maxItems
     */
    public TopSellersWithAllGenresParam(int count, int firstItem, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating TopSellersWithAllGenresParam");
        this.count = count;
        this.firstItem = firstItem;
        this.maxItems = maxItems;
    }
}
