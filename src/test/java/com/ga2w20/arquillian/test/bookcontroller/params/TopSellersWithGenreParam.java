package com.ga2w20.arquillian.test.bookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for two functions from BookJpaController: getBooksWithGenreCount(Integer genreId), getTopSellersWithGenre(int genreId, int firstItem, int maxItems)
 * 
 * @author Svitlana Myronova
 */
public class TopSellersWithGenreParam {
    public final Integer genreId;
    public final int count;
    public final int countInterval;
    
    public final int firstItem;
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(TopSellersWithGenreParam.class);
    
    /**
     * Constructor for TopSellersWithGenreParam
     * 
     * @author Svitlana Myronova
     * @param genreId
     * @param count
     * @param countInterval
     * @param firstItem
     * @param maxItems
     */
    public TopSellersWithGenreParam(Integer genreId, int count, int countInterval, int firstItem, int maxItems)
    {
        // a lot of data in logs
        //LOG.info("creating TopSellersWithGenreParam");
        this.genreId = genreId;
        this.count = count;
        this.countInterval = countInterval;
        this.firstItem = firstItem;
        this.maxItems = maxItems;
    }
}
