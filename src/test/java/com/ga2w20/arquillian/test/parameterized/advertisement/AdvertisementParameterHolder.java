package com.ga2w20.arquillian.test.parameterized.advertisement;

import java.util.Date;

/**
 * The AdvertisementParameterHolder represents all necessary input to be used as
 * test data for the ParameterRule in ParameterizedAdvertisementHolderTest
 * 
 * @author Liam Harbec
 */
public class AdvertisementParameterHolder {
     
    public final Date currentDate;
    public final boolean isExpected;
    
    public AdvertisementParameterHolder(Date currentDate, boolean isExpected)
    {
        this.currentDate = currentDate;
        this.isExpected = isExpected;
    }
    
}
