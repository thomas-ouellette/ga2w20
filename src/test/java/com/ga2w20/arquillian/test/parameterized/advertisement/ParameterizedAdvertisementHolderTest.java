package com.ga2w20.arquillian.test.parameterized.advertisement;

import com.ga2w20.arquillian.test.ParameterRule;
import com.ga2w20.arquillian.test.TestUtil;
import com.ga2w20.entities.Advertisement;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.AdvertisementJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ParameterizedAdvertisementHolderTest executes queries in the
 * AdvertisementJpaController using multiple sets of data as input to represent
 * a single Advertisement.
 *
 * @author Liam Harbec
 */
@RunWith(Arquillian.class)
public class ParameterizedAdvertisementHolderTest {

    private final static Logger LOG = LoggerFactory.getLogger(ParameterizedAdvertisementHolderTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Advertisement.class.getPackage())
                .addPackage(AdvertisementJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ParameterizedAdvertisementHolderTest.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private AdvertisementJpaController ajc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    @Rule
    public ParameterRule rule = new ParameterRule("testParam",    
            /***************************************************/
            /*            Basic advertisement dates            */
            /***************************************************/
            // Before the ads are active
            new AdvertisementParameterHolder(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime(), false),
            // The First day an ad is active
            new AdvertisementParameterHolder(new GregorianCalendar(2020, Calendar.MARCH, 1).getTime(), true),
            // While the ad is active
            new AdvertisementParameterHolder(new GregorianCalendar(2021, Calendar.JANUARY, 1).getTime(), true),
            new AdvertisementParameterHolder(new GregorianCalendar(2022, Calendar.MARCH, 3).getTime(), true),
            // After the ads are active
            new AdvertisementParameterHolder(new GregorianCalendar(2023, Calendar.APRIL, 1).getTime(), false)
    );

    private AdvertisementParameterHolder testParam;
    
    
    /**
     * Tests the getRandomAdvertisement method to make sure an advertisement is
     * successfully retrieved and is between the start and end date, and is active.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetRandomAdvertisement_AdActive() {
        Advertisement ad = ajc.getRandomAdvertisement(testParam.currentDate);
        boolean isAdNotNull = ad != null;
        assertTrue(isAdNotNull == testParam.isExpected);
    }
    

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
