package com.ga2w20.arquillian.test.parameterized.book;

/**
 * The BookParameterHolder represents all necessary input to be used as
 * test data for the ParameterRule in ParameterizedBookHolderTest
 * 
 * @author Liam Harbec
 */
public class BookParameterHolder {
     
    public final Long isbn;
    public final String author;
    public final String genre;
    public final int expectedAuthorResults;
    public final int expectedGenreResults;
    public final double expectedSale;
    
    public BookParameterHolder(Long isbn, String author, String genre,
            int expectedAuthorResults, int expectedGenreResults, double expectedSale)
    {
        this.isbn = isbn;
        this.author = author;
        this.genre = genre;
        this.expectedAuthorResults = expectedAuthorResults;
        this.expectedGenreResults = expectedGenreResults;
        this.expectedSale = expectedSale;
    }
    
}
