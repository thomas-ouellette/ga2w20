package com.ga2w20.arquillian.test.parameterized.book;

import com.ga2w20.arquillian.test.ParameterRule;
import com.ga2w20.arquillian.test.TestUtil;
import com.ga2w20.entities.Book;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.BookJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ParameterizedBookHolderTest executes queries in the BookJpaController
 * using multiple sets of data as input to represent a single Book.
 *
 * @author Liam Harbec
 */
@RunWith(Arquillian.class)
public class ParameterizedBookHolderTest {

    private final static Logger LOG = LoggerFactory.getLogger(ParameterizedBookHolderTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Book.class.getPackage())
                .addPackage(BookJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ParameterizedBookHolderTest.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private BookJpaController bjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    @Rule
    public ParameterRule rule = new ParameterRule("testParam",    
            /***************************************************/
            /*           A basic book of each genre            */
            /***************************************************/
            new BookParameterHolder(9780132350884L, "Robert C. Martin", "Computer Science", 0, 19, 0),
            new BookParameterHolder(9780061138829L, "Archie Brown", "History", 0, 19, 20),
            // Multiple books by the author
            new BookParameterHolder(9780099577282L, "George Eliot", "Romance", 1, 19, 0),
            new BookParameterHolder(9780062018090L, "Sofia Stril-Rever", "Biography", 0, 19, 0),
            // Multiple books by the author
            new BookParameterHolder(9780006913368L, "Carolyn Keene", "Mystery", 3, 19, 0),
            
            /***************************************************/
            /*   An ISBN that does not exist in the database   */
            /***************************************************/
            new BookParameterHolder(1234567890L, "George Eliot", "Romance", 2, 20, 0),
            
            /***************************************************/
            /*   A genre that does not exist in the database   */
            /***************************************************/
            new BookParameterHolder(9780099577282L, "George Eliot", "Invalid Genre", 1, 0, 0),
            
            /***************************************************/
            /*  An author that does not exist in the database  */
            /***************************************************/
            new BookParameterHolder(9780099577282L, "Pablo Diego José Francisco de Paula Juan Nepomuceno María de los Remedios Cipriano de la Santísima Trinidad Ruiz y Picasso", "Romance", 0, 19, 0),
            
            /***************************************************/
            /*A book with several authors, choosing one of them*/
            /***************************************************/
            new BookParameterHolder(9781680502688L, "Paul Gries", "Computer Science", 0, 19, 0),
            
            /***************************************************/
            /*           A book that will be on sale           */
            /***************************************************/
            new BookParameterHolder(9780062073570L, "Agatha Christie", "Mystery", 10, 19, 40)
    );

    private BookParameterHolder testParam;
    
    
    /**
     * Tests the getBooksWithGenre method to make sure it successfully returns a
     * list of books of the same genre as the inputted isbn.
     * It tests based on the returned amount of books.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetBooksWithGenre() {
        List<Book> results = bjc.getBooksWithGenre(testParam.genre,
                testParam.isbn);
        
        assertEquals(testParam.expectedGenreResults, results.size());
    }
    
    
    /**
     * Tests the getBooksWithAuthor method to make sure it successfully returns
     * all books of an inputted author.
     * It tests based on the returned amount of books.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetBooksWithAuthor() {
        List<Book> results = bjc.getBooksWithAuthor(testParam.author, 
                testParam.isbn);
        assertEquals(testParam.expectedAuthorResults, results.size());
    }
    
    
    /**
     * Tests the getSpecialBook method to make sure it successfully returns the
     * sale percentage of a book that is on sale.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetSpecialBook() {
        Calendar testDateCalendar = new GregorianCalendar(2020, Calendar.MAY, 5);
        Date testDate = testDateCalendar.getTime();
        
        BigDecimal result = bjc.getSpecialBook(testParam.isbn, testDate);
        assertEquals(BigDecimal.valueOf(testParam.expectedSale).setScale(0), result.setScale(0));
    }

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
