package com.ga2w20.arquillian.test.parameterized.client;

/**
 * The ClientParameterHolder represents all necessary input to be used as
 * test data for the ParameterRule in ParameterizedClientHolderTest
 * 
 * @author Liam Harbec
 */
public class ClientParameterHolder {
     
    public final String email;
    public final Integer id;
    
    public ClientParameterHolder(String email, Integer id)
    {
        this.email = email;
        this.id = id;
    }
    
}
