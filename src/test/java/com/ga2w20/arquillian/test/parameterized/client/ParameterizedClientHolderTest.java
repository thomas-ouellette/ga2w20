package com.ga2w20.arquillian.test.parameterized.client;

import com.ga2w20.arquillian.test.ParameterRule;
import com.ga2w20.arquillian.test.TestUtil;
import com.ga2w20.entities.Client;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.ClientJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ParameterizedClientHolderTest executes queries in the ClientJpaController
 * using multiple sets of data as input to represent a single Client.
 *
 * @author Liam Harbec
 */
@RunWith(Arquillian.class)
public class ParameterizedClientHolderTest {

    private final static Logger LOG = LoggerFactory.getLogger(ParameterizedClientHolderTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(Client.class.getPackage())
                .addPackage(ClientJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ParameterizedClientHolderTest.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private ClientJpaController cjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    @Rule
    public ParameterRule rule = new ParameterRule("testParam",    
            /***************************************/
            /*            Basic clients            */
            /************************** ************/
            new ClientParameterHolder("cst.send@gmail.com", 2),
            new ClientParameterHolder("Pit@gmail.com", 4),
            new ClientParameterHolder("NotARealEmailInDatabase@fakedomain.ok", -1),
            new ClientParameterHolder("Glass@gmail.com", 7),
            new ClientParameterHolder(null, -1) 
    );

    private ClientParameterHolder testParam;
    
    
    /**
     * Tests the findClientFromLogin method to make sure it successfully returns
     * the corresponding Client object for the inputted email, based on its id.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testFindClientFromLogin_Id() {
        Client result = cjc.findClientFromLogin(testParam.email);
        assertEquals(testParam.id, result.getId());
    }
    

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
