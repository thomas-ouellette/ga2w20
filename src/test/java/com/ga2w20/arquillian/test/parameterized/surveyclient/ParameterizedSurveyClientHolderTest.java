package com.ga2w20.arquillian.test.parameterized.surveyclient;

import com.ga2w20.arquillian.test.ParameterRule;
import com.ga2w20.arquillian.test.TestUtil;
import com.ga2w20.entities.SurveyClient;
import com.ga2w20.exceptions.RollbackFailureException;
import com.ga2w20.jpacontroller.SurveyClientJpaController;
import com.ga2w20.querybeans.SalesBean;
import java.io.File;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import javax.transaction.UserTransaction;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ParameterizedSurveyClientHolderTest executes queries in the
 * SurveyClientJpaController using multiple sets of data as input to represent
 * a single SurveyClient.
 *
 * @author Liam Harbec
 */
@RunWith(Arquillian.class)
public class ParameterizedSurveyClientHolderTest {

    private final static Logger LOG = LoggerFactory.getLogger(ParameterizedSurveyClientHolderTest.class);

    @Deployment
    public static WebArchive deploy() {

        // Use an alternative to the JUnit assert library called AssertJ
        // Need to reference MySQL driver as it is not part of either
        // embedded or remote
        final File[] dependencies = Maven
                .resolver()
                .loadPomFromFile("pom.xml")
                .resolve("mysql:mysql-connector-java",
                        "org.assertj:assertj-core",
                        "org.slf4j:slf4j-api",
                        "org.apache.logging.log4j:log4j-slf4j-impl",
                        "org.apache.logging.log4j:log4j-web"
                ).withTransitivity()
                .asFile();

        // The SQL script to create the database is in src/test/resources
        final WebArchive webArchive = ShrinkWrap.create(WebArchive.class, "test.war")
                .setWebXML(new File("src/main/webapp/WEB-INF/web.xml"))
                .addPackage(SurveyClient.class.getPackage())
                .addPackage(SurveyClientJpaController.class.getPackage())
                .addPackage(RollbackFailureException.class.getPackage())
                .addPackage(ParameterizedSurveyClientHolderTest.class.getPackage())
                .addPackage(ParameterRule.class.getPackage())
                .addPackage(SalesBean.class.getPackage())
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsWebInfResource(new File("src/main/webapp/WEB-INF/payara-resources.xml"), "payara-resources.xml")
                .addAsResource(new File("src/main/resources/META-INF/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource(new File("src/main/resources/log4j2.xml"), "log4j2.xml")
                .addAsResource("createDatabase.sql")
                .addAsLibraries(dependencies)
                .addClass(TestUtil.class);

        return webArchive;
    }

    @Inject
    private SurveyClientJpaController scjc;

    @Resource(lookup = "java:app/jdbc/ga2w20")
    private DataSource ds;

    @PersistenceContext
    private EntityManager em;

    @Resource
    private UserTransaction utx;

    @Rule
    public ParameterRule rule = new ParameterRule("testParam",    
            /***************************************************/
            /*           Basic survey client results           */
            /***************************************************/
            new SurveyClientParameterHolder(1, 0, 1, 1, 1),
            new SurveyClientParameterHolder(2, 2, 0, 0, 1),
            new SurveyClientParameterHolder(3, 2, 0, 1, 0),
            new SurveyClientParameterHolder(7, 0, 1, 1, 0),
            new SurveyClientParameterHolder(-1, 0, 0, 0, 0)
    );

    private SurveyClientParameterHolder testParam;
    
    
    /**
     * Tests the getSurveyResults method to make sure it successfully returns
     * the total of votes for the first result in a particular survey.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetSurveyResults_Result1() {
        List<Integer> results = scjc.getSurveyResults(testParam.surveyId);
        
        assertEquals(testParam.result1, results.get(0));
    }
    
    
    /**
     * Tests the getSurveyResults method to make sure it successfully returns
     * the total of votes for the second result in a particular survey.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetSurveyResults_Result2() {
        List<Integer> results = scjc.getSurveyResults(testParam.surveyId);
        
        assertEquals(testParam.result2, results.get(1));
    }
    
    
    /**
     * Tests the getSurveyResults method to make sure it successfully returns
     * the total of votes for the third result in a particular survey.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetSurveyResults_Result3() {
        List<Integer> results = scjc.getSurveyResults(testParam.surveyId);
        
        assertEquals(testParam.result3, results.get(2));
    }
    
    
    /**
     * Tests the getSurveyResults method to make sure it successfully returns
     * the total of votes for the fourth result in a particular survey.
     * 
     * @author Liam Harbec
     */
    @Test
    public void testGetSurveyResults_Result4() {
        List<Integer> results = scjc.getSurveyResults(testParam.surveyId);
        
        assertEquals(testParam.result4, results.get(3));
    }
    

    /**
     * Restore the database to a known state before testing. This is important
     * if the test is destructive. This routine is courtesy of Bartosz Majsak
     * who also solved my Arquillian remote server problem
     */
    @Before
    public void seedDatabase() {
        TestUtil testUtil = new TestUtil(ds, utx, em);
        testUtil.seedDatabase();
    }
}
