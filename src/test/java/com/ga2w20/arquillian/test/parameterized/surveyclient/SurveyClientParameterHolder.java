package com.ga2w20.arquillian.test.parameterized.surveyclient;

/**
 * The SurveyClientParameterHolder represents all necessary input to be used as
 * test data for the ParameterRule in ParameterizedSurveyClientHolderTest
 * 
 * @author Liam Harbec
 */
public class SurveyClientParameterHolder {
     
    public final Integer surveyId;
    public final Integer result1;
    public final Integer result2;
    public final Integer result3;
    public final Integer result4;
    
    public SurveyClientParameterHolder(Integer surveyId, Integer result1,
            Integer result2, Integer result3, Integer result4)
    {
        this.surveyId = surveyId;
        this.result1 = result1;
        this.result2 = result2;
        this.result3 = result3;
        this.result4 = result4;
    }
    
}
