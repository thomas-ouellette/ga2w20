package com.ga2w20.arquillian.test.reports.params;

/**
 *This is the test bean that will hold all the beans for all the different 
 * kinds of test for the reports on the managers side
 * @author cadin
 */
public class ArquillianReportsTestsParam {
    private ReportSalesParam reportSalesParam;
    private ReportBooksParam reportBooksParam;
    private ReportClientsParam reportClientsParam;
    private ReportPublisherParam reportPublisherParam;
    private ReportAuthorParam reportAuthorParam;
    
    /**
     * Constructor initializes all of the report beans that this bean holds
     * @param reportSalesParam
     * @param reportBooksParam
     * @param reportClientsParam
     * @param reportPublisherParam
     * @param reportAuthorParam 
     */
    public ArquillianReportsTestsParam(ReportSalesParam reportSalesParam, ReportBooksParam reportBooksParam, ReportClientsParam reportClientsParam, ReportPublisherParam reportPublisherParam, ReportAuthorParam reportAuthorParam){
        this.reportSalesParam = reportSalesParam;
        this.reportBooksParam = reportBooksParam;
        this.reportClientsParam = reportClientsParam;
        this.reportPublisherParam = reportPublisherParam;
        this.reportAuthorParam = reportAuthorParam;
        
    }

    /**
     * @return the reportSalesParam
     */
    public ReportSalesParam getReportSalesParam() {
        return reportSalesParam;
    }

    /**
     * @param reportSalesParam the reportSalesParam to set
     */
    public void setReportSalesParam(ReportSalesParam reportSalesParam) {
        this.reportSalesParam = reportSalesParam;
    }

    /**
     * @return the reportBooksParam
     */
    public ReportBooksParam getReportBooksParam() {
        return reportBooksParam;
    }

    /**
     * @param reportBooksParam the reportBooksParam to set
     */
    public void setReportBooksParam(ReportBooksParam reportBooksParam) {
        this.reportBooksParam = reportBooksParam;
    }

    /**
     * @return the reportClientsParam
     */
    public ReportClientsParam getReportClientsParam() {
        return reportClientsParam;
    }

    /**
     * @param reportClientsParam the reportClientsParam to set
     */
    public void setReportClientsParam(ReportClientsParam reportClientsParam) {
        this.reportClientsParam = reportClientsParam;
    }

    /**
     * @return the reportPublisherParam
     */
    public ReportPublisherParam getReportPublisherParam() {
        return reportPublisherParam;
    }

    /**
     * @param reportPublisherParam the reportPublisherParam to set
     */
    public void setReportPublisherParam(ReportPublisherParam reportPublisherParam) {
        this.reportPublisherParam = reportPublisherParam;
    }

    /**
     * @return the reportAuthorParam
     */
    public ReportAuthorParam getReportAuthorParam() {
        return reportAuthorParam;
    }

    /**
     * @param reportAuthorParam the reportAuthorParam to set
     */
    public void setReportAuthorParam(ReportAuthorParam reportAuthorParam) {
        this.reportAuthorParam = reportAuthorParam;
    }
}
