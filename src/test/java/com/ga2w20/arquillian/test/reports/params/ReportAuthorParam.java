package com.ga2w20.arquillian.test.reports.params;

import java.math.BigDecimal;
import java.util.Date;

/**
 *The bean that holds the parameters for all the author reports test
 * @author cadin
 */
public class ReportAuthorParam {
   private Date from;
   private Date to;
   private int authorId;
   private int countHowManyBooksSold;
   private BigDecimal profitMadeOutOfTotalSales;
   private BigDecimal profitMadeOutMostRecentOrder;
   
    /**
     *Constructor initializes all of the properties of this test bean
     * @param from
     * @param to
     * @param authorId
     * @param countHowManyBooksSold
     * @param profitMadeOutOfTotalSales
     * @param profitMadeOutMostRecentBookSold
     */
    public ReportAuthorParam(Date from, Date to, int authorId, int countHowManyBooksSold, BigDecimal profitMadeOutOfTotalSales, BigDecimal profitMadeOutMostRecentBookSold ){
       this.from = from;
       this.to = to;
       this.authorId = authorId;
       this.countHowManyBooksSold = countHowManyBooksSold;
       this.profitMadeOutMostRecentOrder = profitMadeOutMostRecentBookSold ;
       this.profitMadeOutOfTotalSales = profitMadeOutOfTotalSales;
   }

    /**
     * @return the profitMadeOutOfTotalSales
     */
    public BigDecimal getProfitMadeOutOfTotalSales() {
        return profitMadeOutOfTotalSales;
    }

    /**
     * @param profitMadeOutOfTotalSales the profitMadeOutOfTotalSales to set
     */
    public void setProfitMadeOutOfTotalSales(BigDecimal profitMadeOutOfTotalSales) {
        this.profitMadeOutOfTotalSales = profitMadeOutOfTotalSales;
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the authorId
     */
    public int getAuthorId() {
        return authorId;
    }

    /**
     * @param authorId the authorId to set
     */
    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    /**
     * @return the countHowManyBooksSold
     */
    public int getCountHowManyBooksSold() {
        return countHowManyBooksSold;
    }

    /**
     * @param countHowManyBooksSold the countHowManyBooksSold to set
     */
    public void setCountHowManyBooksSold(int countHowManyBooksSold) {
        this.countHowManyBooksSold = countHowManyBooksSold;
    }

    /**
     * @return the profitMadeOutMostRecentOrder
     */
    public BigDecimal getProfitMadeOutMostRecentOrder() {
        return profitMadeOutMostRecentOrder;
    }

    /**
     * @param profitMadeOutMostRecentOrder the profitMadeOutMostRecentOrder to set
     */
    public void setProfitMadeOutMostRecentOrder(BigDecimal profitMadeOutMostRecentOrder) {
        this.profitMadeOutMostRecentOrder = profitMadeOutMostRecentOrder;
    }
}
