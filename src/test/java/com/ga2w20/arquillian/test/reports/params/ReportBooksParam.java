package com.ga2w20.arquillian.test.reports.params;

import java.math.BigDecimal;
import java.util.Date;

/**
*The bean that holds the parameters for all the book reports test
 * @author cadin
 */
public class ReportBooksParam {
    private Date from;
    private Date to;
    private String titleOfMostSoldBook;
    private int totalBooksThatHaveNotSold;
    private int placeOfSpecificBookInStock;
    private BigDecimal totalProfitOfSpecificBookInStock;
    private BigDecimal listPriceOfBookInStock;
    private long amountOfSalesOfBookInStock;
    private long isbnOfBook;
    private BigDecimal totalSalesOfBook;

    /**
     *Constructor initializes all of the properties of this test bean
     * @param from
     * @param to
     * @param titleOfMostSoldBook
     * @param totalBooksThatHaveNotSold
     * @param placeOfSpecificBookInStock
     * @param totalProfitOfSpecificBookInStock
     * @param listPriceOfBookInStock
     * @param amountOfSalesOfBookInStock
     * @param totalSalesOfBook
     * @param isbnOfBook
     */
    public ReportBooksParam(Date from, Date to, String titleOfMostSoldBook, int totalBooksThatHaveNotSold, int placeOfSpecificBookInStock, BigDecimal totalProfitOfSpecificBookInStock, BigDecimal listPriceOfBookInStock, long amountOfSalesOfBookInStock, BigDecimal totalSalesOfBook, long isbnOfBook){
        this.from = from;
        this.to = to;
        this.titleOfMostSoldBook = titleOfMostSoldBook;
        this.totalBooksThatHaveNotSold = totalBooksThatHaveNotSold;
        this.placeOfSpecificBookInStock = placeOfSpecificBookInStock;
        this.totalProfitOfSpecificBookInStock = totalProfitOfSpecificBookInStock;
        this.listPriceOfBookInStock = listPriceOfBookInStock;
        this.amountOfSalesOfBookInStock = amountOfSalesOfBookInStock;
        this.totalSalesOfBook = totalSalesOfBook;
        this.isbnOfBook = isbnOfBook;
    }
    
    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the titleOfMostSoldBook
     */
    public String getTitleOfMostSoldBook() {
        return titleOfMostSoldBook;
    }

    /**
     * @param titleOfMostSoldBook the titleOfMostSoldBook to set
     */
    public void setTitleOfMostSoldBook(String titleOfMostSoldBook) {
        this.titleOfMostSoldBook = titleOfMostSoldBook;
    }

    /**
     * @return the totalBooksThatHaveNotSold
     */
    public int getTotalBooksThatHaveNotSold() {
        return totalBooksThatHaveNotSold;
    }

    /**
     * @param totalBooksThatHaveNotSold the totalBooksThatHaveNotSold to set
     */
    public void setTotalBooksThatHaveNotSold(int totalBooksThatHaveNotSold) {
        this.totalBooksThatHaveNotSold = totalBooksThatHaveNotSold;
    }

    /**
     * @return the placeOfSpecificBookInStock
     */
    public int getPlaceOfSpecificBookInStock() {
        return placeOfSpecificBookInStock;
    }

    /**
     * @param placeOfSpecificBookInStock the placeOfSpecificBookInStock to set
     */
    public void setPlaceOfSpecificBookInStock(int placeOfSpecificBookInStock) {
        this.placeOfSpecificBookInStock = placeOfSpecificBookInStock;
    }

    /**
     * @return the totalProfitOfSpecificBookInStock
     */
    public BigDecimal getTotalProfitOfSpecificBookInStock() {
        return totalProfitOfSpecificBookInStock;
    }

    /**
     * @param totalProfitOfSpecificBookInStock the totalProfitOfSpecificBookInStock to set
     */
    public void setTotalProfitOfSpecificBookInStock(BigDecimal totalProfitOfSpecificBookInStock) {
        this.totalProfitOfSpecificBookInStock = totalProfitOfSpecificBookInStock;
    }

    /**
     * @return the listPriceOfBookInStock
     */
    public BigDecimal getListPriceOfBookInStock() {
        return listPriceOfBookInStock;
    }

    /**
     * @param listPriceOfBookInStock the listPriceOfBookInStock to set
     */
    public void setListPriceOfBookInStock(BigDecimal listPriceOfBookInStock) {
        this.listPriceOfBookInStock = listPriceOfBookInStock;
    }

    /**
     * @return the amountOfSalesOfBookInStock
     */
    public long getAmountOfSalesOfBookInStock() {
        return amountOfSalesOfBookInStock;
    }

    /**
     * @param amountOfSalesOfBookInStock the amountOfSalesOfBookInStock to set
     */
    public void setAmountOfSalesOfBookInStock(long amountOfSalesOfBookInStock) {
        this.amountOfSalesOfBookInStock = amountOfSalesOfBookInStock;
    }

    /**
     * @return the totalSalesOfBook
     */
    public BigDecimal getTotalSalesOfBook() {
        return totalSalesOfBook;
    }

    /**
     * @param totalSalesOfBook the totalSalesOfBook to set
     */
    public void setTotalSalesOfBook(BigDecimal totalSalesOfBook) {
        this.totalSalesOfBook = totalSalesOfBook;
    }

    /**
     * @return the isbnOfBook
     */
    public long getIsbnOfBook() {
        return isbnOfBook;
    }

    /**
     * @param isbnOfBook the isbnOfBook to set
     */
    public void setIsbnOfBook(long isbnOfBook) {
        this.isbnOfBook = isbnOfBook;
    }

            
}
