package com.ga2w20.arquillian.test.reports.params;

import java.math.BigDecimal;
import java.util.Date;

/**
*The bean that holds the parameters for all the clients reports test
 * @author cadin
 */
public class ReportClientsParam {
    private Date from;
    private Date to;
    private int countTopClients;
    private int clientId;
    private BigDecimal profitMadeOutOfClientMostRecentBoughtBook;
    private BigDecimal profitOutOfTotalSales;
    private String fullNameOfTopClient;
    
    /**
     *Constructor initializes all of the properties of this test bean
     * @param from
     * @param to
     * @param countTopClients
     * @param placeOfClient
     * @param profitMadeOutOfClient
     * @param profitOutOfTotalSales
     * @param fullNameOfTopClient
     */
    public ReportClientsParam(Date from, Date to, int countTopClients, int placeOfClient, BigDecimal profitMadeOutOfClient, BigDecimal profitOutOfTotalSales, String fullNameOfTopClient){
        this.from = from;
        this.to = to;
        this.countTopClients = countTopClients;
        this.clientId = placeOfClient;
        this.profitMadeOutOfClientMostRecentBoughtBook = profitMadeOutOfClient;
        this.profitOutOfTotalSales = profitOutOfTotalSales;
        this.fullNameOfTopClient = fullNameOfTopClient;
        
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the countTopClients
     */
    public int getCountTopClients() {
        return countTopClients;
    }

    /**
     * @param countTopClients the countTopClients to set
     */
    public void setCountTopClients(int countTopClients) {
        this.countTopClients = countTopClients;
    }

    /**
     * @return the clientId
     */
    public int getClientId() {
        return clientId;
    }

    /**
     * @param clientId the clientId to set
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /**
     * @return the profitMadeOutOfClientMostRecentBoughtBook
     */
    public BigDecimal getProfitMadeOutOfClientMostRecentBoughtBook() {
        return profitMadeOutOfClientMostRecentBoughtBook;
    }

    /**
     * @param profitMadeOutOfClientMostRecentBoughtBook the profitMadeOutOfClientMostRecentBoughtBook to set
     */
    public void setProfitMadeOutOfClientMostRecentBoughtBook(BigDecimal profitMadeOutOfClientMostRecentBoughtBook) {
        this.profitMadeOutOfClientMostRecentBoughtBook = profitMadeOutOfClientMostRecentBoughtBook;
    }

    /**
     * @return the profitOutOfTotalSales
     */
    public BigDecimal getProfitOutOfTotalSales() {
        return profitOutOfTotalSales;
    }

    /**
     * @param profitOutOfTotalSales the profitOutOfTotalSales to set
     */
    public void setProfitOutOfTotalSales(BigDecimal profitOutOfTotalSales) {
        this.profitOutOfTotalSales = profitOutOfTotalSales;
    }

    /**
     * @return the fullNameOfTopClient
     */
    public String getFullNameOfTopClient() {
        return fullNameOfTopClient;
    }

    /**
     * @param fullNameOfTopClient the fullNameOfTopClient to set
     */
    public void setFullNameOfTopClient(String fullNameOfTopClient) {
        this.fullNameOfTopClient = fullNameOfTopClient;
    }
}
