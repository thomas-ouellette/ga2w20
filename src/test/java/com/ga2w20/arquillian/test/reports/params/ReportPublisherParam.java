package com.ga2w20.arquillian.test.reports.params;

import java.math.BigDecimal;
import java.util.Date;

/**
*The bean that holds the parameters for all the publisher reports test
 * @author cadin
 */
public class ReportPublisherParam {
    private Date from;
    private Date to;
    private int publisherId;
    private BigDecimal totalProfitOfTotalSales;
    private int countOfBooks;
    private BigDecimal totalProfitForMostRecentOrderSold;
    
    /**
     * Constructor initializes all of the properties of this test bean
     * @param from
     * @param to
     * @param publisherId
     * @param totalProfitOfTotalSales
     * @param countOfBooks
     * @param totalProfitForMostRecentBookSold
     */
    public ReportPublisherParam(Date from, Date to, int publisherId, BigDecimal totalProfitOfTotalSales, int countOfBooks, BigDecimal totalProfitForMostRecentBookSold){
        this.from = from;
        this.to = to;
        this.publisherId = publisherId;
        this.totalProfitOfTotalSales = totalProfitOfTotalSales;
        this.countOfBooks = countOfBooks;
        this.totalProfitForMostRecentOrderSold = totalProfitForMostRecentBookSold;
        
    }

    /**
     * @return the from
     */
    public Date getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(Date from) {
        this.from = from;
    }

    /**
     * @return the to
     */
    public Date getTo() {
        return to;
    }

    /**
     * @param to the to to set
     */
    public void setTo(Date to) {
        this.to = to;
    }

    /**
     * @return the totalProfitOfTotalSales
     */
    public BigDecimal getTotalProfitOfTotalSales() {
        return totalProfitOfTotalSales;
    }

    /**
     * @param totalProfitOfTotalSales the totalProfitOfTotalSales to set
     */
    public void setTotalProfitOfTotalSales(BigDecimal totalProfitOfTotalSales) {
        this.totalProfitOfTotalSales = totalProfitOfTotalSales;
    }

    /**
     * @return the countOfBooks
     */
    public int getCountOfBooks() {
        return countOfBooks;
    }

    /**
     * @param countOfBooks the countOfBooks to set
     */
    public void setCountOfBooks(int countOfBooks) {
        this.countOfBooks = countOfBooks;
    }

    /**
     * @return the totalProfitForMostRecentOrderSold
     */
    public BigDecimal getTotalProfitForMostRecentOrderSold() {
        return totalProfitForMostRecentOrderSold;
    }

    /**
     * @param totalProfitForMostRecentOrderSold the totalProfitForMostRecentOrderSold to set
     */
    public void setTotalProfitForMostRecentOrderSold(BigDecimal totalProfitForMostRecentOrderSold) {
        this.totalProfitForMostRecentOrderSold = totalProfitForMostRecentOrderSold;
    }

    /**
     * @return the publisherId
     */
    public int getPublisherId() {
        return publisherId;
    }

    /**
     * @param publisherId the publisherId to set
     */
    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }
}
