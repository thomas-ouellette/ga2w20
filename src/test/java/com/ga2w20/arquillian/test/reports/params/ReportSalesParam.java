package com.ga2w20.arquillian.test.reports.params;

import java.math.BigDecimal;
import java.util.Date;

/**
 *The bean that holds the parameters for all the sales/orders reports test
 * @author cadin
 */
public class ReportSalesParam {
    private Date to;
    private Date from;
    private int bookPlacement;
    private int amountOfSales;
    private BigDecimal totalProfit;
    private BigDecimal profitOfSingleBook;
    
    /**
     * Constructor initializes all of the properties of this test bean
     * @param from
     * @param to
     * @param id
     * @param amountOfSales
     * @param totalProfit
     * @param profitOfSingleBook 
     */
    public ReportSalesParam(Date from, Date to, int id, int amountOfSales, BigDecimal totalProfit, BigDecimal profitOfSingleBook){
        this.to = to;
        this.from = from;
        this.bookPlacement = id;
        this.amountOfSales = amountOfSales;
        this.totalProfit = totalProfit;
        this.profitOfSingleBook = profitOfSingleBook;
    }
     /**
     * @return the from
     */
    public Date getFrom(){
        return this.from;
    }
    /**
     * @param from the from to set
     */
    public void setFrom(Date from){
        this.from = from;
    }
     /**
     * @return the to
     */
    public Date getTo(){
        return this.to;
    }
    /**
     * @param to the to to set
     */
    public void setTo(Date to){
        this.to = to;
    }
    /**
     * 
     * @return getBookPlacement
     */
    public int getBookPlacement(){
        return this.bookPlacement;
    }
    /**
     * 
     * @param bookPlacement the bookPlacement to set
     */
    public void setBookPlacement(int bookPlacement){
        this.bookPlacement = bookPlacement;
    }
    /**
     * 
     * @return the getAmountOfSales
     */
    public int getAmountOfSales(){
        return this.amountOfSales;
    }
    /**
     * 
     * @param amountOfSales the amountOfSales to set
     */
    public void setAmountOfSales(int amountOfSales){
        this.amountOfSales = amountOfSales;
    }

    /**
     * @return the totalProfit
     */
    public BigDecimal getTotalProfit() {
        return totalProfit;
    }

    /**
     * @param totalProfit the totalProfit to set
     */
    public void setTotalProfit(BigDecimal totalProfit) {
        this.totalProfit = totalProfit;
    }

    /**
     * @return the profitOfSingleBook
     */
    public BigDecimal getProfitOfSingleBook() {
        return profitOfSingleBook;
    }

    /**
     * @param profitOfSingleBook the profitOfSingleBook to set
     */
    public void setProfitOfSingleBook(BigDecimal profitOfSingleBook) {
        this.profitOfSingleBook = profitOfSingleBook;
    }
}
