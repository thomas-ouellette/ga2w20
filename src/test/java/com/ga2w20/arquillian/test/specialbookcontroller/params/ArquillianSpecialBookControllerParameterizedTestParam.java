package com.ga2w20.arquillian.test.specialbookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that generates parameters for ArquillianSpecialBookControllerParameterizedTest.java
 * 
 * @author Svitlana Myronova
 */
public class ArquillianSpecialBookControllerParameterizedTestParam {
    // this attribute for testing getSpecialBooksAllGenreActive( int maxItems) that returns List<Book>
    public final SpecialBooksAllGenreParam specialBooksAllGenreParam;
    
    private final static Logger LOG = LoggerFactory.getLogger(ArquillianSpecialBookControllerParameterizedTestParam.class);
    
    /**
     * Constructor for ArquillianSpecialBookControllerParameterizedTestParam
     * 
     * @author Svitlana Myronova
     * @param specialBooksAllGenreParam
     */
    public ArquillianSpecialBookControllerParameterizedTestParam(SpecialBooksAllGenreParam specialBooksAllGenreParam)                                                          
    {
        LOG.info("creating ArquillianSpecialBookControllerParameterizedTestParam");
        this.specialBooksAllGenreParam = specialBooksAllGenreParam;
    }
}
