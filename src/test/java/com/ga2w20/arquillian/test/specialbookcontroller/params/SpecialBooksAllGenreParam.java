package com.ga2w20.arquillian.test.specialbookcontroller.params;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Parameter for testing getSpecialBooksAllGenreActive(int maxItems) function from SpecialBookJpaController
 * 
 * @author Svitlana Myronova
 */
public class SpecialBooksAllGenreParam {
    public final int count;   
    public final int maxItems;
    
    private final static Logger LOG = LoggerFactory.getLogger(SpecialBooksAllGenreParam.class);
    
    /**
     * Constructor for SpecialBooksAllGenreParam
     *
     * @author Svitlana Myronova
     * @param count
     * @param maxItems
     */
    public SpecialBooksAllGenreParam(int count, int maxItems)
    {
        LOG.info("creating SpecialBooksAllGenreParam");
        this.count = count;
        this.maxItems = maxItems;
    }
}
